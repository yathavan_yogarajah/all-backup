//listen buttons
$("#accept_term").click(function () {
    termsadd("yes");
});

$("#notaccept_term").click(function () {
    termsadd("no");
});

//termsadd
function termsadd(terms) {
    $("#preload_overlay").show();
    $.ajax({
        url: path + "HospitalManagementSystem/HMS/Profile/addagreement.jsp",
        type: "POST",
        data: {
            "accept_terms": terms
        },
        success: function (data) {

            var msg = jQuery.trim(data);

            if (msg === "success") {
                $("#preload_overlay").hide();
                window.location = "firstlogin.html";
            } else {
                $("#preload_overlay").hide();
                window.location = "../../../index.html";
            }
        },
        error: function (data) {
            $("#preload_overlay").hide();
            $("#errormsg-terms").html(servererror);
            $("#errormsg-terms").show();
            $("#terms_alert").show();

            var hidestatus = setInterval(function () {
                $("#terms_alert").hide();
                clearInterval(hidestatus);
            }, 3000);
        }
    });
}