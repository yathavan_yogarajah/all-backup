var process_status = 0;

var profilename = localStorage.getItem("userprofilename");

//inital function
function process() {

    process_status++;

    if (process_status == 9) {

        $("#preload_overlaychart").hide("fast", function () {
            $(".pageview").hide();
            $(".chartsload").css({
                "opacity": 1
            });
            //$('#add_custom').modal('show');
            $("#d-1").fadeIn();
        });
    }
}


var date1 = [];
var date2 = [];
var heartrate1 = [];
var heartrate2 = [];

function chartassign() {
    getheartrate();
}

//date format
function dateconvert(dates) {

    var d = dates;
    splitData = d.split('-');
    var months = ["Jan", "Feb", "Mar", "April", "May", "June", "July", "Aug", "Sept", "Oct", "Nov", "Dec"];
    var date = splitData[2];
    var month = months[(splitData[1].replace("", "")) - 1];
    return date + " " + month;
}

//heartrateassign
function heartrateAssign(callother) {

    $('#heartrate').highcharts({
        chart: {
            zoomType: 'xy'
        },
        credits: {
            enabled: false
        },
        title: {
            text: ''
        },
        xAxis: {
            categories: date1
        },
        yAxis: {
            title: {
                text: ''
            }
        },
        tooltip: {
            crosshairs: true,
            headerFormat: '<b>heart rate</b><br>',
            pointFormat: '{point.y}bpm'

        },
        legend: {},
        noData: {
            style: {
                fontWeight: 'bold',
                fontSize: '15px',
                color: '#303030'
            }
        },
        series: [{
                name: 'Bpm',
                showInLegend: false,
                lineWidth: 1,
                color: '#f33',
                data: heartrate1,
                marker: {
                    symbol: 'circle',
                    radius: 3
                }
            }]
    });

    chartHeart = $('#heartrate').highcharts();
    $('#next').on("click", function () {


        chartHeart.series[0].update({
            data: heartrate2
        });
        chartHeart.xAxis[0].setCategories(date2);

    });
    $('#prev').on("click", function () {
        chartHeart.series[0].update({
            data: heartrate1
        });
        chartHeart.xAxis[0].setCategories(date1);
    });

    callother = typeof (callother) == "undefined" ? 1 : callother;

    if (callother !== -1) {
        getoximarity();
    }
}

//get heartrate
function getheartrate(call) {

    var call = typeof (call) == "undefined" ? 1 : call;

    $.ajax({
        url: path + "HospitalManagementSystem/HMS/AppDashBoard/getheartrate.jsp",
        type: "POST",
        data: {},
        success: function (data) {
            var msg = jQuery.trim(data);
            if (msg) {
                obj = JSON.parse(msg);
                if (obj.heartrate.length > 0) {

                    $("#heart_start").html(obj.heartrate[0][0]);
                    $("#heart_end").html(obj.heartrate[obj.heartrate.length - 1][0]);

                    for (i = 0; i < obj.heartrate.length; i++) {

                        if (i < 5) {

                            var covertdate = dateconvert(obj.heartrate[i][0]);
                            date1.push(covertdate);
                            heartrate1.push(obj.heartrate[i][1]);
                        } else {

                            var covertdate = dateconvert(obj.heartrate[i][0]);
                            date2.push(covertdate);
                            heartrate2.push(obj.heartrate[i][1]);
                        }
                    }

                    heartrateAssign(call);
                } else {

                    $("#heart_end").html("Sample Data");
                    var sampledata = "{heartrate:[['2016-06-17', '75', '00:00'],['2016-06-18', '72', '00:00'],['2016-06-19', '65', '00:00'],['2016-06-20', '76', '00:00'], ['2016-06-21', '73', '00:00'], ['2016-06-22', '73', '00:00'], ['2016-06-23', '73', '00:00']]}";
                    obj = JSON.parse(JSON.stringify(eval("(" + sampledata + ")")));
                    for (i = 0; i < obj.heartrate.length; i++) {

                        if (i < 5) {

                            var covertdate = dateconvert(obj.heartrate[i][0]);
                            date1.push(covertdate);
                            heartrate1.push(parseInt(obj.heartrate[i][1]));
                        } else {

                            var covertdate = dateconvert(obj.heartrate[i][0]);
                            date2.push(covertdate);
                            heartrate2.push(parseInt(obj.heartrate[i][1]));
                        }
                    }
                    heartrateAssign(call);
                }
            }
            process();
        },
        error: function (data) {
            heartrateAssign(call);
            process();
        }
    });
    return false;

}

var oxdate1 = [];
var oxdate2 = [];
var oxrate1 = [];
var oxrate2 = [];

//assign oximarity
function assignOximarity(callother) {

    $('#oximetry').highcharts({
        chart: {
            zoomType: 'xy'
        },
        title: {
            text: ''
        },
        xAxis: {
            categories: oxdate1

        },
        yAxis: {
            title: {
                text: ''
            }
        },
        credits: {
            enabled: false
        },
        tooltip: {
            crosshairs: true,
            headerFormat: '<b>Oximetry</b><br>',
            pointFormat: '{point.y}%'
        },
        legend: {},
        noData: {
            style: {
                fontWeight: 'bold',
                fontSize: '15px',
                color: '#303030'
            }
        },
        series: [{
                name: 'Oximetry',
                showInLegend: false,
                lineWidth: 1,
                color: '#399225',
                data: oxrate1,
                marker: {
                    symbol: 'circle',
                    radius: 3
                }
            }]
    });

    chartOximetry = $('#oximetry').highcharts();
    $('#oximetry_next').on("click", function () {
        chartOximetry.series[0].update({
            data: oxrate2
        });
        chartOximetry.xAxis[0].setCategories(oxdate2);
    });

    $('#oximetry_prev').on("click", function () {
        chartOximetry.series[0].update({
            data: oxrate1
        });
        chartOximetry.xAxis[0].setCategories(oxdate1);
    });

    callother = typeof (callother) == "undefined" ? 1 : callother;
    if (callother !== -1) {
        getbloodpreasure();
    }

}

// get oximarity
function getoximarity(call) {

    var call = typeof (call) == "undefined" ? 1 : call;

    $.ajax({
        url: path + "HospitalManagementSystem/HMS/AppDashBoard/getoximetry.jsp",
        type: "POST",
        data: {},
        success: function (data) {
            var msg = jQuery.trim(data);
            if (msg) {
                obj = JSON.parse(msg);
                if (obj.oximetry.length > 0) {

                    $("#oximetry_start").html(obj.oximetry[0][0]);
                    $("#oximetry_end").html(obj.oximetry[obj.oximetry.length - 1][0]);

                    for (i = 0; i < obj.oximetry.length; i++) {
                        if (i < 5) {
                            var covertdate = dateconvert(obj.oximetry[i][0]);
                            oxdate1.push(covertdate);
                            oxrate1.push(obj.oximetry[i][1]);
                        } else {
                            var covertdate = dateconvert(obj.oximetry[i][0]);
                            oxdate2.push(covertdate);
                            oxrate2.push(obj.oximetry[i][1]);
                        }
                    }
                    assignOximarity(call);
                } else {

                    $("#oximetry_end").html("Sample Data");
                    var sampledata = "{oximetry:[['2016-06-17', '75'],['2016-06-18', '72'],['2016-06-19', '65'],['2016-06-20', '76'], ['2016-06-21', '73'], ['2016-06-22', '73'], ['2016-06-23', '73']]}";
                    obj = JSON.parse(JSON.stringify(eval("(" + sampledata + ")")));
                    for (i = 0; i < obj.oximetry.length; i++) {
                        if (i < 5) {
                            var covertdate = dateconvert(obj.oximetry[i][0]);
                            oxdate1.push(covertdate);
                            oxrate1.push(parseFloat(obj.oximetry[i][1]));
                        } else {
                            var covertdate = dateconvert(obj.oximetry[i][0]);
                            oxdate2.push(covertdate);
                            oxrate2.push(parseFloat(obj.oximetry[i][1]));
                        }
                    }

                    assignOximarity(call);
                }
            }
            process();
        },
        error: function (data) {
            assignOximarity(call);
            process();
        }
    });
    return false;
}

var blooddate1 = [];
var blooddate2 = [];

var bloodrate1 = [];
var bloodrate2 = [];
var bloodrate3 = [];
var bloodrate4 = [];

// assign bloodpreasure
function assignBloodpreasure(callother) {

    $('#blood').highcharts({
        chart: {
            zoomType: 'xy'
        },
        title: {
            text: ''
        },
        xAxis: {
            categories: blooddate1
        },
        yAxis: {
            title: {
                text: ''
            }
        },
        credits: {
            enabled: false
        },
        tooltip: {
            crosshairs: true,
            headerFormat: '<b>Blood pressure</b><br>',
            pointFormat: '{point.y}mmHg'

        },
        legend: {},
        noData: {
            style: {
                fontWeight: 'bold',
                fontSize: '15px',
                color: '#303030'
            }
        },
        series: [{
                name: 'SYS(mmHg)',
                lineWidth: 1,
                color: '#825E8E',
                marker: {
                    symbol: 'circle',
                    radius: 3
                },
                data: bloodrate1
            }, {
                name: 'DIA(mmHg)',
                color: '#3a75c4',
                lineWidth: 1,
                marker: {
                    symbol: 'circle',
                    radius: 3
                },
                data: bloodrate3
            }]

    });

    chartBlood = $('#blood').highcharts();
    $('#blood_next').on("click", function () {
        chartBlood.series[0].update({
            data: bloodrate2
        });
        chartBlood.series[1].update({
            data: bloodrate4
        });
        chartBlood.xAxis[0].setCategories(blooddate2);
    });

    $('#blood_prev').on("click", function () {
        ;
        chartBlood.series[0].update({
            data: bloodrate1
        });
        chartBlood.series[1].update({
            data: bloodrate3
        });
        chartBlood.xAxis[0].setCategories(blooddate1);
    });

    callother = typeof (callother) == "undefined" ? 1 : callother;
    if (callother !== -1) {
        getsleep();
    }
}

// get bloodpreasure
function getbloodpreasure(call) {

    var call = typeof (call) == "undefined" ? 1 : call;

    $.ajax({
        url: path + "HospitalManagementSystem/HMS/AppDashBoard/getbloodpreasure.jsp",
        type: "POST",
        data: {},
        success: function (data) {
            var msg = jQuery.trim(data);
            if (msg) {
                var obj = JSON.parse(msg);
                if (obj.bloodrate.length > 0) {
                    var preasuretype1 = 0;
                    var preasuretype2 = 0;

                    $("#blood_start").html(obj.bloodrate[0][0]);
                    $("#blood_end").html(obj.bloodrate[obj.bloodrate.length - 1][0]);

                    for (i = 0; i < obj.bloodrate.length; i++) {

                        var covertdate = dateconvert(obj.bloodrate[i][0]);

                        if (obj.bloodrate[i][2] === "dbp") {
                            if (preasuretype1 < 5) {
                                bloodrate1.push(obj.bloodrate[i][1]);
                            } else {
                                bloodrate2.push(obj.bloodrate[i][1]);
                            }

                            preasuretype1++;
                        } else {

                            if (preasuretype2 < 5) {
                                bloodrate3.push(obj.bloodrate[i][1]);

                            } else {
                                bloodrate4.push(obj.bloodrate[i][1]);

                            }
                            preasuretype2++;
                        }

                        if (i < 5) {
                            blooddate1.push(covertdate);
                        } else {
                            blooddate2.push(covertdate);
                        }
                    }
                    assignBloodpreasure(call);
                } else {

                    $("#blood_end").html("Sample Data");
                    var sampledata = "{bloodrate:[['2016-06-17', '75','dbp'],['2016-06-17', '70', 'sbp'],['2016-06-18', '65','dbp'],['2016-06-18', '90', 'sbp'], ['2016-06-19', '95','dbp'],['2016-06-19', '80', 'sbp'], ['2016-06-20', '55','dbp'],['2016-06-20', '80', 'sbp'], ['2016-06-21', '35','dbp'],['2016-06-21', '80', 'sbp'],['2016-06-22', '75','dbp'],['2016-06-22', '40', 'sbp'], ['2016-06-23', '65','dbp'],['2016-06-23', '30', 'sbp']]}";

                    obj = JSON.parse(JSON.stringify(eval("(" + sampledata + ")")));
                    var preasuretype1 = 0;
                    var preasuretype2 = 0;
                    for (i = 0; i < obj.bloodrate.length; i++) {

                        var covertdate = dateconvert(obj.bloodrate[i][0]);

                        if (obj.bloodrate[i][2] === "dbp") {

                            if (preasuretype1 < 5) {

                                bloodrate1.push(parseFloat(obj.bloodrate[i][1]));

                            } else {

                                bloodrate2.push(parseFloat(obj.bloodrate[i][1]));
                            }

                            preasuretype1++;

                        } else {

                            if (preasuretype2 < 5) {

                                bloodrate3.push(parseFloat(obj.bloodrate[i][1]));

                            } else {

                                bloodrate4.push(parseFloat(obj.bloodrate[i][1]));

                            }
                            preasuretype2++;
                        }

                        if (i < 5) {
                            blooddate1.push(covertdate);
                        } else {
                            blooddate2.push(covertdate);
                        }
                    }
                    assignBloodpreasure(call);
                }
            }
            process();
        },
        error: function (data) {
            assignBloodpreasure(call);
            process();
        }
    });
    return false;
}

var sleepdate1 = [];
var sleepdate2 = [];
var darksleep1 = [];
var darksleep2 = [];
var lightsleep1 = [];
var lightsleep2 = [];
var awake1 = [];
var awake2 = [];

// assign sleep
function assignsleep(callother) {

    $('#sleep').highcharts({
        chart: {
            type: 'column'
        },
        title: {
            text: ''
        },
        credits: {
            enabled: false
        },
        xAxis: {
            categories: sleepdate1
        },
        yAxis: {
            min: 0,
            title: {
                text: ''
            },
            plotLines: [{
                    value: 8,
                    color: 'green',
                    dashStyle: 'shortdash',
                    width: 3,
                    label: {
                        text: '8.00'
                    }
                }]
        },
        tooltip: {
            formatter: function () {
                return '<b>' + this.x + ' </b><br/>' + this.series.name + ': ' + this.y + ' Hour<br/>' + 'Total Sleep : ' + this.point.stackTotal + 'Hour';
            }
        },
        plotOptions: {
            column: {
                stacking: 'normal'
            },
            series: {
                pointWidth: 25
            }
        },
        noData: {
            style: {
                fontWeight: 'bold',
                fontSize: '15px',
                color: '#303030'
            }
        },
        series: [{
                name: 'awake',
                color: '#ffbc52',
                data: awake1
            }, {
                name: 'deep sleep',
                color: '#7cb5ec',
                data: darksleep1
            }, {
                name: 'light sleep',
                color: '#90ed7d',
                data: lightsleep1
            }]
    });

    var chart4 = $('#sleep').highcharts();
    $('#sleep_next').on("click", function () {

        chart4.series[0].update({
            data: awake2
        });
        chart4.series[1].update({
            data: darksleep2
        });
        chart4.series[2].update({
            data: lightsleep2
        });
        chart4.xAxis[0].setCategories(sleepdate2);
    });

    $('#sleep_prev').on("click", function () {

        chart4.series[0].update({
            data: awake1
        });
        chart4.series[1].update({
            data: darksleep1
        });
        chart4.series[2].update({
            data: lightsleep1
        });
        chart4.xAxis[0].setCategories(sleepdate1);
    });

    callother = typeof (callother) == "undefined" ? 1 : callother;
    if (callother !== -1) {
        getweight();
    }
}

//get sleep
function getsleep(call) {

    var call = typeof (call) == "undefined" ? 1 : call;

    $.ajax({
        url: path + "HospitalManagementSystem/HMS/AppDashBoard/getsleep.jsp",
        type: "POST",
        data: {},
        success: function (data) {
            var msg = jQuery.trim(data);
            if (msg) {
                obj = JSON.parse(msg);
                if (obj.sleepdata.length > 0) {

                    $("#sleep_start").html(obj.sleepdata[0][0]);
                    $("#sleep_end").html(obj.sleepdata[obj.sleepdata.length - 1][0]);

                    for (i = 0; i < obj.sleepdata.length; i++) {
                        if (i < 5) {
                            var covertdate = dateconvert(obj.sleepdata[i][0]);
                            sleepdate1.push(covertdate);
                            awake1.push(obj.sleepdata[i][1]);
                            darksleep1.push(obj.sleepdata[i][2]);
                            lightsleep1.push(obj.sleepdata[i][3]);
                        } else {
                            var covertdate = dateconvert(obj.sleepdata[i][0]);
                            sleepdate2.push(covertdate);
                            awake2.push(obj.sleepdata[i][1]);
                            darksleep2.push(obj.sleepdata[i][2]);
                            lightsleep2.push(obj.sleepdata[i][3]);
                        }
                    }
                    assignsleep(call);
                } else {

                    $("#sleep_end").html("Sample Data");
                    var sampledata = "{sleepdata:[['2016-06-17', '0.55', '2.30', '1.10'],['2016-06-18', '0.22', '4.30', '1.16' ],['2016-06-19', '0.55', '3.35', '2.22'],['2016-06-20', '1.36','3.44', '2.24'], ['2016-06-21', '2.22', '3.33', '4.43'], ['2016-06-22', '3.30','1.10','2.20'], ['2016-06-23', '0.30','0.40','0.50']]}";
                    obj = JSON.parse(JSON.stringify(eval("(" + sampledata + ")")));

                    for (i = 0; i < obj.sleepdata.length; i++) {
                        if (i < 5) {
                            var covertdate = dateconvert(obj.sleepdata[i][0]);
                            sleepdate1.push(covertdate);
                            awake1.push(parseFloat(obj.sleepdata[i][1]));
                            darksleep1.push(parseFloat(obj.sleepdata[i][2]));
                            lightsleep1.push(parseFloat(obj.sleepdata[i][3]));
                        } else {
                            var covertdate = dateconvert(obj.sleepdata[i][0]);
                            sleepdate2.push(covertdate);
                            awake2.push(parseFloat(obj.sleepdata[i][1]));
                            darksleep2.push(parseFloat(obj.sleepdata[i][2]));
                            lightsleep2.push(parseFloat(obj.sleepdata[i][3]));
                        }
                    }
                    assignsleep(call);
                }
            }
            process();
        },
        error: function (data) {
            assignsleep(call);
            process();
        }
    });
    return false;
}

var weightdate1 = [];
var weightdate2 = [];
var weightrate1 = [];
var weightrate2 = [];

//assign weight
function assignweight(callother) {

    $('#weightChart').highcharts({
        title: {
            text: ''
        },
        xAxis: {
            categories: weightdate1
        },
        yAxis: {
            title: {
                text: ''
            }
        },
        credits: {
            enabled: false
        },
        tooltip: {
            crosshairs: true,
            shared: true,
            valueSuffix: 'Kg'
        },
        legend: {},
        noData: {
            style: {
                fontWeight: 'bold',
                fontSize: '15px',
                color: '#303030'
            }
        },
        series: [{
                lineWidth: 1,
                name: 'Weight',
                showInLegend: false,
                data: weightrate1,
                zIndex: 1,
                marker: {
                    symbol: 'circle',
                    radius: 3
                }
            }]
    });

    chartWeight = $('#weightChart').highcharts();
    $('#weight_next').on("click", function () {

        chartWeight.series[0].update({
            data: weightrate2
        });
        chartWeight.xAxis[0].setCategories(weightdate2);
    });
    $('#weight_prev').on("click", function () {
        chartWeight.series[0].update({
            data: weightrate1
        });
        chartWeight.xAxis[0].setCategories(weightdate1);
    });

    callother = typeof (callother) == "undefined" ? 1 : callother;
    if (callother !== -1) {
        moodmanagement();
    }

}

// get weight
function getweight(call) {

    var call = typeof (call) == "undefined" ? 1 : call;

    $.ajax({
        url: path + "HospitalManagementSystem/HMS/AppDashBoard/getweight.jsp",
        type: "POST",
        data: {},
        success: function (data) {
            var msg = jQuery.trim(data);
            if (msg) {
                obj = JSON.parse(msg);
                if (obj.weight.length > 0) {

                    $("#weight_start").html(obj.weight[0][0]);
                    $("#weight_end").html(obj.weight[obj.weight.length - 1][0]);

                    for (i = 0; i < obj.weight.length; i++) {
                        if (i < 5) {
                            var covertdate = dateconvert(obj.weight[i][0]);
                            weightdate1.push(covertdate);
                            weightrate1.push(obj.weight[i][1]);
                        } else {
                            var covertdate = dateconvert(obj.weight[i][0]);
                            weightdate2.push(covertdate);
                            weightrate2.push(obj.weight[i][1]);
                        }
                    }

                    assignweight(call);

                } else {

                    $("#weight_end").html("Sample Data");
                    var sampledata = "{weight:[['2016-06-17', '75', 'kg'],['2016-06-18', '72', 'kg'],['2016-06-19', '75', 'kg'],['2016-06-20', '76', 'kg'], ['2016-06-21', '74.5', 'kg'], ['2016-06-22', '74', 'kg'], ['2016-06-23', '75', 'kg']]}";
                    obj = JSON.parse(JSON.stringify(eval("(" + sampledata + ")")));
                    for (i = 0; i < obj.weight.length; i++) {
                        if (i < 5) {
                            var covertdate = dateconvert(obj.weight[i][0]);
                            weightdate1.push(covertdate);
                            weightrate1.push(parseInt(parseFloat(obj.weight[i][1])));
                        } else {
                            var covertdate = dateconvert(obj.weight[i][0]);
                            weightdate2.push(covertdate);
                            weightrate2.push(parseInt(parseFloat(obj.weight[i][1])));
                        }
                    }

                    assignweight(call);
                }
            }
            process();
        },
        error: function (data) {
            assignweight(call);
            process();
        }

    });
    return false;
}

//mood management
var moodData1 = [];
var moodData2 = [];
var moodfirstdate;
var moodlastdate;
var moodsecoundate;
var moodsecoundlastdate;

//mooddataconvert
function mooddataconvert(dates, moodtypes) {

    var localmoodpush = [];
    localmoodpush.length = 0;
    var d = dates;
    var splitData = d.split(' ');
    var splitData1 = splitData[0];
    var splitData2 = splitData1.split('-');
    var year = splitData2[0];
    var month = splitData2[1];
    var convertmonth = month - 1;
    var date = splitData2[2];
    var time = splitData[1];
    var time1 = time.split(":");
    var hour = time1[0];
    var minutes = time1[1];
    var data1;

    if (moodtypes === "bored") {
        data1 = 1;
    } else if (moodtypes === "sad") {
        data1 = 2;
    } else if (moodtypes === "irritated") {
        data1 = 3;
    } else if (moodtypes === "tense") {
        data1 = 4;
    } else if (moodtypes === "neutarl") {
        data1 = 5;
    } else if (moodtypes === "good") {
        data1 = 6;
    } else if (moodtypes === "calm") {
        data1 = 7;
    } else if (moodtypes === "relaxed") {
        data1 = 8;
    } else if (moodtypes === "cheerful") {
        data1 = 9;
    } else if (moodtypes === "excited") {
        data1 = 10;
    }

    var data = Date.UTC(year, convertmonth, date, hour, minutes);
    localmoodpush.push(data);
    localmoodpush.push(data1);
    return localmoodpush;
}

//assign mood
function assignmood(moodData1, moodData2, callother) {

    $('#modeChart').highcharts({
        chart: {
            type: 'area'
        },
        tooltip: {
            formatter: function () {
                var value = this.y;
                if (value == 1) {
                    return '<b>' + 'Mood' + '</b><br/>' +
                            Highcharts.dateFormat('%e - %b - %Y' + '<br/>' + '%I:%M - %p',
                                    new Date(this.x)) + '<br/><b>' + 'Bored' + '<b/>';
                } else if (value == 2) {
                    return '<b>' + 'Mood' + '</b><br/>' +
                            Highcharts.dateFormat('%e - %b - %Y' + '<br/>' + '%I:%M - %p',
                                    new Date(this.x)) + '<br/><b>' + 'Sad' + '<b/>';
                } else if (value == 3) {
                    return '<b>' + 'Mood' + '</b><br/>' +
                            Highcharts.dateFormat('%e - %b - %Y' + '<br/>' + '%I:%M - %p',
                                    new Date(this.x)) + '<br/><b>' + 'Irritated' + '<b/>';
                } else if (value == 4) {
                    return '<b>' + 'Mood' + '</b><br/>' +
                            Highcharts.dateFormat('%e - %b - %Y' + '<br/>' + '%I:%M - %p',
                                    new Date(this.x)) + '<br/><b>' + 'Tense' + '<b/>';
                } else if (value == 5) {
                    return '<b>' + 'Mood' + '</b><br/>' +
                            Highcharts.dateFormat('%e - %b - %Y' + '<br/>' + '%I:%M - %p',
                                    new Date(this.x)) + '<br/><b>' + 'Neutarl' + '<b/>';
                } else if (value == 6) {
                    return '<b>' + 'Mood' + '</b><br/>' +
                            Highcharts.dateFormat('%e - %b - %Y' + '<br/>' + '%I:%M - %p',
                                    new Date(this.x)) + '<br/><b>' + 'Good' + '<b/>';
                } else if (value == 7) {
                    return '<b>' + 'Mood' + '</b><br/>' +
                            Highcharts.dateFormat('%e - %b - %Y' + '<br/>' + '%I:%M - %p',
                                    new Date(this.x)) + '<br/><b>' + 'Calm' + '<b/>';
                } else if (value == 8) {
                    return '<b>' + 'Mood' + '</b><br/>' +
                            Highcharts.dateFormat('%e - %b - %Y' + '<br/>' + '%I:%M - %p',
                                    new Date(this.x)) + '<br/><b>' + 'Relaxed' + '<b/>';
                } else if (value == 9) {
                    return '<b>' + 'Mood' + '</b><br/>' +
                            Highcharts.dateFormat('%e - %b - %Y' + '<br/>' + '%I:%M - %p',
                                    new Date(this.x)) + '<br/><b>' + 'Cheerful' + '<b/>';
                } else if (value == 10) {
                    return '<b>' + 'Mood' + '</b><br/>' +
                            Highcharts.dateFormat('%e - %b - %Y' + '<br/>' + '%I:%M - %p',
                                    new Date(this.x)) + '<br/><b>' + 'Excited' + '<b/>';
                }
            }
        },
        yAxis: {
            min: 1,
            max: 10,
            tickInterval: 1,
            labels: {},
            title: {
                text: 'Mood'
            }
        },
        xAxis: {
            type: 'datetime',
            min: moodfirstdate,
            max: moodsecoundlastdate,
            dateTimeLabelFormats: {
                month: '%e. %b',
                year: '%b'
            },
            title: {
                text: 'Date'
            },
            minTickInterval: 24 * 3600 * 1000
        },
        credits: {
            enabled: false
        },
        title: {
            text: ''
        },
        noData: {
            style: {
                fontWeight: 'bold',
                fontSize: '15px',
                color: '#303030'
            }
        },
        plotOptions: {
            area: {
                fillColor: {
                    linearGradient: {
                        x1: 0,
                        y1: 0,
                        x2: 0,
                        y2: 1
                    },
                    stops: [
                        [0, '#a8cef2'],
                        [1, '#FFF']
                    ]
                },
                marker: {
                    enabled: true,
                    symbol: 'circle',
                    radius: 3,
                }
            }
        },
        series: [{
                color: '#a8cef2',
                name: profilename,
                data: moodData1,
                showInLegend: false
            }]
    });
    $("#mode_next").click(function () {
        modeChartid = $('#modeChart').highcharts();
        modeChartid.series[0].update({
            data: moodData2
        });
        modeChartid.xAxis[0].setExtremes(moodsecoundate, moodsecoundlastdate);
    });
    $("#mode_prev").click(function () {
        modeChartid = $('#modeChart').highcharts();
        modeChartid.series[0].update({
            data: moodData1
        });
        modeChartid.xAxis[0].setExtremes(moodfirstdate, moodlastdate);
    });

    callother = typeof (callother) == "undefined" ? 1 : callother;
    if (callother !== -1) {
        painmanagement();
    }
}

//get mood
function moodmanagement(call) {

    $.ajax({
        url: path + "HospitalManagementSystem/HMS/AppDashBoard/getmoodchart.jsp",
        type: "POST",
        data: {},
        success: function (data) {
            var msg = jQuery.trim(data);
            if (msg) {
                obj = JSON.parse(msg);

                if (obj.mooddetails.length > 0) {

                    moodData1.length = 0;
                    moodData2.length = 0;

                    $("#mood_start").html(obj.mooddetails[0][0].split(" ")[0]);
                    $("#mood_end").html(obj.mooddetails[obj.mooddetails.length - 1][0].split(" ")[0]);

                    for (i = 0; i < obj.mooddetails.length; i++) {


                        if (i < 7) {

                            var covertdate = mooddataconvert(obj.mooddetails[i][0], obj.mooddetails[i][1]);
                            moodData1.push(covertdate);

                            if (i === 0) {
                                moodfirstdate = covertdate[0];
                            }
                            if (obj.mooddetails.length > 7) {
                                if (i === 6) {
                                    moodlastdate = covertdate[0];
                                    moodsecoundlastdate = covertdate[0];
                                }
                            } else {
                                if (i === obj.mooddetails.length - 1) {
                                    moodlastdate = covertdate[0];
                                    moodsecoundlastdate = covertdate[0];
                                }
                            }
                        } else {
                            var covertdate = mooddataconvert(obj.mooddetails[i][0], obj.mooddetails[i][1]);
                            moodData2.push(covertdate);
                            if (i === 7) {
                                moodsecoundate = covertdate[0];
                            }
                            if (i === obj.mooddetails.length - 1) {
                                moodsecoundlastdate = covertdate[0];
                            }
                        }
                    }
                    assignmood(moodData1, moodData2, call);
                } else {

                    $("#mood_end").html("Sample Data");
                    var sampledata = "{mooddetails:[['2016-07-17 11:00:00', 'bored'],['2016-07-18 11:00:00', 'sad'],['2016-07-19 11:00:00', 'neutarl'],['2016-07-20 11:00:00', 'neutarl'], ['2016-07-21 11:00:00', 'irritated'], ['2016-07-22 11:00:00', 'relaxed'], ['2016-07-23 11:00:00', 'excited']]}";
                    obj = JSON.parse(JSON.stringify(eval("(" + sampledata + ")")));

                    for (i = 0; i < obj.mooddetails.length; i++) {


                        if (i < 7) {

                            var covertdate = mooddataconvert(obj.mooddetails[i][0], String(obj.mooddetails[i][1]));
                            moodData1.push(covertdate);

                            if (i === 0) {
                                moodfirstdate = covertdate[0];
                            }
                            if (obj.mooddetails.length > 7) {
                                if (i === 6) {
                                    moodlastdate = covertdate[0];
                                    moodsecoundlastdate = covertdate[0];
                                }
                            } else {
                                if (i === obj.mooddetails.length - 1) {
                                    moodlastdate = covertdate[0];
                                    moodsecoundlastdate = covertdate[0];
                                }
                            }
                        } else {
                            var covertdate = mooddataconvert(obj.mooddetails[i][0], obj.mooddetails[i][1]);
                            moodData2.push(covertdate);
                            if (i === 7) {
                                moodsecoundate = covertdate[0];
                            }
                            if (i === obj.mooddetails.length - 1) {
                                moodsecoundlastdate = covertdate[0];
                            }
                        }
                    }

                    assignmood(moodData1, moodData2, call);
                }
            }
            process();
        },
        error: function (data) {
            assignmood(moodData1, moodData2, call);
            process();
        }
    });
    return false;
}


//pain management
var paindata1 = [];
var paindata2 = [];
var firstdate;
var lastdate;
var secoundate;
var secoundlastdate;

//dataconvert
function dataconvert(dates, painttypes) {

    var localpainpush = [];
    localpainpush.length = 0;
    var d = dates;
    var splitData = d.split(' ');
    var splitData1 = splitData[0];
    var splitData2 = splitData1.split('-');
    var year = splitData2[0];
    var month = splitData2[1];
    var convertmonth = month - 1;
    var date = splitData2[2];
    var time = splitData[1];
    var time1 = time.split(":");
    var hour = time1[0];
    var minutes = time1[1];
    var data1 = painttypes;
    var data = Date.UTC(year, convertmonth, date, hour, minutes);

    localpainpush.push(data);
    localpainpush.push(data1);
    return localpainpush;
}

//assign pain
function assignpain(paindata1, paindata2, callother) {
    var painLabels = ["0", "1", "2", "3", "4", "5", "6", "7", "8", "9", "10"];

    $('#painChart').highcharts({
        chart: {
            type: 'area'
        },
        tooltip: {
            formatter: function () {
                var value = this.y;
                if (value === 0) {
                    return '<b>' + "Pain Type" + '</b><br/>' +
                            Highcharts.dateFormat('%e - %b - %Y' + '<br/>' + '%I:%M - %p',
                                    new Date(this.x)) + '<br/><b>' + 'No Pain' + '<b/>';
                } else if (value === 1) {
                    return '<b>' + "Pain Type" + '</b><br/>' +
                            Highcharts.dateFormat('%e - %b - %Y' + '<br/>' + '%I:%M - %p',
                                    new Date(this.x)) + '<br/><b>' + 'Very Mild' + '<b/>';
                } else if (value === 2) {
                    return '<b>' + "Pain Type" + '</b><br/>' +
                            Highcharts.dateFormat('%e - %b - %Y' + '<br/>' + '%I:%M - %p',
                                    new Date(this.x)) + '<br/><b>' + 'Discomforting' + '<b/>';
                } else if (value === 3) {
                    return '<b>' + "Pain Type" + '</b><br/>' +
                            Highcharts.dateFormat('%e - %b - %Y' + '<br/>' + '%I:%M - %p',
                                    new Date(this.x)) + '<br/><b>' + 'Tolerable' + '<b/>';
                } else if (value === 4) {
                    return '<b>' + "Pain Type" + '</b><br/>' +
                            Highcharts.dateFormat('%e - %b - %Y' + '<br/>' + '%I:%M - %p',
                                    new Date(this.x)) + '<br/><b>' + 'Distressing' + '<b/>';
                } else if (value === 5) {
                    return '<b>' + "Pain Type" + '</b><br/>' +
                            Highcharts.dateFormat('%e - %b - %Y' + '<br/>' + '%I:%M - %p',
                                    new Date(this.x)) + '<br/><b>' + 'Very Distressing' + '<b/>';
                } else if (value === 6) {
                    return '<b>' + "Pain Type" + '</b><br/>' +
                            Highcharts.dateFormat('%e - %b - %Y' + '<br/>' + '%I:%M - %p',
                                    new Date(this.x)) + '<br/><b>' + 'Intense' + '<b/>';
                } else if (value === 7) {
                    return '<b>' + "Pain Type" + '</b><br/>' +
                            Highcharts.dateFormat('%e - %b - %Y' + '<br/>' + '%I:%M - %p',
                                    new Date(this.x)) + '<br/><b>' + 'Very Intense' + '<b/>';
                } else if (value === 8) {
                    return '<b>' + "Pain Type" + '</b><br/>' +
                            Highcharts.dateFormat('%e - %b - %Y' + '<br/>' + '%I:%M - %p',
                                    new Date(this.x)) + '<br/><b>' + 'Utterly Horrible' + '<b/>';
                } else if (value === 9) {
                    return '<b>' + "Pain Type" + '</b><br/>' +
                            Highcharts.dateFormat('%e - %b - %Y' + '<br/>' + '%I:%M - %p',
                                    new Date(this.x)) + '<br/><b>' + 'Excruciating Unbearable' + '<b/>';
                } else if (value === 10) {
                    return '<b>' + "Pain Type" + '</b><br/>' +
                            Highcharts.dateFormat('%e - %b - %Y' + '<br/>' + '%I:%M - %p',
                                    new Date(this.x)) + '<br/><b>' + 'Unimaginable Unspeakable' + '<b/>';
                }
            }
        },
        title: {
            text: ''
        },
        yAxis: {
            min: 1,
            max: 11,
            tickInterval: 1,
            labels: {},
            credits: {
                enabled: false
            },
            title: {
                text: 'Pain Types'
            }
        },
        xAxis: {
            type: 'datetime',
            min: firstdate,
            max: secoundlastdate,
            dateTimeLabelFormats: {// don't display the dummy year
                month: '%e. %b',
                year: '%b'
            },
            visible: false,
            title: {
                text: 'Date'
            },
            minTickInterval: 24 * 3600 * 1000
        },
        credits: {
            enabled: false
        },
        noData: {
            style: {
                fontWeight: 'bold',
                fontSize: '15px',
                color: '#303030'
            }
        },
        plotOptions: {
            area: {
                fillColor: {
                    linearGradient: {
                        x1: 0,
                        y1: 0,
                        x2: 0,
                        y2: 1
                    },
                    stops: [
                        [0, '#f44336'],
                        [1, '#FFF']
                    ]
                },
                marker: {
                    enabled: true,
                    symbol: 'circle',
                    radius: 3,
                }
            }
        },
        series: [{
                color: '#f44336',
                name: profilename,
                data: paindata1,
                showInLegend: false
            }]
    });

    $("#pain_next").click(function () {
        painCartid = $('#painChart').highcharts();
        painCartid.series[0].update({
            data: paindata2
        });
        painCartid.xAxis[0].setExtremes(secoundate, secoundlastdate);
    });

    $("#pain_prev").click(function () {
        painCartid = $('#painChart').highcharts();
        painCartid.series[0].update({
            data: paindata1
        });
        painCartid.xAxis[0].setExtremes(firstdate, lastdate);
    });

    callother = typeof (callother) == "undefined" ? 1 : callother;
    if (callother !== -1) {
        getfatmass();
    }

}

function painmanagement(call) {

    var call = typeof (call) == "undefined" ? 1 : call;

    $.ajax({
        url: path + "HospitalManagementSystem/HMS/AppDashBoard/getpainchart.jsp",
        type: "POST",
        data: {},
        success: function (data) {
            var msg = jQuery.trim(data);
            if (msg) {
                obj = JSON.parse(msg);
                if (obj.paindetails.length > 0) {
                    paindata1.length = 0;
                    paindata2.length = 0;
                    for (i = 0; i < obj.paindetails.length; i++) {

                        $("#pain_start").html(obj.paindetails[0][0].split(" ")[0]);
                        $("#pain_end").html(obj.paindetails[obj.paindetails.length - 1][0].split(" ")[0]);

                        if (i < 7) {
                            var covertdate = dataconvert(obj.paindetails[i][0], obj.paindetails[i][1]);
                            paindata1.push(covertdate);

                            if (i === 0) {
                                firstdate = covertdate[0];
                            }
                            if (obj.paindetails.length > 7) {
                                if (i === 6) {

                                    lastdate = covertdate[0];
                                    secoundlastdate = covertdate[0];
                                }
                            } else {
                                if (i === obj.paindetails.length - 1) {
                                    lastdate = covertdate[0];
                                    secoundlastdate = covertdate[0];

                                }
                            }
                        } else {
                            var covertdate = dataconvert(obj.paindetails[i][0], obj.paindetails[i][1]);
                            paindata2.push(covertdate);
                            if (i === 7) {
                                secoundate = covertdate[0];
                            }
                            if (i === obj.paindetails.length - 1) {
                                secoundlastdate = covertdate[0];
                            }
                        }
                    }
                    assignpain(paindata1, paindata2, call);
                } else {

                    $("#pain_end").html("Sample Data");
                    var sampledata = "{paindetails:[['2016-07-17 11:00:00', '4'],['2016-07-18 12:00:00', '6'],['2016-07-19 10:00:00', '6'],['2016-07-20 11:00:00', '5'], ['2016-07-21 12:00:00', '8'], ['2016-07-22 11:00:00', '9'], ['2016-07-23 10:00:00', '10']]}";
                    obj = JSON.parse(JSON.stringify(eval("(" + sampledata + ")")));
                    for (i = 0; i < obj.paindetails.length; i++) {

                        $("#pain_start").html(obj.paindetails[0][0].split(" ")[0]);
                        $("#pain_end").html(obj.paindetails[obj.paindetails.length - 1][0].split(" ")[0]);

                        if (i < 7) {
                            var covertdate = dataconvert(obj.paindetails[i][0], parseInt(obj.paindetails[i][1]));
                            paindata1.push(covertdate);

                            if (i === 0) {
                                firstdate = covertdate[0];
                            }
                            if (obj.paindetails.length > 7) {
                                if (i === 6) {

                                    lastdate = covertdate[0];
                                    secoundlastdate = covertdate[0];
                                }
                            } else {
                                if (i === obj.paindetails.length - 1) {
                                    lastdate = covertdate[0];
                                    secoundlastdate = covertdate[0];

                                }
                            }
                        } else {
                            var covertdate = dataconvert(obj.paindetails[i][0], parseInt(obj.paindetails[i][1]));
                            paindata2.push(covertdate);
                            if (i === 7) {
                                secoundate = covertdate[0];
                            }
                            if (i === obj.paindetails.length - 1) {
                                secoundlastdate = covertdate[0];
                            }
                        }
                    }
                    assignpain(paindata1, paindata2, call);
                }
            }
            process();
        },
        error: function (data) {
            assignpain(paindata1, paindata2, call);
            process();
        }
    });
    return false;
}


var fatmassdate1 = [];
var fatmassdate2 = [];
var fatmassrate1 = [];
var fatmassrate2 = [];

// assign fatmass
function assignfatmass(callother) {

    $('#fatmasschart').highcharts({
        title: {
            text: ''
        },
        xAxis: {
            categories: fatmassdate1
        },
        yAxis: {
            title: {
                text: ''
            }
        },
        credits: {
            enabled: false
        },
        tooltip: {
            crosshairs: true,
            shared: true
        },
        legend: {},
        noData: {
            style: {
                fontWeight: 'bold',
                fontSize: '15px',
                color: '#303030'
            }
        },
        series: [{
                lineWidth: 1,
                name: 'Fatmass',
                showInLegend: false,
                color: '#c7c11c',
                data: fatmassrate1,
                zIndex: 1,
                marker: {
                    symbol: 'circle',
                    radius: 3
                }
            }]
    });

    chartfatmass = $('#fatmasschart').highcharts();
    $('#fatmass_next').on("click", function () {

        chartfatmass.series[0].update({
            data: fatmassrate2
        });
        chartfatmass.xAxis[0].setCategories(fatmassdate2);
    });

    $('#fatmass_prev').on("click", function () {
        chartfatmass.series[0].update({
            data: fatmassrate1
        });
        chartfatmass.xAxis[0].setCategories(fatmassdate1);
    });

    callother = typeof (callother) == "undefined" ? 1 : callother;
    if (callother !== -1) {
        getglucose();
    }
}

// get fatmass
function getfatmass(call) {

    var call = typeof (call) == "undefined" ? 1 : call;

    $.ajax({
        url: path + "HospitalManagementSystem/HMS/AppDashBoard/getfatmass.jsp",
        type: "POST",
        data: {},
        success: function (data) {
            var msg = jQuery.trim(data);
            if (msg) {
                obj = JSON.parse(msg);
                if (obj.fatmass.length > 0) {
                    for (i = 0; i < obj.fatmass.length; i++) {
                        $("#fat_start").html(obj.fatmass[0][0]);
                        $("#fat_end").html(obj.fatmass[obj.fatmass.length - 1][0]);

                        if (i < 5) {
                            var covertdate = dateconvert(obj.fatmass[i][0]);
                            fatmassdate1.push(covertdate);
                            fatmassrate1.push(obj.fatmass[i][1]);
                        } else {
                            var covertdate = dateconvert(obj.fatmass[i][0]);
                            fatmassdate2.push(covertdate);
                            fatmassrate2.push(obj.fatmass[i][1]);
                        }
                    }
                    assignfatmass(call);
                } else {

                    $("#fat_end").html("Sample Data");
                    var sampledata = "{fatmass:[['2016-06-17', '15'],['2016-06-18', '12'],['2016-06-19', '15'],['2016-06-20', '16'], ['2016-06-21', '13'], ['2016-06-22', '13'], ['2016-06-23', '13']]}";
                    obj = JSON.parse(JSON.stringify(eval("(" + sampledata + ")")));
                    for (i = 0; i < obj.fatmass.length; i++) {
                        $("#fat_start").html(obj.fatmass[0][0]);
                        $("#fat_end").html(obj.fatmass[obj.fatmass.length - 1][0]);

                        if (i < 5) {
                            var covertdate = dateconvert(obj.fatmass[i][0]);
                            fatmassdate1.push(covertdate);
                            fatmassrate1.push(parseFloat(obj.fatmass[i][1]));
                        } else {
                            var covertdate = dateconvert(obj.fatmass[i][0]);
                            fatmassdate2.push(covertdate);
                            fatmassrate2.push(parseFloat(obj.fatmass[i][1]));
                        }
                    }
                    assignfatmass(call);
                }
            }
            process();
        },
        error: function () {
            assignfatmass(call);
            process();
        }
    });
    return false;
}

var glucosedate1 = [];
var glucosedate2 = [];
var glucoserate1 = [];
var glucoserate2 = [];


//glucoserateassign
function glucoseAssign() {

    $('#glucoseChart').highcharts({
        chart: {
            zoomType: 'xy'
        },
        title: {
            text: '',
            x: -20
        },
        credits: {
            enabled: false
        },
        xAxis: {
            categories: glucosedate1
        },
        yAxis: {
            min: 0,
            max: 500,
            tickInterval: 50,
            labels: {
                format: '{value}'
            },
            title: {
                text: 'Glucose (mg/dL)'
            },
            credits: {
                enabled: false
            },
            plotLines: [{
                    value: 0,
                    width: 1,
                    color: '#808080'
                }]
        },
        tooltip: {
            crosshairs: true,
            headerFormat: '<b>Glucose Rate</b><br>',
            pointFormat: '{point.y}(mg/dL)'

        },
        legend: {
            align: 'center',
            borderWidth: 0
        },
        noData: {
            style: {
                fontWeight: 'bold',
                fontSize: '15px',
                color: '#303030'
            }
        },
        series: [{
                lineWidth: 1,
                name: 'Glucose Rate',
                showInLegend: false,
                color: '#d9534f',
                data: glucoserate1,
                marker: {
                    symbol: 'circle',
                    radius: 3
                }
            }]
    });

    chartGlucose = $('#glucoseChart').highcharts();
    $('#glucose_next').on("click", function () {
        chartGlucose.series[0].update({
            data: glucoserate2
        });
        chartGlucose.xAxis[0].setCategories(glucosedate2);
    });
    $('#glucose_prev').on("click", function () {
        chartGlucose.series[0].update({
            data: glucoserate1
        });
        chartGlucose.xAxis[0].setCategories(glucosedate1);
    });

}

// get glucose
function getglucose() {

    $.ajax({
        url: path + "HospitalManagementSystem/HMS/AppDashBoard/getglucose.jsp",
        type: "POST",
        data: {},
        success: function (data) {
            var msg = jQuery.trim(data);
            if (msg) {
                obj = JSON.parse(msg);
                if (obj.glucoserate.length > 0) {

                    for (i = 0; i < obj.glucoserate.length; i++) {
                        $("#glucose_start").html(obj.glucoserate[0][0]);
                        $("#glucose_end").html(obj.glucoserate[obj.glucoserate.length - 1][0]);

                        if (i < 7) {
                            var covertdate = dateconvert(obj.glucoserate[i][0]);
                            glucosedate1.push(covertdate);
                            glucoserate1.push(obj.glucoserate[i][1]);
                        } else {
                            var covertdate = dateconvert(obj.glucoserate[i][0]);
                            glucosedate2.push(covertdate);
                            glucoserate2.push(obj.glucoserate[i][1]);
                        }
                    }
                    glucoseAssign();
                } else {

                    $("#glucose_end").html("Sample Data");
                    var sampledata = "{glucoserate:[['2016-06-17', '35'],['2016-06-18', '52'],['2016-06-19', '65'],['2016-06-20', '46'], ['2016-06-21', '53'], ['2016-06-22', '13'], ['2016-06-23', '73']]}";
                    obj = JSON.parse(JSON.stringify(eval("(" + sampledata + ")")));
                    for (i = 0; i < obj.glucoserate.length; i++) {
                        $("#glucose_start").html(obj.glucoserate[0][0]);
                        $("#glucose_end").html(obj.glucoserate[obj.glucoserate.length - 1][0]);

                        if (i < 7) {
                            var covertdate = dateconvert(obj.glucoserate[i][0]);
                            glucosedate1.push(covertdate);
                            glucoserate1.push(parseFloat(obj.glucoserate[i][1]));
                        } else {
                            var covertdate = dateconvert(obj.glucoserate[i][0]);
                            glucosedate2.push(covertdate);
                            glucoserate2.push(parseFloat(obj.glucoserate[i][1]));
                        }
                    }
                    glucoseAssign();
                }
            }
            process();
        },
        error: function (data) {
            glucoseAssign();
            process();
        }

    });
    return false;
}

//remove chart
function removeChart(id, arr1, arr2, arr3, arr4, arr5, arr6) {

    if (typeof (arr5) !== "undefined" || typeof (arr6) !== "undefined") {
        id.series[0].remove(true);
        arr1.length = 0;
        arr2.length = 0;
        arr3.length = 0;
        arr4.length = 0;
        arr5.length = 0;
        arr6.length = 0;
    } else {
        id.series[0].remove(true);
        arr1.length = 0;
        arr2.length = 0;
        if (typeof (arr3) !== "undefined" || typeof (arr4) !== "undefined") {
            arr3.length = 0;
            arr4.length = 0;
        }
    }
}
