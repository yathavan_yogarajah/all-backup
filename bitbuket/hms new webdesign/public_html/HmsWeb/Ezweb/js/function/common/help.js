//user hep actions
$(".help_icon").click(function () {
    $(".help_overlay").fadeIn("fast", function () {
        $("#help_wrapper").show();
    });
});

//close
$(".help_overlay,#help_gotit").click(function () {
    $(".help_overlay").fadeOut("fast", function () {
        $("#help_wrapper").hide();
    });
});

//gatherHelpDetails
function gatherHelpDetails(obj) {
    $("#helphead").html(obj.header);
    $("#helpcontent_in").html(obj.inner);
}

//Diabetes Diary
var diabetesdiary_help = {
    header: "Diabetes Diary",
    inner: "This is where you would record and maintain your diabetes diary, treatment plans, education, etc.",
    img: "optional"
};

//Dental Issues
var family_help = {
    header: "Family Medical History",
    inner: "This is where you would record your family’s medical issues that may affect your health.",
    img: "optional"
};

//Dental Issues
var dental_help = {
    header: "Dental Issues",
    inner: "This is where you would record your current or historical Dental issues.",
    img: "optional"
};
//Surgery
var surgery_help = {
    header: "Surgery",
    inner: "This is where you would record your current or historical surgery/procedure you have gone through.",
    img: "optional"
};
//Medication
var medication_help = {
    header: "Medication",
    inner: "This is where you would record your current or historical medication usage.",
    img: "optional"
};

//Allergies
var allergies_help = {
    header: "Allergies",
    inner: "This is where you would record your current or historical allergic reaction to medicine, food, etc.",
    img: "optional"
};

//Medical problem
var medical_help = {
    header: "Medical Problems",
    inner: "This is where you would record your current or historical medical problems such as back pain.",
    img: "optional"
};

// wellness check
var wellness = {
    header: "Health Maintenance",
    inner: "This is where you would record and maintain your screening tests that are based on your age group.",
    img: "optional"
};

//moods
var moods_help = {
    header: "My Mood Diary",
    inner: "This is where you would record and maintain your mood diary.",
    img: "optional"
};

//addmeasurement
var measurement_help = {
    header: "Add Measurements",
    inner: "This is where you would manually enter and maintain your vitals such as heart rate, blood pressure, blood glucose, etc.",
    img: "optional"
};

//manage mypain
var pain_help = {
    header: "My Pain Diary",
    inner: "This is where you would record and maintain your pain diary.",
    img: "optional"
};

//who help
var who_help = {
    header: "Who",
    inner: "This is where you would add profile image and your personal details.",
    img: "optional"
};

//contact help
var contact_help = {
    header: "Contact",
    inner: "This is where you would add your contact details.",
    img: "optional"
};

//employer help
var employer_help = {
    header: "Employer",
    inner: "This is where you would add your Employer details.",
    img: "optional"
};
//stats help
var stats_help = {
    header: "Stats",
    inner: "This is where you would provide additional information about you.",
    img: "optional"
};
//insurance help
var insurance_help = {
    header: "Insurance",
    inner: "This is where you would enter your health insurance information for your care provider to bill them.",
    img: "optional"
};
//insurance help
var accessmyprovider_help = {
    header: "Access My Provider",
    inner: "This is where you add and keep your frequently visiting care provider for quick access.  <br>We will also maintain a history of care providers you visited for your reference and easy access.",
    img: "optional"
};