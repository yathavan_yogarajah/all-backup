//provider details alert msg
var providerdetailsuccess = "Your provider details have been added successfully!";
var providerdetailunsuccess = "Unable to save the provider details!";
var providerdetailsalreadyAdd = "Provider details already exists!";
var providerdetailsrequired = "Please fill required fields!";

// access my providers alert msg
var haventprovider = '<img src="../../img/warningSign.gif" class="warn" alt="warning"><span><strong>Sorry!</strong> You have not added any providers yet!</span>';

//common details msg
var successfulladddetails = "Your details have been successfully added!";
var unsuccessfulladddetails = "Unable to save your details!";

//password recover
var successfullrecover = "Your password was successfully reset!<br> Please check your mail!";

//password reset
var successfullreset = "Your password was successfully reset!";
var mismatcholdpass = "Password mismattch!";
var unsuccessfullreset = "Unable to reset your password!";
var newpasswordmismatch = "Password mismattch!";

// user login
var unsuccesslogin = "<b>Error! </b > Email or Password mismatch!";
var invalidemail = "Invalid Email address";
var alreadyhave = "<b>Error! </b > Someone’s already using that email!";

// displayhospital
var unablefindhospital = "<img src = '../../img/warningSign.gif' class = 'warn' alt = 'warning' <span><strong> Sorry! </strong> Unable to find nearby hospitals!";

var unablefindclinics = "<img src = '../../img/warningSign.gif' class = 'warn' alt = 'warning' <span><strong> Sorry! </strong> unable to find nearby clinics!</span>";

var currentlyworking = "<img src = '../../img/warningSign.gif' class = 'warn' alt = 'warning' <span> <strong> Sorry! </strong>Comming soon </span>";

//email
var successfullsend = "Your message has been successfully sent!";
var unsuccessfullsend = "Unable to send your message!";
var nowebsite = '<img src="../../img/warning.png" class="img-responsive warn-alertsendemail" alt="warning"><p id="err-msg"><strong>Sorry!</strong>This hospital/clinic does not have an official web!</p>';

//file attach
var successfullattach = "File has been successfully attcahed!";
var unsuccessfulattach = "Unable to attcah the file!";
var filesizelimit = "Please select less than 5MB file!";

// dashboard preview
var previewerror = "Unable to display the preview!";

//visitng history
var nohistory = '<img src="../../img/warningSign.gif" class="warn" alt="warning"><span><strong>Sorry!</strong> You do not have any visting history!</span>';

//camera
var cameracaptureerror = "<b>Error!</b> unable to take picture!";

//profile image upload
var successfulluploadprofileimage = "Your profile image have been successfully saved!";
var unsuccessfulluploadprofileimage = "Unable to save your profile image!";

//date validation
var dateerror = "Please enter the valid date format! [yyyy-mm-dd]";
var datecheckerror = "Please check the begin & end dates";
var datecheck = "Please check the begin & end dates";
var dateofbirth = "Please enter the valid date of birth!";
var validdate = "Please check date";

//manage pain
var selectbodyloc = "Please select the body location";

//emailadd validation
var valdateemailerror = "<b>Error! </b> Please provide valid email!";

//server error

var servererror = "<b>Error! </b> Server Error!";

//Number validation
var numbererror = "Please enter the valid format data";

//helathmanagement system
var outdatedata = "Your health management data is out dated. please add new test!.";
var cantaccess = "Currently you can't access this feature!.";
var fillprofile = "<b>Error! </b> Please fill your profile details!";

//glugose 
var selectmeals = "Please select food types !";