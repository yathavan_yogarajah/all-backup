//check all the pages internet
setInterval(function () {

    if (navigator.onLine)
    {
        localStorage.setItem("connection_alive", true);
        $("#connection_notify").fadeOut(500);
    } else
    {
        localStorage.setItem("connection_alive", false);
        $("#connection_notify").html("please check internet connection");
        $("#connection_notify").fadeIn(500);
    }

}, 100);

connection = localStorage.getItem("connection_alive");

//getConnection
function getConnection() {
    return localStorage.getItem("connection_alive");
}