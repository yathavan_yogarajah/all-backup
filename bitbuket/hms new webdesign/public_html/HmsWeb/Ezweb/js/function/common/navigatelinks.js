//maninav click
$(".main_nav").click(function () {

    $(".main_nav").removeClass("main_nav_selcted");
    $page = $(this).attr('id') + ".html";

    if ($page === "firstloginMessage.html") {

        $("#main_contents").attr("src", "../common/" + $page);
        $(this).addClass("main_nav_selcted");

    } else if ($page === "who.html") {

        $("#main_contents").attr("src", "../profile/" + $page);
        $(this).addClass("main_nav_selcted");

    } else if ($page !== "chat.html") {

        $("#main_contents").attr("src", "../healthcare/" + $page);
        $(this).addClass("main_nav_selcted");

    } else {

        $('#myModal').modal({
            show: true
        });

        $('.modal-open').removeClass("modal-open");
        $('.modal-backdrop').removeClass("modal-backdrop");

    }
});