
//input focus
$("input,select").focus(function () {
    $input_id = $(this).attr("id");
    if ($("#" + $input_id).hasClass("input-alert"))
        $("#" + $input_id).removeClass("input-alert");
});

// This identifies your website in the createToken call below
Stripe.setPublishableKey('pk_test_0lNC2rciRhGMlTgmJNNSUPUu');

var stripeResponseHandler = function (status, response) {

    var $form = $('#payment-form');
    if (response.error) {
        $("#device_status_card").hide();
        // Show the errors on the form
        $("#errormsg-pay").html(response.error.message);
        $("#errormsg-pay").show();
        var myVar1 = setInterval(function () {
            $("#errormsg-pay").hide();
            $form.find('button').prop('disabled', false);
            clearInterval(myVar1);
        }, 3000);

    } else {

        // token contains id, last4, and card type
        stripecard = response.card.brand;
        if (stripecard) {
            send_info();
        }
        var token = response.id;
        // Insert the token into the form so it gets submitted to the server
        $form.append($('<input type="hidden" name="stripeToken" />').val(token));
        // and re-submit
    }
};

function checklist() {

    error = 0;

    var required = ["plans-pay", "card-hodername", "cardnumber", "cvc", "cm", "cy"];

    for (var x = 0; x < required.length; x++) {
        if ($.trim($("#" + required[x]).val()) === "" || $.trim($("#" + required[x]).val()) === "none") {
            $("#" + required[x]).addClass("input-alert");
            error++;
        }
    }
}

//payment submit
jQuery(function ($) {

    $('#payment-form').submit(function (e) {
        checklist();
        if (error === 0 && $("#cvc").val().length === 3) {
            var $form = $(this);
            $("#device_status_card").show();
            // Disable the submit button to prevent repeated clicks
            $form.find('button').prop('disabled', true);
            Stripe.card.createToken($form, stripeResponseHandler);
            // Prevent the form from submitting with the default action

        } else {
            if ($("#cvc").length !== 3) {
                $("#cvc").addClass("input-alert");
            }
        }
        return false;
    });
});

//send info
function send_info() {

    var plan = $("#plans-pay").val();
    var price = ($("#plan-price").val()) * 100;
    var hodername = $("#card-hodername").val();
    var autorenew = $("input[name=autorenew]").is(":checked") ? "yes" : "no";

    if (plan !== "none" && hodername) {
        $.ajax({
            url: path + "HospitalManagementSystem/HMS/Profile/addsubscription.jsp",
            type: "POST",
            data: {
                "plan": plan,
                "price": price,
                "hodername": hodername,
                "cardtype": stripecard,
                "autorenew": autorenew,
                "cardnumber": $("#cardnumber").val(),
                "cvc": $("#cvc").val(),
                "expm": $("#cm").val(),
                "expy": $("#cy").val()
            },
            success: function (data) {

                var msg = jQuery.trim(data);
                if (msg === "success") {
                    $("#successfull-pay").html(successpay);
                    $("#successfull-pay").show();
                    $("#device_status_card").hide();
                    var myVar1 = setInterval(function () {
                        $("#successfull-pay").hide();
                        remainstate();
                        clearInterval(myVar1);
                    }, 3000);

                } else if (msg === "unsuccess") {
                    $("#device_status_card").hide();
                    $(".payment-errors").html(unsuccesspay);
                    var myVar1 = setInterval(function () {
                        $("#errormsg-pay").html("");
                        clearInterval(myVar1);
                    }, 3000);
                } else {
                    $("#device_status_card").hide();
                    $("#errormsg-pay").html(msg);
                    $("#errormsg-pay").show();
                    var myVar1 = setInterval(function () {
                        $("#errormsg-pay").hide();
                        clearInterval(myVar1);
                    }, 3000);
                }
                $('#payment-form').find('button').removeProp("disabled");

            },
            error: function () {
                $("#device_status_card").hide();
                $("#errormsg-pay").html(servererror);
                $("#errormsg-pay").show();
                var myVar1 = setInterval(function () {
                    $("#errormsg-pay").hide();
                    $('#payment-form').find('button').removeProp("disabled");
                    clearInterval(myVar1);
                }, 3000);
            }
        });
    } else {
        if (!plan || plan === "none") {
            $("#plans-pay").addClass("input-alert");
        }
        if (!hodername) {
            $("#card-hodername").addClass("input-alert");
        }
        $('#payment-form').find('button').removeProp("disabled");
    }
}

function remainstate() {

    $("#pay-card").modal("hide");
    $('#payment-form').find('button').removeProp("disabled");
    $('#plans-pay').find("option:eq(0)").attr("selected", "selected");
    $("#plan-price").val("00");
    $("#card-hodername").val("");
    $("input[name=autorenew]").removeAttr("checked");
    document.getElementById("payment-form").reset();

}