//focus input
$("input,select").focus(function () {
    $input_id = $(this).attr("id");
    if ($("#" + $input_id).hasClass("input-alert"))
        $("#" + $input_id).removeClass("input-alert");
});

// This identifies your website in the createToken call below
Stripe.setPublishableKey('pk_test_0lNC2rciRhGMlTgmJNNSUPUu');

var stripeResponseHandler = function (status, response) {

    var $form = $('#payment-form');
    if (response.error) {
        $("#preload_overlay_light").hide();
        // Show the errors on the form
        $("#errormsg-pay").html(response.error.message);
        $("#errormsg-pay").show();
        var myVar1 = setInterval(function () {
            $("#errormsg-pay").hide();
            $form.find('button').prop('disabled', false);
            clearInterval(myVar1);
        }, 3000);

    } else {

        // token contains id, last4, and card type
        stripecard = response.card.brand;
        if (stripecard) {
            UpdateCardInfo();
        }
        var token = response.id;
        // Insert the token into the form so it gets submitted to the server
        $form.append($('<input type="hidden" name="stripeToken" />').val(token));
        // and re-submit
        // $form.get(0).submit();
    }
};

function checklist() {

    error = 0;
    var required = ["card-hodername", "cardnumber", "cvc", "cm", "cy"];

    for (var x = 0; x < required.length; x++) {
        if ($.trim($("#" + required[x]).val()) === "" || $.trim($("#" + required[x]).val()) === "none") {
            $("#" + required[x]).addClass("input-alert");
            error++;
        }
    }
}

//payment submit
jQuery(function ($) {

    $('#payment-form').submit(function (e) {

        checklist();
        if (error === 0 && $("#cvc").val().length === 3) {
            var $form = $(this);
            $("#preload_overlay_light").show();
            // Disable the submit button to prevent repeated clicks
            $form.find('button').prop('disabled', true);
            Stripe.card.createToken($form, stripeResponseHandler);
            // Prevent the form from submitting with the default action

        } else {
            if ($("#cvc").length !== 3) {
                $("#cvc").addClass("input-alert");
            }
        }
        return false;
    });
});

//get card information
function getCardInfo() {

    $.ajax({
        url: path + "HospitalManagementSystem/HMS/Profile/getcarddetails.jsp",
        type: "POST",
        success: function (data) {
            var msg = jQuery.trim(data);
            paymentobj = jQuery.trim(data);

            $("#card-hodername").val($.trim(paymentobj.hodername));
            $("#cardnumber").val($.trim(paymentobj.cardnumber));
            $("#cvc").val($.trim(paymentobj.cvc));
        },
        error: function () {
            $("#errormsg-pay").html(servererror);
            $("#errormsg-pay").show();
            var hidestatus = setInterval(function () {
                $("#errormsg-pay").hide();
                clearInterval(hidestatus);
            }, 3000);
        }
    });
}

//update card information
function UpdateCardInfo() {

    $.ajax({
        url: path + "HospitalManagementSystem/HMS/Profile/updateexpirecard.jsp",
        type: "POST",
        data: {
            "hodername": $("#card-hodername").val(),
            "cardtype": stripecard,
            "cardnumber": $("#cardnumber").val(),
            "cvc": $("#cvc").val(),
            "expm": $("#cm").val(),
            "expy": $("#cy").val()
        },
        success: function (data) {
            var msg = jQuery.trim(data);

            if (msg === "success") {
                $("#preload_overlay_light").hide();
                $("#successfull-pay").html(successpay);
                $("#successfull-pay").show();
                var myVar1 = setInterval(function () {
                    $("#successfull-pay").hide();
                    window.location = "firstlogin.html";
                    clearInterval(myVar1);
                }, 3000);
            } else if (msg === "unsuccess") {
                $("#preload_overlay_light").hide();
                $(".payment-errors").html(unsuccesspay);
                var myVar1 = setInterval(function () {
                    $("#errormsg-pay").html("");
                    clearInterval(myVar1);
                }, 3000);
            } else {
                $("#preload_overlay_light").hide();
                $("#errormsg-pay").html(msg);
                $("#errormsg-pay").show();
                var myVar1 = setInterval(function () {
                    $("#errormsg-pay").hide();
                    clearInterval(myVar1);
                }, 3000);
            }
        },
        error: function () {
            $("#preload_overlay_light").hide();
            $("#errormsg-pay").html(servererror);
            $("#errormsg-pay").show();
            var myVar1 = setInterval(function () {
                $("#errormsg-pay").hide();
                $('#payment-form').find('button').removeProp("disabled");
                clearInterval(myVar1);
            }, 3000);
        }
    });
}