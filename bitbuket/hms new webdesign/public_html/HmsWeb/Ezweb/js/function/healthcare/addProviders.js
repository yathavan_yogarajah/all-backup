//input focus
$("input,select").focus(function () {
    $input_id = $(this).attr("id");
    if ($("#" + $input_id).hasClass("input-alert"))
        $("#" + $input_id).removeClass("input-alert");
});

//validate email
function validateEmail(Email) {

    var filter = /^[\w\-\.\+]+\@[a-zA-Z0-9\.\-]+\.[a-zA-z0-9]{2,4}$/;
    if (filter.test(Email)) {
        return true;
    } else {
        return false;
    }
}

$(function () {
    // request city
    $("#city").autocomplete({
        source: function (request, response) {
            var count = $("#country").val();
            var state = $("#state").val();
            var err = 0;
            if (count === "" || count === null) {
                err++;
                $("#country").addClass("input-alert");
            }

            if (state === "" || state === null) {
                err++;
                $("#state").addClass("input-alert");
            }

            if (!isNaN($("#city").val())) {
                err++;
                $("#city").addClass("input-alert");
            }
            $("#provider_care").val("");

            if (err === 0) {
                $.getJSON(path + "HospitalManagementSystem/HMS/HealthProvider/getcity.jsp", {
                    "country": $("#country").val(),
                    "state": $("#state").val(),
                    "city": $("#city").val()
                }, response);
            }
        },
        minLength: 1
    });

    // request provider
    $("#provider_care").autocomplete({
        source: function (request, response) {
            var count = $("#country").val();
            var state = $("#state").val();
            var city = $("#city").val();
            var err = 0;

            if (count === "" || count === null) {
                err++;
                $("#country").addClass("input-alert");
            }

            if (state === "" || state === null) {
                err++;
                $("#state").addClass("input-alert");
            }

            if (city === "" || city === null) {
                err++;
                $("#city").addClass("input-alert");
            }

            if (!isNaN($("#provider_care").val())) {
                err++;
                $("#provider_care").addClass("input-alert");
            }

            if (err === 0) {
                $.getJSON(path + "HospitalManagementSystem/HMS/HealthProvider/providerautocomplete.jsp", {
                    "term": $("#provider_care").val(),
                    "center": $('input[name="provider_place"]:checked').val(),
                    "city": $("#city").val(),
                    "state": $("#state").val(),
                    "country": $("#country").val()
                }, response);
            }
        },
        minLength: 1
    });

    $('input[type=radio][name=provider_place]').change(function () {
        $("#provider_care").val("");
    });

    $("#city").keypress(function () {
        $("#provider_care").val("");
    });

});

//add providers
function addProvider() {

    //check internetconnection
    if (getConnection() === "false") {
        return false;
    }

    var centername = $("#provider_care").val();
    var centertype = $('input[name="provider_place"]:checked').val();

    if (centername !== "" && centername !== " ") {
        $("#status_out").show();
        $("#addprovider").prop("disabled", true);
        $.ajax({
            url: path + "HospitalManagementSystem/HMS/HealthProvider/addprovider.jsp",
            type: "POST",
            data: {
                "centername": centername,
                "centertype": centertype,
                "countryname": $("#country").val(),
                "statename": $("#state").val(),
                "cityname": $("#city").val(),
                "email": "nil",
                "providercat": "ownreg"
            },
            success: function (data) {
                var msg = jQuery.trim(data);
                if (msg === "success") {
                    $("#status_out").hide();
                    $("#successfull-send").html(providerdetailsuccess);
                    $("#successfull-send").show();
                    $("#provider_care").val("");
                    var myVar1 = setInterval(function () {
                        $("#successfull-send").hide();
                        clearInterval(myVar1);
                        window.location = "accessMyproviders.html";
                    }, 3000);
                } else if (msg === "alreadyadd") {
                    $("#status_out").hide();
                    $("#errormsg-alert").html(providerdetailsalreadyAdd);
                    $("#errormsg-alert").show();
                    var myVar1 = setInterval(function () {
                        $("#errormsg-alert").hide();
                        $("#addprovider").removeProp("disabled");
                        clearInterval(myVar1);
                    }, 3000);
                } else {
                    $("#status_out").hide();
                    $("#add_custom").modal('show');
                    $("#addprovider").removeProp("disabled");
                }
            },
            error: function () {
                $("#status_out").hide();
                $("#errormsg-alert").html(servererror);
                $("#errormsg-alert").show();
                var myVar1 = setInterval(function () {
                    $("#errormsg-alert").hide();
                    $("#addprovider").removeProp("disabled");
                    clearInterval(myVar1);
                }, 3000);
            }
        });
        return false;
    } else {
        $("#errormsg-alert").html(providerdetailsrequired);
        $("#errormsg-alert").show();
        var myVar1 = setInterval(function () {
            $("#errormsg-alert").hide();
            clearInterval(myVar1);
        }, 3000);
    }
}

// get country
function getcountry() {

    $('#country')[0].options.length = 0;
    $('#country').append('<option value="">Select one *</option>');
    $.ajax({
        url: path + "HospitalManagementSystem/HMS/HealthProvider/getcountry.jsp",
        type: "POST",
        data: {},
        success: function (data) {
            var msg = jQuery.trim(data);
            if (msg) {
                obj = JSON.parse(msg);
                if (obj.countries.length > 0) {
                    for (i = 0; i < obj.countries.length; i++) {
                        $('#country').append('<option value="' + obj.countries[i][0] + '">' + obj.countries[i][0] + '</option>');
                    }
                }
            }
        },
        error: function () {
            $("#status_out").hide();
            $("#errormsg-alert").html(servererror);
            $("#errormsg-alert").show();
            var myVar1 = setInterval(function () {
                $("#errormsg-alert").hide();
                clearInterval(myVar1);
            }, 3000);
        }
    });
    return false;
}

//country change
$("#country").change(function () {
    var country = $("#country").val();
    if (country !== "" && country !== null) {
        getstate();
    }
});


//getstate
function getstate() {

    $("#city").val("");
    $("#provider_care").val("");
    $('#state')[0].options.length = 0;
    $('#state').append('<option value="">Select one *</option>');

    $.ajax({
        url: path + "HospitalManagementSystem/HMS/HealthProvider/getstate.jsp",
        type: "POST",
        data: {
            country: $("#country").val()
        },
        success: function (data) {
            var msg = jQuery.trim(data);
            if (msg) {
                obj = JSON.parse(msg);
                if (obj.states.length > 0) {
                    $('#state').show();
                    for (i = 0; i < obj.states.length; i++) {
                        $('#state').append('<option value="' + obj.states[i][0] + '">' + obj.states[i][0] + '</option>');
                    }
                } else {
                    $('#state').hide();
                }
            }
        },
        error: function () {
            $("#status_out").hide();
            $("#errormsg-alert").html(servererror);
            $("#errormsg-alert").show();
            var myVar1 = setInterval(function () {
                $("#errormsg-alert").hide();
                clearInterval(myVar1);
            }, 3000);
        }
    });
    return false;
}

//state change
$("#state").change(function () {
    $("#city").val("");
    $("#provider_care").val("");

});

//add details
function add_details() {

    if ($("#place_email").val()) {
        if (!validateEmail($("#place_email").val())) {
            $("#place_email").addClass("input-alert");
            return false;
        }
    }
    var email = $("#place_email").val() ? $("#place_email").val() : "nil";

    $("#status_out_add").show();
    var centername = $("#provider_care").val();
    var centertype = $('input[name="provider_place"]:checked').val();

    $.ajax({
        url: path + "HospitalManagementSystem/HMS/HealthProvider/addprovider.jsp",
        type: "POST",
        data: {
            "centername": centername,
            "centertype": centertype,
            "countryname": $("#country").val(),
            "statename": $("#state").val(),
            "cityname": $("#city").val(),
            "email": email,
            "providercat": "self"
        },
        success: function (data) {
            $("#status_out_add").hide();
            $("#successfull-send-add").html(providerdetailsuccess);
            $("#successfull-send-add").show();
            var hidestatus = setInterval(function () {
                $("#successfull-send-add").hide();
                window.location = "accessMyproviders.html";
                $("#add_custom").modal('hide');
                clearInterval(hidestatus);
            }, 3000);
        },
        error: function (data) {
            $("#status_out_add").hide();
            $("#errormsg-alert-add").html(servererror);
            $("#errormsg-alert-add").show();
            var hidestatus = setInterval(function () {
                $("#errormsg-alert-add").hide();
                clearInterval(hidestatus);
            }, 3000);
        }
    });
}