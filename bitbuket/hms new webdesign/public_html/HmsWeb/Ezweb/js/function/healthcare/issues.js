var dob = "";
var range = "";
var age;

//check test
function checktest() {

    $.ajax({
        url: path + "HospitalManagementSystem/HMS/Issues/gettest.jsp",
        type: "POST",
        success: function (data) {

            var msg = jQuery.trim(data);

            if (msg) {

                obj = JSON.parse(msg);

                if (obj.testres.length > 0) {

                    var datadddate = obj.testres[0][1];
                    var today = new Date(datadddate);
                    var birthDate = new Date(dob);
                    var age1 = today.getFullYear() - birthDate.getFullYear();
                    var m = today.getMonth() - birthDate.getMonth();
                    if (m < 0 || (m === 0 && today.getDate() < birthDate.getDate())) {
                        age1--;
                    }

                    if (age >= age1) {

                        var range1 = "";
                        if (age1 >= 18 && age1 <= 39) {
                            range1 = "18-39";
                        } else if (age1 >= 40 && age1 <= 49) {
                            range1 = "40-49";
                        } else if (age1 >= 50 && age1 <= 74) {
                            range1 = "50-74";
                        } else if (age1 >= 75) {
                            range1 = "75-Over";
                        } else {
                            range1 = "cant";
                        }

                        if (range === range1) {

                            localStorage.setItem("healthtest", msg);
                            localStorage.setItem("validdata", "yes");

                        } else {

                            $("#img-healthguide-warning").show();
                            localStorage.setItem("healthtest", msg);
                            localStorage.setItem("validdata", "no");
                        }

                    } else {

                        localStorage.setItem("healthtest", msg);
                        localStorage.setItem("validdata", "no");
                    }

                    $("#preload_overlay_light").hide();

                } else {

                    localStorage.setItem("healthtest", msg);
                    localStorage.setItem("validdata", "no");
                    $("#preload_overlay_light").hide();
                }
            }

        },
        error: function () {
            $("#preload_overlay_light").hide();
        }
    });
}

//get age
function getage() {

    $("#preload_overlay_light").show();
    $.ajax({
        url: path + "HospitalManagementSystem/HMS/Profile/getprofilestatus.jsp",
        type: "POST",
        success: function (data) {
            var msg = jQuery.trim(data);
            if (msg) {

                obj = JSON.parse(msg);

                if (obj.userProfile.length > 0) {

                    dob = obj.userProfile[0][0];
                    localStorage.setItem("sex", obj.userProfile[0][1]);
                    var today = new Date();
                    var birthDate = new Date(dob);
                    age = today.getFullYear() - birthDate.getFullYear();
                    var m = today.getMonth() - birthDate.getMonth();
                    if (m < 0 || (m === 0 && today.getDate() < birthDate.getDate())) {
                        age--;
                    }

                    if (age >= 18 && age <= 39) {
                        localStorage.setItem("range", "18-39");
                    } else if (age >= 40 && age <= 49) {
                        localStorage.setItem("range", "40-49");
                    } else if (age >= 50 && age <= 74) {
                        localStorage.setItem("range", "50-74");
                    } else if (age >= 75) {
                        localStorage.setItem("range", "75-Over");
                    } else {
                        localStorage.setItem("range", "cant");
                    }
                    checktest();

                } else {
                    localStorage.setItem("range", "addprofile");
                    $("#preload_overlay_light").hide();
                }
            }
        },
        error: function () {
            $("#preload_overlay_light").hide();
        }
    });
}
