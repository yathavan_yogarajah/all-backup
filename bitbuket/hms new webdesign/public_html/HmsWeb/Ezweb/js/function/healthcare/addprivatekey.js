//securitycheck
function addprivateQuestion() {

    var check = 0;

    var question = $.trim($("#secqu_email").val());
    var answer = $.trim($("#secans_email").val());
    var reanswer = $.trim($("#sec_anscon_email").val());

    if (question.length > 3 && answer.length > 3 && reanswer.length > 3) {

        if (reanswer !== answer) {

            $("#secans_email").addClass("input-alert");
            $("#sec_anscon_email").addClass("input-alert");
        }

    } else {

        if (question.length < 3) {
            $("#secqu_email").addClass("input-alert");
            check++;
        }
        if (answer.length < 3) {
            $("#secans_email").addClass("input-alert");
            check++;
        }
        if (reanswer.length < 3) {
            $("#sec_anscon_email").addClass("input-alert");
            check++;
        }

    }
    return check;
}

//setprivate key
function setprivatekey() {

    var requesturl = path + "HospitalManagementSystem/HMS/EmailSend/addprivatemailkey.jsp";

    $.ajax({
        type: "POST",
        url: requesturl,
        data: {
            "secrectquestion": $("#secqu_email").val(),
            "secrectkey": $("#secans_email").val()
        },
        success: function (html) {

            var msg = jQuery.trim(html);
            if (msg === "success") {
                clear();
                $("#status_out").hide();
                $("#successfull-send").html(successfullsend);
                $("#successfull-send").show();
                var myVar1 = setInterval(function () {
                    $("#successfull-send").hide();
                    $("#send").removeProp("disabled");
                    $("#successfull-send").hide();
                    clearInterval(myVar1);
                }, 3000);
                $("#send").removeProp("disabled");

            } else {

                $("#status_out").hide();
                $("#errormsg-alert").html(unsuccessfullsend);
                $("#errormsg-alert").show();
                var myVar1 = setInterval(function () {
                    $("#errormsg-alert").hide();
                    $("#send").removeProp("disabled");
                    $("#preload_overlay_light").hide();
                    clearInterval(myVar1);
                }, 3000);
                $("#send").removeProp("disabled");
            }

        },
        error: function () {
            $("#status_out").hide();
            $("#errormsg-alert").html(servererror);
            $("#errormsg-alert").show();
            var myVar1 = setInterval(function () {
                $("#errormsg-alert").hide();
                $("#send").removeProp("disabled");
                $("#preload_overlay_light").hide();
                clearInterval(myVar1);
            }, 3000);
            $("#send").removeProp("disabled");
        }
    });
    return false;
}
