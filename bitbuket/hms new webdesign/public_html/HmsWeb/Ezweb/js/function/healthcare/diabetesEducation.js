//check required
$("input,select,textarea").focus(function () {
    var input_id = $(this).attr("id");
    if ($("#" + input_id).hasClass("input-alert"))
        $("#" + input_id).removeClass("input-alert");
});

//clearall
function clearall() {
    $("#date").val("");
    $("#diabetes_session").val("");
}

// validate diabetes eduction
function diabetes_eduction() {

    var form_data = $("#diabetes_education_program").serialize();
    var diabetes_session = $.trim($("#diabetes_session").val());
    var date = $.trim($("#date").val());
    var type = document.querySelector('input[name = "diabetes_edu"]:checked').value;
    var today = new Date();
    var dd = today.getDate();
    var mm = today.getMonth() + 1; //January is 0!
    var yyyy = today.getFullYear();

    if (dd < 10) {
        dd = '0' + dd;
    }
    if (mm < 10) {
        mm = '0' + mm;
    }

    var today = yyyy + '-' + mm + '-' + dd;

    if (diabetes_session && new Date(date) <= new Date(today) && myFunction(date) === true && isNaN(diabetes_session)) {
        $("#status_out").show();
        $("#submit,#cancel").prop("disabled", true);
        var requesturl = path + "HospitalManagementSystem/HMS/Issues/setdiabitieseduprogram.jsp";

        $.ajax({
            type: "POST",
            url: requesturl,
            data: {
                type: type,
                date: $("#date").val(),
                discription: $("#diabetes_session").val()
            },
            success: function (html) {
                var msg = jQuery.trim(html);
                if (msg === "success") {
                    $("#status_out").hide();
                    $("#successfull-send").html(successfulladddetails);
                    $("#successfull-send").show();
                    var myVar1 = setInterval(function () {
                        $("#successfull-send").hide();
                        clearall();
                        $('#addeducation_model').modal('hide');
                        getpro();
                        $("#submit,#cancel").removeProp("disabled");
                        clearInterval(myVar1);
                    }, 3000);
                } else {
                    $("#status_out").hide();
                    $("#errormsg-alert").html(unsuccessfulladddetails);
                    $("#errormsg-alert").show();
                    var myVar1 = setInterval(function () {
                        $("#errormsg-alert").hide();
                        $("#submit,#cancel").removeProp("disabled");
                        clearInterval(myVar1);
                    }, 3000);
                }
            },
            error: function () {
                $("#status_out").hide();
                $("#errormsg-alert").html(servererror);
                $("#errormsg-alert").show();
                var myVar1 = setInterval(function () {
                    $("#errormsg-alert").hide();
                    $("#submit,#cancel").removeProp("disabled");
                    clearInterval(myVar1);
                }, 3000);
            }
        });
        return false;
    } else {

        if ($.trim($("#diabetes_session").val()) === "" || !isNaN($("#diabetes_session").val())) {
            $("#diabetes_session").addClass("input-alert");
        }

        if (new Date(date) >= new Date(today) || date === "" || date === " ") {
            $("#date").addClass("input-alert");
            $("#errormsg-alert").html(validdate);
            $("#errormsg-alert").show();
            var myVar1 = setInterval(function () {
                $("#errormsg-alert").hide();
                clearInterval(myVar1);
            }, 3000);
        }
    }
}

var obj_pro = "";
var objcount_pro = 0;
var counter_pro = 0;
var proid = 0;

//getprogram
function getpro() {

    $("#preload_overlay_light").show();
    $.ajax({
        url: path + "HospitalManagementSystem/HMS/Issues/getdiabitiesprogram.jsp",
        type: "POST",
        success: function (data) {
            var msgpro = jQuery.trim(data);
            if (msgpro) {
                obj_pro = JSON.parse(msgpro);
                if (obj_pro.program.length > 0) {
                    $("#diabetes_contents").show();
                    objcount_pro = obj_pro.program.length;
                    setpro(obj_pro);
                } else {
                    $("#diabetes_contents").hide();
                    proid = 0;
                }
            }
            $("#preload_overlay_light").hide();
        },
        error: function () {
            $("#diabetes_contents").hide();
            $("#preload_overlay_light").hide();
            $("#status_out_update").hide();
            $("#errormsg-update").html(servererror);
            $("#errormsg-update").show();
            var myVar1 = setInterval(function () {
                $("#errormsg-update").hide();
                clearInterval(myVar1);
            }, 3000);
        }
    });
    return false;
}

$("#pro_backward").on("click", function () {
    counter_pro--;
    if (counter_pro <= 0) {
        setpro(obj_pro);
    }
});

$("#pro_foward").on("click", function () {
    counter_pro++;
    if (counter_pro < objcount_pro) {
        setpro(obj_pro);
    }
});

//setpro
function setpro(obj_pro) {

    if (counter_pro + 1 >= objcount_pro) {
        $("#pro_foward").hide();
    } else {
        $("#pro_foward").show();
    }

    if (counter_pro <= 0) {
        $("#pro_backward").hide();
    } else {
        $("#pro_backward").show();
    }

    proid = obj_pro.program[counter_pro][0];
    $("#date_view").val(obj_pro.program[counter_pro][1]);
    if (obj_pro.program[counter_pro][2] === "G") {
        document.getElementById('group').checked = true;
    } else {

        document.getElementById('individual').checked = true;
    }
    $("#diabetes_session_view").val(obj_pro.program[counter_pro][3]);
}

//diabities upadte
function diabetes_eduction_update() {

    var diabetes_session = $.trim($("#diabetes_session_view").val());
    var date = $.trim($("#date_view").val());
    var type = document.querySelector('input[name = "diabetes_edu_view"]:checked').value;
    var today = new Date();
    var dd = today.getDate();
    var mm = today.getMonth() + 1; //January is 0!
    var yyyy = today.getFullYear();

    if (dd < 10) {
        dd = '0' + dd;
    }
    if (mm < 10) {
        mm = '0' + mm;
    }

    var today = yyyy + '-' + mm + '-' + dd;

    if (diabetes_session && new Date(date) <= new Date(today) && myFunction(date) === true && isNaN(diabetes_session)) {

        $("#status_out_update").show();
        $("#diabetes_update_btn").prop("disabled", true);
        var requesturl = path + "HospitalManagementSystem/HMS/Issues/updatediabitiesprogram.jsp";

        $.ajax({
            type: "POST",
            url: requesturl,
            data: {
                proid: proid,
                type: type,
                date: $("#date_view").val(),
                discription: $("#diabetes_session_view").val()
            },
            success: function (html) {
                var msg = jQuery.trim(html);
                if (msg === "success") {
                    $("#status_out_update").hide();
                    $("#successfull-update").html(successfulladddetails);
                    $("#successfull-update").show();
                    var myVar1 = setInterval(function () {
                        $("#successfull-update").hide();
                        getpro();
                        $("#diabetes_update_btn").removeProp("disabled");
                        clearInterval(myVar1);
                    }, 3000);
                } else {
                    $("#status_out_update").hide();
                    $("#errormsg-update").html(unsuccessfulladddetails);
                    $("#errormsg-update").show();
                    var myVar1 = setInterval(function () {
                        $("#errormsg-update").hide();
                        $("#diabetes_update_btn").removeProp("disabled");
                        clearInterval(myVar1);
                    }, 3000);
                }
            },
            error: function () {
                $("#status_out_update").hide();
                $("#errormsg-update").html(servererror);
                $("#errormsg-update").show();
                var myVar1 = setInterval(function () {
                    $("#errormsg-update").hide();
                    $("#diabetes_update_btn").removeProp("disabled");
                    clearInterval(myVar1);
                }, 3000);
            }
        });
        return false;
    } else {

        if ($.trim($("#diabetes_session_view").val()) === "" || !isNaN($("#diabetes_session_view").val())) {
            $("#diabetes_session_view").addClass("input-alert");
        }

        if (new Date(date) >= new Date(today) || date === "" || date === " ") {
            $("#date_view").addClass("input-alert");
            $("#errormsg-alert").html(validdate);
            $("#errormsg-alert").show();
            var myVar1 = setInterval(function () {
                $("#errormsg-alert").hide();
                clearInterval(myVar1);
            }, 3000);
        }
    }
}