//get getmyproviderLocation
function getmyproviderLocation() {

    navigator.geolocation.getCurrentPosition(function (position) {
        getproviderDetails(position.coords.latitude, position.coords.longitude);

    }, function (error) {
        if (error.code == PositionError.PERMISSION_DENIED) {
            alert("App doesn't have permission to use GPS");
        } else if (error.code == PositionError.POSITION_UNAVAILABLE) {
            alert("No GPS device found");
        } else if (error.code == PositionError.TIMEOUT) {
            alert("Its taking too long find user location");
        } else {
            alert("An unknown error occured");
        }
    }, {
        maximumAge: 300000,
        enableHighAccuracy: true
    });
}

//getproviderDetails
function getproviderDetails(lat, lng) {

    localStorage.setItem("current_lat", lat);
    localStorage.setItem("current_lng", lng);

    /***** Internet check ***/
    if (getConnection() === "true") {

        $("#status_out").show();
        var dataurl = path + "HospitalManagementSystem/HMS/HealthProvider/getprovider.jsp";

        $.ajax({
            type: "POST",
            url: dataurl,
            crossDomain: true,
            cache: false,
            contentType: 'application/json; charset=utf-8',
            data: {},
            success: function (html) {
                var msg = jQuery.trim(html);
                $("#status_out").hide();
                if (msg) {
                    obj = JSON.parse(msg);
                    if (obj.providerDetails) {
                        if (obj.providerDetails.length > 0) {
                            for (i = 0; i < obj.providerDetails.length; i++) {
                                if (obj.providerDetails[i][1] == "h") {
                                    var weburl = obj.providerDetails[i][5];
                                    if (weburl == "nil") {
                                        weburl = "currently not available";
                                    }
                                    $("#table").append("<table class='hospital-list'><tr style='display: none'> <td class='hospital-details'>ID: </td><td  class='hospital-detail-disc'>" + obj.providerDetails[i][0] + "<br></td><tr style='display: none'> <td class='hospital-details'>CenterType: </td><td  class='hospital-detail-disc'>" + obj.providerDetails[i][1] + "<br></td></tr></tr><tr> <td class='hospital-details'>Hospital: </td> <td class='hospital-detail-disc'>" + obj.providerDetails[i][2] + "</td></tr> <tr> <td class='hospital-details'>Address: </td><td  class='hospital-detail-disc'><br>" + obj.providerDetails[i][3] + "<br></td></tr> <tr> <td class='hospital-details'>Phone: </td><td  class='hospital-detail-disc'>" + obj.providerDetails[i][4] + "</td></tr><tr><td class='hospital-details'>Web: </td><td  class='hospital-detail-disc'>" + weburl + "<br></td></tr><tr style='display: none'> <td class='hospital-details'>Type: </td><td  class='hospital-detail-disc'>" + obj.providerDetails[i][6] + "<br></td><tr><td colspan='2' class='pinholder'><img src='../../icons/pinico.png' class='img-responsive pinicon'></td></tr></table><br>");

                                } else if (obj.providerDetails[i][1] == "c") {

                                    var weburl = obj.providerDetails[i][5];
                                    if (weburl == "nil") {
                                        weburl = "currently not available";
                                    }
                                    var set = obj.providerDetails[i][6];
                                    var status;
                                    if (set == "Y") {
                                        status = "Yes";
                                    } else {
                                        status = "No";
                                    }
                                    $("#table").append("<table class='hospital-list'><tr style='display: none'> <td class='hospital-details'>ID: </td><td  class='hospital-detail-disc'>" + obj.providerDetails[i][0] + "<br></td><tr style='display: none'> <td class='hospital-details'>CenterType: </td><td  class='hospital-detail-disc'>" + obj.providerDetails[i][1] + "<br></td></tr></tr><tr> <td class='hospital-details'>Clinic: </td> <td class='hospital-detail-disc'>" + obj.providerDetails[i][2] + "</td></tr> <tr> <td class='hospital-details'>Address: </td><td  class='hospital-detail-disc'><br>" + obj.providerDetails[i][3] + "<br></td></tr> <tr> <td class='hospital-details'>Phone: </td><td  class='hospital-detail-disc'>" + obj.providerDetails[i][4] + "</td></tr><tr><td class='hospital-details'>Web: </td><td  class='hospital-detail-disc'>" + weburl + "<br></td></tr><tr><td class='hospital-details'>Wheel chair: </td><td  class='hospital-detail-disc'>" + status + "<br></td></tr><tr><td class='hospital-details'>Waiting Time: </td><td  class='hospital-detail-disc'>currently not available<br></td></tr></td></tr><tr style='display: none'> <td class='hospital-details'>Type: </td><td  class='hospital-detail-disc'>" + obj.providerDetails[i][7] + "<br></td></tr><tr><td colspan='2' class='pinholder'><img src='../../icons/pinico.png' class='img-responsive pinicon'></td></tr> </table><br>");
                                } else {

                                    var weburl = "currently not available";
                                    var status = "No";
                                    $("#table").append("<table class='hospital-list'><tr style='display: none'> <td class='hospital-details'>ID: </td><td  class='hospital-detail-disc'>" + obj.providerDetails[i][0] + "<br></td><tr style='display: none'> <td class='hospital-details'>CenterType: </td><td  class='hospital-detail-disc'>" + "s" + "<br></td></tr></tr><tr> <td class='hospital-details'>Center: </td> <td class='hospital-detail-disc'>" + obj.providerDetails[i][1] + "</td></tr><tr><td class='hospital-details'>Email: </td><td  class='hospital-detail-disc'>" + obj.providerDetails[i][2] + "</td></tr><tr><td class='hospital-details'><br>Registration: </td><td  class='hospital-detail-disc'>" + "<br>Self Registration" + "<br></td></tr><tr style='display: none'> <td class='hospital-details'>Type: </td><td  class='hospital-detail-disc'>" + obj.providerDetails[i][3] + "<br></td></tr></table><br>");
                                }
                            }

                        } else {
                            $('#alert-warning').html(haventprovider);
                            $('#alert-warning').show();
                        }
                    }
                }
            }, error: function () {
                $("#status_out").hide();
                $('#alert-warning').html(servererror);
                $('#alert-warning').show();
            }
        });
    } else {
        $("#status_out").show();
        setTimeout(function () {
            getmyproviderLocation();
        }, 2000);
    }
}


//goto hospital/clinic details
$(document).on("click", ".hospital-list tr", function (e) {

    var id = $(this).parent().parent().find('td:eq(1)').text();
    var centertype = $(this).parent().parent().find('td:eq(3)').text();
    localStorage.setItem("providerback", "myprovider");

    if (centertype == "h") {
        localStorage.setItem("hosid", id);
        window.location = "selectedHospital.html";
    } else if (centertype == "c") {
        localStorage.setItem("clinicid", id);
        window.location = "selectedClinic.html";
    } else if (centertype == "s") {

        localStorage.setItem("email", $(this).parent().parent().find('td:eq(7)').text());
        localStorage.setItem("hospital-name", $(this).parent().parent().find('td:eq(5)').text());
        localStorage.setItem("web", "");
        localStorage.setItem("tp", "");
        localStorage.setItem("centerid", $(this).parent().parent().find('td:eq(1)').text());
        localStorage.setItem("centertype", "s");
        window.location = "sendEmail.html";
    }
});