//check required
$("input,select,textarea").focus(function () {
    var input_id = $(this).attr("id");
    if ($("#" + input_id).hasClass("input-alert"))
        $("#" + input_id).removeClass("input-alert");
});

//date validate
var datevalidate = 0;
var dateres = true;

//function date check
function datecheck() {

    datevalidate = 0;
    dateres = true;

    if ($("#issue_begin").val()) {
        if (myFunction($("#issue_begin").val()) === false) {
            $("#issue_begin").addClass("input-alert");
            datevalidate++;
        }
    }

    if ($("#issue_end").val()) {
        if (myFunction($("#issue_end").val()) === false) {
            $("#issue_end").addClass("input-alert");
            datevalidate++;
        }
    }

    if (datevalidate !== 0) {
        dateres = false;
    }
    return dateres;
}

//validate medication
function medication() {
    passform = "";
    var required = [];
    fields = $("form#issues_medication").serialize();
    form_array = fields.split('&');

    for (var i = 0; i < form_array.length; i++) {
        name = form_array[i].split('=')[0];
        value = form_array[i].split('=')[1];
        required[i] = value;

        if ($.trim(value) === "") {
            if (name === "issue_end" || name === "discription" || name === "effectiveness" || name === "dosage" || name === "typeofmedication") {
            } else {
                var name_of_input = "#" + name;
                $(name_of_input).addClass("input-alert");
            }
        }
        if (name === "discription" || name === "effectiveness" || name === "dosage" || name === "typeofmedication") {
            var setnil = (value === "") ? "nil" : value;
            passform += name + "=" + setnil + "&";
            setnil = "";
        } else {
            var setnil = (value === "") ? "nil" : value;
            passform += name + "=" + setnil + "&";
            setnil = "";
        }
    }
    count = 0;
    for (var i = 0; i <= required.length; i++) {
        if ($.trim(required[i]) === "") {
            if (i === 2 || i === 3 || i === 4 || i === 5 || i === 6) {
            } else {
                count++;
            }
        }
    }

    if ((count - 1) === 0) {
        if (datecheck()) {
            setmeditations();
        }
    }
}

//setmeditations
function setmeditations() {

    /***** Internet check ***/
    if (getConnection() === "false") {
        return false;
    }

    var beginDate = new Date($("#issue_begin").val());
    var enddateDate = new Date($("#issue_end").val());
    var today = new Date();
    var dd = today.getDate();
    var mm = today.getMonth() + 1; //January is 0!
    var yyyy = today.getFullYear();

    if (dd < 10) {
        dd = '0' + dd;
    }
    if (mm < 10) {
        mm = '0' + mm;
    }

    var today = yyyy + '-' + mm + '-' + dd;
    var datestatus;

    if ($("#issue_end").val() !== "" && $("#issue_end").val() !== " " && $("#issue_end").val() !== null) {
        if (enddateDate <= new Date(today)) {
            if (beginDate <= enddateDate) {
                datestatus = true;
            } else {
                datestatus = false;
            }
        } else {
            datestatus = false;
        }
    } else {
        if (beginDate <= new Date(today)) {
            datestatus = true;
        } else {
            datestatus = false;
        }
    }

    if (datestatus === true) {
        $("#status_out_add").show();
        $("#submit,#cancel").prop("disabled", true);
        $.ajax({
            url: path + "HospitalManagementSystem/HMS/Issues/setmedication.jsp",
            type: "POST",
            data: passform + "issuesid=3",
            success: function (data) {
                var msg = jQuery.trim(data);
                if (msg === "success") {
                    $("#status_out_add").hide();
                    $("#successfull-send").html(successfulladddetails);
                    $("#successfull-send").show();
                    var myVar1 = setInterval(function () {
                        $("#successfull-send").hide();
                        $("#submit,#cancel").removeProp("disabled");
                        clearInterval(myVar1);
                    }, 3000);
                } else {
                    $("#status_out_add").hide();
                    $("#errormsg-alert").html(unsuccessfulladddetails);
                    $("#errormsg-alert").show();
                    var myVar1 = setInterval(function () {
                        $("#errormsg-alert").hide();
                        $("#submit,#cancel").removeProp("disabled");
                        clearInterval(myVar1);
                    }, 3000);
                }
            }, error: function () {
                $("#status_out_add").hide();
                $("#errormsg-alert").html(servererror);
                $("#errormsg-alert").show();
                var myVar1 = setInterval(function () {
                    $("#errormsg-alert").hide();
                    $("#submit,#cancel").removeProp("disabled");
                    clearInterval(myVar1);
                }, 3000);
            }
        });
        return false;
    } else {
        $("#errormsg-alert").html(datecheckerror);
        $("#errormsg-alert").show();
        var myVar1 = setInterval(function () {
            $("#errormsg-alert").hide();
            clearInterval(myVar1);
        }, 3000);
    }
}

$("#issue_title_options").change(function () {
    if ($("#issue_title_options").val() !== null && $("#issue_title_options").val() !== "" && $("#issue_title_options").val() !== " ") {

        getmeditations();
    }
});

// get meditattions
function getmeditations() {

    $("#preload_overlay_light").show();
    $.ajax({
        url: path + "HospitalManagementSystem/HMS/Issues/getmymedication.jsp",
        type: "POST",
        data: {
            "problems": $("#issue_title_options").val(),
            "issuesid": 3
        },
        success: function (data) {
            var msg = jQuery.trim(data);
            if (msg) {
                obj = JSON.parse(msg);
                if (obj.medicalproblems.length > 0) {

                    $("#issue_begin").val(obj.medicalproblems[0][0]);

                    if (obj.medicalproblems[0][1] !== "1000-10-10") {
                        $("#issue_end").val(obj.medicalproblems[0][1]);
                    } else {
                        $("#issue_end").val("");
                    }

                    var whatfor = obj.medicalproblems[0][2] === "nil" ? "" : obj.medicalproblems[0][2];
                    var effectiveness = obj.medicalproblems[0][3] === "nil" ? "" : obj.medicalproblems[0][3];
                    var dosage = obj.medicalproblems[0][4] === "nil" ? "" : obj.medicalproblems[0][4];
                    var type = obj.medicalproblems[0][5] === "nil" ? "" : obj.medicalproblems[0][5];

                    $("#discription").val(whatfor);
                    $("#effectiveness").val(effectiveness);
                    $("#dosage").val(dosage);
                    if (type !== "") {
                        document.getElementById('typeofmedication').value = type;
                    }
                } else {
                    $("#issue_begin").val("");
                    $("#issue_end").val("");
                    $("#discription").val("");
                    $("#effectiveness").val("");
                    $("#dosage").val("");
                    document.getElementById('typeofmedication').value = "";
                    $("#submit span").addClass("glyphicon-ok");
                }
            }
            $("#preload_overlay_light").hide();
        },
        error: function () {
            $("#preload_overlay_light").hide();
            $("#errormsg-alert").html(servererror);
            $("#errormsg-alert").show();
            var myVar1 = setInterval(function () {
                $("#errormsg-alert").hide();
                clearInterval(myVar1);
            }, 3000);
        }
    });
    return false;
}

//addmedicalprob
function addmedicalprob() {

    /***** Internet check ***/
    if (getConnection() === "false") {
        return false;
    }

    var catname = $("#cat_name").val();
    if (catname !== null && catname !== "" && catname !== " " && isNaN(catname)) {
        $("#status_out").show();
        $("#add_meicalprob_cat").prop("disabled", true);
        $.ajax({
            url: path + "HospitalManagementSystem/HMS/Issues/setmedicalissuescat.jsp",
            type: "POST",
            data: {
                catname: catname,
                issuesid: 3
            },
            success: function (data) {
                var msg = jQuery.trim(data);
                if (msg === "success") {
                    getmedicalprob();
                    $("#status_out").hide();
                    $("#successfull-send_prob").html(successfulladddetails);
                    $("#successfull-send_prob").show();
                    document.getElementById("issues_medication").reset();
                    $("#cat_name").val("");
                    var myVar1 = setInterval(function () {
                        $("#successfull-send_prob").hide();
                        $('#add_medicalprocat').modal('hide');
                        $("#submit,#cancel").removeProp("disabled");
                        clearInterval(myVar1);
                    }, 3000);
                } else {
                    $("#status_out").hide();
                    $("#errormsg-alert_prob").html(unsuccessfulladddetails);
                    $("#errormsg-alert_prob").show();
                    var myVar1 = setInterval(function () {
                        $("#errormsg-alert_prob").hide();
                        $("#submit,#cancel").removeProp("disabled");
                        clearInterval(myVar1);
                    }, 3000);
                }
            },
            error: function () {
                $("#status_out").hide();
                $("#errormsg-alert_prob").html(servererror);
                $("#errormsg-alert_prob").show();
                var myVar1 = setInterval(function () {
                    $("#errormsg-alert_prob").hide();
                    $("#submit,#cancel").removeProp("disabled");
                    clearInterval(myVar1);
                }, 3000);
            }
        });
        return false;
    } else {
        $("#cat_name").addClass("input-alert");
    }
}

//getmedicalprob
function getmedicalprob() {

    $('#issue_title_options')[0].options.length = 0;
    $('#issue_title_options').append('<option value="">Select One *</option>');

    $.ajax({
        url: path + "HospitalManagementSystem/HMS/Issues/getmedicalissuescat.jsp",
        type: "POST",
        data: {
            issuesid: 3
        },
        success: function (data) {
            var msg = jQuery.trim(data);
            if (msg) {
                obj = JSON.parse(msg);
                if (obj.medicalproblemscat.length > 0) {
                    for (i = 0; i < obj.medicalproblemscat.length; i++) {
                        $('#issue_title_options').append('<option value="' + obj.medicalproblemscat[i][0] + '">' + obj.medicalproblemscat[i][0] + '</option>');
                    }
                }
            }
        }, error: function () {
            $("#errormsg-alert").html(servererror);
            $("#errormsg-alert").show();
            var myVar1 = setInterval(function () {
                $("#errormsg-alert").hide();
                clearInterval(myVar1);
            }, 3000);
        }
    });
    return false;
}