//check required
$("input").focus(function () {
    input_id = $(this).attr("id");
    if ($("#" + input_id).hasClass("input-alert"))
        $("#" + input_id).removeClass("input-alert");
});

moodtype = "";

//click on mood icon
$(".moodicons").click(function () {
    if ($(".moodicons").hasClass("selected")) {
        var id = $(".selected").attr("id");
        $("#" + id).removeClass("selected");
        $("#" + id).attr("src", "../../icons/moodicons/" + id.toLowerCase() + "_g.png");
        setmoodtype($(this).attr("id"));
    } else {
        setmoodtype($(this).attr("id"));

    }
});

//setmoodtype
function setmoodtype(id) {
    moodtype = $("#" + id).attr("id");
    $("#" + moodtype).addClass("selected");
    var imgname = $("#" + id).attr("src").split("/")[4].split(".png");
    var imgtype = imgname[0].split("_")[1];
    var imgsrc = imgname[0].split("_")[0];
    if (imgtype === "g") {
        $("#" + id).attr("src", "../../icons/moodicons/" + imgsrc + "_c.png");
        mood();
    }
}

//resetmood
function resetmood() {
    var imgname = $("#" + moodtype).attr("src").split("/")[4].split(".png");
    var imgsrc = imgname[0].split("_")[0];
    $("#" + moodtype).attr("src", "../../icons/moodicons/" + imgsrc + "_g.png");
    $(".moodicons").removeClass("selected");
}

//set mood
function mood() {

    var currentdate = new Date();
    mooddate = currentdate.getFullYear() + "-" + (currentdate.getMonth() + 1) + "-" + currentdate.getDate() + " " + currentdate.getHours() + ":" + currentdate.getMinutes() + ":" + currentdate.getSeconds();

    if (mooddate !== "" || mooddate === " ") {
        $("#status_out").show();
        $("#moodsubmit").prop("disabled", true);
        var requesturl = path + "HospitalManagementSystem/HMS/Issues/setmood.jsp";
        $.ajax({
            type: "POST",
            url: requesturl,
            data: {
                date: mooddate,
                mood: moodtype
            },
            success: function (html) {
                var msg = jQuery.trim(html);
                if (msg === "success") {
                    $("#status_out").hide();
                    $("#successfull-send").html(successfulladddetails);
                    $("#successfull-send").show();
                    var myVar1 = setInterval(function () {
                        $("#successfull-send").hide();
                        clearInterval(myVar1);
                        $("#moodsubmit").removeProp("disabled");
                        $("#modeChart_manual .next-2").hide();
                        $("#modeChart_manual .next-1").show();
                        reseticon(moodtype);
                        //removeChart(modeChartid, moodData1, moodData2);
                        moodmanagement(-1);
                    }, 3000);

                } else {
                    $("#status_out").hide();
                    $("#errormsg-alert").html(unsuccessfulladddetails);
                    $("#serrormsg-alert").show();
                    var myVar1 = setInterval(function () {
                        $("#errormsg-alert").hide();
                        resetmood();
                        clearInterval(myVar1);
                    }, 3000);
                }
            }, error: function () {
                $("#status_out").hide();
                $("#errormsg-alert").html(servererror);
                $("#errormsg-alert").show();
                var myVar1 = setInterval(function () {
                    $("#errormsg-alert").hide();
                    resetmood();
                    clearInterval(myVar1);
                }, 3000);
            }
        });
        return false;
    } else {
        $("#mood_date").addClass("input-alert");
    }
}
function reseticon(id) {

    var idsrc = $("#" + id).attr("src").split("_");
    $("#" + id).attr("src", idsrc[0] + "_g.png");
}
