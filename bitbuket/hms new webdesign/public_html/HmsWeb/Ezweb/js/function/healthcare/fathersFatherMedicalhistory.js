// validate  details
$("input,select").focus(function () {
    input_id = $(this).attr("id");
    if ($("#" + input_id).hasClass("input-alert"))
        $("#" + input_id).removeClass("input-alert");
});

//collect data
var father_fatherdata = "";
var father_fatherdatacheck = [];
collectdata("family_father_father");

function collectdata(formname) {
    $("#" + formname).find("input[type=checkbox]").each(function () {
        father_fatherdatacheck.push($(this).attr("id"));
    });
}

// save father_father
function setvaluefather_father() {

    father_fatherdata = "";

    for (var x = 0; x < father_fatherdatacheck.length; x++) {
        var id = father_fatherdatacheck[x];
        var id_check = id.split("_")[2];

        if ($("#" + father_fatherdatacheck[x]).is(":checked")) {
            if (father_fatherdatacheck[x] === "father_father_other") {
                y = id + "=" + $("#father_father_othertxt").val();
            } else {
                y = id + "=" + id_check + "-yes";
            }
        } else {
            y = id + "=" + id_check + "-no";
        }
        father_fatherdata += y + "&";
        y = "";
    }
}

//save father_father
function save_father_father() {

    setvaluefather_father();

    var father_fathername = $("#father_father_name").val();
    var father_fatherhealth = $("#father_father_healthstatus").val();
    var err = 0;
    if (father_fathername === "" || father_fathername === null || father_fathername === " ") {
        err++;
        $("#father_father_name").addClass("input-alert");
    }

    if (father_fatherhealth === "" || father_fatherhealth === null || father_fatherhealth === " ") {
        err++;
        $("#father_father_healthstatus").addClass("input-alert");
    }

    if (err === 0) {

        if ($("#father_father_other").is(":checked") === true) {
            if (!$("#father_father_othertxt").val()) {
                $("#father_father_othertxt").addClass("input-alert");
                return false;
            }

        }

        datasubmit = father_fatherdata + "father_father_name=" + father_fathername + "&father_father_healthstatus=" + father_fatherhealth;
        setfather_father();
    }

}

var father_fatherfamilyhistoryid = 0;

//setfather_father
function setfather_father() {

    //check internet connection
    if (connection === "offline") {
        return false;
    }

    $("#father_father_status_out").show();
    $("#father_father_submit,#father_father_cancel").prop("disabled", true);

    $.ajax({
        url: path + "HospitalManagementSystem/HMS/Issues/setfather-fathermedicalhistory.jsp",
        type: "POST",
        data: datasubmit + "&relationsid=6&relationstatusid=" + father_fatherfamilyhistoryid,
        success: function (data) {
            var msg = jQuery.trim(data);
            if (msg === "success") {
                $("#father_father_status_out").hide();
                $("#successfull-send-fatherfather").html(successfulladddetails);
                $("#successfull-send-fatherfather").show();
                var myVar1 = setInterval(function () {
                    $("#successfull-send-fatherfather").hide();
                    clearInterval(myVar1);
                }, 3000);
                getfather_father();
                $("#father_father").removeClass("in");
                $("#father_father").css("height", "0px");
                $("#father_father_submit,#father_father_cancel").removeProp("disabled");

            } else {
                $("#father_father_status_out").hide();
                $("#errormsg-alert-fatherfather").html(unsuccessfulladddetails);
                $("#errormsg-alert-fatherfather").show();
                var myVar1 = setInterval(function () {
                    $("#errormsg-alert-fatherfather").hide();
                    clearInterval(myVar1);
                }, 3000);
                $("#father_father_submit,#father_father_cancel").removeProp("disabled");
            }
        },
        error: function (data) {
            $("#father_father_status_out").hide();
            $("#errormsg-alert-fatherfather").html(servererror);
            $("#errormsg-alert-fatherfather").show();
            var myVar1 = setInterval(function () {
                $("#errormsg-alert-fatherfather").hide();
                clearInterval(myVar1);
            }, 3000);
            $("#father_father_submit,#father_father_cancel").removeProp("disabled");
        }
    });
    return false;
}


//get father_father
function getfather_father() {

    $("#preload_overlay").show();
    $.ajax({
        url: path + "HospitalManagementSystem/HMS/Issues/getfamilymedicalhistory.jsp",
        type: "POST",
        data: {
            "getrelationid": 6
        },
        success: function (data) {

            var msg_father_father = jQuery.trim(data);
            if (msg_father_father) {

                var obj_father_father = JSON.parse(msg_father_father);
                if (obj_father_father.familymedicalhistory.length > 0) {
                    $("#member2_rank").attr("src", "../../icons/tick.png");
                    father_fatherfamilyhistoryid = obj_father_father.familymedicalhistory[0][0];
                    $("#father_father_name").val(obj_father_father.familymedicalhistory[0][1]);
                    document.getElementById('father_father_healthstatus').value = obj_father_father.familymedicalhistory[0][2];

                    if (obj_father_father.familymedicalhistory[0][3] === "arthritis-yes") {
                        $("#father_father_arthritis").prop("checked", true);
                    } else {
                        $("#father_father_arthritis").prop("checked", false);
                    }

                    if (obj_father_father.familymedicalhistory[0][4] === "cancer-yes") {
                        $("#father_father_cancer").prop("checked", true);
                    } else {
                        $("#father_father_cancer").prop("checked", false);
                    }

                    if (obj_father_father.familymedicalhistory[0][5] === "diabetes-yes") {
                        $("#father_father_diabetes").prop("checked", true);
                    } else {
                        $("#father_father_diabetes").prop("checked", false);
                    }

                    if (obj_father_father.familymedicalhistory[0][6] === "heartCondition-yes") {
                        $("#father_father_heartCondition").prop("checked", true);
                    } else {
                        $("#father_father_heartCondition").prop("checked", false);
                    }

                    if (obj_father_father.familymedicalhistory[0][7] === "lungdisease-yes") {
                        $("#father_father_lungdisease").prop("checked", true);
                    } else {
                        $("#father_father_lungdisease").prop("checked", false);
                    }

                    if (obj_father_father.familymedicalhistory[0][8] === "mentalillness-yes") {
                        $("#father_father_mentalillness").prop("checked", true);
                    } else {
                        $("#father_father_mentalillness").prop("checked", false);
                    }

                    if (obj_father_father.familymedicalhistory[0][9] === "stroke-yes") {
                        $("#father_father_stroke").prop("checked", true);
                    } else {
                        $("#father_father_stroke").prop("checked", false);
                    }

                    if (obj_father_father.familymedicalhistory[0][10] === "other-no") {
                        $("#father_father_other").prop("checked", false);
                    } else {
                        $("#father_father_othertxt").show();
                        $("#father_father_othertxt").val(obj_father_father.familymedicalhistory[0][10]);
                        $("#father_father_other").prop("checked", true);
                    }

                } else {

                    father_fatherfamilyhistoryid = 0;
                }
            }
            $("#preload_overlay").hide();
        },
        error: function () {
            $("#preload_overlay").hide();
            $("#errormsg-alert-fatherfather").html(servererror);
            $("#errormsg-alert-fatherfather").show();
            var myVar1 = setInterval(function () {
                $("#errormsg-alert-fatherfather").hide();
                clearInterval(myVar1);
            }, 3000);
        }
    });
    return false;
}