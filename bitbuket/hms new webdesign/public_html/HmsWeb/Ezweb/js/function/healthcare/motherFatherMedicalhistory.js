// validate  details
$("input,select").focus(function () {
    input_id = $(this).attr("id");
    if ($("#" + input_id).hasClass("input-alert"))
        $("#" + input_id).removeClass("input-alert");
});

//collect data
var mother_fatherdata = "";
var mother_fatherdatacheck = [];
collectdata("family_mother_father");

function collectdata(formname) {
    $("#" + formname).find("input[type=checkbox]").each(function () {
        mother_fatherdatacheck.push($(this).attr("id"));
    });
}
// save mother_father
function setvaluemother_father() {

    mother_fatherdata = "";

    for (var x = 0; x < mother_fatherdatacheck.length; x++) {
        var id = mother_fatherdatacheck[x];
        var id_check = id.split("_")[2];

        if ($("#" + mother_fatherdatacheck[x]).is(":checked")) {
            if (mother_fatherdatacheck[x] === "mother_father_other") {
                y = id + "=" + $("#mother_father_othertxt").val();
            } else {
                y = id + "=" + id_check + "-yes";
            }
        } else {
            y = id + "=" + id_check + "-no";
        }
        mother_fatherdata += y + "&";
        y = "";
    }
}

//save mother_father
function save_mother_father() {

    setvaluemother_father();

    var mother_fathername = $("#mother_father_name").val();
    var mother_fatherhealth = $("#mother_father_healthstatus").val();
    var err = 0;
    if (mother_fathername === "" || mother_fathername === null || mother_fathername === " ") {
        err++;
        $("#mother_father_name").addClass("input-alert");
    }

    if (mother_fatherhealth === "" || mother_fatherhealth === null || mother_fatherhealth === " ") {
        err++;
        $("#mother_father_healthstatus").addClass("input-alert");
    }

    if (err === 0) {

        if ($("#mother_father_other").is(":checked") === true) {
            if (!$("#mother_father_othertxt").val()) {
                $("#mother_father_othertxt").addClass("input-alert");
                return false;
            }

        }

        datasubmit = mother_fatherdata + "mother_father_name=" + mother_fathername + "&mother_father_healthstatus=" + mother_fatherhealth;
        setmother_father();
    }

}

var mother_fatherfamilyhistoryid = 0;

//setmother_father
function setmother_father() {

    //check internet connection
    if (connection === "offline") {
        return false;
    }

    $("#mother_father_status_out").show();
    $("#mother_father_submit,#mother_father_cancel").prop("disabled", true);

    $.ajax({
        url: path + "HospitalManagementSystem/HMS/Issues/setmother-fathermedicalhistory.jsp",
        type: "POST",
        data: datasubmit + "&relationsid=8&relationstatusid=" + mother_fatherfamilyhistoryid,
        success: function (data) {

            var msg = jQuery.trim(data);
            if (msg === "success") {
                $("#mother_father_status_out").hide();
                $("#successfull-send-motherfather").html(successfulladddetails);
                $("#successfull-send-motherfather").show();
                var myVar1 = setInterval(function () {
                    $("#successfull-send-motherfather").hide();
                    clearInterval(myVar1);
                }, 3000);
                getmother_father();
                $("#mother_father").removeClass("in");
                $("#mother_father").css("height", "0px");
                $("#mother_father_submit,#mother_father_cancel").removeProp("disabled");
            } else {
                $("#mother_father_status_out").hide();
                $("#errormsg-alert-motherfather").html(unsuccessfulladddetails);
                $("#errormsg-alert-motherfather").show();
                var myVar1 = setInterval(function () {
                    $("#errormsg-alert-motherfather").hide();
                    clearInterval(myVar1);
                }, 3000);
                $("#mother_father_submit,#mother_father_cancel").removeProp("disabled");
            }
        },
        error: function (data) {
            $("#mother_father_status_out").hide();
            $("#errormsg-alert-motherfather").html(servererror);
            $("#errormsg-alert-motherfather").show();
            var myVar1 = setInterval(function () {
                $("#errormsg-alert-motherfather").hide();
                clearInterval(myVar1);
            }, 3000);
            $("#mother_father_submit,#mother_father_cancel").removeProp("disabled");
        }
    });
    return false;
}


//get mother_father
function getmother_father() {

    $("#preload_overlay").show();
    $.ajax({
        url: path + "HospitalManagementSystem/HMS/Issues/getfamilymedicalhistory.jsp",
        type: "POST",
        data: {
            "getrelationid": 8
        },
        success: function (data) {
            var msg_mother_father = jQuery.trim(data);
            if (msg_mother_father) {
                var obj_mother_father = JSON.parse(msg_mother_father);
                if (obj_mother_father.familymedicalhistory.length > 0) {
                    $("#member5_rank").attr("src", "../../icons/tick.png");
                    mother_fatherfamilyhistoryid = obj_mother_father.familymedicalhistory[0][0];
                    $("#mother_father_name").val(obj_mother_father.familymedicalhistory[0][1]);
                    document.getElementById('mother_father_healthstatus').value = obj_mother_father.familymedicalhistory[0][2];

                    if (obj_mother_father.familymedicalhistory[0][3] === "arthritis-yes") {
                        $("#mother_father_arthritis").prop("checked", true);
                    } else {
                        $("#mother_father_arthritis").prop("checked", false);
                    }

                    if (obj_mother_father.familymedicalhistory[0][4] === "cancer-yes") {
                        $("#mother_father_cancer").prop("checked", true);
                    } else {
                        $("#mother_father_cancer").prop("checked", false);
                    }

                    if (obj_mother_father.familymedicalhistory[0][5] === "diabetes-yes") {
                        $("#mother_father_diabetes").prop("checked", true);
                    } else {
                        $("#mother_father_diabetes").prop("checked", false);
                    }

                    if (obj_mother_father.familymedicalhistory[0][6] === "heartCondition-yes") {
                        $("#mother_father_heartCondition").prop("checked", true);
                    } else {
                        $("#mother_father_heartCondition").prop("checked", false);
                    }

                    if (obj_mother_father.familymedicalhistory[0][7] === "lungdisease-yes") {
                        $("#mother_father_lungdisease").prop("checked", true);
                    } else {
                        $("#mother_father_lungdisease").prop("checked", false);
                    }

                    if (obj_mother_father.familymedicalhistory[0][8] === "mentalillness-yes") {
                        $("#mother_father_mentalillness").prop("checked", true);
                    } else {
                        $("#mother_father_mentalillness").prop("checked", false);
                    }

                    if (obj_mother_father.familymedicalhistory[0][9] === "stroke-yes") {
                        $("#mother_father_stroke").prop("checked", true);
                    } else {
                        $("#mother_father_stroke").prop("checked", false);
                    }

                    if (obj_mother_father.familymedicalhistory[0][10] === "other-no") {
                        $("#mother_father_other").prop("checked", false);
                    } else {
                        $("#mother_father_othertxt").show();
                        $("#mother_father_othertxt").val(obj_mother_father.familymedicalhistory[0][10]);
                        $("#mother_father_other").prop("checked", true);
                    }

                } else {
                    mother_fatherfamilyhistoryid = 0;
                }
            }
            $("#preload_overlay").hide();
        },
        error: function () {
            $("#preload_overlay").hide();
            $("#errormsg-alert-motherfather").html(servererror);
            $("#errormsg-alert-motherfather").show();
            var myVar1 = setInterval(function () {
                $("#errormsg-alert-motherfather").hide();
                clearInterval(myVar1);
            }, 3000);
        }
    });
    return false;
}