var obj_bp = "";
var objcount_bp = 0;
var counter_bp = 0;
var proidbp = 0;

//gethdlc
function getbp() {

    $.ajax({
        url: path + "HospitalManagementSystem/HMS/Issues/gettraget.jsp",
        type: "POST",
        data: {
            tragetid: 6
        },
        success: function (data) {
            var msgpro = jQuery.trim(data);
            if (msgpro) {
                obj_bp = JSON.parse(msgpro);
                if (obj_bp.diatraget.length > 0) {
                    $("#targetlevel-6").show();
                    objcount_bp = obj_bp.diatraget.length;
                    setbp(obj_bp);
                } else {
                    proidbp = 0;
                    $("#targetlevel-6").hide();
                }
            }
            counter();
        },
        error: function () {
            $("#targetlevel-6").hide();
            counter();
            $("#errormsg-alert_targetbpmodify").html(servererror);
            $("#errormsg-alert_targetbpmodify").show();
            var myVar1 = setInterval(function () {
                $("#errormsg-alert_targetbpmodify").hide();
                clearInterval(myVar1);
            }, 3000);
        }
    });
    return false;
}

$("#targetbp_backnav").on("click", function () {
    counter_bp--;
    if (counter_bp <= 0) {
        setbp(obj_bp);
    }
});

$("#targetbp_forwardnav").on("click", function () {
    counter_bp++;
    if (counter_bp < objcount_bp) {
        setbp(obj_bp);
    }
});

$("#targetbp_modify").on("click", function () {
    updatetragetbp();

});

//setldlc
function setbp(obj_bp) {

    if (counter_bp + 1 >= objcount_bp) {
        $("#targetbp_forwardnav").hide();
    } else {
        $("#targetbp_forwardnav").show();
    }

    if (counter_bp <= 0) {
        $("#targetbp_backnav").hide();
    } else {
        $("#targetbp_backnav").show();
    }

    proidbp = obj_bp.diatraget[counter_bp][0];
    $("#targetbp_date_view").val(obj_bp.diatraget[counter_bp][1]);

    if (obj_bp.diatraget[counter_bp][2] !== "null") {
        $("#targetbp_mygoal_view").val(obj_bp.diatraget[counter_bp][2]);
    } else {
        $("#targetbp_mygoal_view").val("");
    }
}

//update alc
function updatetragetbp() {

    var date = $("#targetbp_date_view").val();
    var addgoal = $("#targetbp_mygoal_view").val();
    if (date && addgoal && isNaN(addgoal)) {
        $("#status_out_targetbp_view").show();
        $("#targetbp_modify").prop("disabled", true);
        $.ajax({
            url: path + "HospitalManagementSystem/HMS/Issues/updatediabitiestraget.jsp",
            type: "POST",
            data: {
                date: date,
                tragetid: proidbp,
                goal: addgoal
            },
            success: function (data) {
                var msg = jQuery.trim(data);
                if (msg === "success") {
                    $("#status_out_targetbp_view").hide();
                    $("#successfull-send_targetbpmodify").html(successfulladddetails);
                    $("#successfull-send_targetbpmodify").show();
                    var myVar1 = setInterval(function () {
                        $("#successfull-send_targetbpmodify").hide();
                        getbp();
                        $("#targetbp_modify").removeProp("disabled");
                        clearInterval(myVar1);
                    }, 3000);
                } else {
                    $("#status_out_targetbp_view").hide();
                    $("#errormsg-alert_targetbpmodify").html(unsuccessfulladddetails);
                    $("#errormsg-alert_targetbpmodify").show();
                    var myVar1 = setInterval(function () {
                        $("#errormsg-alert_targetbpmodify").hide();
                        $("#targetbp_modify").removeProp("disabled");
                        clearInterval(myVar1);
                    }, 3000);
                }
            },
            error: function () {
                $("#status_out_targetbp_view").hide();
                $("#errormsg-alert_targetbpmodify").html(servererror);
                $("#errormsg-alert_targetbpmodify").show();
                var myVar1 = setInterval(function () {
                    $("#errormsg-alert_targetbpmodify").hide();
                    $("#targetbp_modify").removeProp("disabled");
                    clearInterval(myVar1);
                }, 3000);
            }
        });
        return false;
    } else {
        if (date === "" || date === " ") {
            $("#targetbp_date_view").addClass("input-alert");
        }
        if (addgoal === "" || addgoal === " " || !isNaN(addgoal)) {
            $("#targetbp_mygoal_view").addClass("input-alert");
        }
    }
}
