// validate  details
$("input,select").focus(function () {
    input_id = $(this).attr("id");
    if ($("#" + input_id).hasClass("input-alert"))
        $("#" + input_id).removeClass("input-alert");
});

//collect data
var father_motherdata = "";
var father_motherdatacheck = [];
collectdata("family_father_mother");

function collectdata(formname) {
    $("#" + formname).find("input[type=checkbox]").each(function () {
        father_motherdatacheck.push($(this).attr("id"));
    });
}

// save father_mother
function setvaluefather_mother() {

    father_motherdata = "";

    for (var x = 0; x < father_motherdatacheck.length; x++) {
        var id = father_motherdatacheck[x];
        var id_check = id.split("_")[2];

        if ($("#" + father_motherdatacheck[x]).is(":checked")) {
            if (father_motherdatacheck[x] === "father_mother_other") {
                y = id + "=" + $("#father_mother_othertxt").val();
            } else {
                y = id + "=" + id_check + "-yes";
            }
        } else {
            y = id + "=" + id_check + "-no";
        }
        father_motherdata += y + "&";
        y = "";
    }
}

//save father_mother
function save_father_mother() {

    setvaluefather_mother();

    var father_mothername = $("#father_mother_name").val();
    var father_motherhealth = $("#father_mother_healthstatus").val();
    var err = 0;
    if (father_mothername === "" || father_mothername === null || father_mothername === " ") {
        err++;
        $("#father_mother_name").addClass("input-alert");
    }

    if (father_motherhealth === "" || father_motherhealth === null || father_motherhealth === " ") {
        err++;
        $("#father_mother_healthstatus").addClass("input-alert");
    }

    if (err === 0) {

        if ($("#father_mother_other").is(":checked") === true) {
            if (!$("#father_mother_othertxt").val()) {
                $("#father_mother_othertxt").addClass("input-alert");
                return false;
            }

        }

        datasubmit = father_motherdata + "father_mother_name=" + father_mothername + "&father_mother_healthstatus=" + father_motherhealth;
        setfather_mother();
    }

}

var father_motherfamilyhistoryid = 0;

//setfather_mother
function setfather_mother() {

    //check internet connection
    if (connection === "offline") {
        return false;
    }

    $("#father_mother_status_out").show();
    $("#father_mother_submit,#father_mother_cancel").prop("disabled", true);

    $.ajax({
        url: path + "HospitalManagementSystem/HMS/Issues/setfather-mothermedicalhistory.jsp",
        type: "POST",
        data: datasubmit + "&relationsid=7&relationstatusid=" + father_motherfamilyhistoryid,
        success: function (data) {
            var msg = jQuery.trim(data);
            if (msg === "success") {

                $("#father_mother_status_out").hide();
                $("#successfull-send-fathermother").html(successfulladddetails);
                $("#successfull-send-fathermother").show();
                var myVar1 = setInterval(function () {
                    $("#successfull-send-fathermother").hide();
                    clearInterval(myVar1);
                }, 3000);
                getfather_mother();
                $("#father_mother").removeClass("in");
                $("#father_mother").css("height", "0px");
                $("#father_mother_submit,#father_mother_cancel").removeProp("disabled");

            } else {
                $("#father_mother_status_out").hide();
                $("#errormsg-alert-fathermother").html(unsuccessfulladddetails);
                $("#errormsg-alert-fathermother").show();
                var myVar1 = setInterval(function () {
                    $("#errormsg-alert-fathermother").hide();
                    clearInterval(myVar1);
                }, 3000);
                $("#father_mother_submit,#father_mother_cancel").removeProp("disabled");
            }
        },
        error: function (data) {

            $("#father_mother_status_out").hide();
            $("#errormsg-alert-fathermother").html(servererror);
            $("#errormsg-alert-fathermother").show();
            var myVar1 = setInterval(function () {
                $("#errormsg-alert-fathermother").hide();
                clearInterval(myVar1);
            }, 3000);
            $("#father_mother_submit,#father_mother_cancel").removeProp("disabled");
        }
    });
    return false;
}

//get father_mother
function getfather_mother() {

    $("#preload_overlay").show();
    $.ajax({
        url: path + "HospitalManagementSystem/HMS/Issues/getfamilymedicalhistory.jsp",
        type: "POST",
        data: {
            "getrelationid": 7
        },
        success: function (data) {
            var msg_father_mother = jQuery.trim(data);
            if (msg_father_mother) {
                var obj_father_mother = JSON.parse(msg_father_mother);
                if (obj_father_mother.familymedicalhistory.length > 0) {
                    $("#member3_rank").attr("src", "../../icons/tick.png");
                    father_motherfamilyhistoryid = obj_father_mother.familymedicalhistory[0][0];
                    $("#father_mother_name").val(obj_father_mother.familymedicalhistory[0][1]);
                    document.getElementById('father_mother_healthstatus').value = obj_father_mother.familymedicalhistory[0][2];

                    if (obj_father_mother.familymedicalhistory[0][3] === "arthritis-yes") {
                        $("#father_mother_arthritis").prop("checked", true);
                    } else {
                        $("#father_mother_arthritis").prop("checked", false);
                    }

                    if (obj_father_mother.familymedicalhistory[0][4] === "cancer-yes") {
                        $("#father_mother_cancer").prop("checked", true);
                    } else {
                        $("#father_mother_cancer").prop("checked", false);
                    }

                    if (obj_father_mother.familymedicalhistory[0][5] === "diabetes-yes") {
                        $("#father_mother_diabetes").prop("checked", true);
                    } else {
                        $("#father_mother_diabetes").prop("checked", false);
                    }

                    if (obj_father_mother.familymedicalhistory[0][6] === "heartCondition-yes") {
                        $("#father_mother_heartCondition").prop("checked", true);
                    } else {
                        $("#father_mother_heartCondition").prop("checked", false);
                    }

                    if (obj_father_mother.familymedicalhistory[0][7] === "lungdisease-yes") {
                        $("#father_mother_lungdisease").prop("checked", true);
                    } else {
                        $("#father_mother_lungdisease").prop("checked", false);
                    }

                    if (obj_father_mother.familymedicalhistory[0][8] === "mentalillness-yes") {
                        $("#father_mother_mentalillness").prop("checked", true);
                    } else {
                        $("#father_mother_mentalillness").prop("checked", false);
                    }

                    if (obj_father_mother.familymedicalhistory[0][9] === "stroke-yes") {
                        $("#father_mother_stroke").prop("checked", true);
                    } else {
                        $("#father_mother_stroke").prop("checked", false);
                    }

                    if (obj_father_mother.familymedicalhistory[0][10] === "other-no") {
                        $("#father_mother_other").prop("checked", false);
                    } else {
                        $("#father_mother_othertxt").show();
                        $("#father_mother_othertxt").val(obj_father_mother.familymedicalhistory[0][10]);
                        $("#father_mother_other").prop("checked", true);
                    }

                } else {

                    father_motherfamilyhistoryid = 0;
                }
            }
            $("#preload_overlay").hide();
        },
        error: function () {
            $("#preload_overlay").hide();
            $("#errormsg-alert-fathermother").html(servererror);
            $("#errormsg-alert-fathermother").show();
            var myVar1 = setInterval(function () {
                $("#errormsg-alert-fathermother").hide();
                clearInterval(myVar1);
            }, 3000);
        }
    });
    return false;
}
