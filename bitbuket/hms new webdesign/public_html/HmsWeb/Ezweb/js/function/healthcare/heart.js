//check required
$("input,select").focus(function () {
    $input_id = $(this).attr("id");
    if ($("#" + $input_id).hasClass("input-alert"))
        $("#" + $input_id).removeClass("input-alert");
});

//function date check
function datecheck() {

    datevalidate = 0;
    dateres = true;
    if ($("#heart_date").val()) {
        if (dateandtime($("#heart_date").val()) === false) {
            $("#heart_date").addClass("input-alert");
            datevalidate++;
        }
    }
    if (datevalidate !== 0) {
        dateres = false;
    }
    return dateres;
}

//validate heart
function heart() {
    var heartrate = $("#heart_rate").val();
    var heartdate = $("#heart_date").val();

    if (heartrate && heartdate) {

        if (dateandtime($("#heart_date").val())) {
            setheart();
        } else {
            $("#heart_date").addClass("input-alert");
            $("#errormsg-alert-heart").html(dateerror);
            $("#errormsg-alert-heart").show();
            var myVar1 = setInterval(function () {
                $("#errormsg-alert-heart").hide();
                clearInterval(myVar1);
            }, 3000);
        }
    } else {
        if (!heartrate) {
            $("#heart_rate").addClass("input-alert");
        }
        if (!heartdate) {
            $("#heart_date").addClass("input-alert");
        }

    }
}

//set hart
function setheart() {

    /**** check internetconnection ******/
    if (getConnection() === "false") {
        return false;
    }

    var heart_date = new Date($("#heart_date").val().split(" ")[0]);
    var today = new Date();
    var dd = today.getDate();
    var mm = today.getMonth() + 1; //January is 0!
    var yyyy = today.getFullYear();

    if (dd < 10) {
        dd = '0' + dd;
    }
    if (mm < 10) {
        mm = '0' + mm;
    }

    var today = yyyy + '-' + mm + '-' + dd;
    var datestatus;

    if ($("#heart_date").val() !== "" && $("#heart_date").val() !== " " && $("#heart_date").val() !== null) {
        if (heart_date <= new Date(today)) {
            datestatus = true;
        } else {
            datestatus = false;
        }
    } else {
        datestatus = false;
    }

    if (datestatus === true) {

        $("#status_out_add_heart").show();
        $("#submit-heart,#cancel-heart").prop("disabled", true);

        var date = $.trim($("#heart_date").val().split(" ")[0]);
        var time = $.trim($("#heart_date").val().split(" ")[1]);
        var rate = $("#heart_rate").val();

        $.ajax({
            url: path + "HospitalManagementSystem/HMS/Issues/setmanualheartrate.jsp",
            type: "POST",
            data: {
                date: date,
                time: time,
                rate: rate
            },
            success: function (data) {
                var msg = jQuery.trim(data);
                if (msg === "success") {
                    $("#status_out_add_heart").hide();
                    $("#successfull-send-heart").html(successfulladddetails);
                    $("#successfull-send-heart").show();
                    var myVar1 = setInterval(function () {
                        $("#successfull-send-heart").hide();
                        $("#heartrate_manual .next-2").hide();
                        $("#heartrate_manual .next-1").show();
                        $("#heart_rate").val("");
                        removeChart(chartHeart, heartrate1, heartrate2, date1, date2);
                        getheartrate(-1);
                        $("#" + dates[2]).val(today + " " + time);//reset date
                        $("#submit-heart,#cancel-heart").removeProp("disabled");
                        clearInterval(myVar1);
                    }, 3000);

                } else {
                    $("#status_out_add_heart").hide();
                    $("#errormsg-alert-heart").html(unsuccessfulladddetails);
                    $("#errormsg-alert-heart").show();
                    var myVar1 = setInterval(function () {
                        $("#errormsg-alert-heart").hide();
                        $("#submit-heart,#cancel-heart").removeProp("disabled");
                        clearInterval(myVar1);
                    }, 3000);
                }
            }, error: function () {
                $("#status_out_add_heart").hide();
                $("#errormsg-alert-heart").html(servererror);
                $("#errormsg-alert-heart").show();
                var myVar1 = setInterval(function () {
                    $("#errormsg-alert-heart").hide();
                    $("#submit-heart,#cancel-heart").removeProp("disabled");
                    clearInterval(myVar1);
                }, 3000);
            }
        });
        return false;
    } else {
        $("#heart_date").addClass("input-alert");
        $("#errormsg-alert-heart").html(datecheckerror);
        $("#errormsg-alert-heart").show();
        var myVar1 = setInterval(function () {
            $("#errormsg-alert-heart").hide();
            clearInterval(myVar1);
        }, 3000);
    }
}

//fill
function fill_select(elem, arr) {
    for (i = 0; i < arr.length; i++) {
        $("#" + elem).append('<option value="' + arr[i] + '">' + arr[i] + '</option>');
    }
}
