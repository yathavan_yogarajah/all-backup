var obj_a1c = "";
var objcount_a1c = 0;
var counter_a1c = 0;
var proida1c = 0;

//geta1c
function geta1c() {

    $.ajax({
        url: path + "HospitalManagementSystem/HMS/Issues/gettraget.jsp",
        type: "POST",
        data: {
            tragetid: 1
        },
        success: function (data) {
            var msgpro = jQuery.trim(data);
            if (msgpro) {
                obj_a1c = JSON.parse(msgpro);
                if (obj_a1c.diatraget.length > 0) {
                    $("#targetlevel-1").show();
                    objcount_a1c = obj_a1c.diatraget.length;
                    seta1c(obj_a1c);
                } else {
                    $("#targetlevel-1").hide();
                    proida1c = 0;
                }
            }
            counter();
        },
        error: function () {
            $("#targetlevel-1").hide();
            counter();
            $("#errormsg-alert_targetA1cmodify").html(servererror);
            $("#errormsg-alert_targetA1cmodify").show();
            var myVar1 = setInterval(function () {
                $("#errormsg-alert_targetA1cmodify").hide();
                clearInterval(myVar1);
            }, 3000);
        }
    });
    return false;
}

$("#targetA1c_backnav").on("click", function () {
    counter_a1c--;
    if (counter_a1c <= 0) {
        seta1c(obj_a1c);
    }
});

$("#targetA1c_forwardnav").on("click", function () {
    counter_a1c++;
    if (counter_a1c < objcount_a1c) {
        seta1c(obj_a1c);
    }
});

//setpro
function seta1c(obj_a1c) {

    if (counter_a1c + 1 >= objcount_a1c) {
        $("#targetA1c_forwardnav").hide();
    } else {
        $("#targetA1c_forwardnav").show();
    }

    if (counter_a1c <= 0) {
        $("#targetA1c_backnav").hide();
    } else {
        $("#targetA1c_backnav").show();
    }

    proida1c = obj_a1c.diatraget[counter_a1c][0];
    $("#targetA1c_date_view").val(obj_a1c.diatraget[counter_a1c][1]);

    if (obj_a1c.diatraget[counter_a1c][2] !== "null") {
        $("#targetA1c_mygoal_view").val(obj_a1c.diatraget[counter_a1c][2]);
    } else {
        $("#targetA1c_mygoal_view").val("");
    }
}

$("#targetA1c_modify").on("click", function () {
    updatetragetalc();
});

//update alc
function updatetragetalc() {

    var date = $("#targetA1c_date_view").val();
    var addgoal = $("#targetA1c_mygoal_view").val();

    if (date && addgoal && isNaN(addgoal)) {
        $("#status_out_targetA1c_view").show();
        $("#targetA1c_modify").prop("disabled", true);
        $.ajax({
            url: path + "HospitalManagementSystem/HMS/Issues/updatediabitiestraget.jsp",
            type: "POST",
            data: {
                date: date,
                tragetid: proida1c,
                goal: addgoal
            },
            success: function (data) {
                var msg = jQuery.trim(data);
                if (msg === "success") {
                    $("#status_out_targetA1c_view").hide();
                    $("#successfull-send_targetA1cmodify").html(successfulladddetails);
                    $("#successfull-send_targetA1cmodify").show();
                    var myVar1 = setInterval(function () {
                        $("#successfull-send_targetA1cmodify").hide();
                        geta1c();
                        $("#targetA1c_modify").removeProp("disabled");
                        clearInterval(myVar1);
                    }, 3000);
                } else {
                    $("#status_out_targetA1c_view").hide();
                    $("#errormsg-alert_targetA1cmodify").html(unsuccessfulladddetails);
                    $("#errormsg-alert_targetA1cmodify").show();
                    var myVar1 = setInterval(function () {
                        $("#errormsg-alert_targetA1cmodify").hide();
                        $("#targetA1c_modify").removeProp("disabled");
                        clearInterval(myVar1);
                    }, 3000);
                }
            },
            error: function () {
                $("#status_out_targetA1c_view").hide();
                $("#errormsg-alert_targetA1cmodify").html(servererror);
                $("#errormsg-alert_targetA1cmodify").show();
                var myVar1 = setInterval(function () {
                    $("#errormsg-alert_targetA1cmodify").hide();
                    $("#targetA1c_modify").removeProp("disabled");
                    clearInterval(myVar1);
                }, 3000);
            }
        });
        return false;
    } else {
        if (date === "" || date === " ") {
            $("#targetA1c_date_view").addClass("input-alert");
        }
        if (addgoal === "" || addgoal === " " || !isNaN(addgoal)) {
            $("#targetA1c_mygoal_view").addClass("input-alert");
        }
    }
}
