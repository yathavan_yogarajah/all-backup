//on change
$("#confidence").change(function () {
    $("#diabetes_confidence").html($(this).val());
});

//check required
$("input,select,textarea").focus(function () {
    input_id = $(this).attr("id");
    if ($("#" + input_id).hasClass("input-alert"))
        ;
    $("#" + input_id).removeClass("input-alert");
});

var other_fiedls = ["mygoaltxt", "difficult_goal", "over_difficult_goal"];

//checkisnan
function checkisnan(passed) {
    var check = 0;
    for (var x = 0; x < passed.length; x++) {

        if ($.trim($("#" + passed[x]).val())) {

            if (!isNaN($.trim($("#" + passed[x]).val()))) {
                $("#" + passed[x]).addClass("input-alert");
                check++;
            }
        }
    }
    return check;
}

// validate mygoal
function mygoal() {

    var required = [];
    $fields = $("form#mygoal_form").serialize();
    form_array = $fields.split('&');

    for (var i = 0; i < form_array.length; i++) {
        name = form_array[i].split('=')[0];
        value = form_array[i].split('=')[1];
        required[i] = value;

        if (name === "difficult_goal" || name === "over_difficult_goal") {
        } else {
            if ($.trim(value) === "") {
                var name_of_input = "#" + name;
                $(name_of_input).addClass("input-alert");
            }
        }
    }

    error = 0;

    for ($i = 0; $i <= 1; $i++) {
        if ($.trim(required[$i]) === "") {
            error++;
        }
    }

    if (error === 0) {
        diabetes_goal();
    }
}

//diabetes goal
function diabetes_goal() {

    var difficulties;
    var overcomedif;

    if ($("#difficult_goal").val() !== "" || $("#difficult_goal").val() !== " " || $("#difficult_goal").val() !== null) {
        difficulties = $("#difficult_goal").val();
    } else {
        difficulties = "nil";
    }

    if ($("#over_difficult_goal").val() !== "" || $("#over_difficult_goal").val() !== " " || $("#over_difficult_goal").val() !== null) {
        overcomedif = $("#over_difficult_goal").val();
    } else {
        overcomedif = "nil";
    }

    if (checkisnan(other_fiedls) !== 0) {
        return false;
    }

    $("#status_out").show();
    $("#submit,#cancel").prop("disabled", true);
    var requesturl = path + "HospitalManagementSystem/HMS/Issues/setmygoal.jsp";
    $.ajax({
        type: "POST",
        url: requesturl,
        data: {
            date: $("#target_date").val(),
            difficulties: difficulties,
            overdifficulties: overcomedif,
            myplan: $("#mygoaltxt").val(),
            meetmygoal: $("#confidence").val()
        },
        success: function (html) {
            var msg = jQuery.trim(html);
            if (msg === "success") {
                $("#status_out").hide();
                $("#successfull-send").html(successfulladddetails);
                $("#successfull-send").show();
                var myVar1 = setInterval(function () {
                    $("#successfull-send").hide();
                    clearInterval(myVar1);
                    document.getElementById("mygoal_form").reset();
                    $("#submit,#cancel").removeProp("disabled");
                    //  window.location = "myGoal.html";
                }, 3000);

            } else {
                $("#status_out").hide();
                $("#errormsg-alert").html(unsuccessfulladddetails);
                $("#errormsg-alert").show();
                var myVar1 = setInterval(function () {
                    $("#errormsg-alert").hide();
                    $("#submit,#cancel").removeProp("disabled");
                    clearInterval(myVar1);
                }, 3000);
            }
        },
        error: function () {
            $("#status_out").hide();
            $("#errormsg-alert").html(servererror);
            $("#errormsg-alert").show();
            var myVar1 = setInterval(function () {
                $("#errormsg-alert").hide();
                $("#submit,#cancel").removeProp("disabled");
                clearInterval(myVar1);
            }, 3000);
        }
    });
    return false;
}
