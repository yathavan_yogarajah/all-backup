var obj_egfr = "";
var objcount_egfr = 0;
var counter_egfr = 0;
var proidegfr = 0;

//gethdlc
function getegfr() {

    $.ajax({
        url: path + "HospitalManagementSystem/HMS/Issues/gettraget.jsp",
        type: "POST",
        data: {
            tragetid: 5
        },
        success: function (data) {
            var msgpro = jQuery.trim(data);
            if (msgpro) {
                obj_egfr = JSON.parse(msgpro);
                if (obj_egfr.diatraget.length > 0) {
                    $("#targetlevel-5").show();
                    objcount_egfr = obj_egfr.diatraget.length;
                    setegfr(obj_egfr);
                } else {
                    proidegfr = 0;
                    $("#targetlevel-5").hide();
                }
            }
            counter();
        },
        error: function () {
            counter();
            $("#targetlevel-5").hide();
            $("#errormsg-alert_targetegfrmodify").html(servererror);
            $("#errormsg-alert_targetegfrmodify").show();
            var myVar1 = setInterval(function () {
                $("#errormsg-alert_targetegfrmodify").hide();
                clearInterval(myVar1);
            }, 3000);
        }
    });
    return false;
}

$("#targetegfr_backnav").on("click", function () {
    counter_egfr--;
    if (counter_egfr <= 0) {
        setegfr(obj_egfr);
    }
});

$("#targetegfr_forwardnav").on("click", function () {
    counter_egfr++;
    if (counter_egfr < objcount_egfr) {
        setegfr(obj_egfr);
    }
});

$("#targetegfr_modify").on("click", function () {
    updatetragetegfr();
});

//setldlc
function setegfr(obj_egfr) {

    if (counter_egfr + 1 >= objcount_egfr) {
        $("#targetegfr_forwardnav").hide();
    } else {
        $("#targetegfr_forwardnav").show();
    }

    if (counter_egfr <= 0) {
        $("#targetegfr_backnav").hide();
    } else {
        $("#targetegfr_backnav").show();
    }

    proidegfr = obj_egfr.diatraget[counter_egfr][0];
    $("#targetegfr_date_view").val(obj_egfr.diatraget[counter_egfr][1]);

    if (obj_egfr.diatraget[counter_egfr][2] !== "null") {
        $("#targetegfr_mygoal_view").val(obj_egfr.diatraget[counter_egfr][2]);
    } else {
        $("#targetegfr_mygoal_view").val("");
    }

}

//update alc
function updatetragetegfr() {

    var date = $("#targetegfr_date_view").val();
    var addgoal = $("#targetegfr_mygoal_view").val();
    if (date && addgoal && isNaN(addgoal)) {
        $("#status_out_targetegfr_view").show();
        $("#targetegfr_modify").prop("disabled", true);
        $.ajax({
            url: path + "HospitalManagementSystem/HMS/Issues/updatediabitiestraget.jsp",
            type: "POST",
            data: {
                date: date,
                tragetid: proidegfr,
                goal: addgoal
            },
            success: function (data) {
                var msg = jQuery.trim(data);
                if (msg === "success") {
                    $("#status_out_targetegfr_view").hide();
                    $("#successfull-send_targetegfrmodify").html(successfulladddetails);
                    $("#successfull-send_targetegfrmodify").show();
                    var myVar1 = setInterval(function () {
                        $("#successfull-send_targetegfrmodify").hide();
                        getegfr();
                        clearInterval(myVar1);
                        $("#targetegfr_modify").removeProp("disabled");
                    }, 3000);
                } else {
                    $("#status_out_targetegfr_view").hide();
                    $("#errormsg-alert_targetegfrmodify").html(unsuccessfulladddetails);
                    $("#errormsg-alert_targetegfrmodify").show();
                    var myVar1 = setInterval(function () {
                        $("#errormsg-alert_targetegfrmodify").hide();
                        $("#targetegfr_modify").removeProp("disabled");
                        clearInterval(myVar1);
                    }, 3000);
                }
            },
            error: function () {
                $("#status_out_targetegfr_view").hide();
                $("#errormsg-alert_targetegfrmodify").html(servererror);
                $("#errormsg-alert_targetegfrmodify").show();
                var myVar1 = setInterval(function () {
                    $("#errormsg-alert_targetegfrmodify").hide();
                    $("#targetegfr_modify").removeProp("disabled");
                    clearInterval(myVar1);
                }, 3000);
            }
        });
        return false;
    } else {
        if (date === "" || date === " ") {
            $("#targetegfr_date_view").addClass("input-alert");
        }
        if (addgoal === "" || addgoal === " " || !isNaN(addgoal)) {
            $("#targetegfr_mygoal_view").addClass("input-alert");
        }
    }
}
