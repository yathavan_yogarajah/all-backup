var genraldata = [];
var skindata = [];
var headdata = [];
var earsdata = [];
var eyesdata = [];
var nosedata = [];
var throatdata = [];
var neckdata = [];
var breastsdata = [];
var respiratorydata = [];
var cardiovasculardata = [];
var gastrointestinaldata = [];
var urinarydata = [];
var vasculardata = [];
var musculoskeletaldata = [];
var neurologicdata = [];
var hematologicdata = [];
var endocrinedata = [];
var psychiatricdata = [];

//addros
function addros(callback) {

    genraldata.length = 0;
    var genral = document.querySelectorAll('#general input[type="checkbox"]:checked');
    for (var i = 0; i < genral.length; i++) {
        genraldata[i] = genral[i].id;
    }

    skindata.length = 0;
    var skin = document.querySelectorAll('#skin input[type="checkbox"]:checked');
    for (var i = 0; i < skin.length; i++) {
        skindata[i] = skin[i].id;
    }

    headdata.length = 0;
    var head = document.querySelectorAll('#head input[type="checkbox"]:checked');
    for (var i = 0; i < head.length; i++) {
        headdata[i] = head[i].id;
    }

    earsdata.length = 0;
    var ears = document.querySelectorAll('#ears input[type="checkbox"]:checked');
    for (var i = 0; i < ears.length; i++) {
        earsdata[i] = ears[i].id;
    }

    eyesdata.length = 0;
    var eyes = document.querySelectorAll('#eyes input[type="checkbox"]:checked');
    for (var i = 0; i < eyes.length; i++) {
        eyesdata[i] = eyes[i].id;
    }

    nosedata.length = 0;
    var nose = document.querySelectorAll('#nose input[type="checkbox"]:checked');
    for (var i = 0; i < nose.length; i++) {
        nosedata[i] = nose[i].id;
    }

    throatdata.length = 0;
    var throat = document.querySelectorAll('#throats input[type="checkbox"]:checked');
    for (var i = 0; i < throat.length; i++) {
        throatdata[i] = throat[i].id;
    }

    neckdata.length = 0;
    var neck = document.querySelectorAll('#neck input[type="checkbox"]:checked');
    for (var i = 0; i < neck.length; i++) {
        neckdata[i] = neck[i].id;
    }

    breastsdata.length = 0;
    var breasts = document.querySelectorAll('#breasts input[type="checkbox"]:checked');
    for (var i = 0; i < breasts.length; i++) {
        breastsdata[i] = breasts[i].id;
    }

    respiratorydata.length = 0;
    var respiratory = document.querySelectorAll('#respiratory input[type="checkbox"]:checked');
    for (var i = 0; i < respiratory.length; i++) {
        respiratorydata[i] = respiratory[i].id;
    }

    cardiovasculardata.length = 0;
    var cardiovascular = document.querySelectorAll('#cardiovascular input[type="checkbox"]:checked');
    for (var i = 0; i < cardiovascular.length; i++) {
        cardiovasculardata[i] = cardiovascular[i].id;
    }

    gastrointestinaldata.length = 0;
    var gastrointestinal = document.querySelectorAll('#gastrointestinal input[type="checkbox"]:checked');
    for (var i = 0; i < gastrointestinal.length; i++) {
        gastrointestinaldata[i] = gastrointestinal[i].id;
    }

    urinarydata.length = 0;
    var urinary = document.querySelectorAll('#urinary input[type="checkbox"]:checked');
    for (var i = 0; i < urinary.length; i++) {
        urinarydata[i] = urinary[i].id;
    }

    vasculardata.length = 0;
    var vascular = document.querySelectorAll('#vascular input[type="checkbox"]:checked');
    for (var i = 0; i < vascular.length; i++) {
        vasculardata[i] = vascular[i].id;
    }

    musculoskeletaldata.length = 0;
    var musculoskeletal = document.querySelectorAll('#musculoskeletal input[type="checkbox"]:checked');
    for (var i = 0; i < musculoskeletal.length; i++) {
        musculoskeletaldata[i] = musculoskeletal[i].id;
    }

    neurologicdata.length = 0;
    var neurologic = document.querySelectorAll('#neurologic input[type="checkbox"]:checked');
    for (var i = 0; i < neurologic.length; i++) {
        neurologicdata[i] = neurologic[i].id;
    }

    hematologicdata.length = 0;
    var hematologic = document.querySelectorAll('#hematologic input[type="checkbox"]:checked');
    for (var i = 0; i < hematologic.length; i++) {
        hematologicdata[i] = hematologic[i].id;
    }

    endocrinedata.length = 0;
    var endocrine = document.querySelectorAll('#endocrine input[type="checkbox"]:checked');
    for (var i = 0; i < endocrine.length; i++) {
        endocrinedata[i] = endocrine[i].id;
    }

    psychiatricdata.length = 0;
    var psychiatric = document.querySelectorAll('#psychiatric input[type="checkbox"]:checked');
    for (var i = 0; i < psychiatric.length; i++) {
        psychiatricdata[i] = psychiatric[i].id;
    }
    assignval(function () {
        callback();
    });

}

//assignval
function assignval(callback) {

    if (genraldata.length === 0) {
        genraldata[0] = 0;
    }
    if (skindata.length === 0) {
        skindata[0] = 0;
    }
    if (headdata.length === 0) {
        headdata[0] = 0;
    }
    if (earsdata.length === 0) {
        earsdata[0] = 0;
    }
    if (eyesdata.length === 0) {
        eyesdata[0] = 0;
    }
    if (nosedata.length === 0) {
        nosedata[0] = 0;
    }
    if (throatdata.length === 0) {
        throatdata[0] = 0;
    }
    if (neckdata.length === 0) {
        neckdata[0] = 0;
    }
    if (breastsdata.length === 0) {
        breastsdata[0] = 0;
    }
    if (respiratorydata.length === 0) {
        respiratorydata[0] = 0;
    }
    if (cardiovasculardata.length === 0) {
        cardiovasculardata[0] = 0;
    }
    if (gastrointestinaldata.length === 0) {
        gastrointestinaldata[0] = 0;
    }
    if (urinarydata.length === 0) {
        urinarydata[0] = 0;
    }
    if (vasculardata.length === 0) {
        vasculardata[0] = 0;
    }
    if (musculoskeletaldata.length === 0) {
        musculoskeletaldata[0] = 0;
    }
    if (neurologicdata.length === 0) {
        neurologicdata[0] = 0;
    }
    if (hematologicdata.length === 0) {
        hematologicdata[0] = 0;
    }
    if (endocrinedata.length === 0) {
        endocrinedata[0] = 0;
    }
    if (psychiatricdata.length === 0) {
        psychiatricdata[0] = 0;
    }
    callback();
}

//setros
function setros() {

    $("#status_out_review").show();
    $("#add-review,#close-review").prop("disabled", true);

    addros(function () {
        $.ajax({
            url: path + "HospitalManagementSystem/HMS/HealthProvider/addros.jsp",
            type: "POST",
            data: {
                "genraldata": genraldata,
                "skindata": skindata,
                "heartdata": headdata,
                "earsdata": earsdata,
                "eyesdata": eyesdata,
                "nosedata": nosedata,
                "throatdata": throatdata,
                "neckdata": neckdata,
                "breastsdata": breastsdata,
                "respiratorydata": respiratorydata,
                "cardiovasculardata": cardiovasculardata,
                "gastrointestinaldata": gastrointestinaldata,
                "urinarydata": urinarydata,
                "vasculardata": vasculardata,
                "musculoskeletaldata": musculoskeletaldata,
                "neurologicdata": neurologicdata,
                "hematologicdata": hematologicdata,
                "endocrinedata": endocrinedata,
                "psychiatricdata": psychiatricdata
            },
            success: function (data) {
                var msg = jQuery.trim(data);
                if (msg === "success") {
                    $("#status_out_review").hide();
                    $("#successfull-send_review").html(successfulladddetails);
                    $("#successfull-send_review").show();
                    $("#resource-of-sym").modal("hide");
                    // clearmood();
                    var myVar1 = setInterval(function () {
                        $("#successfull-send_review").hide();
                        $("#add-review,#close-review").removeProp("disabled");
                        clearInterval(myVar1);
                    }, 3000);
                } else {
                    $("#status_out_review").hide();
                    $("#errormsg-alert_review").html(unsuccessfulladddetails);
                    $("#errormsg-alert_review").show();
                    var myVar1 = setInterval(function () {
                        $("#errormsg-alert_review").hide();
                        $("#add-review,#close-review").removeProp("disabled");
                        clearInterval(myVar1);
                    }, 3000);
                }
            }, error: function () {
                $("#status_out_review").hide();
                $("#errormsg-alert_review").html(servererror);
                $("#errormsg-alert_review").show();
                var myVar1 = setInterval(function () {
                    $("#errormsg-alert_review").hide();
                    $("#add-review,#close-review").removeProp("disabled");
                    clearInterval(myVar1);
                }, 3000);
            }
        });
        return false;
    });
}

//clearmood
function clearmood() {
    for (i = 1; i < 106; i++) {
        if (document.getElementById(i).checked) {
            document.getElementById(i).checked = false;
        }
    }
}
