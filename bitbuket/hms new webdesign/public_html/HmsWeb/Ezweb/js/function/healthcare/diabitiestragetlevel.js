var formtype = "";

$(".add_icon_diabetes_target").click(function () {
    target = ["&le;7.0%", "&le;2mmol/L", "&lt;4.0", "M: &lt; 2.0 mg/mmol F: &lt; 2.0 mg/mmol", "&lt; 130/80 mmHg", "&gt;60 ml/min"];
    form = ["A1C", "LDLC", "HDLC", "ACR", "eGFR", "Blood Pressure"]; // jsp page
    var current = $(this).attr("id");
    formtype = form[current];
    $(".dyntitle").html("Target Level " + target[current]);
});

//add traget
function addtraget() {

    var tragetid = 0;

    if (formtype === "A1C") {
        tragetid = 1;
    } else if (formtype === "LDLC") {
        tragetid = 2;
    } else if (formtype === "HDLC") {
        tragetid = 3;
    } else if (formtype === "ACR") {
        tragetid = 4;
    } else if (formtype === "eGFR") {
        tragetid = 5;
    } else if (formtype === "Blood Pressure") {
        tragetid = 6;
    }

    var date = $("#target_date_add").val();
    var goal = $("#target_goal_add").val();

    if (isNaN(goal)) {

        $("#status_out_target_add").show();
        $("#submittarget,#canceltarget").prop("disabled", true);

        $.ajax({
            url: path + "HospitalManagementSystem/HMS/Issues/setdiabitiestraget.jsp",
            type: "POST",
            data: {
                date: date,
                tragetid: tragetid,
                goal: goal
            },
            success: function (data) {
                var msg = jQuery.trim(data);
                if (msg === "success") {
                    $("#status_out_target_add").hide();
                    $("#successfull-send_targetadd").html(successfulladddetails);
                    $("#successfull-send_targetadd").show();
                    var myVar1 = setInterval(function () {
                        //    after clear the field
                        $("#target_date_add").val("");
                        $("#target_goal_add").val("");
                        $("#successfull-send_targetadd").hide();
                        $('#targetmodel').modal('hide');
                        geta1c();
                        getldlc();
                        getacr();
                        gethdlc();
                        getegfr();
                        getbp();
                        $("#submittarget,#canceltarget").removeProp("disabled");
                        clearInterval(myVar1);
                    }, 3000);
                } else {
                    $("#status_out_target_add").hide();
                    $("#errormsg-alert_targetadd").html(unsuccessfulladddetails);
                    $("#errormsg-alert_targetadd").show();
                    var myVar1 = setInterval(function () {
                        $("#errormsg-alert_targetadd").hide();
                        $("#submittarget,#canceltarget").removeProp("disabled");
                        clearInterval(myVar1);
                    }, 3000);
                }
            },
            error: function () {
                $("#status_out_target_add").hide();
                $("#errormsg-alert_targetadd").html(servererror);
                $("#errormsg-alert_targetadd").show();
                var myVar1 = setInterval(function () {
                    $("#errormsg-alert_targetadd").hide();
                    $("#submittarget,#canceltarget").removeProp("disabled");
                    clearInterval(myVar1);
                }, 3000);
                $("#target_date_add").val("");
                $("#target_goal_add").val("");
            }
        });
    } else {
        $("#target_goal_add").addClass("input-alert");
    }
    return false;
}



