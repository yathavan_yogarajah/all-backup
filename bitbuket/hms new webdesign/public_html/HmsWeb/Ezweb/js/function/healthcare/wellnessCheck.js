//assigndata
function assigndata() {

    var sex = localStorage.getItem("sex");

    if (sex === "Male") {
        $("#person_womenContents").hide();
    } else if (sex === "Female") {
        $("#person_menContents").hide();
    }

    var datavalid = localStorage.getItem("validdata");
    testdata = JSON.parse(localStorage.getItem("healthtest"));

    if (datavalid === "yes" && testdata.testres.length > 0) {
    }

    if (datavalid === "yes" && testdata.testres.length > 0) {
        for (i = 0; i < testdata.testres.length; i++) {
            document.getElementById(testdata.testres[i][0]).checked = true;
        }
    }
    // localStorage.removeItem("sex");
    localStorage.removeItem("validdata");
    localStorage.removeItem("healthtest");
}

//collect data
var wellness = "";
var genraldata = [];

function collectdata() {

    $("#health-maintanace").find("input[type=checkbox]").each(function () {
        var health = "";
        var id = $(this).attr("id");

        if ($("#" + id).is(":checked")) {
            health = id + "-yes";
        } else {
            health = id + "-no";
        }
        genraldata.push(health);
    });

}


//add data
function adddata() {
    collectdata();

    var today = new Date();
    var yyyy = today.getFullYear();
    var dd = today.getDate();
    var mm = today.getMonth() + 1; //January is 0!

    if (dd < 10) {
        dd = '0' + dd;
    }
    if (mm < 10) {
        mm = '0' + mm;
    }

    var today = yyyy + '-' + mm + '-' + dd;
    $("#quest_status_out").show();
    $("#quest_submit,#quest_cancel").prop("disabled", true);
    $.ajax({
        url: path + "HospitalManagementSystem/HMS/Issues/settest.jsp",
        type: "POST",
        data: {
            "genraldata": genraldata,
            "date": today
        },
        success: function (data) {
            var msg = jQuery.trim(data);
            if (msg === "success") {
                $("#quest_status_out").hide();
                $("#quest_successfull-send").html(successfulladddetails);
                $("#quest_successfull-send").show();
                var myVar = setInterval(function () {
                    $("#quest_successfull-send").hide();
                    $("#quest_submit,#quest_cancel").removeProp("disabled");
                    $("#commonquestion input[type='checkbox']").removeProp("checked");
                    clearInterval(myVar);
                }, 3000);
            } else {
                $("#quest_status_out").hide();
                $("#quest_errormsg-alert").html(unsuccessfulladddetails);
                $("#quest_errormsg-alert").show();
                var myVar = setInterval(function () {
                    $("#quest_errormsg-alert").hide();
                    $("#quest_submit,#quest_cancel").removeProp("disabled");
                    clearInterval(myVar);
                }, 3000);
            }
        },
        error: function () {
            $("#quest_status_out").hide();
            $("#quest_errormsg-alert").html(servererror);
            $("#quest_errormsg-alert").show();
            var myVar = setInterval(function () {
                $("#quest_errormsg-alert").hide();
                $("#quest_submit,#quest_cancel").removeProp("disabled");
                clearInterval(myVar);
            }, 3000);
        }
    });
    return false;
}
