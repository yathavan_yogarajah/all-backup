//check required
$("input,select").focus(function () {
    $input_id = $(this).attr("id");
    if ($("#" + $input_id).hasClass("input-alert"))
        $("#" + $input_id).removeClass("input-alert");
});

//function date check
function datecheck() {

    datevalidate = 0;
    dateres = true;

    if ($("#bp_date").val()) {
        if (dateandtime($("#bp_date").val()) === false) {
            $("#bp_date").addClass("input-alert");
            datevalidate++;
        }
    }
    if (datevalidate !== 0) {
        dateres = false;
    }

    return dateres;
}

//validate heart
function bp() {

    var required = [];
    $fields = $("form#bp_form").serialize();
    form_array = $fields.split('&');
    for (var i = 0; i < form_array.length; i++) {
        name = form_array[i].split('=')[0];
        value = form_array[i].split('=')[1];

        if ($.trim(value) === "") {
            required[i] = value;
            var name_of_input = "#" + name;
            $(name_of_input).addClass("input-alert");
        }
    }

    count = 0;
    for ($i = 0; $i <= required.length; $i++) {

        if ($.trim(required[$i]) === "") {
            count++;
        }
    }

    if ((count - 1) === 0) {

        if (datecheck()) {
            setbp();

        } else {
            notifyalert(0, dateerror);
        }


    }
}

//set heart rate
function setbp() {

    /**** check internetconnection ******/
    if (getConnection() === "false") {
        return false;
    }


    var bp_date = new Date($("#bp_date").val().split(" ")[0]);
    var today = new Date();
    var dd = today.getDate();
    var mm = today.getMonth() + 1; //January is 0!

    var yyyy = today.getFullYear();
    if (dd < 10) {
        dd = '0' + dd;
    }
    if (mm < 10) {
        mm = '0' + mm;
    }

    var today = yyyy + '-' + mm + '-' + dd;
    var datestatus;

    if ($("#bp_date").val() !== "" && $("#bp_date").val() !== " " && $("#bp_date").val() !== null) {

        if (bp_date <= new Date(today)) {
            datestatus = true;
        } else {
            datestatus = false;
        }

    } else {

        datestatus = false;
    }

    if (datestatus === true) {

        $("#status_out_add_bp").show();
        $("#submit-bp,#cancel-bp").prop("disabled", true);

        var date = $.trim($("#bp_date").val().split(" ")[0]);
        var time = $.trim($("#bp_date").val().split(" ")[1]);
        var dbprate = $("#diastolic_pressure").val();

        if (dbprate === "" || dbprate === " " || dbprate === null) {
            dbprate = "00.00";
        }

        var sbprate = $("#systolic_pressure").val();
        if (sbprate === "" || sbprate === " " || sbprate === null) {
            sbprate = "00.00";
        }

        $.ajax({
            url: path + "HospitalManagementSystem/HMS/Issues/setmanaualbp.jsp",
            type: "POST",
            data: {
                date: date,
                time: time,
                dbprate: dbprate,
                sbprate: sbprate
            },
            success: function (data) {
                var msg = jQuery.trim(data);
                if (msg === "success") {
                    $("#successfull-send-bp").html(successfulladddetails);
                    $("#successfull-send-bp").show();
                    var myVar1 = setInterval(function () {
                        $("#successfull-send-bp").hide();
                        document.getElementById("bp_form").reset();
                        $("#submit-bp,#cancel-bp").removeProp("disabled");
                        clearInterval(myVar1);
                    }, 3000);

                    $("#blood_manual .next-2").hide();
                    $("#blood_manual .next-1").show();
                    removeChart(chartBlood, bloodrate1, bloodrate2, blooddate1, blooddate2, bloodrate3, bloodrate4);
                    getbloodpreasure(-1);
                    $("#" + dates[4]).val(today + " " + time);//reset date
                    $("#systolic_pressure,#diastolic_pressure").val("");
                    $("#submit-bp,#cancel-bp").removeProp("disabled");
                    $("#status_out_add_bp").hide();

                } else {

                    $("#status_out_add_bp").hide();
                    $("#errormsg-alert-bp").html(unsuccessfulladddetails);
                    $("#errormsg-alert-bp").show();
                    var myVar1 = setInterval(function () {
                        $("#errormsg-alert-bp").hide();
                        $("#submit-bp,#cancel-bp").removeProp("disabled");
                        clearInterval(myVar1);
                    }, 3000);
                }
            },
            error: function () {
                $("#status_out_add_bp").hide();
                $("#errormsg-alert-bp").html(servererror);
                $("#errormsg-alert-bp").show();
                var myVar1 = setInterval(function () {
                    $("#errormsg-alert-bp").hide();
                    $("#submit-bp,#cancel-bp").removeProp("disabled");
                    clearInterval(myVar1);
                }, 3000);
            }
        });

        return false;

    } else {

        $("#bp_date").addClass("input-alert");
        $("#status_out_add_bp").hide();
        $("#errormsg-alert-bp").html(datecheckerror);
        $("#errormsg-alert-bp").show();
        var myVar1 = setInterval(function () {
            $("#errormsg-alert-bp").hide();
            $("#submit-bp,#cancel-bp").removeProp("disabled");
            clearInterval(myVar1);
        }, 3000);
    }
}