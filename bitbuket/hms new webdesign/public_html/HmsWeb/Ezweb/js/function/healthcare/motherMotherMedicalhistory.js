// validate  details
$("input,select").focus(function () {
    input_id = $(this).attr("id");
    if ($("#" + input_id).hasClass("input-alert"))
        $("#" + input_id).removeClass("input-alert");
});

//collect data
var mother_motherdata = "";
var mother_motherdatacheck = [];

collectdata("family_mother_mother");

function collectdata(formname) {

    $("#" + formname).find("input[type=checkbox]").each(function () {
        mother_motherdatacheck.push($(this).attr("id"));
    });
}

// save mother_mother
function setvaluemother_mother() {

    mother_motherdata = "";

    for (var x = 0; x < mother_motherdatacheck.length; x++) {
        var id = mother_motherdatacheck[x];
        var id_check = id.split("_")[2];

        if ($("#" + mother_motherdatacheck[x]).is(":checked")) {
            if (mother_motherdatacheck[x] === "mother_mother_other") {
                y = id + "=" + $("#mother_mother_othertxt").val();
            } else {
                y = id + "=" + id_check + "-yes";
            }
        } else {
            y = id + "=" + id_check + "-no";
        }
        mother_motherdata += y + "&";
        y = "";
    }
}

//save mother_mother
function save_mother_mother() {

    setvaluemother_mother();

    var mother_mothername = $("#mother_mother_name").val();
    var mother_motherhealth = $("#mother_mother_healthstatus").val();
    var err = 0;
    if (mother_mothername === "" || mother_mothername === null || mother_mothername === " ") {
        err++;
        $("#mother_mother_name").addClass("input-alert");
    }

    if (mother_motherhealth === "" || mother_motherhealth === null || mother_motherhealth === " ") {
        err++;
        $("#mother_mother_healthstatus").addClass("input-alert");
    }

    if (err === 0) {

        if ($("#mother_mother_other").is(":checked") === true) {
            if (!$("#mother_mother_othertxt").val()) {
                $("#mother_mother_othertxt").addClass("input-alert");
                return false;
            }

        }

        datasubmit = mother_motherdata + "mother_mother_name=" + mother_mothername + "&mother_mother_healthstatus=" + mother_motherhealth;
        setmother_mother();
    }

}

var mother_motherfamilyhistoryid = 0;

//setmother_mother
function setmother_mother() {

    //check internet connection
    if (connection === "offline") {
        return false;
    }

    $("#mother_mother_status_out").show();
    $("#mother_mother_submit,#mother_mother_cancel").prop("disabled", true);

    $.ajax({
        url: path + "HospitalManagementSystem/HMS/Issues/setmother-mothermedicalhistory.jsp",
        type: "POST",
        data: datasubmit + "&relationsid=9&relationstatusid=" + mother_motherfamilyhistoryid,
        success: function (data) {
            var msg = jQuery.trim(data);
            if (msg === "success") {
                $("#mother_mother_status_out").hide();
                getmother_mother();
                $("#successfull-send-mothermother").html(successfulladddetails);
                $("#successfull-send-mothermother").show();
                var myVar1 = setInterval(function () {
                    $("#successfull-send-mothermother").hide();
                    clearInterval(myVar1);
                }, 3000);
                $("#mother_mother").removeClass("in");
                $("#mother_mother").css("height", "0px");
                $("#mother_mother_submit,#mother_mother_cancel").removeProp("disabled");
            } else {

                $("#mother_mother_status_out").hide();
                $("#errormsg-alert-mothermother").html(unsuccessfulladddetails);
                $("#errormsg-alert-mothermother").show();
                var myVar1 = setInterval(function () {
                    $("#errormsg-alert-mothermother").hide();
                    clearInterval(myVar1);
                }, 3000);
                $("#mother_mother_submit,#mother_mother_cancel").removeProp("disabled");
            }
        },
        error: function (data) {
            $("#mother_mother_status_out").hide();
            $("#errormsg-alert-mothermother").html(servererror);
            $("#errormsg-alert-mothermother").show();
            var myVar1 = setInterval(function () {
                $("#errormsg-alert-mothermother").hide();
                clearInterval(myVar1);
            }, 3000);
            $("#mother_mother_submit,#mother_mother_cancel").removeProp("disabled");

        }
    });
    return false;
}

//get mother_mother
function getmother_mother() {

    $("#preload_overlay").show();
    $.ajax({
        url: path + "HospitalManagementSystem/HMS/Issues/getfamilymedicalhistory.jsp",
        type: "POST",
        data: {
            "getrelationid": 9
        },
        success: function (data) {
            var msg_mother_mother = jQuery.trim(data);
            if (msg_mother_mother) {
                var obj_mother_mother = JSON.parse(msg_mother_mother);
                if (obj_mother_mother.familymedicalhistory.length > 0) {
                    $("#member6_rank").attr("src", "../../icons/tick.png");
                    mother_motherfamilyhistoryid = obj_mother_mother.familymedicalhistory[0][0];
                    $("#mother_mother_name").val(obj_mother_mother.familymedicalhistory[0][1]);
                    document.getElementById('mother_mother_healthstatus').value = obj_mother_mother.familymedicalhistory[0][2];

                    if (obj_mother_mother.familymedicalhistory[0][3] === "arthritis-yes") {
                        $("#mother_mother_arthritis").prop("checked", true);
                    } else {
                        $("#mother_mother_arthritis").prop("checked", false);
                    }

                    if (obj_mother_mother.familymedicalhistory[0][4] === "cancer-yes") {
                        $("#mother_mother_cancer").prop("checked", true);
                    } else {
                        $("#mother_mother_cancer").prop("checked", false);
                    }

                    if (obj_mother_mother.familymedicalhistory[0][5] === "diabetes-yes") {
                        $("#mother_mother_diabetes").prop("checked", true);
                    } else {
                        $("#mother_mother_diabetes").prop("checked", false);
                    }

                    if (obj_mother_mother.familymedicalhistory[0][6] === "heartCondition-yes") {
                        $("#mother_mother_heartCondition").prop("checked", true);
                    } else {
                        $("#mother_mother_heartCondition").prop("checked", false);
                    }

                    if (obj_mother_mother.familymedicalhistory[0][7] === "lungdisease-yes") {
                        $("#mother_mother_lungdisease").prop("checked", true);
                    } else {
                        $("#mother_mother_lungdisease").prop("checked", false);
                    }

                    if (obj_mother_mother.familymedicalhistory[0][8] === "mentalillness-yes") {
                        $("#mother_mother_mentalillness").prop("checked", true);
                    } else {
                        $("#mother_mother_mentalillness").prop("checked", false);
                    }

                    if (obj_mother_mother.familymedicalhistory[0][9] === "stroke-yes") {
                        $("#mother_mother_stroke").prop("checked", true);
                    } else {
                        $("#mother_mother_stroke").prop("checked", false);
                    }

                    if (obj_mother_mother.familymedicalhistory[0][10] === "other-no") {
                        $("#mother_mother_other").prop("checked", false);
                    } else {
                        $("#mother_mother_othertxt").show();
                        $("#mother_mother_othertxt").val(obj_mother_mother.familymedicalhistory[0][10]);
                        $("#mother_mother_other").prop("checked", true);
                    }

                } else {
                    mother_motherfamilyhistoryid = 0;
                }
            }
            $("#preload_overlay").hide();
        },
        error: function () {
            $("#preload_overlay").hide();
            $("#errormsg-alert-mothermother").html(servererror);
            $("#errormsg-alert-mothermother").show();
            var myVar1 = setInterval(function () {
                $("#errormsg-alert-mothermother").hide();
                clearInterval(myVar1);
            }, 3000);
        }
    });
    return false;
}
