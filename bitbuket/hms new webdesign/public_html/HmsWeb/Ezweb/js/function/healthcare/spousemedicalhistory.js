// validate children details
$("input,select").focus(function () {
    input_id = $(this).attr("id");
    if ($("#" + input_id).hasClass("input-alert"))
        $("#" + input_id).removeClass("input-alert");
});

//collect data
var spousedata = "";
var spousedatacheck = [];
collectdata("family_spouse");

function collectdata(formname) {
    $("#" + formname).find("input[type=checkbox]").each(function () {
        spousedatacheck.push($(this).attr("id"));
    });
}

// save spouse
function setvaluespouse() {

    spousedata = "";

    for (var x = 0; x < spousedatacheck.length; x++) {
        var id = spousedatacheck[x];
        var id_check = id.split("_")[1];

        if ($("#" + spousedatacheck[x]).is(":checked")) {
            if (spousedatacheck[x] === "spouse_other") {
                y = id + "=" + $("#spouse_othertxt").val();
            } else {
                y = id + "=" + id_check + "-yes";
            }
        } else {
            y = id + "=" + id_check + "-no";
        }
        spousedata += y + "&";
        y = "";
    }
}

//save spouse
function save_spouse() {

    setvaluespouse();

    var spousename = $("#spouse_name").val();
    var spousehealth = $("#spouse_healthstatus").val();
    var err = 0;
    if (spousename === "" || spousename === null || spousename === " ") {
        err++;
        $("#spouse_name").addClass("input-alert");
    }

    if (spousehealth === "" || spousehealth === null || spousehealth === " ") {
        err++;
        $("#spouse_healthstatus").addClass("input-alert");
    }

    if (err === 0) {

        if ($("#spouse_other").is(":checked") === true) {
            if (!$("#spouse_othertxt").val()) {
                $("#spouse_othertxt").addClass("input-alert");
                return false;
            }

        }

        datasubmit = spousedata + "spouse_name=" + spousename + "&spouse_healthstatus=" + spousehealth;
        setspouse();
    }

}

var spousefamilyhistoryid = 0;

//setspouse
function setspouse() {

    /**** check internetconnection ******/
    if (connection === "offline") {
        return false;
    }

    $("#spouse_status_out").show();
    $("#spouse_submit,#spouse_cancel").prop("disabled", true);
    $.ajax({
        url: path + "HospitalManagementSystem/HMS/Issues/setspousemedicalhistory.jsp",
        type: "POST",
        data: datasubmit + "&relationsid=5&relationstatusid=" + spousefamilyhistoryid,
        success: function (data) {
            var msg = jQuery.trim(data);
            if (msg === "success") {

                $("#spouse_status_out").hide();
                $("#spouse_successfull-send").html(successfulladddetails);
                $("#spouse_successfull-send").show();
                var myVar1 = setInterval(function () {
                    $("#spouse_successfull-send").hide();
                    clearInterval(myVar1);
                }, 3000);
                getspouse();
                $("#spouse").removeClass("in");
                $("#spouse").css("height", "0px");
                $("#spouse_submit,#spouse_cancel").removeProp("disabled");
            } else {

                $("#spouse_status_out").hide();
                $("#spouse_errormsg-alert").html(unsuccessfulladddetails);
                $("#spouse_errormsg-alert").show();
                var myVar1 = setInterval(function () {
                    $("#spouse_errormsg-alert").hide();
                    clearInterval(myVar1);
                }, 3000);
                $("#spouse_submit,#spouse_cancel").removeProp("disabled");
            }
        },
        error: function () {

            $("#spouse_status_out").hide();
            $("#spouse_errormsg-alert").html(servererror);
            $("#spouse_errormsg-alert").show();
            var myVar1 = setInterval(function () {
                $("#spouse_errormsg-alert").hide();
                clearInterval(myVar1);
            }, 3000);
            $("#spouse_submit,#spouse_cancel").removeProp("disabled");
        }
    });
    return false;
}

//getspouse
function getspouse() {

    $("#preload_overlay").show();
    $.ajax({
        url: path + "HospitalManagementSystem/HMS/Issues/getfamilymedicalhistory.jsp",
        type: "POST",
        data: {
            "getrelationid": 5
        },
        success: function (data) {
            var msg = jQuery.trim(data);
            if (msg) {
                var obj = JSON.parse(msg);

                if (obj.familymedicalhistory.length > 0) {
                    $("#member8_rank").attr("src", "../../icons/tick.png");
                    spousefamilyhistoryid = obj.familymedicalhistory[0][0];
                    $("#spouse_name").val(obj.familymedicalhistory[0][1]);
                    document.getElementById('spouse_healthstatus').value = obj.familymedicalhistory[0][2];

                    if (obj.familymedicalhistory[0][3] === "arthritis-yes") {
                        $("#spouse_arthritis").prop("checked", true);
                    } else {
                        $("#spouse_arthritis").prop("checked", false);
                    }

                    if (obj.familymedicalhistory[0][4] === "cancer-yes") {
                        $("#spouse_cancer").prop("checked", true);
                    } else {
                        $("#spouse_cancer").prop("checked", false);
                    }

                    if (obj.familymedicalhistory[0][5] === "diabetes-yes") {
                        $("#spouse_diabetes").prop("checked", true);
                    } else {
                        $("#spouse_diabetes").prop("checked", false);
                    }

                    if (obj.familymedicalhistory[0][6] === "heartCondition-yes") {
                        $("#spouse_heartCondition").prop("checked", true);
                    } else {
                        $("#spouse_heartCondition").prop("checked", false);
                    }

                    if (obj.familymedicalhistory[0][7] === "lungdisease-yes") {
                        $("#spouse_lungdisease").prop("checked", true);
                    } else {
                        $("#spouse_lungdisease").prop("checked", false);
                    }

                    if (obj.familymedicalhistory[0][8] === "mentalillness-yes") {
                        $("#spouse_mentalillness").prop("checked", true);
                    } else {
                        $("#spouse_mentalillness").prop("checked", false);
                    }

                    if (obj.familymedicalhistory[0][9] === "stroke-yes") {
                        $("#spouse_stroke").prop("checked", true);
                    } else {
                        $("#spouse_stroke").prop("checked", false);
                    }

                    if (obj.familymedicalhistory[0][10] === "other-no") {
                        $("#spouse_other").prop("checked", false);
                    } else {
                        $("#spouse_othertxt").show();
                        $("#spouse_othertxt").val(obj.familymedicalhistory[0][10]);
                        $("#spouse_other").prop("checked", true);
                    }
                } else {
                    spousefamilyhistoryid = 0;
                }
            }
            $("#preload_overlay").hide();
        },
        error: function () {
            $("#preload_overlay").hide();
            $("#spouse_errormsg-alert").html(servererror);
            $("#spouse_errormsg-alert").show();
            var myVar1 = setInterval(function () {
                $("#spouse_errormsg-alert").hide();
                clearInterval(myVar1);
            }, 3000);
        }
    });
    return false;
}