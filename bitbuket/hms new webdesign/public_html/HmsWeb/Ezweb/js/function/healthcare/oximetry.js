//check required
$("input,select").focus(function () {
    $input_id = $(this).attr("id");
    if ($("#" + $input_id).hasClass("input-alert"))
        $("#" + $input_id).removeClass("input-alert");
});

//function date check
function datecheck() {

    datevalidate = 0;
    dateres = true;

    if ($("#oximetry_date").val()) {
        if (myFunction($("#oximetry_date").val()) === false) {
            $("#oximetry_date").addClass("input-alert");
            datevalidate++;
        }
    }
    if (datevalidate !== 0) {
        dateres = false;
    }
    return dateres;
}

//validate oximetry
function oximetry() {

    var required = [];
    $fields = $("form#oximetry_form").serialize();
    form_array = $fields.split('&');

    for (var i = 0; i < form_array.length; i++) {

        name = form_array[i].split('=')[0];
        value = form_array[i].split('=')[1];
        required[i] = value;

        if ($.trim(value) === "") {
            var name_of_input = "#" + name;
            $(name_of_input).addClass("input-alert");
        }
    }
    count = 0;
    for ($i = 0; $i <= required.length; $i++) {
        if ($.trim(required[$i]) === "") {
            count++;
        }
    }

    if ($("#spo2").val() < 0) {
        $("#spo2").addClass("input-alert");
        $("#errormsg-alert-oximetry").html(numbererror);
        $("#errormsg-alert-oximetry").show();
        var myVar1 = setInterval(function () {
            $("#errormsg-alert-oximetry").hide();
            clearInterval(myVar1);
        }, 3000);
        count++;
    }

    if ((count - 1) === 0) {
        if (datecheck()) {
            setoximetry();
        } else {
            $("#errormsg-alert-oximetry").html(dateerror);
            $("#errormsg-alert-oximetry").show();
            var myVar1 = setInterval(function () {
                $("#errormsg-alert-oximetry").hide();
                clearInterval(myVar1);
            }, 3000);
        }
    }
}

//setoximetry
function setoximetry() {

    /**** check internetconnection ******/
    if (getConnection() === "false") {
        return false;
    }

    var oximetry_date = new Date($("#oximetry_date").val());
    var today = new Date();
    var dd = today.getDate();
    var mm = today.getMonth() + 1; //January is 0!
    var yyyy = today.getFullYear();

    if (dd < 10) {
        dd = '0' + dd;
    }
    if (mm < 10) {
        mm = '0' + mm;
    }

    var today = yyyy + '-' + mm + '-' + dd;
    var datestatus;

    if ($("#oximetry_date").val() !== "" && $("#oximetry_date").val() !== " " && $("#oximetry_date").val() !== null) {
        if (oximetry_date <= new Date(today)) {
            datestatus = true;
        } else {
            datestatus = false;
        }

    } else {
        datestatus = false;
    }

    if (datestatus === true) {

        $("#status_out_add_oximetry").show();
        $("#submit-oximetry,#cancel-oximetry").prop("disabled", true);

        $.ajax({
            url: path + "HospitalManagementSystem/HMS/Issues/setmanualoximarity.jsp",
            type: "POST",
            data: {
                date: $("#oximetry_date").val(),
                oxi: $("#spo2").val()
            },
            success: function (data) {
                var msg = jQuery.trim(data);
                if (msg === "success") {
                    $("#status_out_add_oximetry").hide();
                    $("#successfull-send-oximetry").html(successfulladddetails);
                    $("#successfull-send-oximetry").show();

                    var myVar1 = setInterval(function () {
                        $("#successfull-send-oximetry").hide();
                        $("#oximetry_manual .next-2").hide();
                        $("#oximetry_manual .next-1").show();
                        removeChart(chartOximetry, oxrate1, oxrate2, oxdate1, oxdate2);
                        getoximarity(-1);
                        $("#" + dates[3]).val(today);
                        $("#spo2").val("");
                        $("#submit-oximetry,#cancel-oximetry").removeProp("disabled");
                        clearInterval(myVar1);
                    }, 3000);

                } else {
                    $("#status_out_add_oximetry").hide();
                    $("#errormsg-alert-oximetry").html(unsuccessfulladddetails);
                    $("#errormsg-alert-oximetry").show();
                    var myVar1 = setInterval(function () {
                        $("#errormsg-alert-oximetry").hide();
                        $("#submit-oximetry,#cancel-oximetry").removeProp("disabled");
                        clearInterval(myVar1);
                    }, 3000);
                }
            }, error: function () {
                $("#status_out_add_oximetry").hide();
                $("#errormsg-alert-oximetry").html(servererror);
                $("#errormsg-alert-oximetry").show();
                var myVar1 = setInterval(function () {
                    $("#errormsg-alert-oximetry").hide();
                    $("#submit-oximetry,#cancel-oximetry").removeProp("disabled");
                    clearInterval(myVar1);
                }, 3000);
            }
        });
        return false;
    } else {
        $("#oximetry_date").addClass("input-alert");
        $("#errormsg-alert-oximetry").html(datecheckerror);
        $("#errormsg-alert-oximetry").show();
        var myVar1 = setInterval(function () {
            $("#errormsg-alert-oximetry").hide();
            clearInterval(myVar1);
        }, 3000);
    }
}
