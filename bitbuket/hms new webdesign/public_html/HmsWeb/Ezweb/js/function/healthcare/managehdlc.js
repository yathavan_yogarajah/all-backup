var obj_hdlc = "";
var objcount_hdlc = 0;
var counter_hdlc = 0;
var proidhdlc = 0;

//gethdlc
function gethdlc() {

    $.ajax({
        url: path + "HospitalManagementSystem/HMS/Issues/gettraget.jsp",
        type: "POST",
        data: {
            tragetid: 3
        },
        success: function (data) {
            var msgpro = jQuery.trim(data);
            if (msgpro) {
                obj_hdlc = JSON.parse(msgpro);
                if (obj_hdlc.diatraget.length > 0) {
                    $("#targetlevel-3").show();
                    objcount_hdlc = obj_hdlc.diatraget.length;
                    sethdlc(obj_hdlc);
                } else {
                    $("#targetlevel-3").hide();
                    proidhdlc = 0;
                }
            }
            counter();
        },
        error: function () {
            $("#targetlevel-3").hide();
            counter();
            $("#errormsg-alert_targethdlcmodify").html(servererror);
            $("#errormsg-alert_targethdlcmodify").show();
            var myVar1 = setInterval(function () {
                $("#errormsg-alert_targethdlcmodify").hide();
                clearInterval(myVar1);
            }, 3000);
        }
    });
    return false;
}

$("#targethdlc_backnav").on("click", function () {
    counter_hdlc--;
    if (counter_hdlc <= 0) {
        sethdlc(obj_hdlc);
    }
});

$("#targethdlc_forwardnav").on("click", function () {
    counter_hdlc++;
    if (counter_hdlc < objcount_hdlc) {
        sethdlc(obj_hdlc);
    }
});

$("#targethdlc_modify").on("click", function () {
    updatetragethdlc();
});

//setldlc
function sethdlc(obj_hdlc) {

    if (counter_hdlc + 1 >= objcount_hdlc) {
        $("#targethdlc_forwardnav").hide();
    } else {
        $("#targethdlc_forwardnav").show();
    }

    if (counter_hdlc <= 0) {
        $("#targethdlc_backnav").hide();
    } else {
        $("#targethdlc_backnav").show();
    }

    proidhdlc = obj_hdlc.diatraget[counter_hdlc][0];
    $("#targethdlc_date_view").val(obj_hdlc.diatraget[counter_hdlc][1]);

    if (obj_hdlc.diatraget[counter_hdlc][2] !== "null") {
        $("#targethdlc_mygoal_view").val(obj_hdlc.diatraget[counter_hdlc][2]);
    } else {
        $("#targethdlc_mygoal_view").val("");
    }
}

//update alc
function updatetragethdlc() {

    var date = $("#targethdlc_date_view").val();
    var addgoal = $("#targethdlc_mygoal_view").val();
    if (date && addgoal && isNaN(addgoal)) {
        $("#status_out_targethdlc_view").show();
        $("#targethdlc_modify").prop("disabled", true);
        $.ajax({
            url: path + "HospitalManagementSystem/HMS/Issues/updatediabitiestraget.jsp",
            type: "POST",
            data: {
                date: date,
                tragetid: proidhdlc,
                goal: addgoal
            },
            success: function (data) {
                var msg = jQuery.trim(data);
                if (msg === "success") {
                    $("#status_out_targethdlc_view").hide();
                    $("#successfull-send_targethdlcmodify").html(successfulladddetails);
                    $("#successfull-send_targethdlcmodify").show();
                    var myVar1 = setInterval(function () {
                        $("#successfull-send_targethdlcmodify").hide();
                        gethdlc();
                        $("#targethdlc_modify").removeProp("disabled");
                        clearInterval(myVar1);
                    }, 3000);
                } else {
                    $("#status_out_targethdlc_view").hide();
                    $("#errormsg-alert_targethdlcmodify").html(unsuccessfulladddetails);
                    $("#errormsg-alert_targethdlcmodify").show();
                    var myVar1 = setInterval(function () {
                        $("#errormsg-alert_targethdlcmodify").hide();
                        $("#targethdlc_modify").removeProp("disabled");
                        clearInterval(myVar1);
                    }, 3000);
                }
            },
            error: function () {
                $("#status_out_targethdlc_view").hide();
                $("#errormsg-alert_targethdlcmodify").html(servererror);
                $("#errormsg-alert_targethdlcmodify").show();
                var myVar1 = setInterval(function () {
                    $("#errormsg-alert_targethdlcmodify").hide();
                    $("#targethdlc_modify").removeProp("disabled");
                    clearInterval(myVar1);
                }, 3000);
            }
        });
        return false;
    } else {
        if (date === "" || date === " ") {
            $("#targethdlc_date_view").addClass("input-alert");
        }
        if (addgoal === "" || addgoal === " " || !isNaN(addgoal)) {
            $("#targethdlc_mygoal_view").addClass("input-alert");
        }
    }
}
