// validate details
$("input,select").focus(function () {
    input_id = $(this).attr("id");
    if ($("#" + input_id).hasClass("input-alert"))
        $("#" + input_id).removeClass("input-alert");
});


grandparentsdata = "";
grandparentsdatacheck = [];
collectdata("family_grandparents");
//collect data
function collectdata(formname) {
    $("#" + formname).find("input[type=checkbox]").each(function () {
        grandparentsdatacheck.push($(this).attr("id"));
    });
}

// save grandparents
function setvaluegrandparents() {

    grandparentsdata = "";

    for (var x = 0; x < grandparentsdatacheck.length; x++) {
        var id = grandparentsdatacheck[x];
        var id_check = id.split("_")[1];

        if ($("#" + grandparentsdatacheck[x]).is(":checked")) {
            if (grandparentsdatacheck[x] === "grandparents_other") {
                y = id + "=" + $("#grandparents_othertxt").val();
            } else {
                y = id + "=" + id_check + "-yes";
            }
        } else {
            y = id + "=" + id_check + "-no";
        }
        grandparentsdata += y + "&";
        y = "";
    }
}

//save grandparents
function save_grandparents() {

    setvaluegrandparents();

    var grandparentsname = $("#grandparents_name").val();
    var grandparentshealth = $("#grandparents_healthstatus").val();
    var err = 0;

    if (grandparentsname === "" || grandparentsname === null || grandparentsname === " ") {
        err++;
        $("#grandparents_name").addClass("input-alert");
    }

    if (grandparentshealth === "" || grandparentshealth === null || grandparentshealth === " ") {
        err++;
        $("#grandparents_healthstatus").addClass("input-alert");
    }

    if (err === 0) {
        if ($("#grandparents_other").is(":checked") === true) {
            if (!$("#grandparents_othertxt").val()) {
                $("#grandparents_othertxt").addClass("input-alert");
                return false;
            }
        }
        datasubmit = grandparentsdata + "grandparents_name=" + grandparentsname + "&grandparents_healthstatus=" + grandparentshealth;
        setgrandparents();
    }
}


var grandparentsfamilyhistoryid = 0;

//setgrandparents
function setgrandparents() {

    /**** check internetconnection ******/
    if (connection === "offline") {
        return false;
    }

    $("#grandparents_status_out").show();
    $("#grandparents_submit,#grandparents_cancel").prop("disabled", true);

    $.ajax({
        url: path + "HospitalManagementSystem/HMS/Issues/setgrandparentsmedicalhistory.jsp",
        type: "POST",
        data: datasubmit + "&relationsid=4&relationstatusid=" + grandparentsfamilyhistoryid,
        success: function (data) {
            var msg = jQuery.trim(data);
            if (msg == "success") {
                $("#grandparents_status_out").hide();
                getgrandparents();
                $("#grandparents_successfull-send").html(successfulladddetails);
                $("#grandparents_successfull-send").show();
                var myVar1 = setInterval(function () {
                    $("#grandparents_successfull-send").hide();
                    $("#grandparents_submit,#grandparents_cancel").removeProp("disabled");
                    clearInterval(myVar1);
                }, 3000);
            } else {
                $("#grandparents_status_out").hide();
                $("#grandparents_errormsg-alert").html(unsuccessfulladddetails);
                $("#grandparents_errormsg-alert").show();
                var myVar1 = setInterval(function () {
                    $("#grandparents_errormsg-alert").hide();
                    $("#grandparents_submit,#grandparents_cancel").removeProp("disabled");
                    clearInterval(myVar1);
                }, 3000);
            }
        },
        error: function (data) {
            $("#grandparents_status_out").hide();
            $("#grandparents_errormsg-alert").html(servererror);
            $("#grandparents_errormsg-alert").show();
            var myVar1 = setInterval(function () {
                $("#grandparents_errormsg-alert").hide();
                $("#grandparents_submit,#grandparents_cancel").removeProp("disabled");
                clearInterval(myVar1);
            }, 3000);
        }
    });
    return false;
}

var obj_gp = "";
var objcount_gp = 0;
var counter_gp = 0;

//getgrandparents
function getgrandparents() {

    $("#preload_overlay").show();

    $.ajax({
        url: path + "HospitalManagementSystem/HMS/Issues/getfamilymedicalhistory.jsp",
        type: "POST",
        data: {
            "getrelationid": 4
        },
        success: function (data) {
            var msg_gp = jQuery.trim(data);
            if (msg_gp) {
                obj_gp = JSON.parse(msg_gp);
                if (obj_gp.familymedicalhistory.length > 0) {
                    objcount_gp = obj_gp.familymedicalhistory.length;
                    $("#family_grandparents").show();
                    setgp(obj_gp);
                } else {
                    grandparentsfamilyhistoryid = 0;
                }
            }
            $("#preload_overlay").hide();
        },
        error: function () {
            $("#preload_overlay").hide();
            $("#grandparents_errormsg-alert").html(servererror);
            $("#grandparents_errormsg-alert").show();
            var myVar1 = setInterval(function () {
                $("#grandparents_errormsg-alert").hide();
                clearInterval(myVar1);
            }, 3000);
        }
    });
    return false;
}

$("#family_grandparents_backward").on("click", function () {
    counter_gp--;
    if (counter_gp <= 0) {
        setgp(obj_gp);
    }
});

$("#family_grandparents_foward").on("click", function () {
    counter_gp++;
    if (counter_gp < objcount_gp) {
        setgp(obj_gp);
    }
});

//set grandparents
function setgp(obj_gp) {

    if (counter_gp + 1 >= objcount_gp) {
        $("#family_grandparents_foward").hide();
    } else {
        $("#family_grandparents_foward").show();
    }

    if (counter_gp <= 0) {
        $("#family_grandparents_backward").hide();
    } else {
        $("#family_grandparents_backward").show();
    }

    grandparentsfamilyhistoryid = obj_gp.familymedicalhistory[counter_gp][0];
    $("#grandparents_name").val(obj_gp.familymedicalhistory[counter_gp][1]);
    document.getElementById('grandparents_healthstatus').value = obj_gp.familymedicalhistory[counter_gp][2];

    if (obj_gp.familymedicalhistory[counter_gp][3] === "arthritis-yes") {
        $("#grandparents_arthritis").prop("checked", true);
    } else {
        $("#grandparents_arthritis").prop("checked", false);
    }

    if (obj_gp.familymedicalhistory[counter_gp][4] === "cancer-yes") {
        $("#grandparents_cancer").prop("checked", true);
    } else {
        $("#grandparents_cancer").prop("checked", false);
    }

    if (obj_gp.familymedicalhistory[counter_gp][5] === "diabetes-yes") {
        $("#grandparents_diabetes").prop("checked", true);
    } else {
        $("#grandparents_diabetes").prop("checked", false);
    }

    if (obj_gp.familymedicalhistory[counter_gp][6] === "heartCondition-yes") {
        $("#grandparents_heartCondition").prop("checked", true);
    } else {
        $("#grandparents_heartCondition").prop("checked", false);
    }

    if (obj_gp.familymedicalhistory[counter_gp][7] === "lungdisease-yes") {
        $("#grandparents_lungdisease").prop("checked", true);
    } else {
        $("#grandparents_lungdisease").prop("checked", false);
    }

    if (obj_gp.familymedicalhistory[counter_gp][8] === "mentalillness-yes") {
        $("#grandparents_mentalillness").prop("checked", true);
    } else {
        $("#grandparents_mentalillness").prop("checked", false);
    }

    if (obj_gp.familymedicalhistory[counter_gp][9] === "stroke-yes") {
        $("#grandparents_stroke").prop("checked", true);
    } else {
        $("#grandparents_stroke").prop("checked", false);
    }

    if (obj_gp.familymedicalhistory[counter_gp][10] === "other-no") {
        $("#grandparents_other").prop("checked", false);
    } else {
        $("#grandparents_other").prop("checked", true);
        $("#grandparents_othertxt").val(obj_gp.familymedicalhistory[counter_gp][10]);
        $("#grandparents_othertxt").show();
    }
}