//get profile details
function getprofiledetails() {
    getprofimg();
}

//check required
$("input,select").focus(function () {
    var input_id = $(this).attr("id");
    if ($("#" + input_id).hasClass("input-alert"))
        $("#" + input_id).removeClass("input-alert");
});

//validate who
function who() {

    /***** Internet check ***/
    if (getConnection() === "false") {
        return false;
    }

    var title = $("#who_title").val();
    var fname = $("#who_fname").val();
    var lname = $("#who_lname").val();
    var dob = $("#who_dob").val();
    var sex = $("#who_sex").val();
    var status = $("#who_status").val();
    var today = new Date();
    var dd = today.getDate();
    var mm = today.getMonth() + 1; //January is 0!

    var yyyy = today.getFullYear();
    if (dd < 10) {
        dd = '0' + dd;
    }
    if (mm < 10) {
        mm = '0' + mm;
    }
    var today = yyyy + '-' + mm + '-' + dd;

    var middlenamecheck = true;

    if ($("#who_mname").val() && !isNaN($("#who_mname").val())) {
        middlenamecheck = false;
    } else if ($("#who_mname").val() === "") {
        middlenamecheck = true;
    } else {
        middlenamecheck = true;
    }
    if (middlenamecheck === true) {

        if (title && fname && lname && dob && sex && status) {

            if ($("#who_dob").val()) {

                if (myFunction($("#who_dob").val())) {

                    if (dob <= today) {
                        $("#status_out").show();
                        setwho();
                    }
                    else {
                        $("#errormsg-alert").html(dateofbirth);
                        $("#errormsg-alert").show();
                        var myVar1 = setInterval(function () {
                            $("#errormsg-alert").hide();
                            clearInterval(myVar1);
                        }, 3000);
                    }
                }
                else {
                    $("#who_dob").addClass("input-alert");
                    $("#errormsg-alert").html(dateofbirth);
                    $("#errormsg-alert").show();
                    var myVar1 = setInterval(function () {
                        $("#errormsg-alert").hide();
                        clearInterval(myVar1);
                    }, 3000);
                }
            }
        }
        else {
            var contact = ["#who_title", "#who_fname", "#who_lname", "#who_dob", "#who_status", "#who_sex"];
            for (i = 0; i < contact.length; i++) {
                var current = $(contact[i]).val();
                if (current === "" || !isNaN(current)) {
                    err_msg = "Please enter ";
                    $(contact[i]).addClass("input-alert");
                }
            }
        }
    } else {
        $("#who_mname").addClass("input-alert");
    }
}

// set who
function setwho() {

    $("#submit,#cancel").prop("disabled", true);

    $.ajax({
        url: path + "HospitalManagementSystem/HMS/Profile/addwho.jsp",
        type: "POST",
        data: $("#who").serialize() + "&requesttype=update",
        success: function (data) {
            var msg = jQuery.trim(data);
            if (msg === "success") {
                $("#status_out").hide();
                $("#successfull-send").html(successfulladddetails);
                $("#successfull-send").show();
                var myVar1 = setInterval(function () {
                    $("#successfull-send").hide();
                    $("#submit,#cancel").removeProp("disabled");
                    clearInterval(myVar1);
                }, 3000);
                getage();
            }
            else {
                $("#status_out").hide();
                $("#errormsg-alert").html(unsuccessfulladddetails);
                $("#errormsg-alert").show();
                var myVar1 = setInterval(function () {
                    $("#errormsg-alert").hide();
                    $("#submit,#cancel").removeProp("disabled");
                    clearInterval(myVar1);
                }, 3000);
            }
        },
        error: function () {
            $("#status_out").hide();
            $("#errormsg-alert").html(servererror);
            $("#errormsg-alert").show();
            var myVar1 = setInterval(function () {
                $("#errormsg-alert").hide();
                $("#submit,#cancel").removeProp("disabled");
                clearInterval(myVar1);
            }, 3000);
        }
    });
    return false;
}

//get who
function getwho() {

    $.ajax({
        url: path + "HospitalManagementSystem/HMS/Profile/getwho.jsp",
        type: "POST",
        success: function (data) {
            var msg = jQuery.trim(data);
            if (msg) {
                obj = JSON.parse(msg);
                if (obj.userProfile.length > 0) {

                    $("#who_title").val(obj.userProfile[0][0]);
                    $("#who_fname").val(obj.userProfile[0][1]);

                    if (obj.userProfile[0][3] !== "nil") {
                        $("#who_mname").val(obj.userProfile[0][3]);
                    } else {
                        $("#who_mname").val("");
                    }

                    $("#who_lname").val(obj.userProfile[0][2]);
                    $("#who_dob").val(obj.userProfile[0][4]);
                    $("#who_sex").val(obj.userProfile[0][5]);
                    $("#who_status").val(obj.userProfile[0][6]);

                    if (obj.userProfile[0][7] !== "nil") {
                        $("#who_drivers_license").val(obj.userProfile[0][7]);
                    } else {
                        $("#who_drivers_license").val("");
                    }

                    if (obj.userProfile[0][8] !== "nil") {
                        $("#who_ss").val(obj.userProfile[0][8]);
                    } else {
                        $("#who_ss").val("");
                    }
                }
            }
            $("#preload_overlay").hide();
        },
        error: function () {
            $("#preload_overlay").hide();
            $("#errormsg-alert").html(servererror);
            $("#errormsg-alert").show();
            var myVar1 = setInterval(function () {
                $("#errormsg-alert").hide();
                clearInterval(myVar1);
            }, 3000);
        }
    });
    return false;
}

var imgcount = 0;
$(function () {
    $("#profileimg").on('load', function () {
        imgcount++;
    });
});

function profimgupload() {

    /***** Internet check ***/
    if (getConnection() === "false") {
        return false;
    }
    $("#add_prof_img").prop("disabled", true);

    profile64base = $("#profileimg").attr("src").split("base64,")[1];

    if (((imgcount - 1) > 0)) {
        $("#status_out_medcat").show();

        $.ajax({
            url: path + "HospitalManagementSystem/HMS/Profile/addprofileimg.jsp",
            type: "POST",
            data: {
                "fm": $("#profileimg").attr("src").split("base64,")[1]
            },
            success: function (data) {
                var msg = jQuery.trim(data);
                if (msg === "success") {
                    $("#status_out_medcat").hide();
                    $("#successfull-send_medcat").html(successfulluploadprofileimage);
                    $("#successfull-send_medcat").show();
                    var myVar1 = setInterval(function () {
                        $("#successfull-send_medcat").hide();
                        $("#add_prof_img").removeProp("disabled");
                        clearInterval(myVar1);
                    }, 3000);
                    getprofimg();
                    imgcount = 0;
                } else {
                    imgcount = 0;
                    $("#status_out_medcat").hide();
                    $("#errormsg-alert_medcat").html(unsuccessfulluploadprofileimage);
                    $("#errormsg-alert_medcat").show();
                    var myVar1 = setInterval(function () {
                        $("#errormsg-alert_medcat").hide();
                        $("#add_prof_img").removeProp("disabled");
                        clearInterval(myVar1);
                    }, 3000);
                }
            }, error: function () {
                $("#status_out_medcat").hide();
                $("#errormsg-alert_medcat").html(servererror);
                $("#errormsg-alert_medcat").show();
                var myVar1 = setInterval(function () {
                    $("#errormsg-alert_medcat").hide();
                    $("#add_prof_img").removeProp("disabled");
                    clearInterval(myVar1);
                }, 3000);
            }
        });
    }
}

//getprofimg
function getprofimg() {

    $("#preload_overlay").show();
    $.ajax({
        url: path + "HospitalManagementSystem/HMS/Profile/getprofileimage.jsp",
        type: "POST",
        success: function (data) {
            var msg = jQuery.trim(data);
            if (msg) {
                $("#profileimg").attr("src", msg);
                parent.getprofimg();
            } else {
                $("#profileimg").attr("src", "../../img/default.png");
                parent.getprofimg();
            }
            getwho();
        },
        error: function () {
            getwho();
        }
    });
    return false;
}

//previewFile
function previewFile() {

    var preview = document.getElementById('profileimg');
    var file = document.querySelector('input[type=file]').files[0];
    var reader = new FileReader();

    reader.onloadend = function () {
        preview.src = reader.result;
    }
    if (file) {
        reader.readAsDataURL(file);
    } else {
        preview.src = "../../img/default.png";
    }
}

///**** Script capture via webcam ****/
var video = document.querySelector("#videoElement");
navigator.getUserMedia = navigator.getUserMedia || navigator.webkitGetUserMedia || navigator.mozGetUserMedia || navigator.msGetUserMedia || navigator.oGetUserMedia;

if (navigator.getUserMedia) {
    // get webcam feed if available
    navigator.getUserMedia({
        video: true
    }, handleVideo, videoError);
}

function handleVideo(stream) {
    // if found attach feed to video element
    video.src = window.URL.createObjectURL(stream);
}

function videoError(e) {
    $("#video_error").show();
}
var v, canvas, context, w, h;
var imgtag = document.getElementById('profileimg'); // get reference to img tag

document.addEventListener('DOMContentLoaded', function () {
    // get canvas 2D context
    v = document.getElementById('videoElement');
    canvas = document.getElementById('canvas');
    context = canvas.getContext('2d');
    w = canvas.width;
    h = canvas.height;
}, false);

function draw(v, c, w, h) {
    if (v.paused || v.ended)
        return false; // if no video, exit here
    context.drawImage(v, 0, 0, w, h); // draw video feed to canvas
    var uri = canvas.toDataURL("image/png"); // convert canvas to data URI
    imgtag.src = uri; // add URI to IMG tag src
}

document.getElementById('capture').addEventListener('click', function (e) {
    draw(v, context, w, h);
    $('#takepic').modal('hide');
    $capture = $("#profileimg").attr("src").split("data:image/png;base64,");
});

// close the model after capture
$(".close").on("click", function () {
    $('#takepic').modal('hide');

});