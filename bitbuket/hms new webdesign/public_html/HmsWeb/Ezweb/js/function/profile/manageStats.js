//check required
$("input,select").focus(function () {
    var input_id = $(this).attr("id");
    if ($("#" + input_id).hasClass("input-alert"))
        $("#" + input_id).removeClass("input-alert");
});

var other_stats = ["stats_homeless"];
//checkisnan
function checkisnan(passed) {
    var check = 0;
    for (var x = 0; x < passed.length; x++) {
        if ($.trim($("#" + passed[x]).val())) {
            if (!isNaN($.trim($("#" + passed[x]).val()))) {
                $("#" + passed[x]).addClass("input-alert");
                check++;
            }
        }
    }
    return check;
}
// validate stats
function stats() {

    var required = [];
    fields = $("form#stats").serialize();
    form_array = fields.split('&');

    for (var i = 0; i < form_array.length; i++) {
        name = form_array[i].split('=')[0];
        value = form_array[i].split('=')[1];

        if (name === "stats_monthly_income") {
        } else {

            required[i] = value;

            if ($.trim(value) === "") {
                var name_of_input = "#" + name;

                if (name === "stats_language") {
                    $(name_of_input).addClass("input-alert");

                } else {
                    $(name_of_input).addClass("input-alert");
                }
            }
        }
    }

    count = 0;

    for (var i = 0; i <= required.length; i++) {
        if ($.trim(required[i]) === "") {
            count++;
        }
    }

    if (isNaN($("#stats_family_size").val())) {
        $("#stats_family_size").addClass("input-alert");
    }

    if ((count - 1) === 0 && !isNaN($("#stats_family_size").val())) {

        if (checkisnan(other_stats) === 0) {
            setstats();
        }
    }
}

//set status
function setstats() {
    var monthlyincome = ($("#stats_monthly_income").val() == "" || $("#stats_monthly_income").val() == null || $("#stats_monthly_income").val() == " ") ? "00.00" : $("#stats_monthly_income").val();

    /***** Internet check ***/
    if (getConnection() === "false") {
        return false;
    }

    $("#status_out").show();
    $("#submit,#cancel").prop("disabled", true);

    $.ajax({
        url: path + "HospitalManagementSystem/HMS/Profile/addstats.jsp",
        type: "POST",
        data: {stats_language: $("#stats_language").val(),
            stats_ethnicity: $("#stats_ethnicity").val(),
            stats_family_size: $("#stats_family_size").val(),
            stats_monthly_income: monthlyincome},
        success: function (data) {
            var msg = jQuery.trim(data);
            if (msg === "success") {
                $("#status_out").hide();
                $("#successfull-send").html(successfullsend);
                $("#successfull-send").show();
                var myVar1 = setInterval(function () {
                    $("#successfull-send").hide();
                    $("#submit,#cancel").removeProp("disabled");
                    clearInterval(myVar1);
                }, 3000);
            } else {
                $("#status_out").hide();
                $("#errormsg-alert").html(unsuccessfullsend);
                $("#errormsg-alert").show();
                var myVar1 = setInterval(function () {
                    $("#errormsg-alert").hide();
                    $("#submit,#cancel").removeProp("disabled");
                    clearInterval(myVar1);
                }, 3000);
            }
        },
        error: function () {
            $("#status_out").hide();
            $("#errormsg-alert").html(servererror);
            $("#errormsg-alert").show();
            var myVar1 = setInterval(function () {
                $("#errormsg-alert").hide();
                $("#submit,#cancel").removeProp("disabled");
                clearInterval(myVar1);
            }, 3000);
        }
    });
    return false;
}

//getstats
function getstats() {

    $("#preload_overlay").show();
    $.ajax({
        url: path + "HospitalManagementSystem/HMS/Profile/getstats.jsp",
        type: "POST",
        success: function (data) {
            var msg = jQuery.trim(data);
            if (msg) {
                obj = JSON.parse(msg);
                if (obj.userStats.length > 0) {
                    $("#stats_language").val(obj.userStats[0][0]);
                    $("#stats_ethnicity").val(obj.userStats[0][1]);
                    $("#stats_family_size").val(obj.userStats[0][2]);
                    if (obj.userStats[0][5] != "00.00") {
                        $("#stats_monthly_income").val(obj.userStats[0][3]);
                    } else {
                        $("#stats_monthly_income").val("");
                    }
                }
            }
            $("#preload_overlay").hide();
        },
        error: function () {
            $("#preload_overlay").hide();
            $("#errormsg-alert").html(servererror);
            $("#errormsg-alert").show();
            var myVar1 = setInterval(function () {
                $("#errormsg-alert").hide();
                clearInterval(myVar1);
            }, 3000);
        }
    });
    return false;
}