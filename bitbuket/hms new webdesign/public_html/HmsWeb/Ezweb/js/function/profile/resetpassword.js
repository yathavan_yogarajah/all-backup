$("input,select").focus(function () {
    $input_id = $(this).attr("id");
    if ($("#" + $input_id).hasClass("input-alert"))
        $("#" + $input_id).removeClass("input-alert");
});

//clearall
function clearall() {
    $("#old_password").val("");
    $("#new_password").val("");
    $("#verify_new_password").val("");
}

//reset
function reset() {
    /***** Internet check ***/
    if (getConnection() === "false") {
        return false;
    }

    var error = 0;

    if ($("#old_password").val() === "" || $("#old_password").val() === " ") {
        $("#old_password").addClass("input-alert");
        error++;
    }

    if ($("#new_password").val() === "" || $("#new_password").val() === " ") {
        $("#new_password").addClass("input-alert");
        error++;
    }

    if ($("#verify_new_password").val() === "" || $("#verify_new_password").val() === " ") {
        $("#verify_new_password").addClass("input-alert");
        error++;
    }

    if (error === 0) {

        if ($("#new_password").val() === $("#verify_new_password").val()) {

            $("#status_out").show();
            $("#submit,#cancel").prop("disabled", true);

            $.ajax({
                url: path + "HospitalManagementSystem/HMS/User/newpassword.jsp",
                type: "POST",
                data: $("#password-change").serialize(),
                success: function (data) {
                    var msg = jQuery.trim(data);
                    if (msg === "success") {
                        $("#status_out").hide();
                        $("#successfull-send").html(successfullreset);
                        $("#successfull-send").show();
                        var myVar1 = setInterval(function () {
                            $("#successfull-send").hide();
                            clearall();
                            $("#submit,#cancel").removeProp("disabled");
                            clearInterval(myVar1);
                        }, 3000);
                    } else if (msg === "notold") {

                        $("#status_out").hide();
                        $("#errormsg-alert").html(mismatcholdpass);
                        $("#errormsg-alert").show();
                        var myVar1 = setInterval(function () {
                            $("#errormsg-alert").hide();
                            $("#submit,#cancel").removeProp("disabled");
                            clearInterval(myVar1);
                        }, 3000);
                    } else {
                        $("#status_out").hide();
                        $("#errormsg-alert").html(unsuccessfullreset);
                        $("#errormsg-alert").show();
                        var myVar1 = setInterval(function () {
                            $("#errormsg-alert").hide();
                            $("#submit,#cancel").removeProp("disabled");
                            clearInterval(myVar1);
                        }, 3000);
                    }
                },
                error: function () {
                    $("#status_out").hide();
                    $("#errormsg-alert").html(servererror);
                    $("#errormsg-alert").show();
                    var myVar1 = setInterval(function () {
                        $("#errormsg-alert").hide();
                        $("#submit,#cancel").removeProp("disabled");
                        clearInterval(myVar1);
                    }, 3000);
                }
            });
            return false;
        } else {
            $("#errormsg-alert").html(newpasswordmismatch);
            $("#errormsg-alert").show();
            var myVar1 = setInterval(function () {
                $("#errormsg-alert").hide();
                clearInterval(myVar1);
            }, 3000);
        }
    }
}
