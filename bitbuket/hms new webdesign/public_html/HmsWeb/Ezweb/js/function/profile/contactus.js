$(function () {
    $("input,textarea").focus(function () {
        var input_id = $(this).attr("id");
        $("#" + input_id).removeClass("input-alert");
    });

});

//validateEmail
function validateEmail(Email) {

    var filter = /^[\w\-\.\+]+\@[a-zA-Z0-9\.\-]+\.[a-zA-z0-9]{2,4}$/;
    if (filter.test(Email)) {
        return true;
    } else {
        return false;
    }
}

//get email
function getemail() {

    $.ajax({
        url: path + "HospitalManagementSystem/HMS/Profile/getemail.jsp",
        type: "POST",
        success: function (data) {
            var msg = jQuery.trim(data);
            $("#sender_email").val(msg);
        }
    });
    return false;
}

// send contact email
function contact() {

    var email = $("#sender_email").val();
    var reason = $("#reason").val();
    var emailvalidation = validateEmail(email);

    if (emailvalidation === true && reason !== "" && reason !== " ") {
        $("#status_out").show();
        $("#submit,#cancel").prop("disabled", true);
        $.ajax({
            url: path + "HospitalManagementSystem/HMS/Profile/sendhelp.jsp",
            type: "POST",
            data: {
                "content": reason,
                "email": email
            },
            success: function (data) {
                var msg = jQuery.trim(data);
                if (msg === "success") {
                    $("#status_out").hide();
                    $("#successfull-send").html(successfulladddetails);
                    $("#successfull-send").show();
                    var hidestatus = setInterval(function () {
                        $("#successfull-send").hide();
                        $("#reason").val("");
                        $("#submit,#cancel").removeProp("disabled");
                        clearInterval(hidestatus);
                    }, 3000);
                } else {
                    $("#status_out").hide();
                    $("#errormsg-alert").html(unsuccessfulladddetails);
                    $("#errormsg-alert").show();
                    var hidestatus = setInterval(function () {
                        $("#errormsg-alert").hide();
                        $("#submit,#cancel").removeProp("disabled");
                        clearInterval(hidestatus);
                    }, 3000);
                }
            },
            error: function () {
                $("#status_out").hide();
                $("#errormsg-alert").html(servererror);
                $("#errormsg-alert").show();
                var hidestatus = setInterval(function () {
                    $("#errormsg-alert").hide();
                    $("#submit,#cancel").removeProp("disabled");
                    clearInterval(hidestatus);
                }, 3000);
            }
        });
        return false;
    } else {
        if ($("#reason").val() === "" || $("#reason").val() === null) {
            $("#reason").addClass("input-alert");
        }
        if ($("#sender_email").val() === "" || $("#sender_email").val() === null || emailvalidation === false) {
            $("#sender_email").addClass("input-alert");
        }
    }
}
