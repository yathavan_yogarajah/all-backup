//validate dates
function myFunction(date) {
    var rgx = /(\d{4})-(\d{2})-(\d{2})/;
    var results = rgx.test(date);
    return results;
}

//validates date and time
function dateandtime(datetime) {
var rgx_date = /(\d{4})-(\d{2})-(\d{2}) (\d{2}):(\d{2})/;
   var results_date = rgx_date.test(datetime);
    return results_date;
}