//page transection script
function transitionPage() {
    // Hide to left / show from left
    $("#main").toggle("slide", {
        direction: "left"
    }, 500);

    // Show from right / hide to right
    $("#dvlogin").toggle("slide", {
        direction: "right"
    }, 500);
}

function transitionrPage() {
    // Hide to left / show from left
    $("#main").toggle("slide", {
        direction: "left"
    }, 500);

    // Show from right / hide to right
    $("#dvreg").toggle("slide", {
        direction: "right"
    }, 500);

}

function transitionback() {
    // Hide to left / show from left
    $("#main").toggle("slide", {
        direction: "left"
    }, 500);

    // Show from right / hide to right
    $("#main-sub").toggle("slide", {
        direction: "right"
    }, 500);
}