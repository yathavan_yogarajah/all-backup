//check required
$("input,select").focus(function () {
    $input_id = $(this).attr("id");
    if ($("#" + $input_id).hasClass("input-alert"))
        $("#" + $input_id).removeClass("input-alert");
});

//function date check
function datecheck() {

    datevalidate = 0;
    dateres = true;

    if ($("#weight_date").val()) {
        if (myFunction($("#weight_date").val()) === false) {
            $("#weight_date").addClass("input-alert");
            datevalidate++;
        }
    }
    if (datevalidate !== 0) {
        dateres = false;
    }
    return dateres;
}

//validate weight
function weight() {

    var required = [];
    $fields = $("form#weight_form").serialize();
    form_array = $fields.split('&');

    for (var i = 0; i < form_array.length; i++) {

        name = form_array[i].split('=')[0];
        value = form_array[i].split('=')[1];
        required[i] = value;

        if ($.trim(value) === "") {
            if (name === "fatmass") {
            } else {
                var name_of_input = "#" + name;
                $(name_of_input).addClass("input-alert");
            }
        }
    }

    count = 0;
    for ($i = 0; $i <= required.length; $i++) {
        if ($.trim(required[$i]) === "" && $i!=2 ) {
                count++;
        }
    }

    if ($("#weight").val() < 0) {
        $("#weight").addClass("input-alert");
        $("#errormsg-alert-weight").html(numbererror);
        $("#errormsg-alert-weight").show();
        var myVar1 = setInterval(function () {
            $("#errormsg-alert-weight").hide();
            clearInterval(myVar1);
        }, 3000);
        count++;
    }

    if ((count - 1) === 0) {
        if (datecheck()) {
            setweight();
        } else {
            $("#errormsg-alert-weight").html(dateerror);
            $("#errormsg-alert-weight").show();
            var myVar1 = setInterval(function () {
                $("#errormsg-alert-weight").hide();
                clearInterval(myVar1);
            }, 3000);
        }
    }
}


function setweight() {

    /**** check internetconnection ******/
    if (getConnection() === "false") {
        return false;
    }

    var weight_date = new Date($("#weight_date").val());
    var today = new Date();
    var dd = today.getDate();
    var mm = today.getMonth() + 1; //January is 0!
    var yyyy = today.getFullYear();

    if (dd < 10) {
        dd = '0' + dd;
    }
    if (mm < 10) {
        mm = '0' + mm;
    }

    var today = yyyy + '-' + mm + '-' + dd;
    var datestatus;

    if ($("#weight_date").val() !== "" && $("#weight_date").val() !== " " && $("#weight_date").val() !== null) {
        if (weight_date <= new Date(today)) {
            datestatus = true;
        } else {
            datestatus = false;
        }
    } else {
        datestatus = false;
    }

    if (datestatus === true) {
        $("#status_out_add_weight").show();
        $("#submit-weight,#cancel-weight").prop("disabled", true);
        var date = $("#weight_date").val();
        var weight = $("#weight").val();
        var fatmass = $("#fatmass").val();
        if (fatmass === "" || fatmass === " " || fatmass === null) {
            fatmass = "00.00";
        }
        $.ajax({
            url: path + "HospitalManagementSystem/HMS/Issues/setmanualweight.jsp",
            type: "POST",
            data: {
                date: date,
                weight: weight,
                fatmass: fatmass,
                metric: $("#weight_units").val()
            },
            success: function (data) {
                var msg = jQuery.trim(data);
                if (msg === "success") {
                    $("#status_out_add_weight").hide();
                    $("#successfull-send-weight").html(successfulladddetails);
                    $("#successfull-send-weight").show();
                    var myVar1 = setInterval(function () {
                        $("#successfull-send-weight").hide();
                        document.getElementById("weight_form").reset();
                        $("#submit-weight,#cancel-weight").removeProp("disabled");
                        clearInterval(myVar1);
                    }, 3000);
                } else {
                    $("#status_out_add_weight").hide();
                    $("#errormsg-alert-weight").html(unsuccessfulladddetails);
                    $("#errormsg-alert-weight").show();
                    var myVar1 = setInterval(function () {
                        $("#errormsg-alert-weight").hide();
                        $("#submit-weight,#cancel-weight").removeProp("disabled");
                        clearInterval(myVar1);
                    }, 3000);
                }
            },
            error: function () {
                $("#status_out_add_weight").hide();
                $("#errormsg-alert-weight").html(servererror);
                $("#errormsg-alert-weight").show();
                var myVar1 = setInterval(function () {
                    $("#errormsg-alert-weight").hide();
                    $("#submit-weight,#cancel-weight").removeProp("disabled");
                    clearInterval(myVar1);
                }, 3000);
            }
        });
        return false;
    }
    else {
        $("#weight_date").addClass("input-alert");
        $("#errormsg-alert-weight").html(datecheckerror);
        $("#errormsg-alert-weight").show();
        var myVar1 = setInterval(function () {
            $("#errormsg-alert-weight").hide();
            clearInterval(myVar1);
        }, 3000);
    }
}
