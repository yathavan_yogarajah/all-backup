//food type click
$("#glucose-foodtype").click(function () {
    $(".overlayother").show("fast", function () {
        $("#foodtypes_main").show();
        $("#foodtypes_main").animate({
            "bottom": "0px"
        }, 300);
    });
});

//close
$(".overlayother,#food_close").click(function () {
    $(".overlayother").hide("fast", function () {
        $("#foodtypes_main").animate({
            "bottom": "-350px"
        }, 300).hide(500);
    });
});

foods = [];

//food icon click
$(".food_icoall").click(function () {

    var currimg = $(this).attr("src").split("food_ico/")[1];
    var dynimg_ori = $.trim(currimg.split("_")[0]);
    var dynimg = currimg.split("_")[1];
    var foodname = $.trim($(this).attr("title"));
    var inarrfood = foods.indexOf(foodname);

    if (dynimg === "g.png") {

        foods.push(foodname);
        $(this).attr("src", "../../icons/food_ico/" + dynimg_ori + "_c.png");

    } else if (dynimg === "c.png") {

        if (inarrfood !== -1) {
            foods.splice(inarrfood, 1);
        }

        $(this).attr("src", "../../icons/food_ico/" + dynimg_ori + "_g.png");
    }

});

function glugose() {
   
    var currentdate = new Date();
    var date = currentdate.getFullYear() + "-" + (currentdate.getMonth() + 1) + "-" + currentdate.getDate() + " " + currentdate.getHours() + ":" + currentdate.getMinutes() + ":" + currentdate.getSeconds();
    var formdata = $("#glucose_form").serialize() + "&foods=" + foods;

    if ($("#blood_glucose").val()) {
        $("#status_out_add_glucose").show();
        $("#submit-glucose,#cancel-glucose").prop("disabled", true);

        var meal_desc = $("#glucose_meal").val() !== "" ? $("#glucose_meal").val() : "nil";

        $.ajax({
            url: path + "HospitalManagementSystem/HMS/Issues/setglucose.jsp",
            type: "POST",
            data: {
                bloodglucose: $("#blood_glucose").val(),
                glucose_meal: meal_desc,
                foods: foods.toString(),
                date: date
            }, success: function (data) {
                var msg = jQuery.trim(data);
                if (msg === "success") {
                    $("#status_out_add_glucose").hide();
                    $("#successfull-send-glucose").html(successfulladddetails);
                    $("#successfull-send-glucose").show();
                    var myVar1 = setInterval(function () {
                        $("#successfull-send-glucose").hide();
                        $("#submit-glucose,#cancel-glucose").removeProp("disabled");
                        document.getElementById("glucose_form").reset();
                        clearInterval(myVar1);
                    }, 3000);
                } else {
                    $("#status_out_add_glucose").hide();
                    $("#errormsg-alert-glucose").html(unsuccessfulladddetails);
                    $("#errormsg-alert-glucose").show();
                    var myVar1 = setInterval(function () {
                        $("#errormsg-alert-glucose").hide();
                        $("#submit-glucose,#cancel-glucose").removeProp("disabled");
                        clearInterval(myVar1);
                    }, 3000);
                }
            },
            error: function () {
                $("#status_out_add_glucose").hide();
                $("#errormsg-alert-glucose").html(servererror);
                $("#errormsg-alert-glucose").show();
                var myVar1 = setInterval(function () {
                    $("#errormsg-alert-glucose").hide();
                    $("#submit-glucose,#cancel-glucose").removeProp("disabled");
                    clearInterval(myVar1);
                }, 3000);
            }
        });
    } else {
        $("#blood_glucose").addClass("input-alert");
    }

}
