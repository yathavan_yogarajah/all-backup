// validate children details
$("input,select").focus(function () {
    input_id = $(this).attr("id");
    if ($("#" + input_id).hasClass("input-alert"))
        $("#" + input_id).removeClass("input-alert");
});

//collect data
fatherdata = "";
fatherdatacheck = [];
collectdata("family_father");

function collectdata(formname) {
    $("#" + formname).find("input[type=checkbox]").each(function () {
        fatherdatacheck.push($(this).attr("id"));
    });
}
// save father
function setvaluefather() {

    fatherdata = "";

    for (var x = 0; x < fatherdatacheck.length; x++) {
        var id = fatherdatacheck[x];
        var id_check = id.split("_")[1];

        if ($("#" + fatherdatacheck[x]).is(":checked")) {
            if (fatherdatacheck[x] === "father_other") {
                y = id + "=" + $("#father_othertxt").val();
            } else {
                y = id + "=" + id_check + "-yes";
            }
        } else {
            y = id + "=" + id_check + "-no";
        }
        fatherdata += y + "&";
        y = "";
    }
}
function save_father() {

    setvaluefather();

    var fathername = $("#father_name").val();
    var fatherhealth = $("#father_healthstatus").val();
    var err = 0;
    if (fathername === "" || fathername === null || fathername === " ") {
        err++;
        $("#father_name").addClass("input-alert");
    }

    if (fatherhealth === "" || fatherhealth === null || fatherhealth === " ") {
        err++;
        $("#father_healthstatus").addClass("input-alert");
    }

    if (err === 0) {

        if ($("#father_other").is(":checked") === true) {
            if (!$("#father_othertxt").val()) {
                $("#father_othertxt").addClass("input-alert");
                return false;
            }

        }
        datasubmit = fatherdata + "father_name=" + fathername + "&father_healthstatus=" + fatherhealth;
        setfather();
    }

}

var fatherfamilyhistoryid = 0;

//setfather
function setfather() {

    /***** Internet check ***/
    if (getConnection() === "false") {
        return false;
    }

    $("#status_out").show();
    $("#father_submit,#father_cancel").prop("disabled", true);
    
    $.ajax({
        url: path + "HospitalManagementSystem/HMS/Issues/setfathermedicalhistory.jsp",
        type: "POST",
        data: datasubmit + "&relationsid=1&relationstatusid=" + fatherfamilyhistoryid,
        success: function (data) {
            var msg = jQuery.trim(data);
            if (msg === "success") {
                $("#status_out").hide();
                getfather();
                $("#successfull-send").html(successfulladddetails);
                $("#successfull-send").show();
                var myVar1 = setInterval(function () {
                    $("#successfull-send").hide();
                    $("#father_submit,#father_cancel").removeProp("disabled");
                    clearInterval(myVar1);
                }, 3000);
            } else {

                $("#status_out").hide();
                $("#errormsg-alert").html(unsuccessfulladddetails);
                $("#errormsg-alert").show();
                var myVar1 = setInterval(function () {
                    $("#errormsg-alert").hide();
                    $("#father_submit,#father_cancel").removeProp("disabled");
                    clearInterval(myVar1);
                }, 3000);
            }
        }, error: function () {
            $("#status_out").hide();
            $("#errormsg-alert").html(servererror);
            $("#errormsg-alert").show();
            var myVar1 = setInterval(function () {
                $("#errormsg-alert").hide();
                $("#father_submit,#father_cancel").removeProp("disabled");
                clearInterval(myVar1);
            }, 3000);
        }
    });
    return false;
}

//get father
function getfather() {
    
    $("#preload_overlay").show();
    
    $.ajax({
        url: path + "HospitalManagementSystem/HMS/Issues/getfamilymedicalhistory.jsp",
        type: "POST",
        data: {
            "getrelationid": 1
        },
        success: function (data) {
            var msg_father = jQuery.trim(data);
            if (msg_father) {
                var obj_father = JSON.parse(msg_father);
                if (obj_father.familymedicalhistory.length > 0) {

                    fatherfamilyhistoryid = obj_father.familymedicalhistory[0][0];
                     $("#father_name").val(obj_father.familymedicalhistory[0][1]);
                    document.getElementById('father_healthstatus').value = obj_father.familymedicalhistory[0][2];

                    if (obj_father.familymedicalhistory[0][3] === "arthritis-yes") {
                        $("#father_arthritis").prop("checked", true);
                    } else {
                        $("#father_arthritis").prop("checked", false);
                    }

                    if (obj_father.familymedicalhistory[0][4] === "cancer-yes") {
                        $("#father_cancer").prop("checked", true);
                    } else {
                        $("#father_cancer").prop("checked", false);
                    }

                    if (obj_father.familymedicalhistory[0][5] === "diabetes-yes") {
                        $("#father_diabetes").prop("checked", true);
                    } else {
                        $("#father_diabetes").prop("checked", false);
                    }

                    if (obj_father.familymedicalhistory[0][6] === "heartCondition-yes") {
                        $("#father_heartCondition").prop("checked", true);
                    } else {
                        $("#father_heartCondition").prop("checked", false);
                    }

                    if (obj_father.familymedicalhistory[0][7] === "lungdisease-yes") {
                        $("#father_lungdisease").prop("checked", true);
                    } else {
                        $("#father_lungdisease").prop("checked", false);
                    }

                    if (obj_father.familymedicalhistory[0][8] === "mentalillness-yes") {
                        $("#father_mentalillness").prop("checked", true);
                    } else {
                        $("#father_mentalillness").prop("checked", false);
                    }

                    if (obj_father.familymedicalhistory[0][9] === "stroke-yes") {
                        $("#father_stroke").prop("checked", true);
                    } else {
                        $("#father_stroke").prop("checked", false);
                    }

                    if (obj_father.familymedicalhistory[0][10] === "other-no") {
                        $("#father_other").prop("checked", false);
                    } else {
                        $("#father_othertxt").show();
                        $("#father_othertxt").val(obj_father.familymedicalhistory[0][10]);
                        $("#father_other").prop("checked", true);
                    }

                } else {
                    fatherfamilyhistoryid = 0;
                }
            }
            $("#preload_overlay").hide();
        },
        error: function () {
            $("#preload_overlay").hide();
            $("#errormsg-alert").html(servererror);
            $("#errormsg-alert").show();
            var myVar1 = setInterval(function () {
                $("#errormsg-alert").hide();
                clearInterval(myVar1);
            }, 3000);
        }
    });
    return false;
}