//check required
$("input,select").focus(function () {
    $input_id = $(this).attr("id");
    if ($("#" + $input_id).hasClass("input-alert"))
        $("#" + $input_id).removeClass("input-alert");
});

//function date check
function datecheck() {

    datevalidate = 0;
    dateres = true;
    if ($("#height_date").val()) {
        if (myFunction($("#height_date").val()) === false) {
            $("#height_date").addClass("input-alert");
            datevalidate++;
        }
    }

    if (datevalidate !== 0) {
        dateres = false;
    }
    return dateres;
}

//validate height
function height() {

    var required = [];
    $fields = $("form#height_form").serialize();
    form_array = $fields.split('&');

    for (var i = 0; i < form_array.length; i++) {
        name = form_array[i].split('=')[0];
        value = form_array[i].split('=')[1];
        required[i] = value;
        if ($.trim(value) === "") {
            var name_of_input = "#" + name;
            $(name_of_input).addClass("input-alert");
        }
    }
    
    count = 0;
    
    for ($i = 0; $i <= required.length; $i++) {
        if ($.trim(required[$i]) === "") {
            count++;
        }
    }

    if ($("#height").val() < 0) {
        $("#height").addClass("input-alert");
        $("#errormsg-alert-height").html(numbererror);
        $("#errormsg-alert-height").show();
        var myVar1 = setInterval(function () {
            $("#errormsg-alert-height").hide();
            clearInterval(myVar1);
        }, 3000);
        count++;
    }

    if ((count - 1) === 0) {
        if (datecheck()) {
            setheight();
        } else {
            $("#errormsg-alert-height").html(dateerror);
            $("#errormsg-alert-height").show();
            var myVar1 = setInterval(function () {
                $("#errormsg-alert-height").hide();
                clearInterval(myVar1);
            }, 3000);
        }
    }
}

//set height
function setheight() {

    /**** check internetconnection ******/
    if (getConnection() === "false") {
        return false;
    }

    var height_date = new Date($("#height_date").val());
    var today = new Date();
    var dd = today.getDate();
    var mm = today.getMonth() + 1; //January is 0!
    var yyyy = today.getFullYear();

    if (dd < 10) {
        dd = '0' + dd;
    }
    if (mm < 10) {
        mm = '0' + mm;
    }

    var today = yyyy + '-' + mm + '-' + dd;
    var datestatus;

    if ($("#height_date").val() !== "" && $("#height_date").val() !== " " && $("#height_date").val() !== null) {
        if (height_date <= new Date(today)) {
            datestatus = true;
        } else {
            datestatus = false;
        }

    } else {
        datestatus = false;
    }

    if (datestatus === true) {
        
        $("#status_out_add_height").show();
        $("#submit-height,#cancel-height").prop("disabled", true);
        
        $.ajax({
            url: path + "HospitalManagementSystem/HMS/Issues/setmanualheight.jsp",
            type: "POST",
            data: {
                height: $("#height").val(),
                date: $("#height_date").val(),
                metric: $("#height_units").val()
            },
            success: function (data) {
                var msg = jQuery.trim(data);
                if (msg === "success") {
                    $("#status_out_add_height").hide();
                    $("#successfull-send-height").html(successfulladddetails);
                    $("#successfull-send-height").show();
                    var myVar1 = setInterval(function () {
                        $("#successfull-send-height").hide();
                        document.getElementById("height_form").reset();
                        $("#submit-height,#cancel-height").removeProp("disabled");
                        clearInterval(myVar1);
                    }, 3000);
                } else {
                    $("#status_out_add_height").hide();
                    $("#errormsg-alert-height").html(unsuccessfulladddetails);
                    $("#errormsg-alert-height").show();
                    var myVar1 = setInterval(function () {
                        $("#errormsg-alert-height").hide();
                        $("#submit-height,#cancel-height").removeProp("disabled");
                        clearInterval(myVar1);
                    }, 3000);
                }
            }, error: function () {
                $("#status_out_add_height").hide();
                $("#errormsg-alert-height").html(servererror);
                $("#errormsg-alert-height").show();
                var myVar1 = setInterval(function () {
                    $("#errormsg-alert-height").hide();
                    $("#submit-height,#cancel-height").removeProp("disabled");
                    clearInterval(myVar1);
                }, 3000);
            }
        });
        return false;
    }
    else {
        $("#height_date").addClass("input-alert");
        $("#errormsg-alert-height").html(datecheckerror);
        $("#errormsg-alert-height").show();
        var myVar1 = setInterval(function () {
            $("#errormsg-alert-height").hide();
            clearInterval(myVar1);
        }, 3000);
    }
}
