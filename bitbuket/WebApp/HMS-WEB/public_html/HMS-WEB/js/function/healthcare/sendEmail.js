window.onload = function () {
    assigns();
    $("#name").html("Center name: " + localStorage.getItem("hospital-name"));
    $("#official-no").html(localStorage.getItem("tp"));
    var centermail = localStorage.getItem("email");
    if (centermail !== "nil" && centermail !== "") {
        $("#center_email").val(centermail);
    } else {
        $("#center_email").val("");
    }
};

$("input,textarea").focus(function () {
    input_id = $(this).attr("id");
    if ($("#" + input_id).hasClass("input-alert"))
        $("#" + input_id).removeClass("input-alert");
});

//device management
$("#device-brand").on("change", function () {

    if ($("#device-brand").val() === "Data From Wearable Device") {
        $("#devicesub-cat").show();
        $("#doc-notes").hide();
        $("#lab-report").hide();
        $("#xray-img").hide();

    } else if ($("#device-brand").val() === "Doctor's Note") {
        $("#doc-notes").show();
        $("#devicesub-cat").hide();
        $("#lab-report").hide();
        $("#xray-img").hide();

    } else if ($("#device-brand").val() === "Lab Report") {
        $("#lab-report").show();
        $("#devicesub-cat").hide();
        $("#doc-notes").hide();
        $("#xray-img").hide();

    } else if ($("#device-brand").val() === "X-Ray Image") {
        $("#xray-img").show();
        $("#lab-report").hide();
        $("#devicesub-cat").hide();
        $("#doc-notes").hide();

    } else {
        $("#devicesub-cat").hide();
        $("#doc-notes").hide();
        $("#lab-report").hide();
    }
});

//go to web
$("#official-web").on("click", function () {
    var url = localStorage.getItem("web");
    if (url === "nil" || url === "") {
        $("#user-alert").show();
        var myVar1 = setInterval(function () {
            $("#user-alert").hide();
            clearInterval(myVar1);
        }, 3000);
    }
    else {
        if (url.indexOf("http://") !== -1) {
            window.location.assign(url);
        } else {
            window.location.assign("http://" + url);
        }
    }
});

$("#submit").on("click", function () {
    $('#device').modal('hide');
    setdata();
    $('#device-brand option:first-child').attr("selected", "selected");
    $("#devicesub-cat").hide();
});

$("#close-modeldevice").on("click", function () {
    $('#device').modal('hide');
    localStorage.setItem("dataadd", "no");
    setdata();
    $('#device-brand option:first-child').attr("selected", "selected");
    $("#devicesub-cat").hide();
});

$("#close-modelpic").on("click", function () {
    $('#takepic').modal('hide');
});

//validateEmail
function validateEmail(Email) {
    
    var filter = /^[\w\-\.\+]+\@[a-zA-Z0-9\.\-]+\.[a-zA-z0-9]{2,4}$/;
    if (filter.test(Email)) {
        return true;
    } else {
        return false;
    }
}

//check above 10 times
function checkuserStatus() {
    
    $("#preload_overlay_light").show();
    // $("#pay").modal('show');
    $.ajax({
        url: path + "HospitalManagementSystem/HMS/EmailSend/checkpermission.jsp",
        type: "POST",
        data: {},
        success: function (data) {
            var msg = jQuery.trim(data);
            if (msg === "permission") {
                $("#preload_overlay_light").hide();
                sendEmail();
            } else if (msg === "required") {
                $("#preload_overlay_light").hide();
                $("#pay").modal('show');
            }
        },
        error: function () {
            $("#status_out").hide();
            $("#errormsg-alert").html(servererror);
            $("#errormsg-alert").show();
            var myVar1 = setInterval(function () {
                $("#errormsg-alert").hide();
                $("#send").removeProp("disabled");
                clearInterval(myVar1);
            }, 3000);
            return false;
        }
    });
}

// send email
function sendEmail() {

    /***** Internet check ***/
    if (getConnection() === "false") {
        return false;
    }

    //firstupload();
    var reason = $("#reason").val();
    var email = $("#center_email").val();
    var centerid = localStorage.getItem("centerid");
    var centertype = localStorage.getItem("centertype");
    var today = new Date();
    var dd = today.getDate();
    var mm = today.getMonth() + 1;
    var yyyy = today.getFullYear();

    if (dd < 10) {
        dd = '0' + dd;
    }
    if (mm < 10) {
        mm = '0' + mm;
    }
    var todaydate = yyyy + '-' + mm + '-' + dd+ " " + today.getHours() + ":" + today.getMinutes() + ":" + today.getSeconds();

    var emailvalidation = validateEmail(email);

    if (emailvalidation === true && reason !== "" && reason !== " ") {

        if (localStorage.getItem("is_booked") === "yes") {
            var bookdate = $("#bookdate").val().split(" ")[0];
            if (dateandtime($("#bookdate").val()) === false || todaydate > bookdate) {
                $("#bookdate").addClass("input-alert");
                return false;
            }
        }

        $("#status_out").show();
        $("#send").prop("disabled", true);
        
        $.ajax({
            url: path + "HospitalManagementSystem/HMS/EmailSend/messagestore.jsp",
            type: "POST",
            data: {
                "reason": reason,
                "date": todaydate,
                "email": email,
                "cc": "nil",
                "bcc": "nil",
                "centerid": centerid,
                "centertype": centertype
            },
            success: function (data) {
                var msg = jQuery.trim(data);
                if (msg === "success") {
                    firstupload();
                } else {
                    $("#status_out").hide();
                    $("#errormsg-alert").html(unsuccessfullsend);
                    $("#errormsg-alert").show();
                    var myVar1 = setInterval(function () {
                        $("#errormsg-alert").hide();
                        $("#send").removeProp("disabled");
                        clearInterval(myVar1);
                    }, 3000);
                }
            }, error: function () {
                $("#status_out").hide();
                $("#errormsg-alert").html(servererror);
                $("#errormsg-alert").show();
                var myVar1 = setInterval(function () {
                    $("#errormsg-alert").hide();
                    $("#send").removeProp("disabled");
                    clearInterval(myVar1);
                }, 3000);
            }
        });
        return false;
    } else {

        if ($("#reason").val() === "" || $("#reason").val() === null) {
            $("#reason").addClass("input-alert");
        }
        if ($("#center_email").val() === "" || $("#center_email").val() === null || emailvalidation === false) {
            $("#center_email").addClass("input-alert");
        }

        if (localStorage.getItem("is_booked") == "yes") {
            var bookdate = $("#bookdate").val().split(" ")[0];
            if (dateandtime($("#bookdate").val()) === false || todaydate > bookdate) {
                $("#bookdate").addClass("input-alert");
                return false;
            }
        }
    }
}

// first image uploadn event
function firstupload() {
    
    formData = $("#photoone").attr("src").split("data:image/png;base64,")[1];
    
    if ($("#photoone").attr("src").indexOf("data:image/png;base64") > -1 === true) {
        $.ajax({
            url: path + "HospitalManagementSystem/HMS/EmailSend/webimagemanuplation.jsp",
            type: "POST",
            data: {
                "fm": formData,
                "imgstate": 1
            },
            success: function (data) {
                var msg = jQuery.trim(data);
                if (msg === "success") {
                    secoundupload();
                } else {
                    $("#status_out").hide();
                    $("#errormsg-alert").html(unsuccessfullsend);
                    $("#errormsg-alert").show();
                    var myVar1 = setInterval(function () {
                        $("#errormsg-alert").hide();
                        $("#send").removeProp("disabled");
                        clearInterval(myVar1);
                    }, 3000);
                }
            }, error: function () {
                $("#status_out").hide();
                $("#errormsg-alert").html(servererror);
                $("#errormsg-alert").show();
                var myVar1 = setInterval(function () {
                    $("#errormsg-alert").hide();
                    $("#send").removeProp("disabled");
                    clearInterval(myVar1);
                }, 3000);
            }
        });
    }
    else {
        doctornotesuploads();
    }
}

// secound image upload
function secoundupload() {
    
    formData = $("#phototwo").attr("src").split("data:image/png;base64,")[1];
    
    if ($("#phototwo").attr("src").indexOf("data:image/png;base64") > -1 === true) {
        $.ajax({
            url: path + "HospitalManagementSystem/HMS/EmailSend/webimagemanuplation.jsp",
            type: "POST",
            data: {
                "fm": formData,
                "imgstate": 2
            },
            success: function (data) {
                var msg = jQuery.trim(data);
                if (msg === "success") {
                    thirdupload();
                } else {
                    $("#status_out").hide();
                    $("#errormsg-alert").html(unsuccessfullsend);
                    $("#errormsg-alert").show();
                    var myVar1 = setInterval(function () {
                        $("#errormsg-alert").hide();
                        $("#send").removeProp("disabled");
                        clearInterval(myVar1);
                    }, 3000);
                }
            }, error: function () {
                $("#status_out").hide();
                $("#errormsg-alert").html(servererror);
                $("#errormsg-alert").show();
                var myVar1 = setInterval(function () {
                    $("#errormsg-alert").hide();
                    $("#send").removeProp("disabled");
                    clearInterval(myVar1);
                }, 3000);
            }
        });
    } else {
        doctornotesuploads();
    }
}

// third image upload
function thirdupload() {
    
    formData = $("#photothree").attr("src").split("data:image/png;base64,")[1];
    
    if ($("#photothree").attr("src").indexOf("data:image/png;base64") > -1 === true) {
        $.ajax({
            url: path + "HospitalManagementSystem/HMS/EmailSend/webimagemanuplation.jsp",
            type: "POST",
            data: {
                "fm": formData,
                "imgstate": 3
            },
            success: function (data) {
                var msg = jQuery.trim(data);
                if (msg === "success") {
                    doctornotesuploads();
                } else {
                    $("#status_out").hide();
                    $("#errormsg-alert").html(unsuccessfullsend);
                    $("#errormsg-alert").show();
                    var myVar1 = setInterval(function () {
                        $("#errormsg-alert").hide();
                        $("#send").removeProp("disabled");
                        clearInterval(myVar1);
                    }, 3000);
                }
            }, error: function () {
                $("#status_out").hide();
                $("#errormsg-alert").html(servererror);
                $("#errormsg-alert").show();
                var myVar1 = setInterval(function () {
                    $("#errormsg-alert").hide();
                    $("#send").removeProp("disabled");
                    clearInterval(myVar1);
                }, 3000);
            }
        });
    } else {
        doctornotesuploads();
    }
}

// doctor image convert
var doctornotes64base = "";
var docnotesaval = false;
var docnoteext = "";
var docnotesdisc = "";

//doctornoteconvert
function doctornoteconvert() {
    
    docnotesaval = false;
    doctornotes64base = "";
    docnoteext = "";
    var filesSelected = document.getElementById("docnotes-files").files;

    if (filesSelected.length > 0) {

        var fileselectsize = document.getElementById("docnotes-files").files[0].size;
        var filesize = parseInt(fileselectsize / 1024 / 1024);
        var fileName = $('#docnotes-files').val();
        docnoteext = $('#docnotes-files').val().split('.').pop().toLowerCase();

        if (filesize <= 5) {

            var files = document.getElementById("docnotes-files").files;
            var file = files[0];

            if (isNaN($('#docnotes-disc').val())) {

                if (files && file) {
                    var reader = new FileReader();

                    reader.onload = function (readerEvt) {
                        var binaryString = readerEvt.target.result;
                        doctornotes64base = btoa(binaryString);
                        dodtornotes64base_save(doctornotes64base);

                        $("#status_out_fileadd").hide();
                        $("#successfull-fileadd").html(successfullattach);
                        $("#successfull-fileadd").show();
                        var myVar1 = setInterval(function () {
                            $("#successfull-fileadd").hide();
                            clearInterval(myVar1);
                        }, 3000);
                    };
                    reader.readAsBinaryString(file);
                }
            }
            else {
                $('#docnotes-disc').addClass("input-alert");
            }
        } else {
            $("#errormsg-fileadd").html(filesizelimit);
            $("#errormsg-fileadd").show();
            var myVar1 = setInterval(function () {
                $("#errormsg-fileadd").hide();
                clearInterval(myVar1);
            }, 3000);
        }
    } else {
        $("#errormsg-fileadd").html(unsuccessfulattach);
        $("#errormsg-fileadd").show();
        var myVar1 = setInterval(function () {
            $("#errormsg-fileadd").hide();
            clearInterval(myVar1);
        }, 3000);
    }
}

// doctornotes save to string
function dodtornotes64base_save(getstr) {
    
    doctornotes64base = getstr;
    docnotesaval = true;
    if ($("#docnotes-disc").val() !== "" && $("#docnotes-disc").val() !== " " && $("#docnotes-disc").val() !== null) {
        docnotesdisc = $("#docnotes-disc").val();
    } else {
        docnotesdisc = "nil";
    }
}

//lab report convert
var labreport64base = "";
var labreportaval = false;
var labreportext = "";
var labreportdisc = "";

//labreportconvert
function labreportconvert() {

    labreportaval = false;
    labreport64base = "";
    labreportext = "";
    var filesSelected = document.getElementById("labreport-files").files;

    if (filesSelected.length > 0) {

        var fileselectsize = document.getElementById("labreport-files").files[0].size;
        var filesize = parseInt(fileselectsize / 1024 / 1024);
        var fileName = $('#labreport-files').val();
        labreportext = $('#labreport-files').val().split('.').pop().toLowerCase();

        if (filesize <= 5) {

            var files = document.getElementById("labreport-files").files;
            var file = files[0];

            if (isNaN($("#labreport-disc").val())) {

                if (files && file) {
                    var reader = new FileReader();

                    reader.onload = function (readerEvt) {
                        var binaryString = readerEvt.target.result;
                        labreport64base = btoa(binaryString);
                        labreport64base_save(labreport64base);
                        $("#successfull-labreport").hide();
                        $("#successfull-labreport").html(successfullattach);
                        $("#successfull-labreport").show();
                        var myVar1 = setInterval(function () {
                            $("#successfull-labreport").hide();
                            clearInterval(myVar1);
                        }, 3000);
                    };
                    reader.readAsBinaryString(file);
                }
            }
            else {
                $("#labreport-disc").addClass("input-alert");
            }
        } else {
            $("#errormsg-labreport").html(filesizelimit);
            $("#errormsg-labreport").show();
            var myVar1 = setInterval(function () {
                $("#errormsg-labreport").hide();
                clearInterval(myVar1);
            }, 3000);
        }
    } else {
        $("#errormsg-labreport").html(unsuccessfulattach);
        $("#errormsg-labreport").show();
        var myVar1 = setInterval(function () {
            $("#errormsg-labreport").hide();
            clearInterval(myVar1);
        }, 3000);
    }
}

// lab report save
function labreport64base_save(getstr) {
    
    labreport64base = getstr;
    labreportaval = true;
    if ($("#labreport-disc").val() !== "" && $("#labreport-disc").val() !== " " && $("#labreport-disc").val() !== null) {
        labreportdisc = $("#labreport-disc").val();
    } else {
        labreportdisc = "nil";
    }
}

// xray image convert
var xray64base = "";
var xrayaval = false;
var xrayimgdisc = "";

//xrayimgconvert
function xrayimgconvert() {

    xrayaval = false;
    xray64base = "";
    var filesSelected = document.getElementById("xray-files").files;

    if (filesSelected.length > 0) {

        var fileselectsize = document.getElementById("xray-files").files[0].size;
        var filesize = parseInt(fileselectsize / 1024 / 1024);
        var fileName = $('#xray-files').val();

        if (filesize <= 5) {

            var files = document.getElementById("xray-files").files;
            var file = files[0];

            if (isNaN($('#xray-disc').val())) {

                if (files && file) {
                    var reader = new FileReader();

                    reader.onload = function (readerEvt) {
                        var binaryString = readerEvt.target.result;
                        xray64base = btoa(binaryString);
                        xray64base_save(xray64base);
                        $("#status_out_xray").hide();
                        $("#successfull-xray").html(successfullattach);
                        $("#successfull-xray").show();
                        var myVar1 = setInterval(function () {
                            $("#successfull-xray").hide();
                            clearInterval(myVar1);
                        }, 3000);
                    };
                    reader.readAsBinaryString(file);
                }
            }
            else {
                $('#xray-disc').addClass("input-alert");
            }
        } else {
            $("#errormsg-xray").html(filesizelimit);
            $("#errormsg-xray").show();
            var myVar1 = setInterval(function () {
                $("#errormsg-xary").hide();
                clearInterval(myVar1);
            }, 3000);
        }
    } else {
        $("#errormsg-xray").html(unsuccessfulattach);
        $("#errormsg-xray").show();
        var myVar1 = setInterval(function () {
            $("#errormsg-xray").hide();
            clearInterval(myVar1);
        }, 3000);
    }
}

// xray image save to string
function xray64base_save(getstr) {
    
    xray64base = getstr;
    xrayaval = true;
    if ($("#xray-disc").val() !== "" && $("#xray-disc").val() !== " " && $("#xray-disc").val() !== null) {
        xrayimgdisc = $("#xray-disc").val();
    } else {
        xrayimgdisc = "nil";
    }
}

// docnotes upload
function doctornotesuploads() {
    
    if (docnotesaval === true) {
        $.ajax({
            url: path + "HospitalManagementSystem/HMS/EmailSend/adddoctornotes.jsp",
            type: "POST",
            data: {
                "docdata": doctornotes64base,
                "extenstion": docnoteext,
                "disc": docnotesdisc
            },
            success: function (data) {
                var msg = jQuery.trim(data);
                if (msg === "success") {
                    labreportuploads();
                } else {

                    $("#status_out").hide();
                    $("#errormsg-alert").html(unsuccessfullsend);
                    $("#errormsg-alert").show();
                    var myVar1 = setInterval(function () {
                        $("#errormsg-alert").hide();
                        $("#send").removeProp("disabled");
                        clearInterval(myVar1);
                    }, 3000);
                }
            }, error: function () {
                $("#status_out").hide();
                $("#errormsg-alert").html(servererror);
                $("#errormsg-alert").show();
                var myVar1 = setInterval(function () {
                    $("#errormsg-alert").hide();
                    $("#send").removeProp("disabled");
                    clearInterval(myVar1);
                }, 3000);
            }
        });
        return false;
    } else {
        labreportuploads();
    }
}

// labreport upload
function labreportuploads() {
    
    if (labreportaval === true) {
        $.ajax({
            url: path + "HospitalManagementSystem/HMS/EmailSend/addlabreoprt.jsp",
            type: "POST",
            data: {
                "data": labreport64base,
                "extenstion": labreportext,
                "disc": labreportdisc
            },
            success: function (data) {
                var msg = jQuery.trim(data);
                if (msg === "success") {
                    xrayuploads();
                } else {
                    $("#status_out").hide();
                    $("#errormsg-alert").html(unsuccessfullsend);
                    $("#errormsg-alert").show();
                    var myVar1 = setInterval(function () {
                        $("#errormsg-alert").hide();
                        $("#send").removeProp("disabled");
                        clearInterval(myVar1);
                    }, 3000);
                }
            }, error: function () {
                $("#status_out").hide();
                $("#errormsg-alert").html(servererror);
                $("#errormsg-alert").show();
                var myVar1 = setInterval(function () {
                    $("#errormsg-alert").hide();
                    $("#send").removeProp("disabled");
                    clearInterval(myVar1);
                }, 3000);
            }
        });
        return false;
    } else {
        xrayuploads();
    }
}

// xray uploads
function xrayuploads() {

    if (xrayaval === true) {
        $.ajax({
            url: path + "HospitalManagementSystem/HMS/EmailSend/addxrayimg.jsp",
            type: "POST",
            data: {
                "data": xray64base,
                "disc": xrayimgdisc
            },
            success: function (data) {
                var msg = jQuery.trim(data);
                if (msg === "success") {
                    sendemailRequest();
                } else {
                    $("#status_out").hide();
                    $("#errormsg-alert").html(unsuccessfullsend);
                    $("#errormsg-alert").show();
                    var myVar1 = setInterval(function () {
                        $("#errormsg-alert").hide();
                        $("#send").removeProp("disabled");
                        clearInterval(myVar1);
                    }, 3000);
                }
            }, error: function () {
                $("#status_out").hide();
                $("#errormsg-alert").html(servererror);
                $("#errormsg-alert").show();
                var myVar1 = setInterval(function () {
                    $("#errormsg-alert").hide();
                    $("#send").removeProp("disabled");
                    clearInterval(myVar1);
                }, 3000);
            }
        });
        return false;
    } else {
        sendemailRequest();
    }
}

// email request
function sendemailRequest() {
    
    var sendrequest = localStorage.getItem("sendtype");
    var date = localStorage.getItem("is_booked") === "yes" ? $("#bookdate").val() : "0000-00-00";
    
    if (sendrequest === "sendemail") {
        $.ajax({
            url: path + "HospitalManagementSystem/HMS/EmailSend/sendemail.jsp",
            type: "POST",
            data: {"date": date},
            success: function (data) {
                var msg = jQuery.trim(data);
                if (msg === "success") {
                    vistingHistory();
                } else {
                    $("#status_out").hide();
                    $("#errormsg-alert").html(unsuccessfullsend);
                    $("#errormsg-alert").show();
                    var myVar1 = setInterval(function () {
                        $("#errormsg-alert").hide();
                        $("#send").removeProp("disabled");
                        clearInterval(myVar1);
                    }, 3000);
                }
            }, error: function () {
                $("#status_out").hide();
                $("#errormsg-alert").html(servererror);
                $("#errormsg-alert").show();
                var myVar1 = setInterval(function () {
                    $("#errormsg-alert").hide();
                    $("#send").removeProp("disabled");
                    clearInterval(myVar1);
                }, 3000);
            }
        });
        return false;
    } else if (sendrequest === "preview") {
        getmessageid();
    }
}

// get message id
function getmessageid() {
    
    $.ajax({
        url: path + "HospitalManagementSystem/HMS/EmailSend/getmessageid.jsp",
        type: "POST",
        data: {},
        success: function (data) {
            var msg = jQuery.trim(data);
            if (msg) {
                $("#status_out").hide();
                $url = "http://ec2-52-24-34-18.us-west-2.compute.amazonaws.com:8080/DashBoard-Preview/index.html?messageid=" + msg;
                window.open($url, "_blank");
            } else {
                $("#status_out").hide();
                $("#errormsg-alert").html(previewerror);
                $("#errormsg-alert").show();
                var myVar1 = setInterval(function () {
                    $("#errormsg-alert").hide();
                    clearInterval(myVar1);
                }, 3000);
            }
        }, error: function () {
            $("#status_out").hide();
            $("#errormsg-alert").html(servererror);
            $("#errormsg-alert").show();
            var myVar1 = setInterval(function () {
                $("#errormsg-alert").hide();
                $("#send").removeProp("disabled");
                clearInterval(myVar1);
            }, 3000);
        }
    });
    return false;
}

//store visiting history
function vistingHistory() {
    
    $.ajax({
        url: path + "HospitalManagementSystem/HMS/EmailSend/addvisitinghistory.jsp",
        type: "POST",
        data: {},
        success: function (data) {
            var msg = jQuery.trim(data);
            if (msg === "success") {
                clear();
                $("#status_out").hide();
                $("#successfull-send").html(successfullsend);
                $("#successfull-send").show();
                var myVar1 = setInterval(function () {
                    $("#successfull-send").hide();
                    $("#send").removeProp("disabled");
                    clearInterval(myVar1);
                }, 3000);
            } else {
                $("#status_out").hide();
                $("#errormsg-alert").html(unsuccessfullsend);
                $("#errormsg-alert").show();
                var myVar1 = setInterval(function () {
                    $("#errormsg-alert").hide();
                    $("#send").removeProp("disabled");
                    clearInterval(myVar1);
                }, 3000);
            }
        }, error: function () {
            $("#status_out").hide();
            $("#errormsg-alert").html(servererror);
            $("#errormsg-alert").show();
            var myVar1 = setInterval(function () {
                $("#errormsg-alert").hide();
                $("#send").removeProp("disabled");
                clearInterval(myVar1);
            }, 3000);
        }
    });
    return false;
}

//clear all
function clear() {
    
    $("#reason").val("");
    $("#photocontent1").hide();
    $("#photocontent2").hide();
    $("#photocontent3").hide();
    document.getElementById("photoone").src = "";
    document.getElementById("phototwo").src = "";
    document.getElementById("photothree").src = "";
    document.getElementById("docnotes-files").value = "";
    document.getElementById("docnotes-disc").value = "";
    document.getElementById("xray-files").value = "";
    document.getElementById("xray-disc").value = "";
    document.getElementById("labreport-files").value = "";
    document.getElementById("labreport-disc").value = "";
}

$("#photoone-close").on("click", function () {
    var pic1 = document.getElementById("photoone");
    var pic2 = document.getElementById("phototwo");
    var pic3 = document.getElementById("photothree");
    pic1.src = "";
    if (pic1.src.indexOf("data:image/png;base64") < 1) {
        $("#photocontent1").hide();
    }
});

$("#phototwo-close").on("click", function () {
    var pic1 = document.getElementById("photoone");
    var pic2 = document.getElementById("phototwo");
    var pic3 = document.getElementById("photothree");
    pic2.src = "";
    if (pic2.src.indexOf("data:image/png;base64") < 1) {
        $("#photocontent2").hide();
    }
});

$("#photothree-close").on("click", function () {
    var pic1 = document.getElementById("photoone");
    var pic2 = document.getElementById("phototwo");
    var pic3 = document.getElementById("photothree");
    pic3.src = "";
    if (pic3.src.indexOf("data:image/png;base64") < 1) {
        $("#photocontent3").hide();
    }
});


count = 1;
///**** Script capture via webcam ****/
var video = document.querySelector("#videoElement");
navigator.getUserMedia = navigator.getUserMedia || navigator.webkitGetUserMedia || navigator.mozGetUserMedia || navigator.msGetUserMedia || navigator.oGetUserMedia;

if (navigator.getUserMedia) {
    // get webcam feed if available
    navigator.getUserMedia({
        video: true
    }, handleVideo, videoError);
}

function handleVideo(stream) {
    // if found attach feed to video element
    video.src = window.URL.createObjectURL(stream);
}

function videoError(e) {
    $("#video_error").show();
}
var v, canvas, context, w, h;
// var imgtag = document.getElementById('profileimg'); // get reference to img tag

document.addEventListener('DOMContentLoaded', function () {
    // when DOM loaded, get canvas 2D context and store width and height of element
    v = document.getElementById('videoElement');
    canvas = document.getElementById('canvas');
    context = canvas.getContext('2d');
    w = canvas.width;
    h = canvas.height;
}, false);

function draw(v, c, w, h) {
    if (v.paused || v.ended)
        return false; // if no video, exit here
    context.drawImage(v, 0, 0, w, h); // draw video feed to canvas
    var uri = canvas.toDataURL("image/png"); // convert canvas to data URI

    var pic1 = document.getElementById("photoone");
    var pic2 = document.getElementById("phototwo");
    var pic3 = document.getElementById("photothree");

    $name = ("pic" + count).toString();

    if ($name === "pic1") {
        pic1.src = uri;
        $("#photocontent1").show();
    } else if ($name === "pic2") {
        pic2.src = uri;
        $("#photocontent2").show();
    } else if ($name === "pic3") {
        pic3.src = uri;
        $("#photocontent3").show();
    }

    if (pic1.src.indexOf("data:image/png;base64") > -1) {
        $("#photocontent1").show();
    }
    if (pic2.src.indexOf("data:image/png;base64") > -1) {
        $("#photocontent2").show();
    }
    if (pic3.src.indexOf("data:image/png;base64") > -1) {
        $("#photocontent3").show();
    }
    count++;

    // add URI to IMG tag src
    if (count === 4)
        count = 1;
}

document.getElementById('capture').addEventListener('click', function (e) {
    draw(v, context, w, h);
    $('#takepic').modal('hide');// close the model after capture
}
);
