//check required
$("input,select").focus(function () {
    input_id = $(this).attr("id");
    if ($("#" + input_id).hasClass("input-alert"))
        $("#" + input_id).removeClass("input-alert");
});

//collect data
motherdata = "";
motherdatacheck = [];
collectdata("family_mother");

function collectdata(formname) {
    $("#" + formname).find("input[type=checkbox]").each(function () {
        motherdatacheck.push($(this).attr("id"));
    });
}
// save mother
function setvaluemother() {

    motherdata = "";

    for (var x = 0; x < motherdatacheck.length; x++) {
        var id = motherdatacheck[x];
        var id_check = id.split("_")[1];

        if ($("#" + motherdatacheck[x]).is(":checked")) {
            if (motherdatacheck[x] === "mother_other") {
                y = id + "=" + $("#mother_othertxt").val();
            } else {
                y = id + "=" + id_check + "-yes";
            }
        } else {
            y = id + "=" + id_check + "-no";
        }
        motherdata += y + "&";
        y = "";
    }
}


function save_mother() {

    setvaluemother();

    var mothername = $("#mother_name").val();
    var motherhealth = $("#mother_healthstatus").val();
    var err = 0;
    if (mothername === "" || mothername === null || mothername === " ") {
        err++;
        $("#mother_name").addClass("input-alert");
    }

    if (motherhealth === "" || motherhealth === null || motherhealth === " ") {
        err++;
        $("#mother_healthstatus").addClass("input-alert");
    }

    if (err === 0) {

        if ($("#mother_other").is(":checked") === true) {
            if (!$("#mother_othertxt").val()) {
                $("#mother_othertxt").addClass("input-alert");
                return false;
            }
        }
        datasubmit = motherdata + "mother_name=" + mothername + "&mother_healthstatus=" + motherhealth;
        setmother();
    }

}

var motherfamilyhistoryid = 0;
//setmother details
function setmother() {

   /***** Internet check ***/
    if (getConnection() === "false") {
        return false;
    }

    $("#mother_status_out").show();
    $("#mother_submit,#mother_cancel").prop("disabled", true);

    $.ajax({
        url: path + "HospitalManagementSystem/HMS/Issues/setmothermedicalhistory.jsp",
        type: "POST",
        data: datasubmit + "&relationsid=2&relationstatusid=" + motherfamilyhistoryid,
        success: function (data) {
            var msg = jQuery.trim(data);
            if (msg === "success") {
                $("#mother_status_out").hide();
                getmother();
                $("#mother_successfull-send").html(successfulladddetails);
                $("#mother_successfull-send").show();
                var myVar1 = setInterval(function () {
                    $("#mother_successfull-send").hide();
                    $("#mother_submit,#mother_cancel").removeProp("disabled");
                    clearInterval(myVar1);
                }, 3000);
            } else {

                $("#mother_status_out").hide();
                $("#mother_errormsg-alert").html(unsuccessfulladddetails);
                $("#mother_errormsg-alert").show();
                var myVar1 = setInterval(function () {
                    $("#mother_errormsg-alert").hide();
                    $("#mother_submit,#mother_cancel").removeProp("disabled");
                    clearInterval(myVar1);
                }, 3000);
            }
        },
        error: function () {
            $("#mother_status_out").hide();
            $("#mother_errormsg-alert").html(servererror);
            $("#mother_errormsg-alert").show();
            var myVar1 = setInterval(function () {
                $("#mother_errormsg-alert").hide();
                $("#mother_submit,#mother_cancel").removeProp("disabled");
                clearInterval(myVar1);
            }, 3000);
        }
    });
    return false;
}

//getmother
function getmother() {

    $("#preload_overlay").show();
    $.ajax({
        url: path + "HospitalManagementSystem/HMS/Issues/getfamilymedicalhistory.jsp",
        type: "POST",
        data: {
            "getrelationid": 2
        },
        success: function (data) {
            var msg_mother = jQuery.trim(data);
            if (msg_mother) {
                var obj_mother = JSON.parse(msg_mother);
                if (obj_mother.familymedicalhistory.length > 0) {

                    motherfamilyhistoryid = obj_mother.familymedicalhistory[0][0];
                    $("#mother_name").val(obj_mother.familymedicalhistory[0][1]);
                    document.getElementById('mother_healthstatus').value = obj_mother.familymedicalhistory[0][2];

                    if (obj_mother.familymedicalhistory[0][3] === "arthritis-yes") {
                        $("#mother_arthritis").prop("checked", true);
                    } else {
                        $("#mother_arthritis").prop("checked", false);
                    }

                    if (obj_mother.familymedicalhistory[0][4] === "cancer-yes") {
                        $("#mother_cancer").prop("checked", true);
                    } else {
                        $("#mother_cancer").prop("checked", false);
                    }

                    if (obj_mother.familymedicalhistory[0][5] === "diabetes-yes") {
                        $("#mother_diabetes").prop("checked", true);
                    } else {
                        $("#mother_diabetes").prop("checked", false);
                    }

                    if (obj_mother.familymedicalhistory[0][6] === "heartCondition-yes") {
                        $("#mother_heartCondition").prop("checked", true);
                    } else {
                        $("#mother_heartCondition").prop("checked", false);
                    }

                    if (obj_mother.familymedicalhistory[0][7] === "lungdisease-yes") {
                        $("#mother_lungdisease").prop("checked", true);
                    } else {
                        $("#mother_lungdisease").prop("checked", false);
                    }

                    if (obj_mother.familymedicalhistory[0][8] === "mentalillness-yes") {
                        $("#mother_mentalillness").prop("checked", true);
                    } else {
                        $("#mother_mentalillness").prop("checked", false);
                    }

                    if (obj_mother.familymedicalhistory[0][9] === "stroke-yes") {
                        $("#mother_stroke").prop("checked", true);
                    } else {
                        $("#mother_stroke").prop("checked", false);
                    }

                    if (obj_mother.familymedicalhistory[0][10] === "other-no") {
                        $("#mother_other").prop("checked", false);
                    } else {
                        $("#mother_other").prop("checked", true);
                        $("#mother_othertxt").val(obj_mother.familymedicalhistory[0][10]);
                        $("#mother_othertxt").show();
                    }

                } else {
                    motherfamilyhistoryid = 0;
                }
            }
            $("#preload_overlay").hide();
        },
        error: function () {
            $("#preload_overlay").hide();
            $("#mother_errormsg-alert").html(servererror);
            $("#mother_errormsg-alert").show();
            var myVar1 = setInterval(function () {
                $("#mother_errormsg-alert").hide();
                clearInterval(myVar1);
            }, 3000);
        }
    });
    return false;
}
