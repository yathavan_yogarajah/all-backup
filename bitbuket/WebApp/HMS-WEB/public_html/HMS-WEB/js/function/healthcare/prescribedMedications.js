//this not support on callback
$("input,select,textarea").focus(function () {
    var input_id = $(this).attr("id");
    if ($("#" + input_id).hasClass("input-alert"))
        $("#" + input_id).removeClass("input-alert");
});

var other_premedications = ["medication", "often", "conditions"];
var other_premedications_view = ["medication_view", "often_view", "conditions_view"];

//checkisnan
function checkisnan(passed) {
    var check = 0;
    for (var x = 0; x < passed.length; x++) {
        if ($.trim($("#" + passed[x]).val())) {
            if (!isNaN($.trim($("#" + passed[x]).val()))) {
                $("#" + passed[x]).addClass("input-alert");
                check++;
            }
        }
    }
    return check;
}

// validate premedications
function diabetes_premedications() {

    var required = [];
    $fields = $("#diabetes_premedications").serialize();
    form_array = $fields.split('&');

    for (var i = 0; i < form_array.length; i++) {

        name = form_array[i].split('=')[0];
        value = form_array[i].split('=')[1];

        if (name === "conditions") {
        } else {
            required[i] = value;
            if ($.trim(value) === "") {
                var name_of_input = "#" + name;
                $(name_of_input).addClass("input-alert");
            }
        }
    }

    count = 0;

    if (isNaN($("#date").val()) === true) {
        count++;
        $("#date").addClass("input-alert");
    }
    for ($i = 0; $i < required.length; $i++) {
        if ($.trim(required[$i]) === "") {
            count++;
        }
    }
    if (count === 0 && checkisnan(other_premedications) == 0) {
        addpremed(); // ajax here
    }
}

//clearall
function clearall() {
    $("#date").val("");
    $("#medication").val("");
    $("#often").val("");
    $("#conditions").val("");
}

//add
function addpremed() {

    var condition;

    if ($("#conditions").val() !== "" && $("#conditions").val() !== " " && $("#conditions").val() !== null) {
        condition = $("#conditions").val();
    } else {
        condition = "nil";
    }

    $("#status_out").show();
    $("#submit,#cancel").prop("disabled", true);
    var requesturl = path + "HospitalManagementSystem/HMS/Issues/setpremedications.jsp";
    $.ajax({
        type: "POST",
        url: requesturl,
        data: {
            time: $("#date").val(),
            medication: $("#medication").val(),
            condition: condition,
            does: $("#often").val()
        },
        success: function (html) {
            var msg = jQuery.trim(html);
            if (msg === "success") {
                $("#status_out").hide();
                $("#successfull-send").html(successfulladddetails);
                $("#successfull-send").show();
                var myVar1 = setInterval(function () {
                    $("#successfull-send").hide();
                    clearInterval(myVar1);
                    $("#addPreMedications_model").modal('hide');
                    clearall();
                    getpro();
                    $("#submit,#cancel").removeProp("disabled");
                }, 3000);
            }
            else {
                $("#status_out").hide();
                $("#errormsg-alert").html(unsuccessfulladddetails);
                $("#errormsg-alert").show();
                var myVar1 = setInterval(function () {
                    $("#errormsg-alert").hide();
                    $("#submit,#cancel").removeProp("disabled");
                    clearInterval(myVar1);
                }, 3000);
            }
        },
        error: function () {
            $("#status_out").hide();
            $("#errormsg-alert").html(servererror);
            $("#errormsg-alert").show();
            var myVar1 = setInterval(function () {
                $("#errormsg-alert").hide();
                $("#submit,#cancel").removeProp("disabled");
                clearInterval(myVar1);
            }, 3000);
        }
    });
    return false;
}

var obj_pro;
var objcount_pro;
var counter_pro;
var proid;

//getprogram
function getpro() {
    $("#preload_overlay_light").show();
    obj_pro = "";
    objcount_pro = 0;
    counter_pro = 0;
    proid = 0;

    $.ajax({
        url: path + "HospitalManagementSystem/HMS/Issues/getpremedication.jsp",
        type: "POST",
        success: function (data) {
            var msgpro = jQuery.trim(data);
            if (msgpro) {
                obj_pro = JSON.parse(msgpro);

                if (obj_pro.program.length > 0) {
                    $("#diabetes_contents").show();
                    objcount_pro = obj_pro.program.length;
                    setpro(obj_pro);
                } else {
                    $("#diabetes_contents").hide();
                    proid = 0;
                }
            }
            $("#preload_overlay_light").hide();
        },
        error: function () {
            $("#diabetes_contents").hide();
            $("#preload_overlay_light").hide();
            $("#errormsg-alert1").html(servererror);
            $("#errormsg-alert1").show();
            var myVar1 = setInterval(function () {
                $("#errormsg-alert1").hide();
                clearInterval(myVar1);
            }, 3000);
        }
    });
    return false;
}

$("#pro_backward").on("click", function () {
    counter_pro--;
    if (counter_pro <= 0) {
        setpro(obj_pro);
    }
});

$("#pro_foward").on("click", function () {
    counter_pro++;
    if (counter_pro < objcount_pro) {
        setpro(obj_pro);
    }
});

//setpro
function setpro(obj_pro) {

    if (counter_pro + 1 >= objcount_pro) {
        $("#pro_foward").hide();
    } else {
        $("#pro_foward").show();
    }

    if (counter_pro <= 0) {
        $("#pro_backward").hide();
    } else {
        $("#pro_backward").show();
    }

    proid = obj_pro.program[counter_pro][0];
    $("#date_view").val(obj_pro.program[counter_pro][1]);
    $("#medication_view").val(obj_pro.program[counter_pro][2]);
    $("#often_view").val(obj_pro.program[counter_pro][3]);
    if (obj_pro.program[counter_pro][4] !== "nil") {
        $("#conditions_view").val(obj_pro.program[counter_pro][4]);
    } else {
        $("#conditions_view").val("");
    }

}

//update
function updatevalidation() {

    var check = 0;
    var returnval;

    if ($("#date_view").val() === "" || $("#date_view").val() === " " || $("#date_view").val() === null || isNaN($("#date_view").val())) {
        check++;
        $("#date_view").addClass("input-alert");
    }
    if ($("#medication_view").val() === "" || $("#medication_view").val() === " " || $("#medication_view").val() === null) {
        check++;
        $("#medication_view").addClass("input-alert");
    }
    if ($("#often_view").val() === "" || $("#often_view").val() === " " || $("#often_view").val() === null) {
        check++;
        $("#often_view").addClass("input-alert");
    }
    if (check === 0) {
        returnval = true;
    } else {
        returnval = false;
    }
    return returnval;
}

//update prescibed medication
function updatepremed() {

    var res = updatevalidation();

    if (res === true) {

        var condition;
        if ($("conditions_view").val() !== "" && $("#conditions_view").val() !== " " && $("conditions_view").val() !== null) {
            condition = $("conditions_view").val();
        } else {
            condition = "nil";
        }
        if (checkisnan(other_premedications_view) !== 0) {
            return false;
        }
        $("#status_out1").show();
        $("#diabetes_update_btn").prop("disabled", true);
        var requesturl = path + "HospitalManagementSystem/HMS/Issues/updatepremedications.jsp";
        
        $.ajax({
            type: "POST",
            url: requesturl,
            data: {
                preid: proid,
                time: $("#date_view").val(),
                medication: $("#medication_view").val(),
                condition: condition,
                does: $("#often_view").val()
            },
            success: function (html) {
                var msg = jQuery.trim(html);
                if (msg === "success") {
                    $("#status_ou1t1").hide();
                    $("#successfull-send1").html(successfulladddetails);
                    $("#successfull-send1").show();
                    var myVar1 = setInterval(function () {
                        $("#successfull-send1").hide();
                        clearInterval(myVar1);
                        getpro();
                        $("#diabetes_update_btn").removeProp("disabled");
                    }, 3000);
                }
                else {
                    $("#status_out1").hide();
                    $("#errormsg-alert1").html(unsuccessfulladddetails);
                    $("#errormsg-alert1").show();
                    var myVar1 = setInterval(function () {
                        $("#errormsg-alert1").hide();
                        $("#diabetes_update_btn").removeProp("disabled");
                        clearInterval(myVar1);
                    }, 3000);
                }
            },
            error: function () {
                $("#preload_overlay").hide();
                $("#status_out1").hide();
                $("#errormsg-alert1").html(servererror);
                $("#errormsg-alert1").show();
                var myVar1 = setInterval(function () {
                    $("#errormsg-alert1").hide();
                    $("#diabetes_update_btn").removeProp("disabled");
                    clearInterval(myVar1);
                }, 3000);
            }
        });
        return false;
    }
    else {
        $("#errormsg-alert1").html(unsuccessfulladddetails);
        $("#errormsg-alert1").show();
        var myVar1 = setInterval(function () {
            $("#errormsg-alert1").hide();
            clearInterval(myVar1);
        }, 3000);
    }
}
