// validate children details
$("input,select").focus(function () {
    input_id = $(this).attr("id");
    if ($("#" + input_id).hasClass("input-alert"))
        $("#" + input_id).removeClass("input-alert");
});

//collect data
sibilingsdata = "";
sibilingsdatacheck = [];
collectdata("family_sibilings");

function collectdata(formname) {
    $("#" + formname).find("input[type=checkbox]").each(function () {
        sibilingsdatacheck.push($(this).attr("id"));
    });
}

function removecheckbox(formname) {
    $("#" + formname).find("input[type=checkbox]").each(function () {
        var id = $(this).attr("id");
        $("#" + id).removeAttr("checked");
    });
}


// save sibilings
function setvaluesibilings() {

    sibilingsdata = "";

    for (var x = 0; x < sibilingsdatacheck.length; x++) {
        var id = sibilingsdatacheck[x];
        var id_check = id.split("_")[1];

        if ($("#" + sibilingsdatacheck[x]).is(":checked")) {
            if (sibilingsdatacheck[x] === "sibilings_other") {
                y = id + "=" + $("#sibilings_othertxt").val();
            } else {
                y = id + "=" + id_check + "-yes";
            }
        } else {
            y = id + "=" + id_check + "-no";
        }
        sibilingsdata += y + "&";
        y = "";
    }
}

//save sibilings
function save_sibilings() {

    setvaluesibilings();

    var sibilingsname = $("#sibilings_name").val();
    var sibilingshealth = $("#sibilings_healthstatus").val();
    var err = 0;
    if (sibilingsname === "" || sibilingsname === null || sibilingsname === " ") {
        err++;
        $("#sibilings_name").addClass("input-alert");
    }

    if (sibilingshealth === "" || sibilingshealth === null || sibilingshealth === " ") {
        err++;
        $("#sibilings_healthstatus").addClass("input-alert");
    }

    if (err === 0) {

        if ($("#sibilings_other").is(":checked") === true) {
            if (!$("#sibilings_othertxt").val()) {
                $("#sibilings_othertxt").addClass("input-alert");
                return false;
            }

        }

        datasubmit = sibilingsdata + "sibilings_name=" + sibilingsname + "&sibilings_healthstatus=" + sibilingshealth;
        setsibilings();
    }

}

var sibilingsfamilyhistoryid = 0;

//setsibilings
function setsibilings() {

    /***** Internet check ***/
    if (getConnection() === "false") {
        return false;
    }

    $("#sibilings_status_out").show();
    $("#sibilings_submit,#sibilings_cancel").prop("disabled", true);
    $.ajax({
        url: path + "HospitalManagementSystem/HMS/Issues/setsibilingsmedicalhistory.jsp",
        type: "POST",
        data: datasubmit + "&relationsid=3&relationstatusid=" + sibilingsfamilyhistoryid,
        success: function (data) {

            var msg = jQuery.trim(data);
            if (msg === "success") {
                $("#sibilings_status_out").hide();
                getsibilings();
                $("#sibilings_successfull-send").html(successfulladddetails);
                $("#sibilings_successfull-send").show();
                var myVar1 = setInterval(function () {
                    $("#sibilings_successfull-send").hide();
                    $("#sibilings_submit,#sibilings_cancel").removeProp("disabled");
                    clearInterval(myVar1);
                }, 3000);

            } else {

                $("#sibilings_status_out").hide();
                $("#sibilings_errormsg-alert").html(unsuccessfulladddetails);
                $("#sibilings_errormsg-alert").show();
                var myVar1 = setInterval(function () {
                    $("#sibilings_errormsg-alert").hide();
                    $("#sibilings_submit,#sibilings_cancel").removeProp("disabled");
                    clearInterval(myVar1);
                }, 3000);
            }
        },
        error: function () {
            $("#sibilings_status_out").hide();
            $("#sibilings_errormsg-alert").html(servererror);
            $("#sibilings_errormsg-alert").show();
            var myVar1 = setInterval(function () {
                $("#sibilings_errormsg-alert").hide();
                $("#sibilings_submit,#sibilings_cancel").removeProp("disabled");
                clearInterval(myVar1);
            }, 3000);
        }
    });
    return false;
}


var obj_sib = "";
var objcount_sib = 0;
var counter_sib = 0;

// get sublings
function getsibilings() {

    $("#preload_overlay").show();
    $.ajax({
        url: path + "HospitalManagementSystem/HMS/Issues/getfamilymedicalhistory.jsp",
        type: "POST",
        data: {
            "getrelationid": 3
        },
        success: function (data) {
            var msg = jQuery.trim(data);
            if (msg) {
                obj_sib = JSON.parse(msg);
                if (obj_sib.familymedicalhistory.length > 0) {
                    objcount_sib = obj_sib.familymedicalhistory.length;
                    $("#family_sibilings").show();
                    setsib(obj_sib);
                } else {
                    sibilingsfamilyhistoryid = 0;
                }
            }
            $("#preload_overlay").hide();
        },
        error: function () {
            $("#preload_overlay").hide();
            $("#sibilings_errormsg-alert").html(servererror);
            $("#sibilings_errormsg-alert").show();
            var myVar1 = setInterval(function () {
                $("#sibilings_errormsg-alert").hide();
                clearInterval(myVar1);
            }, 3000);
        }
    });
    return false;
}

$("#family_sibilings_backward").on("click", function () {
    counter_sib--;
    if (counter_sib <= 0) {
        setsib(obj_sib);
    }
});

$("#family_sibilings_foward").on("click", function () {
    counter_sib++;
    if (counter_sib < objcount_sib) {
        setsib(obj_sib);
    }
});

//set sibilings
function setsib(obj_sib) {

    if (counter_sib + 1 >= objcount_sib) {
        $("#family_sibilings_foward").hide();
    } else {
        $("#family_sibilings_foward").show();
    }

    if (counter_sib <= 0) {
        $("#family_sibilings_backward").hide();
    } else {
        $("#family_sibilings_backward").show();
    }

    sibilingsfamilyhistoryid = obj_sib.familymedicalhistory[counter_sib][0];

    $("#sibilings_name").val(obj_sib.familymedicalhistory[counter_sib][1]);
    document.getElementById('sibilings_healthstatus').value = obj_sib.familymedicalhistory[counter_sib][2];



    if (obj_sib.familymedicalhistory[counter_sib][3] === "arthritis-yes") {
        $("#sibilings_arthritis").prop("checked", true);
    } else {
        $("#sibilings_arthritis").prop("checked", false);
    }

    if (obj_sib.familymedicalhistory[counter_sib][4] === "cancer-yes") {
        $("#sibilings_cancer").prop("checked", true);
    } else {
        $("#sibilings_cancer").prop("checked", false);
    }

    if (obj_sib.familymedicalhistory[counter_sib][5] === "diabetes-yes") {
        $("#sibilings_diabetes").prop("checked", true);
    } else {
        $("#sibilings_diabetes").prop("checked", false);
    }

    if (obj_sib.familymedicalhistory[counter_sib][6] === "heartCondition-yes") {
        $("#sibilings_heartCondition").prop("checked", true);
    } else {
        $("#sibilings_heartCondition").prop("checked", false);
    }

    if (obj_sib.familymedicalhistory[counter_sib][7] === "lungdisease-yes") {
        $("#sibilings_lungdisease").prop("checked", true);
    } else {
        $("#sibilings_lungdisease").prop("checked", false);
    }

    if (obj_sib.familymedicalhistory[counter_sib][8] === "mentalillness-yes") {
        $("#sibilings_mentalillness").prop("checked", true);
    } else {
        $("#sibilings_mentalillness").prop("checked", false);
    }

    if (obj_sib.familymedicalhistory[counter_sib][9] === "stroke-yes") {
        $("#sibilings_stroke").prop("checked", true);
    } else {
        $("#sibilings_stroke").prop("checked", false);
    }

    if (obj_sib.familymedicalhistory[counter_sib][10] === "other-no") {
        $("#sibilings_other").prop("checked", false);
    } else {
        $("#sibilings_other").prop("checked", true);
        $("#sibilings_othertxt").val(obj_sib.familymedicalhistory[counter_sib][10]);
        $("#sibilings_othertxt").show();
    }
}
