var obj_acr = "";
var objcount_acr = 0;
var counter_acr = 0;
var proidacr = 0;

//getcar
function getacr() {

    $.ajax({
        url: path + "HospitalManagementSystem/HMS/Issues/gettraget.jsp",
        type: "POST",
        data: {
            tragetid: 4
        },
        success: function (data) {
            var msgpro = jQuery.trim(data);
            if (msgpro) {
                obj_acr = JSON.parse(msgpro);
                if (obj_acr.diatraget.length > 0) {
                    $("#targetlevel-4").show();
                    objcount_acr = obj_acr.diatraget.length;
                    setacr(obj_acr);
                }
                else {
                    $("#targetlevel-4").hide();
                    proidacr = 0;
                }
            }
            counter();
        },
        error: function () {
            $("#targetlevel-4").hide();
            counter();
            $("#errormsg-alert_targetacrmodify").html(servererror);
            $("#errormsg-alert_targetacrmodify").show();
            var myVar1 = setInterval(function () {
                $("#errormsg-alert_targetacrmodify").hide();
                clearInterval(myVar1);
            }, 3000);
        }
    });
    return false;
}

$("#targetacr_backnav").on("click", function () {
    counter_acr--;
    if (counter_acr <= 0) {
        setacr(obj_acr);
    }
});

$("#targetacr_forwardnav").on("click", function () {
    counter_acr++;
    if (counter_acr < objcount_acr) {
        setacr(obj_acr);
    }
});

$("#targetacr_modify").on("click", function () {
    updatetragetacr();
});

//setldlc
function setacr(obj_acr) {

    if (counter_acr + 1 >= objcount_acr) {
        $("#targetacr_forwardnav").hide();
    } else {
        $("#targetacr_forwardnav").show();
    }

    if (counter_acr <= 0) {
        $("#targetacr_backnav").hide();
    } else {
        $("#targetacr_backnav").show();
    }

    proidacr = obj_acr.diatraget[counter_acr][0];
    $("#targetacr_date_view").val(obj_acr.diatraget[counter_acr][1]);

    if (obj_acr.diatraget[counter_acr][2] !== "null") {
        $("#targetacr_mygoal_view").val(obj_acr.diatraget[counter_acr][2]);
    } else {

        $("#targetacr_mygoal_view").val("");
    }

}

//update alc
function updatetragetacr() {

    var date = $("#targetacr_date_view").val();
    var addgoal = $("#targetacr_mygoal_view").val();
    if (date && addgoal && isNaN(addgoal)) {
        $("#status_out_targetacr_view").show();
        $("#targetacr_modify").prop("disabled", true);
        $.ajax({
            url: path + "HospitalManagementSystem/HMS/Issues/updatediabitiestraget.jsp",
            type: "POST",
            data: {
                date: date,
                tragetid: proidacr,
                goal: addgoal
            },
            success: function (data) {
                var msg = jQuery.trim(data);
                if (msg === "success") {
                    $("#status_out_targetacr_view").hide();
                    $("#successfull-send_targetacrmodify").html(successfulladddetails);
                    $("#successfull-send_targetacrmodify").show();
                    var myVar1 = setInterval(function () {
                        $("#successfull-send_targetacrmodify").hide();
                        getacr();
                        $("#targetacr_modify").removeProp("disabled");
                        clearInterval(myVar1);
                    }, 3000);
                }
                else {
                    $("#status_out_targetacr_view").hide();
                    $("#errormsg-alert_targetacrmodify").html(unsuccessfulladddetails);
                    $("#errormsg-alert_targetacrmodify").show();
                    var myVar1 = setInterval(function () {
                        $("#errormsg-alert_targetacrmodify").hide();
                        $("#targetacr_modify").removeProp("disabled");
                        clearInterval(myVar1);
                    }, 3000);
                }
            },
            error: function () {
                $("#errormsg-alert_targetacrmodify").html(servererror);
                $("#errormsg-alert_targetacrmodify").show();
                var myVar1 = setInterval(function () {
                    $("#errormsg-alert_targetacrmodify").hide();
                    $("#targetacr_modify").removeProp("disabled");
                    clearInterval(myVar1);
                }, 3000);
            }
        });
        return false;
    }
    else {
        if (date === "" || date === " ") {
            $("#targetacr_date_view").addClass("input-alert");
        }
        if (addgoal === "" || addgoal === " " || !isNaN(addgoal)) {
            $("#targetacr_mygoal_view").addClass("input-alert");
        }
    }
}
