// validate children details
$("input,select").focus(function () {
    input_id = $(this).attr("id");
    if ($("#" + input_id).hasClass("input-alert"))
        $("#" + input_id).removeClass("input-alert");
});

//collect data
childrendata = "";
childrendatacheck = [];
collectdata("family_children");

function collectdata(formname) {
    $("#" + formname).find("input[type=checkbox]").each(function () {
        childrendatacheck.push($(this).attr("id"));
    });
}
// save children
function setvaluechildren() {

    childrendata = "";

    for (var x = 0; x < childrendatacheck.length; x++) {
        var id = childrendatacheck[x];
        var id_check = id.split("_")[1];

        if ($("#" + childrendatacheck[x]).is(":checked")) {
            if (childrendatacheck[x] === "children_other") {
                y = id + "=" + $("#children_othertxt").val();
            } else {
                y = id + "=" + id_check + "-yes";
            }
        } else {
            y = id + "=" + id_check + "-no";
        }
        childrendata += y + "&";
        y = "";
    }
}


function save_children() {

    setvaluechildren();

    var childrenname = $("#children_name").val();
    var childrenhealth = $("#children_healthstatus").val();
    var err = 0;
    if (childrenname === "" || childrenname === null || childrenname === " ") {
        err++;
        $("#children_name").addClass("input-alert");
    }

    if (childrenhealth === "" || childrenhealth === null || childrenhealth === " ") {
        err++;
        $("#children_healthstatus").addClass("input-alert");
    }

    if (err === 0) {

        if ($("#children_other").is(":checked") === true) {
            if (!$("#children_othertxt").val()) {
                $("#children_othertxt").addClass("input-alert");
                return false;
            }

        }

        datasubmit = childrendata + "children_name=" + childrenname + "&children_healthstatus=" + childrenhealth;
        setchildren();
    }

}
// set children medical issues
var childrenfamilyhistoryid = 0;

//setchildren
function setchildren() {

    //check internetconnection
    if (connection === "offline") {
        return false;
    }

    $("#children_status_out").show();
    $("#children_submit,#children_cancel").prop("disabled", true);
    
    $.ajax({
        url: path + "HospitalManagementSystem/HMS/Issues/setchildrenmedicalhistory.jsp",
        type: "POST",
        data: datasubmit + "&relationsid=5&relationstatusid=" + childrenfamilyhistoryid,
        success: function (data) {
            var msg = jQuery.trim(data);
            if (msg === "success") {
                $("#children_status_out").hide();
                getchildren();
                $("#children_successfull-send").html(successfulladddetails);
                $("#children_successfull-send").show();
                var myVar1 = setInterval(function () {
                    $("#children_successfull-send").hide();
                    $("#children_submit,#children_cancel").removeProp("disabled");
                    clearInterval(myVar1);
                }, 3000);
            } else {

                $("#children_status_out").hide();
                $("#children_errormsg-alert").html(unsuccessfulladddetails);
                $("#children_errormsg-alert").show();
                var myVar1 = setInterval(function () {
                    $("#children_errormsg-alert").hide();
                    $("#children_submit,#children_cancel").removeProp("disabled");
                    clearInterval(myVar1);
                }, 3000);
            }
        },
        error: function () {
            $("#children_status_out").hide();
            $("#children_errormsg-alert").html(servererror);
            $("#children_errormsg-alert").show();
            var myVar1 = setInterval(function () {
                $("#children_errormsg-alert").hide();
                $("#children_submit,#children_cancel").removeProp("disabled");
                clearInterval(myVar1);
            }, 3000);
        }
    });
    return false;
}

var obj_child = "";
var objcount_child = 0;
var counter_child = 0;

//getchildren
function getchildren() {

    $("#preload_overlay").show();
    $.ajax({
        url: path + "HospitalManagementSystem/HMS/Issues/getfamilymedicalhistory.jsp",
        type: "POST",
        data: {
            "getrelationid": 5
        },
        success: function (data) {
            var msgchild = jQuery.trim(data);
            if (msgchild) {
                obj_child = JSON.parse(msgchild);
                if (obj_child.familymedicalhistory.length > 0) {
                    objcount_child = obj_child.familymedicalhistory.length;
                    $("#family_children").show();
                    setchild(obj_child);

                } else {
                    childrenfamilyhistoryid = 0;
                }
            }
            $("#preload_overlay").hide();
        },
        error: function () {
            $("#preload_overlay").hide();
            $("#children_errormsg-alert").html(servererror);
            $("#children_errormsg-alert").show();
            var myVar1 = setInterval(function () {
                $("#children_errormsg-alert").hide();
                clearInterval(myVar1);
            }, 3000);
        }
    });
    return false;
}

//childern backward arrow
$("#family_childern_backward").on("click", function () {
    counter_child--;
    if (counter_child <= 0) {
        setchild(obj_child);
    }
});

//childern foward arrow
$("#family_childern_foward").on("click", function () {
    counter_child++;
    if (counter_child < objcount_child) {
        setchild(obj_child);
    }
});

//setchild
function setchild(obj_child) {

    if (counter_child + 1 >= objcount_child) {
        $("#family_childern_foward").hide();
    } else {
        $("#family_childern_foward").show();
    }

    if (counter_child <= 0) {
        $("#family_childern_backward").hide();
    } else {
        $("#family_childern_backward").show();
    }

    childrenfamilyhistoryid = obj_child.familymedicalhistory[counter_child][0];
     $("#children_name").val(obj_child.familymedicalhistory[counter_child][1]);
    document.getElementById('children_healthstatus').value = obj_child.familymedicalhistory[counter_child][2];

    if (obj_child.familymedicalhistory[counter_child][3] === "arthritis-yes") {
        $("#children_arthritis").prop("checked", true);
    } else {
        $("#children_arthritis").prop("checked", false);
    }

    if (obj_child.familymedicalhistory[counter_child][4] === "cancer-yes") {
        $("#children_cancer").prop("checked", true);
    } else {
        $("#children_cancer").prop("checked", false);
    }

    if (obj_child.familymedicalhistory[counter_child][5] === "diabetes-yes") {
        $("#children_diabetes").prop("checked", true);
    } else {
        $("#children_diabetes").prop("checked", false);
    }

    if (obj_child.familymedicalhistory[counter_child][6] === "heartCondition-yes") {
        $("#children_heartCondition").prop("checked", true);
    } else {
        $("#children_heartCondition").prop("checked", false);
    }

    if (obj_child.familymedicalhistory[counter_child][7] === "lungdisease-yes") {
        $("#children_lungdisease").prop("checked", true);
    } else {
        $("#children_lungdisease").prop("checked", false);
    }

    if (obj_child.familymedicalhistory[counter_child][8] === "mentalillness-yes") {
        $("#children_mentalillness").prop("checked", true);
    } else {
        $("#children_mentalillness").prop("checked", false);
    }

    if (obj_child.familymedicalhistory[counter_child][9] === "stroke-yes") {
        $("#children_stroke").prop("checked", true);
    } else {
        $("#children_stroke").prop("checked", false);
    }

    if (obj_child.familymedicalhistory[counter_child][10] === "other-no") {
        $("#children_other").prop("checked", false);
    } else {
        $("#children_other").prop("checked", true);
        $("#children_othertxt").val(obj_child.familymedicalhistory[counter_child][10]);
        $("#children_othertxt").show();
    }

}