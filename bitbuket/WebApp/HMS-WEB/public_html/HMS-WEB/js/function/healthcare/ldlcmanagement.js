var obj_ldlc = "";
var objcount_ldlc = 0;
var counter_ldlc = 0;
var proidldlc = 0;

//geta1c
function getldlc() {

    $.ajax({
        url: path + "HospitalManagementSystem/HMS/Issues/gettraget.jsp",
        type: "POST",
        data: {
            tragetid: 2
        },
        success: function (data) {
            var msgpro = jQuery.trim(data);
            if (msgpro) {
                obj_ldlc = JSON.parse(msgpro);
                if (obj_ldlc.diatraget.length > 0) {
                    $("#targetlevel-2").show();
                    objcount_ldlc = obj_ldlc.diatraget.length;
                    setldlc(obj_ldlc);
                } else {
                    $("#targetlevel-2").hide();
                    proidldlc = 0;
                }
            }
            counter();
        },
        error: function () {
            $("#targetlevel-2").hide();
            counter();
            $("#errormsg-alert_targetLdlcmodify").html(servererror);
            $("#errormsg-alert_targetLdlcmodify").show();
            var myVar1 = setInterval(function () {
                $("#errormsg-alert_targetLdlcmodify").hide();
                clearInterval(myVar1);
            }, 3000);
        }
    });
    return false;
}

$("#targetLdlc_backnav").on("click", function () {
    counter_ldlc--;
    if (counter_ldlc <= 0) {
        setldlc(obj_ldlc);
    }
});

$("#targetLdlc_forwardnav").on("click", function () {
    counter_ldlc++;
    if (counter_ldlc < objcount_ldlc) {
        setldlc(obj_ldlc);
    }
});

$("#targetLdlc_modify").on("click", function () {
    updatetragetldlc();
});

//setldlc
function setldlc(obj_ldlc) {

    if (counter_ldlc + 1 >= objcount_ldlc) {
        $("#targetLdlc_forwardnav").hide();
    } else {
        $("#targetLdlc_forwardnav").show();
    }

    if (counter_ldlc <= 0) {
        $("#targetLdlc_backnav").hide();
    } else {
        $("#targetLdlc_backnav").show();
    }

    proidldlc = obj_ldlc.diatraget[counter_ldlc][0];
    $("#targetLdlc_date_view").val(obj_ldlc.diatraget[counter_ldlc][1]);

    if (obj_ldlc.diatraget[counter_ldlc][2] !== "null") {
        $("#targetLdlc_mygoal_view").val(obj_ldlc.diatraget[counter_ldlc][2]);
    } else {
        $("#targetLdlc_mygoal_view").val("");
    }
}

//update alc
function updatetragetldlc() {

    var date = $("#targetLdlc_date_view").val();
    var addgoal = $("#targetLdlc_mygoal_view").val();
    
    if (date && addgoal && isNaN(addgoal)) {
        
        $("#status_out_targetLdlc_view").show();
        $("#targetLdlc_modify").prop("disabled", true);
        
        $.ajax({
            url: path + "HospitalManagementSystem/HMS/Issues/updatediabitiestraget.jsp",
            type: "POST",
            data: {
                date: date,
                tragetid: proidldlc,
                goal: addgoal
            },
            success: function (data) {
                var msg = jQuery.trim(data);
                if (msg === "success") {
                    $("#status_out_targetLdlc_view").hide();
                    $("#successfull-send_targetLdlcmodify").html(successfulladddetails);
                    $("#successfull-send_targetLdlcmodify").show();
                    var myVar1 = setInterval(function () {
                        $("#successfull-send_targetLdlcmodify").hide();
                        getldlc();
                        $("#targetLdlc_modify").removeProp("disabled");
                        clearInterval(myVar1);
                    }, 3000);

                } else {
                    $("#status_out_targetLdlc_view").hide();
                    $("#errormsg-alert_targetLdlcmodify").html(unsuccessfulladddetails);
                    $("#errormsg-alert_targetLdlcmodify").show();
                    var myVar1 = setInterval(function () {
                        $("#errormsg-alert_targetLdlcmodify").hide();
                        $("#targetLdlc_modify").removeProp("disabled");
                        clearInterval(myVar1);
                    }, 3000);
                }
            },
            error: function () {
                $("#status_out_targetLdlc_view").hide();
                $("#errormsg-alert_targetLdlcmodify").html(servererror);
                $("#errormsg-alert_targetLdlcmodify").show();
                var myVar1 = setInterval(function () {
                    $("#errormsg-alert_targetLdlcmodify").hide();
                    $("#targetLdlc_modify").removeProp("disabled");
                    clearInterval(myVar1);
                }, 3000);
            }
        });
        return false;
    }
    else {
        if (date === "" || date === " ") {
            $("#targetLdlc_date_view").addClass("input-alert");
        }
        if (addgoal === "" || addgoal === " " || !isNaN(addgoal)) {
            $("#targetLdlc_mygoal_view").addClass("input-alert");
        }
    }
}
