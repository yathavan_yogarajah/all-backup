var dob = "";
var range = "";
var age;

//loadpage
function loadpage(page) {
    window.location = page + ".html";
}

//check test
function checktest() {

    $.ajax({
        url: path + "HospitalManagementSystem/HMS/Issues/gettest.jsp",
        type: "POST",
        success: function (data) {
            var msg = jQuery.trim(data);
            if (msg) {
                obj = JSON.parse(msg);
                if (obj.testres.length > 0) {
                    var datadddate = obj.testres[0][1];
                    var today = new Date(datadddate);
                    var birthDate = new Date(dob);
                    var age1 = today.getFullYear() - birthDate.getFullYear();
                    var m = today.getMonth() - birthDate.getMonth();
                    if (m < 0 || (m === 0 && today.getDate() < birthDate.getDate())) {
                        age1--;
                    }

                    if (age >= age1) {
                        var range1 = "";
                        if (age1 >= 18 && age1 <= 39) {
                            range1 = "18-39";
                        } else if (age1 >= 40 && age1 <= 49) {
                            range1 = "40-49";
                        } else if (age1 >= 50 && age1 <= 74) {
                            range1 = "50-74";
                        } else if (age1 >= 75) {
                            range1 = "75-over";
                        } else {
                            range1 = "cant";
                        }

                        if (range === range1) {
                            localStorage.setItem("healthtest", msg);
                            localStorage.setItem("validdata", "yes");
                            if (range === "18-39") {
                                loadpage(range);
                            } else if (range === "40-49") {
                                loadpage(range);
                            } else if (range === "50-74") {
                                loadpage(range);
                            } else if (range === "75-over") {
                                loadpage(range);
                            } else if (range === "addprofile") {
                                $("#errormsg-alert").html(fillprofile);
                                $("#errormsg-alert").show();
                            }
                            else {
                                $("#errormsg-alert").hide();
                                $("#errormsg-alert").html(cantaccess);
                                $("#errormsg-alert").show();
                            }
                            $("#preload_overlay_light").hide();
                        } else {
                            $("#preload_overlay_light").hide();
                            $("#errormsg-alert").html(outdatedata);
                            $("#errormsg-alert").show();
                            localStorage.setItem("healthtest", msg);
                            localStorage.setItem("validdata", "no");
                        }

                    } else {
                        $("#preload_overlay_light").hide();
                        loadpage(range);
                        localStorage.setItem("healthtest", msg);
                        localStorage.setItem("validdata", "no");
                    }
                }
                else {
                    $("#preload_overlay_light").hide();
                    loadpage(range);
                    localStorage.setItem("healthtest", msg);
                    localStorage.setItem("validdata", "no");
                }
            }
        },
        error: function () {
            $("#preload_overlay_light").hide();
            $("#errormsg-alert").html(servererror);
            $("#errormsg-alert").show();
            var myVar1 = setInterval(function () {
                $("#errormsg-alert").hide();
                clearInterval(myVar1);
            }, 3000);
        }
    });
}

//get age
function getage() {

    $("#preload_overlay_light").show();
    $.ajax({
        url: path + "HospitalManagementSystem/HMS/Profile/getprofilestatus.jsp",
        type: "POST",
        success: function (data) {
            var msg = jQuery.trim(data);
            if (msg) {
                obj = JSON.parse(msg);
                if (obj.userProfile.length > 0) {
                    dob = obj.userProfile[0][0];
                    localStorage.setItem("sex", obj.userProfile[0][1]);
                    var today = new Date();
                    var birthDate = new Date(dob);
                    age = today.getFullYear() - birthDate.getFullYear();
                    var m = today.getMonth() - birthDate.getMonth();
                    if (m < 0 || (m === 0 && today.getDate() < birthDate.getDate())) {
                        age--;
                    }

                    if (age >= 18 && age <= 39) {
                        range = "18-39";
                    } else if (age >= 40 && age <= 49) {
                        range = "40-49";
                    } else if (age >= 50 && age <= 74) {
                        range = "50-74";
                    } else if (age >= 75) {
                        range = "75-over";
                    } else {
                        range = "cant";
                    }
                    checktest();
                }
                else {
                    range = "addprofile";
                    $("#preload_overlay_light").hide();
                }
            }
        },
        error: function () {
            $("#preload_overlay_light").hide();
            $("#errormsg-alert").html(servererror);
            $("#errormsg-alert").show();
        }
    });
}
