// family history validate
$("input,select").focus(function () {
    $input_id = $(this).attr("id");
    if ($("#" + $input_id).hasClass("input-alert"))
        $("#" + $input_id).removeClass("input-alert");
});

//getmedicalhistory
function getmedicalhistory() {
    $("#family_sibilings").hide();
    $("#family_children").hide();
    $("#family_grandparents").hide();
    getfather();
    getmother();
    getsibilings();
    getgrandparents();
    getchildren();
    getspouse();
}