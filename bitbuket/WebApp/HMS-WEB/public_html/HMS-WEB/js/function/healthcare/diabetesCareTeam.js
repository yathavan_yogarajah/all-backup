//check required
$("input,select,textarea").focus(function () {
    var input_id = $(this).attr("id");
    if ($("#" + input_id).hasClass("input-alert"))
        $("#" + input_id).removeClass("input-alert");
});

// validate diabetes_care_team
function diabetes_team() {

    if (($("#name").val() !== "" && $("#name").val() !== " ") && (isNaN($("#name").val()) && ($("#phone").val() !== "" && $("#phone").val() !== " "))) {

        $("#status_out").show();
        $("#submit,#cancel").prop("disabled", true);
        
        var requesturl = path + "HospitalManagementSystem/HMS/Issues/setcareteam.jsp";
        $.ajax({
            type: "POST",
            url: requesturl,
            data: {
                teamid: $("#type").val(),
                name: $("#name").val(),
                phone: $("#phone").val()
            },
            success: function (html) {
                var msg = jQuery.trim(html);
                if (msg === "success") {
                    $("#status_out").hide();
                    $("#successfull-send").html(successfulladddetails);
                    $("#successfull-send").show();
                    var myVar1 = setInterval(function () {
                        $("#successfull-send").hide();
                        $("#submit,#cancel").removeProp("disabled");
                        clearInterval(myVar1);
                    }, 3000);
                }
                else {
                    $("#status_out").hide();
                    $("#errormsg-alert").html(unsuccessfulladddetails);
                    $("#errormsg-alert").show();
                    var myVar1 = setInterval(function () {
                        $("#errormsg-alert").hide();
                        $("#submit,#cancel").removeProp("disabled");
                        clearInterval(myVar1);
                    }, 3000);
                }
            },
            error: function () {
                $("#status_out").hide();
                $("#errormsg-alert").html(servererror);
                $("#errormsg-alert").show();
                var myVar1 = setInterval(function () {
                    $("#errormsg-alert").hide();
                    $("#submit,#cancel").removeProp("disabled");
                    clearInterval(myVar1);
                }, 3000);
            }
        });
        return false;
    }
    else {

        if ($("#name").val() === "" || !isNaN($("#name").val()) || $("#name").val() === " ") {
            $("#name").addClass("input-alert");
        }
        if ($("#phone").val() === "") {
            $("#phone").addClass("input-alert");
        }
    }
}
//change type
$("#type").change(function () {
    $("#submit span").addClass("glyphicon-ok");
    getcareteam();
});

//get care team
function getcareteam() {

    $("#name").val("");
    $("#phone").val("");
    var requesturl = path + "HospitalManagementSystem/HMS/Issues/getcareteam.jsp";
    $.ajax({
        type: "POST",
        url: requesturl,
        data: {
            teamid: $("#type").val()
        },
        success: function (html) {
            var msg = jQuery.trim(html);
            if (msg) {
                obj = JSON.parse(msg);
                if (obj.careteam.length > 0) {
                    $("#name").val(obj.careteam[0][0]);
                    $("#phone").val(obj.careteam[0][1]);
                }
            } else {
            }
        },
        error: function () {
            $("#errormsg-alert").html(servererror);
            $("#errormsg-alert").show();
            var myVar1 = setInterval(function () {
                $("#errormsg-alert").hide();
                clearInterval(myVar1);
            }, 3000);
        }
    });
    return false;
}
