//assigns
function assigns() {
    var useremail = localStorage.getItem("userdeviceemail");
    if (useremail !== "") {
        $("#InputEmail").val(useremail);
        $("#remember").prop('checked', true);
    }
}

//validateEmail
function validateEmail(Email) {
    var filter = /^[\w\-\.\+]+\@[a-zA-Z0-9\.\-]+\.[a-zA-z0-9]{2,4}$/;
    if (filter.test(Email)) {
        return true;
    } else {
        return false;
    }
}

$(function () {
    $("#InputEmail").focus(function () {
        if ($("#InputEmail").hasClass("input-alert"))
            $("#InputEmail").removeClass("input-alert");
    });
});

//fitbit
function fitbit() {

    var currentTime = new Date();
    var enddate = currentTime.getFullYear() + "-" + (currentTime.getMonth() + 1) + "-" + currentTime.getDate();
    var d = new Date(enddate);
    d.setDate(d.getDate() - 14);
    var startdate = d.getFullYear() + "-" + (d.getMonth() + 1) + "-" + d.getDate();
    var userid = $("#InputEmail").val();
    var emailvalidate = validateEmail(userid);

    if (emailvalidate === true) {
        $("#device_status_out").show();
        if ($("#remember").is(":checked")) {
            localStorage.setItem("userdeviceemail", $("#InputEmail").val());

        } else {
            localStorage.setItem("userdeviceemail", "");
        }

        $.ajax({
            url: path + "HospitalManagementSystem/HMS/DashBoard/getfitbitoauth.jsp",
            type: "POST",
            data: {
                "useremail": userid,
                "begindate": startdate,
                "enddate": enddate
            },
            success: function (data) {
                var msg = jQuery.trim(data);
                if (msg !== "error") {
                    $("#device_status_out").hide();
                    loadurl(msg);
                    $("#add_custom").modal("show");
                } else {
                    $("#device_status_out").hide();
                    $("#device-errormsg-alert").html("your message havent successfully add");
                    $("#device-errormsg-alert").show();
                    var myVar1 = setInterval(function () {
                        $("#device-errormsg-alert").hide();
                        clearInterval(myVar1);
                    }, 3000);
                }
            },
            error: function () {
                $("#device_status_out").hide();
                $("#device-errormsg-alert").html(servererror);
                $("#device-errormsg-alert").show();
                var myVar1 = setInterval(function () {
                    $("#device-errormsg-alert").hide();
                    clearInterval(myVar1);
                }, 3000);
            }
        });
        return false;
    } else {
        $("#InputEmail").addClass("input-alert");
    }

}

//withings
function withings() {

    var userid = $("#InputEmail").val();
    var emailvalidate = validateEmail(userid);

    if (emailvalidate === true) {
        $("#device_status_out").show();
        if ($("#remember").is(":checked")) {
            localStorage.setItem("userdeviceemail", $("#InputEmail").val());
        } else {
            localStorage.setItem("userdeviceemail", "");
        }

        $.ajax({
            url: path + "HospitalManagementSystem/HMS/DashBoard/getwithingsoauth.jsp",
            type: "POST",
            data: {
                useremail: userid
            },
            success: function (data) {
                var msg = jQuery.trim(data);
                if (msg !== "error") {
                    $("#device_status_out").hide();
                    loadurl(msg);
                    $("#add_custom").modal("show");
                } else {
                    $("#device_status_out").hide();
                    $("#device-errormsg-alert").html("your message havent successfully add data");
                    $("#device-errormsg-alert").show();
                    var myVar1 = setInterval(function () {
                        $("#device-errormsg-alert").hide();
                        clearInterval(myVar1);
                    }, 3000);
                }
            },
            error: function () {
                $("#device_status_out").hide();
                $("#device-errormsg-alert").html(servererror);
                $("#device-errormsg-alert").show();
                var myVar1 = setInterval(function () {
                    $("#device-errormsg-alert").hide();
                    clearInterval(myVar1);
                }, 3000);
            }
        });
        return false;

    } else {
        $("#InputEmail").addClass("input-alert");
    }

}

//jawbon data
function jawbone() {

    var userid = $("#InputEmail").val();
    var emailvalidate = validateEmail(userid);

    if (emailvalidate === true) {
        $("#device_status_out").show();
        if ($("#remember").is(":checked")) {
            localStorage.setItem("userdeviceemail", $("#InputEmail").val());
        } else {
            localStorage.setItem("userdeviceemail", "");
        }

        $.ajax({
            url: path + "HospitalManagementSystem/HMS/DashBoard/getjawboneoauth.jsp",
            type: "POST",
            data: {},
            success: function (data) {
                var msg = jQuery.trim(data);
                if (msg !== "error") {
                    $("#device_status_out").hide();
                    loadurl(msg);
                    $("#add_custom").modal("show");
                } else {
                    $("#device_status_out").hide();
                    $("#device-errormsg-alert").html("your message havent successfully add data");
                    $("#device-errormsg-alert").show();
                    var myVar1 = setInterval(function () {
                        $("#device-errormsg-alert").hide();
                        clearInterval(myVar1);
                    }, 3000);
                }
            },
            error: function () {
                $("#device_status_out").hide();
                $("#device-errormsg-alert").html(servererror);
                $("#device-errormsg-alert").show();
                var myVar1 = setInterval(function () {
                    $("#device-errormsg-alert").hide();
                    clearInterval(myVar1);
                }, 3000);
            }
        });
        return false;
    } else {
        $("#InputEmail").addClass("input-alert");
    }

}

//loadurl
function loadurl(url) {
   window.open(url, "_blank", "toolbar=yes, scrollbars=yes, resizable=no, top=20, left=250, width=900, height=600");
}