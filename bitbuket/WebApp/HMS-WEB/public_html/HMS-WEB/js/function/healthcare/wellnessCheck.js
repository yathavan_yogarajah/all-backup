//assigndata
function assigndata() {

    var sex = localStorage.getItem("sex");

    if (sex === "Male") {
        $("#person_womenContents").hide();
    } else if (sex === "Female") {
        $("#person_menContents").hide();
    }

    var datavalid = localStorage.getItem("validdata");
    testdata = JSON.parse(localStorage.getItem("healthtest"));

    if (datavalid === "yes" && testdata.testres.length > 0) {
    }

    if (datavalid === "yes" && testdata.testres.length > 0) {
        for (i = 0; i < testdata.testres.length; i++) {
            document.getElementById(testdata.testres[i][0]).checked = true;
        }
    }
    localStorage.removeItem("sex");
    localStorage.removeItem("validdata");
    localStorage.removeItem("healthtest");
}


//add data
function adddata() {

    var genraldata = [];
    genraldata.length = 0;
    var genral = document.querySelectorAll('.healthguide input[type="radio"]:checked');
    for (var i = 0; i < genral.length; i++) {
        genraldata[i] = genral[i].id;
    }

    var today = new Date();
    var yyyy = today.getFullYear();
    var dd = today.getDate();
    var mm = today.getMonth() + 1; //January is 0!

    if (dd < 10) {
        dd = '0' + dd;
    }
    if (mm < 10) {
        mm = '0' + mm;
    }

    var today = yyyy + '-' + mm + '-' + dd;
    $("#status_out").show();
    $("#submit,#cancel").prop("disabled", true);
    $.ajax({
        url: path + "HospitalManagementSystem/HMS/Issues/settest.jsp",
        type: "POST",
        data: {
            "genraldata": genraldata,
            "date": today
        },
        success: function (data) {
            var msg = jQuery.trim(data);
            if (msg === "success") {
                $("#status_out").hide();
                $("#successfull-send").html(successfulladddetails);
                $("#successfull-send").show();
                var myVar = setInterval(function () {
                    $("#successfull-send").hide();
                    $("#submit,#cancel").removeProp("disabled");
                    document.getElementById("wellness-checkform").reset();
                    clearInterval(myVar);
                }, 3000);
            } else {
                $("#errormsg-alert").hide();
                $("#errormsg-alert").html(unsuccessfulladddetails);
                $("#errormsg-alert").show();
                var myVar = setInterval(function () {
                    $("#errormsg-alert").hide();
                    $("#submit,#cancel").removeProp("disabled");
                    clearInterval(myVar);
                }, 3000);
            }
        },
        error: function () {
            $("#status_out").hide();
            $("#errormsg-alert").html(servererror);
            $("#errormsg-alert").show();
            var myVar = setInterval(function () {
                $("#errormsg-alert").hide();
                $("#submit,#cancel").removeProp("disabled");
                clearInterval(myVar);
            }, 3000);
        }
    });
    return false;
}
