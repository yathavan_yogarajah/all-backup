// display nearest Hospital
function getLocation() {

    if (getConnection() === "true") {
        $("#status_out").show();
        navigator.geolocation.getCurrentPosition(showPosition);
    } else {
        $("#status_out").show();
        setTimeout(function () {
            getLocation();
        }, 2000);
    }
}

//showPosition
function showPosition(position) {
    var latitude = position.coords.latitude;
    var longitude = position.coords.longitude;
    localStorage.setItem("current_lat1", latitude);
    localStorage.setItem("current_lng1", longitude);
    gethospital(latitude, longitude);
}

//get hospital
function gethospital(lat, lng) {

    localStorage.setItem("hospital-avaible", "no");
    
    var dataurl = path + "HospitalManagementSystem/HMS/HealthProvider/findnearesthospitals.jsp";
    $.ajax({
        type: "POST",
        url: dataurl,
        data: {
            "lat": lat,
            "lng": lng,
            "requestType": "hospaitalList"
        },
        success: function (html) {
            var msg = jQuery.trim(html);
            $("#status_out").hide();
            if (msg) {
                obj = JSON.parse(msg);
                if (obj.nearestHospitalList) {
                    if (obj.nearestHospitalList.length > 0) {
                        localStorage.setItem("hospital-avaible", "yes");
                        $("#details-heading").show();
                        hospital = obj;
                        listhospitals();
                    } else {
                        $('#alert-warning').html(unablefindhospital);
                        $('#alert-warning').show();
                    }
                } else {
                    getLocation();
                }
            }
        }, error: function () {
            $("#status_out").hide();
            $('#alert-warning').html(servererror);
            $('#alert-warning').show();
        }
    });
    return false;
}

//select hospital
$(document).on("click", ".hospital-list tr", function (e) {
    var id = $(this).parent().parent().find('td:eq(1)').text();
    localStorage.setItem("hosid", id);
    window.location = "selectedHospital.html";
});

//get paticular hospital details
function gethospitalDetails() {

    var id = localStorage.getItem("hosid");
    var dataurl = path + "HospitalManagementSystem/HMS/HealthProvider/hospitaldetails.jsp";
    
    $.ajax({
        type: "POST",
        url: dataurl,
        data: {
            "hospitalid": id,
            "requestType": "hospaitaldetails"
        },
        success: function (html) {
            var msg = jQuery.trim(html);
            if (msg) {
                obj = JSON.parse(msg);
                $("#hospital-name").html(obj.hospitalDetails[0][0]);
                localStorage.setItem("email", obj.hospitalDetails[0][3]);
                localStorage.setItem("hospital-name", obj.hospitalDetails[0][0]);
                localStorage.setItem("web", obj.hospitalDetails[0][4]);
                localStorage.setItem("tp", obj.hospitalDetails[0][5]);
                localStorage.setItem("centerid", id);
                localStorage.setItem("centertype", "h");
                initialize(obj.hospitalDetails[0][1], obj.hospitalDetails[0][2]);
            }
        }
    });
    return false;
}

$("#hospital").on("click", function () {
    window.location = "displayHospital.html";
});

//get clinic details
function getLocationclinic() {

    if (getConnection() === "true") {
        $("#status_out").show();
        navigator.geolocation.getCurrentPosition(showPositionclinic);
    } else {
        $("#status_out").show();
        setTimeout(function () {
            getLocationclinic();
        }, 2000);
    }
}

//show clinic Position
function showPositionclinic(position) {
    var latitude = position.coords.latitude;
    var longitude = position.coords.longitude;
    localStorage.setItem("current_lat", latitude);
    localStorage.setItem("current_lng", longitude);
    getClinic(latitude, longitude);
}

//getClinic
function getClinic(lat, lng) {

    var dataurl = path + "HospitalManagementSystem/HMS/HealthProvider/findnearestclinics.jsp";
    $.ajax({
        type: "POST",
        url: dataurl,
        data: {
            "lat": lat,
            "lng": lng
        },
        success: function (html) {
            $("#status_out").hide();
            var msg = jQuery.trim(html);
            if (msg) {
                obj = JSON.parse(msg);
                if (obj.nearestCilinicsList) {
                    if (obj.nearestCilinicsList.length > 0) {
                         clinic = obj;
                        listclinic();
                        
                    } else {
                        var hospitalavaible = localStorage.getItem("hospital-avaible");
                        if (hospitalavaible === "yes") {
                            $("#alert-warning").html(unablefindclinics);
                            $('#alert-warning').show();
                        } else {
                            $("#alert-warning").html(currentlyworking);
                            $('#alert-warning').show();
                        }
                    }
                } else {
                    getLocationclinic();
                }
            }
        }, error: function () {
            $("#status_out").hide();
            $('#alert-warning').html(servererror);
            $('#alert-warning').show();
        }
    });
    return false;
}

$("#walk").on("click", function () {
    window.location = "diplayWalkclinic.html";
});

// get paticular clinic details
function getclinicdetails() {
    var id = localStorage.getItem("clinicid");
    var dataurl = path + "HospitalManagementSystem/HMS/HealthProvider/clinicdetails.jsp";
    
    $.ajax({
        type: "POST",
        url: dataurl,
        data: {
            "id": id
        },
        success: function (html) {
            var msg = jQuery.trim(html);
            if (msg) {
                obj = JSON.parse(msg);
                $("#hospital-name").html(obj.clinicDetails[0][0]);
                localStorage.setItem("email1", obj.clinicDetails[0][3]);
                localStorage.setItem("hospital-name", obj.clinicDetails[0][0]);
                localStorage.setItem("web", obj.clinicDetails[0][4]);
                localStorage.setItem("tp", obj.clinicDetails[0][5]);
                localStorage.setItem("centerid", id);
                localStorage.setItem("centertype", "c");
                initialize(obj.clinicDetails[0][1], obj.clinicDetails[0][2]);
            }
        }
    });
    return false;
}

$(document).on("click", ".hospital-listclinic tr", function (e) {
    var id = $(this).parent().parent().find('td:eq(1)').text();
    localStorage.setItem("clinicid", id);
    window.location = "selectedClinic.html";
});

// google map integrations
function initialize(lat, lan) {
    
    var map = null;
    var infowindow = new google.maps.InfoWindow();
    var bounds = new google.maps.LatLngBounds();
    //The list of points to be connected
    var markers = [
        {
            "lat": localStorage.getItem('current_lat'),
            "lng": localStorage.getItem('current_lng')
        },
        {
            "lat": lat,
            "lng": lan
        }

    ];
    var mapOptions = {
        center: new google.maps.LatLng(
                parseFloat(markers[0].lat),
                parseFloat(markers[0].lng)),
        zoom: 15,
        mapTypeId: google.maps.MapTypeId.ROADMAP
    };
    var path = new google.maps.MVCArray();
    var service = new google.maps.DirectionsService();

    var infoWindow = new google.maps.InfoWindow();
    map = new google.maps.Map(document.getElementById("map"), mapOptions);
    var poly = new google.maps.Polyline({
        map: map,
        strokeColor: '#F3443C'
    });

    var lat_lng = new Array();
    var marker = new google.maps.Marker({
        position: map.getCenter(),
        map: map,
        draggable: false
    });
    bounds.extend(marker.getPosition());
    google.maps.event.addListener(marker, "click", function (evt) {
        infowindow.setContent("coord:" + marker.getPosition().toUrlValue(6));
        infowindow.open(map, marker);
    });
    for (var i = 0; i < markers.length; i++) {
        if ((i + 1) < markers.length) {
            var src = new google.maps.LatLng(parseFloat(markers[i].lat),
                    parseFloat(markers[i].lng));
            var smarker = new google.maps.Marker({
                position: src,
                map: map,
                draggable: false
            });
            bounds.extend(smarker.getPosition());
            google.maps.event.addListener(smarker, "click", function (evt) {
                infowindow.setContent("You current Location");
                infowindow.open(map, smarker);
            });
            var des = new google.maps.LatLng(parseFloat(markers[i + 1].lat),
                    parseFloat(markers[i + 1].lng));
            var dmarker = new google.maps.Marker({
                position: des,
                map: map,
                draggable: false,
                icon: '../../img/hospital.png'
            });
            bounds.extend(dmarker.getPosition());
            google.maps.event.addListener(dmarker, "click", function (evt) {
                infowindow.setContent(localStorage.getItem("hospital-name"));
                infowindow.open(map, dmarker);
            });
            //  poly.setPath(path);
            service.route({
                origin: src,
                destination: des,
                travelMode: google.maps.DirectionsTravelMode.DRIVING
            }, function (result, status) {
                if (status === google.maps.DirectionsStatus.OK) {
                    for (var i = 0, len = result.routes[0].overview_path.length; i < len; i++) {
                        path.push(result.routes[0].overview_path[i]);
                    }
                    poly.setPath(path);
                    map.fitBounds(bounds);
                }
            });
        }
    }
}
function listhospitals() {
    
    h_length = hospital.nearestHospitalList.length;
    estimate = h_length - 5;

    if (h_length > 5) {
        printhospital(0, 5);
        $("#hospital_more").show();
    } else {
        printhospital(0, h_length);
    }
}

function printhospital(start, limit) {

    for (var i = start; i < limit; i++) {
        var weburl = hospital.nearestHospitalList[i][4];
        if (weburl == "nil") {
            weburl = "currently not available";
        }
        $("#table-hospital").append("<table class='hospital-list'> <tr style='display: none'> <td class='hospital-details'>ID: </td><td  class='hospital-detail-disc'>" + obj.nearestHospitalList[i][0] + "<br></td></tr><tr> <td class='hospital-details'>Hospital: </td> <td class='hospital-detail-disc'>" + obj.nearestHospitalList[i][1] + "</td></tr> <tr> <td class='hospital-details'>Address: </td><td  class='hospital-detail-disc'><br>" + obj.nearestHospitalList[i][2] + "<br></td></tr> <tr> <td class='hospital-details'>Phone: </td><td  class='hospital-detail-disc'>" + obj.nearestHospitalList[i][3] + "</td></tr><tr><td class='hospital-details'>Web: </td><td  class='hospital-detail-disc'>" + weburl + "<br></td></tr></tr><tr><td class='hospital-details'>Waiting Time: </td><td  class='hospital-detail-disc'>currently not available<br></td></tr><tr><td colspan='2' class='pinholder'><img src='../../icons/pinico.png' class='img-responsive pinicon'></td></tr></table><br>");
    }
    num_hos = $("#table-hospital").find("table").length;
}

$("#hospital_more").click(function () {
    
    if (parseInt(num_hos) > 5) {

        for (var y = h_length - 1; y >= 5; y--) {
            $("#table-hospital").find("table:eq(" + y + ")").remove();
            num_hos = $("#table-hospital").find("table").length;
        }

        $("#hospital_more").html("Show more");

    } else {
        printhospital(5, 5 + estimate);
        $("#hospital_more").html("Less");
    }

});
function listclinic() {
    
    c_length = clinic.nearestCilinicsList.length;
    estimate_c = c_length - 5;

    if (c_length > 5) {
        printclinic(0, 5);
        $("#clinic_more").show();
    } else {
        printclinic(0, c_length);
    }
}

function printclinic(start, limit) {

    for (var i = start; i < limit; i++) {
        var weburl = clinic.nearestCilinicsList[i][4];
        if (weburl == "nil") {
            weburl = "currently not available";
        }
        var set = clinic.nearestCilinicsList[i][5];
        var status;
        if (set == "Y") {
            status = "Yes";
        } else {
            status = "No";
        }

         $("#table-clinic").append("<table class='hospital-listclinic'> <tr style='display: none'> <td class='hospital-details'>ID: </td><td  class='hospital-detail-disc'>" + obj.nearestCilinicsList[i][0] + "<br></td></tr><tr> <td class='hospital-details'>Clinic: </td> <td class='hospital-detail-disc'>" + obj.nearestCilinicsList[i][1] + "</td></tr> <tr> <td class='hospital-details'>Address: </td><td  class='hospital-detail-disc'><br>" + obj.nearestCilinicsList[i][2] + "<br></td></tr> <tr> <td class='hospital-details'>Phone: </td><td  class='hospital-detail-disc'>" + obj.nearestCilinicsList[i][3] + "</td></tr><tr><td class='hospital-details'>Web: </td><td  class='hospital-detail-disc'>" + weburl + "<br></td></tr><tr><td class='hospital-details'>Wheel chair: </td><td  class='hospital-detail-disc'>" + status + "<br></td></tr><tr><td class='hospital-details'>Waiting Time: </td><td  class='hospital-detail-disc'>currently not available<br></td></tr><tr><td colspan='2' class='pinholder'><img src='../../icons/pinico.png' class='img-responsive pinicon'></td></tr></table><br>");
    }
    num_clinic = $("#table-clinic").find("table").length;
}




$("#clinic_more").click(function () {

    if (parseInt(num_clinic) > 5) {

        for (var y = c_length - 1; y >= 5; y--) {
            $("#table-clinic").find("table:eq(" + y + ")").remove();
            num_clinic = $("#table-clinic").find("table").length;
        }

        $("#clinic_more").html("Show more");

    } else {
        printclinic(5, 5 + estimate_c);
        $("#clinic_more").html("Less");
    }

});

//click on check in
$("#check-in").on("click", function () {
    window.location = "sendEmail.html";
});
//click on Book Appointment
$("#Book-Appointment").on("click", function () {
    window.location = "sendEmail.html";
});