//check required
$("input,select,textarea").focus(function () {
    var input_id = $(this).attr("id");
    if ($("#" + input_id).hasClass("input-alert"))
        $("#" + input_id).removeClass("input-alert");
});

//function date check
function datecheck() {

    datevalidate = 0;
    dateres = true;

    if ($("#issue_begin").val()) {
        if (myFunction($("#issue_begin").val()) === false) {
            $("#issue_begin").addClass("input-alert");
            datevalidate++;
        }
    }
    if ($("#issue_end").val()) {
        if (myFunction($("#issue_end").val()) === false) {
            $("#issue_end").addClass("input-alert");
            datevalidate++;
        }
    }
    if (datevalidate !== 0) {
        dateres = false;
    }
    return dateres;
}

//validate allergies
function allergies() {

    var required = [];
    $fields = $("form#issues_allergies").serialize();
    form_array = $fields.split('&');

    for (var i = 0; i < form_array.length; i++) {
        name = form_array[i].split('=')[0];
        value = form_array[i].split('=')[1];
        required[i] = value;

        if ($.trim(value) === "") {
            if (name === "issue_end") {

            } else {
                var name_of_input = "#" + name;
                $(name_of_input).addClass("input-alert");
            }
        }
    }

    count = 0;

    for (i = 0; i <= required.length; i++) {
        if ($.trim(required[i]) === "") {
            if (i === 2) {
            } else {
                count++;
            }
        }
    }

    if ((count - 1) === 0) {
        
        if (datecheck()) {
            setallergies();
        } else {
            $("#errormsg-alert").html(dateerror);
            $("#errormsg-alert").show();
            var myVar1 = setInterval(function () {
                $("#errormsg-alert").hide();
                clearInterval(myVar1);
            }, 3000);
        }
    }
}

// set allergies
function setallergies() {
    
    /***** Internet check ***/
    if (getConnection() === "false") {
        return false;
    }

    var beginDate = new Date($("#issue_begin").val());
    var enddateDate = new Date($("#issue_end").val());
    var today = new Date();
    var dd = today.getDate();
    var mm = today.getMonth() + 1; //January is 0!
    var yyyy = today.getFullYear();

    if (dd < 10) {
        dd = '0' + dd;
    }
    if (mm < 10) {
        mm = '0' + mm;
    }
    var today = yyyy + '-' + mm + '-' + dd;

    var datestatus;
    
    if ($("#issue_end").val() !== "" && $("#issue_end").val() !== " " && $("#issue_end").val() !== null) {
        
        if (enddateDate <= new Date(today)) {
            
            if (beginDate <= enddateDate) {
                datestatus = true;
            } else {
                datestatus = false;
            }
        } else {
            datestatus = false;
        }
    } else {
        
        if (beginDate <= new Date(today)) {
            datestatus = true;
        } else {
            datestatus = false;
        }
    }

    if (datestatus === true) {
        
        $("#status_out_add").show();
        $("#submit,#cancel").prop("disabled", true);
        $.ajax({
            url: path + "HospitalManagementSystem/HMS/Issues/setmedicalissues.jsp",
            type: "POST",
            data: $("#issues_allergies").serialize() + "&issuesid=2",
            success: function (data) {
                var msg = jQuery.trim(data);
                if (msg === "success") {
                    $("#status_out_add").hide();
                    $("#successfull-send").html(successfulladddetails);
                    $("#successfull-send").show();
                    var myVar1 = setInterval(function () {
                        $("#successfull-send").hide();
                        $("#submit,#cancel").removeProp("disabled");
                        clearInterval(myVar1);
                    }, 3000);
                } else {
                    $("#status_out_add").hide();
                    $("#errormsg-alert").html(unsuccessfulladddetails);
                    $("#errormsg-alert").show();
                    var myVar1 = setInterval(function () {
                        $("#errormsg-alert").hide();
                        $("#submit,#cancel").removeProp("disabled");
                        clearInterval(myVar1);
                    }, 3000);
                }
            },
            error: function () {
                $("#status_out_add").hide();
                $("#errormsg-alert").html(servererror);
                $("#errormsg-alert").show();
                var myVar1 = setInterval(function () {
                    $("#errormsg-alert").hide();
                    $("#submit,#cancel").removeProp("disabled");
                    clearInterval(myVar1);
                }, 3000);
            }
        });
        return false;
    }
    else {
        $("#errormsg-alert").html(datecheckerror);
        $("#errormsg-alert").show();
        var myVar1 = setInterval(function () {
            $("#errormsg-alert").hide();
            clearInterval(myVar1);
        }, 3000);
    }
}

//change issue
$("#issue_title_options").change(function () {
    if ($("#issue_title_options").val() !== null && $("#issue_title_options").val() !== "" && $("#issue_title_options").val() !== " ") {
        getallergies();
    }
});

//get allergies
function getallergies() {
    
    $("#preload_overlay_light").show();
    $.ajax({
        url: path + "HospitalManagementSystem/HMS/Issues/getmedicalissues.jsp",
        type: "POST",
        data: {
            "problems": $("#issue_title_options").val(),
            "issuesid": 2
        },
        success: function (data) {
            var msg = jQuery.trim(data);
            if (msg) {
                obj = JSON.parse(msg);
                if (obj.medicalproblems.length > 0) {

                    $("#issue_begin").val(obj.medicalproblems[0][0]);

                    if (obj.medicalproblems[0][1] !== "1000-10-10") {
                        $("#issue_end").val(obj.medicalproblems[0][1]);
                    } else {
                        $("#issue_end").val("");
                    }

                    if (obj.medicalproblems[0][2] !== "nil") {
                        $("#discription").val(obj.medicalproblems[0][2]);
                    } else {
                        $("#discription").val("");
                    }

                } else {
                    $("#issue_begin").val("");
                    $("#issue_end").val("");
                     $("#discription").val("");
                    $("#submit span").addClass("glyphicon-ok");
                }
            }
            $("#preload_overlay_light").hide();
        },
        error: function () {
            $("#preload_overlay_light").hide();
            $("#status_out_add").hide();
            $("#errormsg-alert").html(servererror);
            $("#errormsg-alert").show();
            var myVar1 = setInterval(function () {
                $("#errormsg-alert").hide();
                clearInterval(myVar1);
            }, 3000);
        }
    });
    return false;
}

//addmedicalproblem
function addmedicalprob() {
    
    /***** Internet check ***/
    if (getConnection() === "false") {
        return false;
    }

    var catname = $("#cat_name").val();
    if (catname !== null && catname !== "" && catname !== " " && isNaN(catname)) {
        $("#status_out").show();
        $("#add_meicalprob_cat").prop("disabled", true);
        $.ajax({
            url: path + "HospitalManagementSystem/HMS/Issues/setmedicalissuescat.jsp",
            type: "POST",
            data: {
                catname: catname,
                issuesid: 2
            },
            success: function (data) {
                var msg = jQuery.trim(data);
                if (msg === "success") {
                    getmedicalprob();
                    $("#status_out").hide();
                    $("#successfull-send_prob").html(successfulladddetails);
                    $("#successfull-send_prob").show();
                    document.getElementById("issues_allergies").reset();
                    $("#cat_name").val("");
                    var myVar1 = setInterval(function () {
                        $("#successfull-send_prob").hide();
                        $('#add_medicalprocat').modal('hide');
                        $("#add_meicalprob_cat").removeProp("disabled");
                        clearInterval(myVar1);
                    }, 3000);
                } else {
                    $("#status_out").hide();
                    $("#errormsg-alert_prob").html(unsuccessfulladddetails);
                    $("#errormsg-alert_prob").show();
                    var myVar1 = setInterval(function () {
                        $("#errormsg-alert_prob").hide();
                        $("#add_meicalprob_cat").removeProp("disabled");
                        clearInterval(myVar1);
                    }, 3000);
                }
            }, error: function () {
                $("#status_out").hide();
                $("#errormsg-alert_prob").html(servererror);
                $("#errormsg-alert_prob").show();
                var myVar1 = setInterval(function () {
                    $("#errormsg-alert_prob").hide();
                    $("#add_meicalprob_cat").removeProp("disabled");
                    clearInterval(myVar1);
                }, 3000);
            }
        });
        return false;
    }
    else {
        $("#cat_name").addClass("input-alert");
    }
}

//getmedicalproblem
function getmedicalprob() {

    $('#issue_title_options')[0].options.length = 0;
    $('#issue_title_options').append('<option value="">Select One *</option>');

    $.ajax({
        url: path + "HospitalManagementSystem/HMS/Issues/getmedicalissuescat.jsp",
        type: "POST",
        data: {
            "problems": $("#issue_title_options").val(),
            issuesid: 2
        },
        success: function (data) {
            var msg = jQuery.trim(data);
            if (msg) {
                obj = JSON.parse(msg);
                if (obj.medicalproblemscat.length > 0) {
                    for (i = 0; i < obj.medicalproblemscat.length; i++) {

                        $('#issue_title_options').append('<option value="' + obj.medicalproblemscat[i][0] + '">' + obj.medicalproblemscat[i][0] + '</option>');
                    }
                }
            }
        }, error: function () {
            $("#errormsg-alert").html(servererror);
            $("#errormsg-alert").show();
            var myVar1 = setInterval(function () {
                $("#errormsg-alert").hide();
                clearInterval(myVar1);
            }, 3000);
        }
    });
    return false;
}