//check required
$("input,select").focus(function () {
    input_id = $(this).attr("id");
    if ($("#" + input_id).hasClass("input-alert"))
        $("#" + input_id).removeClass("input-alert");
});

//add test
function addtestvalidation() {

    var date_test = $("#test_date_add").val();
    var today = new Date();
    var dd = today.getDate();
    var mm = today.getMonth() + 1; //January is 0!
    var yyyy = today.getFullYear();

    if (dd < 10) {
        dd = '0' + dd;
    }
    if (mm < 10) {
        mm = '0' + mm;
    }
    var today = yyyy + '-' + mm + '-' + dd;

    if (myFunction($("#test_date_add").val()) && new Date(date_test) <= new Date(today)) {

        var required = [];
        var required_fields = [];
        fields = $("form#diabetes_test_add").serialize();
        form_array = fields.split('&');

        for (var i = 0; i < form_array.length; i++) {
            name = form_array[i].split('=')[0];
            value = form_array[i].split('=')[1];
            var name_of_input = "#" + name;
            if (name.indexOf("_add") !== -1) {
                if (name !== "test_date_add") {
                    required_fields.push(name);
                }
            }
            if (name.indexOf("_add") !== -1 && $.trim($(name_of_input).val()) !== "" && name !== "test_date_add") {
                required.push(name);
            }
        }
        error = 0;
        if (required.length >= 1) {
            error = 0;
        }
        else {
            for (var j = 0; j < required_fields.length; j++) {
                $("#" + required_fields[j]).addClass("input-alert");
                error++;
            }
        }

        if (isNaN($("#weight_add").val())) {
            $("#weight_add").addClass("input-alert");
            error++;
        }

        if (error === 0) {
            addtest(); // call function here
        }
    }
    else {
        $("#status_out_a1c_add").hide();
        $("#errormsg-alert_a1cadd").html(validdate);
        $("#errormsg-alert_a1cadd").show();
        var myVar1 = setInterval(function () {
            $("#errormsg-alert_a1cadd").hide();
            clearInterval(myVar1);
        }, 3000);
    }
}

$("#targetA1c_add_cancel").click(function () {
    $("#target_date_add").val("");
    $("#target_goal_add").val("");
});

//add test
function addtest() {
    var a1c;
    var ldlc;
    var hdlc;
    var acr;
    var eGFR;
    var bloodpreasure;
    var weight;
    var retinaleye;
    var footexam;

    if ($("#a1c_add").val() !== "" && $("#a1c_add").val() !== null && $("#a1c_add").val() !== " ") {
        a1c = $("#a1c_add").val();
    } else {
        a1c = "nil";
    }

    if ($("#ldlc_add").val() !== "" && $("#ldlc_add").val() !== null && $("#ldlc_add").val() !== " ") {
        ldlc = $("#ldlc_add").val();
    } else {
        ldlc = "nil";
    }
    if ($("#hdlc_add").val() !== "" && $("#hdlc_add").val() !== null && $("#hdlc_add").val() !== " ") {
        hdlc = $("#hdlc_add").val();
    } else {
        hdlc = "nil";
    }
    if ($("#acr_add").val() !== "" && $("#acr_add").val() !== null && $("#acr_add").val() !== " ") {
        acr = $("#acr_add").val();
    } else {
        acr = "nil";
    }
    if ($("#eGFR_add").val() !== "" && $("#eGFR_add").val() !== null && $("#eGFR_add").val() !== " ") {
        eGFR = $("#eGFR_add").val();
    } else {
        eGFR = "nil";
    }
    if ($("#bloodpreasure_add").val() !== "" && $("#bloodpreasure_add").val() !== null && $("#bloodpreasure_add").val() !== " ") {
        bloodpreasure = $("#bloodpreasure_add").val();
    } else {
        bloodpreasure = "nil";
    }
    if ($("#weight_add").val() !== "" && $("#weight_add").val() !== null && $("#weight_add").val() !== " ") {
        weight = $("#weight_add").val();
    } else {
        weight = 0;
    }
    if ($("#retinaleye_add").val() !== "" && $("#retinaleye_add").val() !== null && $("#retinaleye_add").val() !== " ") {
        retinaleye = $("#retinaleye_add").val();
    } else {
        retinaleye = "nil";
    }
    if ($("#footexam_add").val() !== "" && $("#footexam_add").val() !== null && $("#footexam_add").val() !== " ") {
        footexam = $("#footexam_add").val();
    } else {
        footexam = "nil";
    }

    $("#status_out_a1c_add").show();
    $("#addsubmit_test,#addcancel_test").prop("disabled", true);
    
    $.ajax({
        url: path + "HospitalManagementSystem/HMS/Issues/setdiabitestest.jsp",
        type: "POST",
        data: {
            date: $("#test_date_add").val(),
            a1c: a1c,
            ldlc: ldlc,
            hdlc: hdlc,
            acr: acr,
            eGFR: eGFR,
            bloodpreasure: bloodpreasure,
            weight: weight,
            retinaleye: retinaleye,
            footexam: footexam
        },
        success: function (data) {
            var msg = jQuery.trim(data);
            if (msg === "success") {
                $("#status_out_a1c_add").hide();
                $("#successfull-send_a1cadd").html(successfulladddetails);
                $("#successfull-send_a1cadd").show();
                document.getElementById("diabetes_test_add").reset();

                var myVar1 = setInterval(function () {
                    $("#successfull-send_a1cadd").hide();
                    $("#modal_test").hide();
                    $("#modal_test").modal('hide');
                    getpro();
                    $("#addsubmit_test,#addcancel_test").removeProp("disabled");
                    clearInterval(myVar1);
                }, 3000);

            } else {
                $("#status_out_a1c_add").hide();
                $("#errormsg-alert_a1cadd").html(unsuccessfulladddetails);
                $("#errormsg-alert_a1cadd").show();
                var myVar1 = setInterval(function () {
                    $("#errormsg-alert_a1cadd").hide();
                    $("#addsubmit_test,#addcancel_test").removeProp("disabled");
                    clearInterval(myVar1);
                }, 3000);
            }
        },
        error: function () {
            $("#status_out_a1c_add").hide();
            $("#errormsg-alert_a1cadd").html(servererror);
            $("#errormsg-alert_a1cadd").show();
            var myVar1 = setInterval(function () {
                $("#errormsg-alert_a1cadd").hide();
                $("#addsubmit_test,#addcancel_test").removeProp("disabled");
                clearInterval(myVar1);
            }, 3000);
        }
    });
    return false;
}

var obj_pro = "";
var objcount_pro = 0;
var counter_pro = 0;
var proid = 0;

//getprogram
function getpro() {

    $.ajax({
        url: path + "HospitalManagementSystem/HMS/Issues/getdiabitiestest.jsp",
        type: "POST",
        success: function (data) {
            var msgpro = jQuery.trim(data);
            if (msgpro) {
                obj_pro = JSON.parse(msgpro);
                if (obj_pro.program.length > 0) {
                    $("#diabetes_contents").show();
                    objcount_pro = obj_pro.program.length;
                    setpro(obj_pro);
                } else {
                    $("#diabetes_contents").hide();
                    proid = 0;
                }
            }
            counter();
        },
        error: function () {
            $("#diabetes_contents").hide();
            counter();
            $("#status_out_dia_test").hide();
            $("#errormsg-alert_dia_test").html(servererror);
            $("#errormsg-alert_dia_test").show();
            var myVar1 = setInterval(function () {
                $("#errormsg-alert_dia_test").hide();
                clearInterval(myVar1);
            }, 3000);
        }
    });
    return false;
}

$("#pro_backward").on("click", function () {
    counter_pro--;
    if (counter_pro <= 0) {
        setpro(obj_pro);
    }
});

$("#pro_foward").on("click", function () {
    counter_pro++;
    if (counter_pro < objcount_pro) {
        setpro(obj_pro);
    }
});

//setpro
function setpro(obj_pro) {
    if (counter_pro + 1 >= objcount_pro) {
        $("#pro_foward").hide();
    } else {
        $("#pro_foward").show();
    }

    if (counter_pro <= 0) {
        $("#pro_backward").hide();
    } else {
        $("#pro_backward").show();
    }

    proid = obj_pro.program[counter_pro][0];
    $("#test_date_view").val(obj_pro.program[counter_pro][1]);



    if (obj_pro.program[counter_pro][2] !== "nil") {
        $("#a1c_view").val(obj_pro.program[counter_pro][2]);
    } else {

        $("#a1c_view").val("");
    }

    if (obj_pro.program[counter_pro][3] !== "nil") {
        $("#ldlc_view").val(obj_pro.program[counter_pro][3]);
    } else {
        $("#ldlc_view").val("");
    }

    if (obj_pro.program[counter_pro][4] !== "nil") {
        $("#hdlc_view").val(obj_pro.program[counter_pro][4]);
    } else {
        $("#hdlc_view").val("");
    }

    if (obj_pro.program[counter_pro][5] !== "nil") {
        $("#acr_view").val(obj_pro.program[counter_pro][5]);
    } else {
        $("#acr_view").val("");
    }

    if (obj_pro.program[counter_pro][6] !== "nil") {
        $("#eGFR_view").val(obj_pro.program[counter_pro][6]);
    } else {
        $("#eGFR_view").val("");
    }

    if (obj_pro.program[counter_pro][7] !== "nil") {
        $("#bloodpreasure_view").val(obj_pro.program[counter_pro][7]);
    } else {
        $("#bloodpreasure_view").val("");
    }

    if (obj_pro.program[counter_pro][8] !== 0) {
        $("#weight_view").val(obj_pro.program[counter_pro][8]);
    } else {
        $("#weight_view").val("");
    }

    if (obj_pro.program[counter_pro][9] !== "nil") {
        $("#retinaleye_view").val(obj_pro.program[counter_pro][9]);
    } else {
        $("#retinaleye_view").val("");
    }

    if (obj_pro.program[counter_pro][10] !== "nil") {
        $("#footexam_view").val(obj_pro.program[counter_pro][10]);
    } else {
        $("#footexam_view").val("");
    }
}

//updatetest
function updatetest() {
    var a1c;
    var ldlc;
    var hdlc;
    var acr;
    var eGFR;
    var bloodpreasure;
    var weight;
    var retinaleye;
    var footexam;

    var date = $("#test_date_view").val();
    var today = new Date();
    var dd = today.getDate();
    var mm = today.getMonth() + 1; //January is 0!
    var yyyy = today.getFullYear();
    if (dd < 10) {
        dd = '0' + dd;
    }
    if (mm < 10) {
        mm = '0' + mm;
    }
    var today = yyyy + '-' + mm + '-' + dd;

    if (myFunction(date) && new Date(date) <= new Date(today)) {

        var error = 0;

        if ($("#a1c_view").val() !== "" && $("#a1c_view").val() !== null && $("#a1c_view").val() !== " ") {
            a1c = $("#a1c_view").val();
        } else {
            error++;
            $("#a1c_view").addClass("input-alert");
        }

        if ($("#ldlc_view").val() !== "" && $("#ldlc_view").val() !== null && $("#ldlc_view").val() !== " ") {
            ldlc = $("#ldlc_view").val();
        } else {
            error++;
            $("#ldlc_view").addClass("input-alert");
        }
        if ($("#hdlc_view").val() !== "" && $("#hdlc_view").val() !== null && $("#hdlc_view").val() !== " ") {
            hdlc = $("#hdlc_view").val();
        } else {
            error++;
            $("#hdlc_view").addClass("input-alert");
        }
        if ($("#acr_view").val() !== "" && $("#acr_view").val() !== null && $("#acr_view").val() !== " ") {
            acr = $("#acr_view").val();
        } else {
            error++;
            $("#acr_view").addClass("input-alert");
        }
        if ($("#eGFR_view").val() !== "" && $("#eGFR_view").val() !== null && $("#eGFR_view").val() !== " ") {
            eGFR = $("#eGFR_view").val();
        } else {
            error++;
            $("#eGFR_view").addClass("input-alert");
        }
        if ($("#bloodpreasure_view").val() !== "" && $("#bloodpreasure_view").val() !== null && $("#bloodpreasure_view").val() !== " ") {
            bloodpreasure = $("#bloodpreasure_view").val();
        } else {
            error++;
            $("#bloodpreasure_view").addClass("input-alert");
        }

        if ($("#weight_view").val() !== "" && $("#weight_view").val() !== null && $("#weight_view").val() !== " " && isNaN($("#weight_view").val()) === false) {
            weight = $("#weight_view").val();
        } else {
            error++;
            $("#weight_view").addClass("input-alert");

        }
        if ($("#retinaleye_view").val() !== "" && $("#retinaleye_view").val() !== null && $("#retinaleye_view").val() !== " ") {
            retinaleye = $("#retinaleye_view").val();
        } else {
            error++;
            $("#retinaleye_view").addClass("input-alert");
        }
        if ($("#footexam_view").val() !== "" && $("#footexam_view").val() !== null && $("#footexam_view").val() !== " ") {
            footexam = $("#footexam_view").val();
        } else {
            error++;
            $("#footexam_view").addClass("input-alert");
        }

        if (error === 0) {
            $("#status_out_dia_test").show();
            $("#dia_test_modify").prop("disabled", true);
            $.ajax({
                url: path + "HospitalManagementSystem/HMS/Issues/updatediabitiestest.jsp",
                type: "POST",
                data: {
                    testid: proid,
                    date: $("#test_date_view").val(),
                    a1c: a1c,
                    ldlc: ldlc,
                    hdlc: hdlc,
                    acr: acr,
                    eGFR: eGFR,
                    bloodpreasure: bloodpreasure,
                    weight: weight,
                    retinaleye: retinaleye,
                    footexam: footexam
                },
                success: function (data) {
                    var msg = jQuery.trim(data);
                    if (msg === "success") {
                        $("#status_out_dia_test").hide();
                        $("#successfull-send_dia_test").html(successfulladddetails);
                        $("#successfull-send_dia_test").show();
                        var myVar1 = setInterval(function () {
                            $("#successfull-send_dia_test").hide();
                            getpro();
                            $("#dia_test_modify").removeProp("disabled");
                            clearInterval(myVar1);
                        }, 3000);

                    } else {

                        $("#status_out_dia_test").hide();
                        $("#errormsg-alert_dia_test").html(unsuccessfulladddetails);
                        $("#errormsg-alert_dia_test").show();
                        var myVar1 = setInterval(function () {
                            $("#errormsg-alert_dia_test").hide();
                            $("#dia_test_modify").removeProp("disabled");
                            clearInterval(myVar1);
                        }, 3000);
                    }
                },
                error: function () {
                    $("#status_out_dia_test").hide();
                    $("#errormsg-alert_dia_test").html(servererror);
                    $("#errormsg-alert_dia_test").show();
                    var myVar1 = setInterval(function () {
                        $("#errormsg-alert_dia_test").hide();
                        $("#dia_test_modify").removeProp("disabled");
                        clearInterval(myVar1);
                    }, 3000);
                }
            });
            return false;
        }
    }
    else {
        $("#status_out_dia_test").hide();
        $("#errormsg-alert_dia_test").html(validdate);
        $("#errormsg-alert_dia_test").show();
        var myVar1 = setInterval(function () {
            $("#errormsg-alert_dia_test").hide();
            clearInterval(myVar1);
        }, 3000);
    }
}
