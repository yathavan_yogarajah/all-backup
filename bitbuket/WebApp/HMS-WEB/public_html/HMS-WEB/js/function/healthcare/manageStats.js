//check required
$("input,select").focus(function () {
    $input_id = $(this).attr("id");
    if ($("#" + $input_id).hasClass("input-alert"))
        $("#" + $input_id).removeClass("input-alert");
});

// validate stats
function stats() {

    var required = [];
    $fields = $("form#stats").serialize();
    form_array = $fields.split('&');
    for (var i = 0; i < form_array.length; i++) {
        name = form_array[i].split('=')[0];
        value = form_array[i].split('=')[1];

        if (name === "stats_monthly_income" || name === "stats_financial_review" || name === "stats_homeless") {
        } else {
            required[i] = value;
            if ($.trim(value) === "") {
                var name_of_input = "#" + name;
                if (name === "stats_language") {
                    $err_msg = "Please select ";
                    $(name_of_input).addClass("input-alert");

                } else {
                    $err_msg = "Please enter ";
                    $(name_of_input).addClass("input-alert");
                }
            }
        }
    }
    
    count = 0;
    
    for ($i = 0; $i <= required.length; $i++) {
        if ($.trim(required[$i]) === "") {
            count++;
        }
    }
    
    if (isNaN($("#stats_family_size").val())) {
        $("#stats_family_size").addClass("input-alert");
    }
    
    if ((count - 1) === 0 && !isNaN($("#stats_family_size").val())) {
        if ($("#stats_financial_review").val()) {

            if (myFunction($("#stats_financial_review").val())) {
                setstats();
            } else {
                $("#errormsg-alert").html(dateerror);
                $("#errormsg-alert").show();
                var myVar1 = setInterval(function () {
                    $("#errormsg-alert").hide();
                    clearInterval(myVar1);
                }, 3000);
            }
        } else {
            setstats();
        }
    }
}

//set status
function setstats() {

    /***** Internet check ***/
    if (getConnection() === "false") {
        return false;
    }
    $("#status_out").show();
    $.ajax({
        url: path + "HospitalManagementSystem/HMS/Profile/addstats.jsp",
        type: "POST",
        data: $("#stats").serialize(),
        success: function (data) {
            var msg = jQuery.trim(data);
            if (msg === "success") {
                $("#status_out").hide();
                $("#successfull-send").html(successfullsend);
                $("#successfull-send").show();
                var myVar1 = setInterval(function () {
                    $("#successfull-send").hide();
                    clearInterval(myVar1);
                }, 3000);
            } else {
                $("#status_out").hide();
                $("#errormsg-alert").html(unsuccessfullsend);
                $("#errormsg-alert").show();
                var myVar1 = setInterval(function () {
                    $("#errormsg-alert").hide();
                    clearInterval(myVar1);
                }, 3000);
            }
        }, error: function () {
            $("#status_out").hide();
            $("#errormsg-alert").html(servererror);
            $("#errormsg-alert").show();
            var myVar1 = setInterval(function () {
                $("#errormsg-alert").hide();
                clearInterval(myVar1);
            }, 3000);
        }
    });
    return false;
}

//getstats
function getstats() {
    $("#preload_overlay").show();
    $.ajax({
        url: path + "HospitalManagementSystem/HMS/Profile/getstats.jsp",
        type: "POST",
        success: function (data) {
            var msg = jQuery.trim(data);
            if (msg) {
                obj = JSON.parse(msg);
                if (obj.userStats.length > 0) {

                    $("#stats_language").val(obj.userStats[0][0]);
                    $("#stats_ethnicity").val(obj.userStats[0][1]);
                    $("#stats_race").val(obj.userStats[0][2]);
                    $("#stats_family_size").val(obj.userStats[0][3]);

                    if (obj.userStats[0][4] !== "1000-10-10") {
                        $("#stats_financial_review").val(obj.userStats[0][4]);
                    } else {
                        $("#stats_financial_review").val("");
                    }
                    if (obj.userStats[0][5] !== "00.00") {
                        $("#stats_monthly_income").val(obj.userStats[0][5]);
                    } else {
                        $("#stats_monthly_income").val("");
                    }
                    if (obj.userStats[0][6] !== "nil") {
                        $("#stats_homeless").val(obj.userStats[0][6]);
                    } else {
                        $("#stats_homeless").val("");
                    }
                    $("#submit").html("modify");
                }
                $("#preload_overlay").hide();
            }
        },
        error: function () {
            $("#preload_overlay").hide();
            $("#errormsg-alert").html(servererror);
            $("#errormsg-alert").show();
            var myVar1 = setInterval(function () {
                $("#errormsg-alert").hide();
                clearInterval(myVar1);
            }, 3000);
        }
    });
    return false;
}