//check required
$("input,select,textarea").focus(function () {
    var input_id = $(this).attr("id");
    if ($("#" + input_id).hasClass("input-alert"))
        $("#" + input_id).removeClass("input-alert");
});

paintype = "";
painpassed = "No Pain";

$(function () {

    $(".painicons").click(function () {
        if ($(".painicons").hasClass("selected")) {
            var id = $(".selected").attr("id");
            painpassed = $("#" + id).attr("title");
            $("#" + id).removeClass("selected");
            $("#" + id).attr("src", "../../icons/painicons/" + id.toLowerCase() + "_g.png");
            setpaintype($(this).attr("id"));
        } else {
            setpaintype($(this).attr("id"));

        }
    });

    function setpaintype(id) {

        paintype = $("#" + id).attr("id");
        painpassed = $("#" + id).attr("title");
        $("#" + paintype).addClass("selected");
        var imgname = $("#" + id).attr("src").split("/")[4].split(".png");
        var imgtype = imgname[0].split("_")[1];
        var imgsrc = imgname[0].split("_")[0];
        if (imgtype === "g") {
            $("#" + id).attr("src", "../../icons/painicons/" + imgsrc + "_c.png");
        }
    }
});

other_fields = ["notes"];

var today = new Date();
var dd = today.getDate();
var mm = today.getMonth() + 1; //January is 0!
var yyyy = today.getFullYear();

if (dd < 10) {
    dd = '0' + dd;
}
if (mm < 10) {
    mm = '0' + mm;
}
var today = yyyy + '-' + mm + '-' + dd;

//checkisnan
function checkisnan(passed) {
    var check = 0;
    for (var x = 0; x < passed.length; x++) {
        if ($.trim($("#" + passed[x]).val())) {
            if (!isNaN($.trim($("#" + passed[x]).val()))) {
                $("#" + passed[x]).addClass("input-alert");
                check++;
            }
        }
    }
    return check;
}

//this not support on callback
var bodypartname = "";

$("path").click(function () {
    bodypartname = $(this).attr("id");
    $("path").attr({
        "fill": "transparent",
        "fill-opacity": 1,
        "stroke": "transparent"
    });
    $(this).attr({
        "fill": "red",
        "fill-opacity": 0.5,
        "stroke": "transparent"
    });
});

// rotate structure
function rotateStructure() {

    if ($("#male").css("display") === "block") {

        if ($("#male").find("#male_body_structure").css("display") === "block") {
            $("#male_body_structure").hide();
            $("#male_back_structure").show();
        } else {
            $("#male_body_structure").show();
            $("#male_back_structure").hide();
        }
    } else {

        if ($("#female").find("#female_body_structure").css("display") === "block") {
            $("#female_body_structure").hide();
            $("#female_back_structure").show();
        } else {
            $("#female_body_structure").show();
            $("#female_back_structure").hide();
        }
    }
}

//change model
function changeModel() {

    if ($("#change_holder").html() === "Female") {
        $("#change_holder").html("Male");
    } else {
        $("#change_holder").html("Female");
    }
    if ($("#male").css("display") === "block") {
        $("#male").hide();
        $("#female").show();
    } else {
        $("#female").hide();
        $("#male").show();

    }
}

var rangeValue = "";

//setpain
function setpain() {
    error_count = 0;
    rangeValue = parseInt(painpassed);

    /**** check internetconnection ******/
    if (getConnection() === "false") {
        return false;
    }

    var bodyloc = "";
    var assosiatedsym = "";
    var charctor = "";
    var aggravating = "";
    var alleviating = "";
    var startdate = "0000-00-00 00:00:00";
    var enddate = "0000-00-00 00:00:00";
    var environment = "";
    var typeofpain = "";
    var servity = 0;
    var notes = "";

    bodyloc = bodypartname;

    if ($("#symptoms_anxiety").is(':checked')) {
        assosiatedsym = $("#symptoms_anxiety").val();
    }

    if ($("#symptoms_dizziness").is(':checked')) {
        if (assosiatedsym !== "") {
            assosiatedsym = assosiatedsym + ", " + $("#symptoms_dizziness").val();
        } else {
            assosiatedsym = $("#symptoms_dizziness").val();
        }
    }

    if ($("#irregular_breathing").is(':checked')) {
        if (assosiatedsym !== "") {
            assosiatedsym = assosiatedsym + ", " + $("#irregular_breathing").val();
        } else {
            assosiatedsym = $("#irregular_breathing").val();
        }
    }

    if ($("#symptoms_nausea").is(':checked')) {
        if (assosiatedsym !== "") {
            assosiatedsym = assosiatedsym + ", " + $("#symptoms_nausea").val();
        } else {
            assosiatedsym = $("#symptoms_nausea").val();
        }
    }

    if ($("#symptoms_vomiting").is(':checked')) {
        if (assosiatedsym !== "") {
            assosiatedsym = assosiatedsym + ", " + $("#symptoms_vomiting").val();
        } else {
            assosiatedsym = $("#symptoms_vomiting").val();
        }
    }

    if (assosiatedsym === "") {
        assosiatedsym = "nil";
    }

    /* charactor*/
    if ($("#character_burning").is(':checked')) {
        charctor = $("#character_burning").val();
    }

    if ($("#character_cramping").is(':checked')) {
        if (charctor !== "") {
            charctor = charctor + ", " + $("#character_cramping").val();
        } else {
            charctor = $("#character_cramping").val();
        }
    }

    if ($("#character_dull").is(':checked')) {
        if (charctor !== "") {
            charctor = charctor + ", " + $("#character_dull").val();
        } else {
            charctor = $("#character_dull").val();
        }
    }

    if ($("#character_sharp").is(':checked')) {
        if (charctor !== "") {
            charctor = charctor + ", " + $("#character_sharp").val();
        } else {
            charctor = $("#character_sharp") + val();
        }
    }

    if ($("#character_throbbing").is(':checked')) {
        if (charctor !== "") {
            charctor = charctor + ", " + $("#character_throbbing").val();
        } else {
            charctor = $("#character_throbbing").val();
        }
    }

    if (charctor === "") {
        charctor = "nil";
    }

    /*aggravating*/
    if ($("#aggravating_exercise").is(':checked')) {
        aggravating = $("#aggravating_exercise").val();
    }

    if ($("#aggravating_food").is(':checked')) {
        if (aggravating !== "") {
            aggravating = aggravating + ", " + $("#aggravating_food").val();
        } else {
            aggravating = $("#aggravating_food").val();
        }
    }

    if ($("#aggravating_inactivity").is(':checked')) {
        if (aggravating !== "") {
            aggravating = aggravating + ", " + $("#aggravating_inactivity").val();
        } else {
            aggravating = $("#aggravating_inactivity").val();
        }
    }

    if ($("#aggravating_stress").is(':checked')) {
        if (aggravating !== "") {
            aggravating = aggravating + ", " + $("#aggravating_stress").val();
        } else {
            aggravating = $("#aggravating_stress").val();
        }
    }

    if ($("#aggravating_weather").is(':checked')) {
        if (aggravating !== "") {
            aggravating = aggravating + ", " + $("#aggravating_weather").val();
        } else {
            aggravating = $("#aggravating_weather").val();
        }
    }

    if (aggravating === "") {
        aggravating = "nil";
    }

    /*allevating*/
    if ($("#alleviating_heat").is(':checked')) {
        alleviating = $("#alleviating_heat").val();
    }
    if ($("#alleviating_ice").is(':checked')) {
        if (alleviating !== "") {
            alleviating = alleviating + ", " + $("#alleviating_ice").val();
        } else {
            alleviating = $("#alleviating_ice").val();
        }
    }
    if ($("#alleviating_inactivity").is(':checked')) {
        if (alleviating !== "") {
            alleviating = alleviating + ", " + $("#alleviating_inactivity").val();
        } else {
            alleviating = $("#alleviating_inactivity").val();
        }
    }

    if ($("#alleviating_massage").is(':checked')) {
        if (alleviating !== "") {
            alleviating = alleviating + ", " + $("#alleviating_massage").val();
        } else {
            alleviating = $("#alleviating_massage").val();
        }
    }

    if ($("#alleviating_medication_non").is(':checked')) {
        if (alleviating !== "") {
            alleviating = alleviating + ", " + $("#alleviating_medication_non").val();
        } else {
            alleviating = $("#alleviating_medication_non").val();
        }
    }
    if ($("#alleviating_medication").is(':checked')) {
        if (alleviating !== "") {
            alleviating = alleviating + ", " + $("#alleviating_medication").val();
        } else {
            alleviating = $("#alleviating_medication").val();
        }
    }

    if ($("#alleviating_repositioning").is(':checked')) {
        if (alleviating !== "") {
            alleviating = alleviating + ", " + $("#alleviating_repositioning").val();
        } else {
            alleviating = $("#alleviating_repositioning").val();
        }
    }

    if ($("#alleviating_stretching").is(':checked')) {
        if (alleviating !== "") {
            alleviating = alleviating + ", " + $("#alleviating_stretching").val();
        } else {
            alleviating = $("#alleviating_stretching").val();
        }
    }

    if (alleviating === "") {
        alleviating = "nil";
    }

    startdate = $("#manageMypainStartTime").val().split(" ")[0];
    if (dateandtime($("#manageMypainStartTime").val()) === false || today < startdate) {
        $("#manageMypainStartTime").addClass("input-alert");
        error_count++;
    }
    else {
        startdate = $("#manageMypainStartTime").val() + ":00";
    }

    if ($("#manageMypainEndTime").val()) {
        enddate = $("#manageMypainEndTime").val().split(" ")[0];
        if (dateandtime($("#manageMypainEndTime").val()) === false || today < enddate) {
            $("#manageMypainEndTime").addClass("input-alert");
            error_count++;
        } else {
            enddate = $("#manageMypainEndTime").val() + ":00";
        }
    }
    if ($("#manageMypainStartTime").val() && $("#manageMypainEndTime").val()) {
        var date_start = $("#manageMypainStartTime").val().split(" ")[0];
        var date_end = $("#manageMypainEndTime").val().split(" ")[0];
        if (dateandtime($("#manageMypainStartTime").val()) === false || dateandtime($("#manageMypainEndTime").val()) === false || (date_start > date_end)) {
            $("#manageMypainEndTime").addClass("input-alert");
            error_count++;
        } else {
            startdate = $("#manageMypainStartTime").val() + ":00";
            enddate = $("#manageMypainEndTime").val() + ":00";
        }
    }
    
    /*type of pain*/
    typeofpain = $("input[name='radio_pain']:checked").val();

    /*environment */
    if ($("#environment_home").is(':checked')) {
        environment = $("#environment_home").val();
    }

    if ($("#environment_work").is(':checked')) {
        if (environment !== "") {
            environment = environment + ", " + $("#environment_work").val();
        } else {
            environment = $("#environment_work").val();
        }
    }

    if (environment === "") {
        environment = "nil";
    }

    /*servity*/
    servity = rangeValue;

    /*notes*/
    notes = $("#notes").val();
    if (notes === "" || notes === null) {
        notes = "nil";
    }

    /*submission*/
    if (bodyloc !== "null" && bodyloc !== "" && bodyloc !== " " && error_count === 0) {

        if (checkisnan(other_fields) !== 0) {
            return false;
        }

        $("#status_out").show();
        $("#submit,#cancel").prop("disabled", true);

        $.ajax({
            url: path + "HospitalManagementSystem/HMS/Issues/setmanagepain.jsp",
            type: "POST",
            data: {
                bodyloc: bodyloc,
                assosiatedsym: assosiatedsym,
                charctor: charctor,
                aggravating: aggravating,
                alleviating: alleviating,
                startdate: startdate,
                enddate: enddate,
                environment: environment,
                typeofpain: typeofpain,
                servity: servity,
                notes: notes

            },
            success: function (data) {
                var msg = jQuery.trim(data);
                if (msg === "success") {
                    $("#status_out").hide();
                    $("#successfull-send").html(successfulladddetails);
                    $("#successfull-send").show();
                    localStorage.setItem("bodypart", "null");
                    var myVar1 = setInterval(function () {
                        $("#successfull-send").hide();
                        reset();
                        $("#submit,#cancel").removeProp("disabled");
                        clearInterval(myVar1);
                    }, 3000);
                } else {
                    $("#status_out").hide();
                    $("#errormsg-alert").html(unsuccessfulladddetails);
                    $("#errormsg-alert").show();
                    var myVar1 = setInterval(function () {
                        $("#errormsg-alert").hide();
                        $("#submit,#cancel").removeProp("disabled");
                        clearInterval(myVar1);
                    }, 3000);
                }
            },
            error: function () {
                $("#preload_overlay").hide();
                $("#status_out").hide();
                $("#errormsg-alert").html(servererror);
                $("#errormsg-alert").show();
                var myVar1 = setInterval(function () {
                    $("#errormsg-alert").hide();
                    $("#submit,#cancel").removeProp("disabled");
                    clearInterval(myVar1);
                }, 3000);
            }
        });
        return false;

    } else {
        if (error_count !== 0) {
        } else {
            $("#errormsg-alert").html(selectbodyloc);
            $("#errormsg-alert").show();
            var myVar1 = setInterval(function () {
                $("#errormsg-alert").hide();
                clearInterval(myVar1);
            }, 3000);
        }
    }
}

//reset
function reset() {
    location.reload();
}
function getsex() {

    $("#preload_overlay_light").show();
    $.ajax({
        url: path + "HospitalManagementSystem/HMS/Profile/getprofilestatus.jsp",
        type: "POST",
        success: function (data) {
            var msg = jQuery.trim(data);
            if (msg) {
                obj = JSON.parse(msg);
                if (obj.userProfile.length > 0) {
                    dob = obj.userProfile[0][0];
                    localStorage.setItem("sex", obj.userProfile[0][1]);
                    window.location = "manageMyPainMain.html";
                }
                else {
                    $("#errormsg-alert").html(fillprofile);
                    $("#errormsg-alert").show();
                    $("#preload_overlay_light").hide();
                }
            }
        },
        error: function () {
            $("#preload_overlay_light").hide();
            $("#errormsg-alert").html(servererror);
            $("#errormsg-alert").show();
        }
    });
}