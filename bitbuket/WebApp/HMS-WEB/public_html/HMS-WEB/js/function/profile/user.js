/* assingn store user details */
function assigns() {
    var username1 = localStorage.getItem("useremail");
    var password1 = localStorage.getItem("password");
    if (username1 !== "" && password1 !== "") {
        var plaintext = Crypt.AES.decrypt(password1); //decrypt
        $("#InputEmail").val(username1);
        $("#InputPassword").val(plaintext);
        $("#remember").prop('checked', true);
    }
}

//username
function username() {
    
    $.ajax({
        url: path + "HospitalManagementSystem/HMS/Profile/getusername.jsp",
        type: "POST",
        data: {
        },
        success: function (data) {
            var msg = jQuery.trim(data);
            if (msg) {
                localStorage.setItem("userprofilename", msg);
                acceptterms();
            } else {
                localStorage.setItem("userprofilename", "New User");
                $("#status_out").hide();
                window.location = "../../process/common/termsAndcondition.html";
            }

            var text = "";
            var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
            for (var i = 0; i < 10; i++)
            {
                text += possible.charAt(Math.floor(Math.random() * possible.length));
            }
            localStorage.setItem("session", text);
        }
    });
    return false;
}

/* user login request */
function login() {

    /***** Internet check ***/
    if (getConnection() === "false") {
        return false;
    }

//validateEmail
    function validateEmail(Email) {

        var filter = /^[\w\-\.\+]+\@[a-zA-Z0-9\.\-]+\.[a-zA-z0-9]{2,4}$/;
        if (filter.test(Email)) {
            return true;
        } else {
            return false;
        }
    }

    if ($("#InputEmail").val() !== "" && $("#InputPassword").val() !== "") {

        if (validateEmail($("#InputEmail").val()) == true) {

            var form = $("#login-form");
            var requesturl = path + "HospitalManagementSystem/HMS/User/userLogin.jsp";
            $("#status_out").show();
            
            $.ajax({
                type: "POST",
                url: requesturl,
                data: form.serialize(),
                success: function (html) {
                    var msg = jQuery.trim(html);
                    if (msg === "success") {
                        username();
                        localStorage.setItem("usertype", "registeruser");
                        if ($("#remember").is(":checked")) {
                            var ciphertext = Crypt.AES.encrypt($("#InputPassword").val());//encrypt text
                            localStorage.setItem("useremail", $("#InputEmail").val());
                            localStorage.setItem("password", ciphertext);
                        } else {
                            localStorage.setItem("useremail", "");
                            localStorage.setItem("password", "");
                        }
                    } else {
                        $("#status_out").hide();
                        $("#user-alert").html(unsuccesslogin);
                        $("#user-alert ").show();
                        var myVar1 = setInterval(function () {
                            $("#user-alert ").hide();
                            clearInterval(myVar1);
                        }, 3000);
                        $("#status_out ").hide();
                    }
                },
                error: function () {
                    $("#status_out").hide();
                    $("#user-alert").html(servererror);
                    $("#user-alert ").show();
                    var myVar1 = setInterval(function () {
                        $("#user-alert ").hide();
                        clearInterval(myVar1);
                    }, 3000);
                    $("#status_out ").hide();
                }
            });
            return false;
        } else {
            $("#user-alert").html(valdateemailerror);
            $("#user-alert").show();
            var hidestatus = setInterval(function () {
                $("#user-alert").hide();
                clearInterval(hidestatus);
            }, 3000);
        }
    }
}

/* user Register */
function userRegister() {

    /***** Internet check ***/
    if (getConnection() === "false") {
        return false;
    }

    if (validateEmail($("#InputEmailReg").val()) === true) {

        var firstpassword = $("#InputPasswordReg").val();
        var secoundpassword = $("#InputSecoundPasswordReg").val();

        if (firstpassword.length >= 5 && secoundpassword.length >= 5) {

            if (firstpassword === secoundpassword) {
                $("#reg_status_out").show();
                var form = $('#signin-form-reg');
                var requesturl = path + "HospitalManagementSystem/HMS/User/userRegister.jsp";

                $.ajax({
                    type: "POST",
                    url: requesturl,
                    data: form.serialize(),
                    success: function (html) {
                        var msg = jQuery.trim(html);
                        if (msg === "success") {
                            username();
                            localStorage.setItem("usertype", "registeruser");
                            $("#InputEmailReg").val("");
                            $("#InputPasswordReg").val("");
                            $("#InputSecoundPasswordReg").val("");
                            $("#reg_status_out").hide();
                            //window.location = "firstlogin.html";

                        } else if (msg === "alreadyhave")
                        {
                            $("#user-alert-reg-aval").html(unsuccesslogin);
                            $("#user-alert-reg-aval").show();
                            var myVar1 = setInterval(function () {
                                $("#user-alert-reg-aval").hide();
                                clearInterval(myVar1);
                            }, 3000);
                            $("#sreg_status_out").hide();
                        } else {
                            $("#user-alert-reg").html(unsuccesslogin);
                            $("#user-alert-reg").show();
                            var myVar1 = setInterval(function () {
                                $("#user-alert-reg").hide();
                                clearInterval(myVar1);
                            }, 3000);
                            $("#sreg_status_out").hide();
                        }
                    }, error: function () {
                        $("#user-alert-reg").html(servererror);
                        $("#user-alert-reg").show();
                        var myVar1 = setInterval(function () {
                            $("#user-alert-reg").hide();
                            clearInterval(myVar1);
                        }, 3000);
                        $("#sreg_status_out").hide();
                    }
                });
                return false;
            } else {

                $("#user-alert-reg").html(unsuccesslogin);
                $("#user-alert-reg").show();
                var myVar1 = setInterval(function () {
                    $("#user-alert-reg").hide();
                    clearInterval(myVar1);
                }, 3000);
                $("#status_out").hide();
            }
        } else {

            $("#user-alert-reg").html(unsuccesslogin);
            $("#user-alert-reg").show();
            var myVar1 = setInterval(function () {
                $("#user-alert-reg").hide();
                clearInterval(myVar1);
            }, 3000);
        }

    } else {

        $("#user-alert-reg").html(valdateemailerror);
        $("#user-alert-reg").show();
        var myVar1 = setInterval(function () {
            $("#user-alert-reg").hide();
            clearInterval(myVar1);
        }, 3000);
    }
}

//check required
$(function () {
    $("input").focus(function () {
        $input_id = $(this).attr("id");
        if ($("#" + $input_id).hasClass("input-alert"))
            $("#" + $input_id).removeClass("input-alert");
    });

    $("#submit_first").click(function () {
//        $(name_of_input).removeClass("input-alert");
        $fields = $("form#login-form").serialize();
        form_array = $fields.split('&');
        for (var i = 0; i < form_array.length; i++) {
            name = form_array[i].split('=')[0];
            value = form_array[i].split('=')[1];

            if ($.trim(value) === "") {
                var name_of_input = "#" + name;
                $(name_of_input).addClass("input-alert");
            }
        }
    });

//register on click
    $("#register").click(function () {
        $fields = $("form#signin-form-reg").serialize();
        form_array = $fields.split('&');
        for (var i = 0; i < form_array.length; i++) {
            name = form_array[i].split('=')[0];
            value = form_array[i].split('=')[1];
            if ($.trim(value) === "") {
                var name_of_input = "#" + name;
                $(name_of_input).addClass("input-alert");
            }
        }
    });
});

//close modal
$(".close").on("click", function () {
    $("input").removeClass("input-alert");
    $('#forgotpassword_model').modal('hide');
});

//email validation
function validateEmail(Email) {
    var filter = /^[\w\-\.\+]+\@[a-zA-Z0-9\.\-]+\.[a-zA-z0-9]{2,4}$/;
    if (filter.test(Email)) {
        return true;
    } else {
        return false;
    }
}

//passwordrecover
function passwordrecover() {
    /***** Internet check ***/
    if (getConnection() === "false") {
        return false;
    }

    if ($("#recover_email").val() !== "" && $("#recover_email").val() !== " " && validateEmail($("#recover_email").val())) {
        $("#recover_status_out").show();
        var requesturl = path + "HospitalManagementSystem/HMS/User/passwordreset.jsp";
        $.ajax({
            type: "POST",
            url: requesturl,
            data: {
                email: $("#recover_email").val()
            },
            success: function (html) {
                var msg = jQuery.trim(html);
                if (msg === "success") {
                    $("#recover_status_out").hide();
                    $("#recover-successfull-send").html(successfullrecover);
                    $("#recover-successfull-send").show();
                    $("#recover_email").val("");
                    var myVar1 = setInterval(function () {
                        $("#recover-successfull-send").hide();
                        clearInterval(myVar1);
                    }, 3000);

                } else {
                    $("#recover_status_out").hide();
                    $("#recover-errormsg-alert").hide();
                    $("#recover-errormsg-alert").html(invalidemail);
                    $("#recover-errormsg-alert").show();
                    var myVar1 = setInterval(function () {
                        $("#recover-errormsg-alert").hide();
                        clearInterval(myVar1);
                    }, 3000);
                }
            }, error: function () {
                $("#recover_status_out").hide();
                $("#recover-errormsg-alert").hide();
                $("#recover-errormsg-alert").html(servererror);
                $("#recover-errormsg-alert").show();
                var myVar1 = setInterval(function () {
                    $("#recover-errormsg-alert").hide();
                    clearInterval(myVar1);
                }, 3000);
            }
        });
        return false;
    }
    else {
        $("#recover_email").addClass("input-alert");
    }
}

//acceptterms
function acceptterms() {

    $.ajax({
        url: path + "HospitalManagementSystem/HMS/Profile/getagrrementstatus.jsp",
        type: "POST",
        data: {},
        success: function (data) {
            $("#status_out").hide();
            $("#signin").removeProp("disabled");
            var msg = jQuery.trim(data);

            if (msg == "yes") {
                window.location = "../../process/common/firstlogin.html";
            } else {
                window.location = "../../process/common/termsAndcondition.html";
            }
        },
        error: function () {

            $("#status_out").hide();
            $("#signin").removeProp("disabled");
            $("#user-alert").html(servererror);
            $("#user-alert").show();
            var hidestatus = setInterval(function () {
                $("#user-alert").hide();
                clearInterval(hidestatus);
            }, 3000);
        }
    });

}