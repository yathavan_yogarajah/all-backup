//check required
$("input,select").focus(function () {
    var input_id = $(this).attr("id");
    if ($("#" + input_id).hasClass("input-alert"))
        $("#" + input_id).removeClass("input-alert");
});

//contact before transition validateEmail
function validateEmail(Email) {
    
    var filter = /^[\w\-\.\+]+\@[a-zA-Z0-9\.\-]+\.[a-zA-z0-9]{2,4}$/;
    if (filter.test(Email)) {
        return true;
    } else {
        return false;
    }
}

//slide contactNextPage
function contactNextPage() {

    var address = $.trim($("#contact_street").val());
    var city = $.trim($("#contact_city").val());
    var state = $.trim($("#contact_state").val());
    var countrycode = $.trim($("#contact_country_code").val());
    var postal = $.trim($("#contact_postalcode").val());
    var contact = ["contact_street", "contact_state", "contact_country_code", "contact_city", "contact_postalcode"];
    other_contacts = ["contact_mothersname", "contact_guardiansname"];
    error = 0;

    if (isNaN(address) && isNaN(state) && isNaN(countrycode) && isNaN(city) && postal && error === 0) {
    } else {
        for (i = 0; i < contact.length; i++) {
            var current = $("#" + contact[i]).val();
            if (current === "" || !isNaN(current)) {
                error++;
                if (contact[i] === "contact_country_code") {
                    $("#" + contact[i]).addClass("input-alert");
                } else {
                    $("#" + contact[i]).addClass("input-alert");
                }
            }
        }
    }
}

//checkisnan
function checkisnan(passed) {
    var check = 0;
    for (var x = 0; x < passed.length; x++) {
        if ($.trim($("#" + passed[x]).val())) {
            if (!isNaN($.trim($("#" + passed[x]).val()))) {
                $("#" + passed[x]).addClass("input-alert");
                check++;
            }
        }
    }
    return check;
}

// validate contact
function contact() {

    contactNextPage();
    email = $("#contact_email").val();
    phone = $("#contact_phone_cell").val();

    if (!$("#contact_phone_cell").val()) {
        $("#contact_phone_cell").addClass("input-alert");
        error++;
    }
    if (!validateEmail(email)) {
        $("#contact_email").addClass("input-alert");
        error++;
    }

    if (phone && validateEmail(email) === true && (error === 0)) {
        if (checkisnan(other_contacts) === 0) {
            setcontact(); //ajax to submit db
        }
    }
}
// set contact
function setcontact() {

    /***** Internet check ***/
    if (getConnection() === "false") {
        return false;
    }

    $("#status_out").show();
    $("#submit,#cancel").prop("disabled", true);

    $.ajax({
        url: path + "HospitalManagementSystem/HMS/Profile/addcontact.jsp",
        type: "POST",
        data: $("#contact").serialize(),
        success: function (data) {
            var msg = jQuery.trim(data);
            if (msg === "success") {
                $("#status_out").hide();
                $("#successfull-send").html(successfulladddetails);
                $("#successfull-send").show();
                var myVar1 = setInterval(function () {
                    $("#successfull-send").hide();
                    $("#submit,#cancel").removeProp("disabled");
                    clearInterval(myVar1);
                }, 3000);
            } else {
                $("#status_out").hide();
                $("#errormsg-alert").html(unsuccessfulladddetails);
                $("#errormsg-alert").show();
                var myVar1 = setInterval(function () {
                    $("#errormsg-alert").hide();
                    $("#submit,#cancel").removeProp("disabled");
                    clearInterval(myVar1);
                }, 3000);
            }
        },
        error: function () {
            $("#status_out").hide();
            $("#errormsg-alert").html(servererror);
            $("#errormsg-alert").show();
            var myVar1 = setInterval(function () {
                $("#errormsg-alert").hide();
                $("#submit,#cancel").removeProp("disabled");
                clearInterval(myVar1);
            }, 3000);
        }
    });
    return false;
}

//get contact
function getcontact() {
    $("#preload_overlay").show();
    $.ajax({
        url: path + "HospitalManagementSystem/HMS/Profile/getcontact.jsp",
        type: "POST",
        success: function (data) {
            var msg = jQuery.trim(data);
            if (msg) {
                obj = JSON.parse(msg);
                if (obj.userContact.length > 0) {

                    $("#contact_street").val(obj.userContact[0][0]);
                    $("#contact_city").val(obj.userContact[0][1]);
                    $("#contact_state").val(obj.userContact[0][2]);
                    $("#contact_postalcode").val(obj.userContact[0][3]);
                    $("#contact_country_code").val(obj.userContact[0][4]);

                    if (obj.userContact[0][5] !== "nil") {
                        $("#contact_mothersname").val(obj.userContact[0][5]);
                    } else {
                        $("#contact_mothersname").val("");
                    }

                    if (obj.userContact[0][6] !== "nil") {
                        $("#contact_guardiansname").val(obj.userContact[0][6]);
                    } else {
                        $("#contact_guardiansname").val("");
                    }

                    if (obj.userContact[0][7] !== "nil") {
                        $("#contact_emergency").val(obj.userContact[0][7]);
                    } else {
                        $("#contact_emergency").val("");
                    }

                    $("#contact_email").val(obj.userContact[0][8]);

                    if (obj.userContact[0][9] !== "nil") {
                        $("#contact_phone_em").val(obj.userContact[0][9]);
                    } else {
                        $("#contact_phone_em").val("");
                    }

                    if (obj.userContact[0][10] !== "nil") {
                        $("#contact_phone_biz").val(obj.userContact[0][10]);
                    } else {
                        $("#contact_phone_biz").val("");
                    }

                    if (obj.userContact[0][11] !== "nil") {
                        $("#contact_phone_home").val(obj.userContact[0][11]);
                    } else {
                        $("#contact_phone_home").val("");
                    }
                    $("#contact_phone_cell").val(obj.userContact[0][12]);
                } else {
                    getemail();
                }
            }
            $("#preload_overlay").hide();
        },
        error: function () {
            $("#preload_overlay").hide();
            $("#errormsg-alert").html(servererror);
            $("#errormsg-alert").show();
            var myVar1 = setInterval(function () {
                $("#errormsg-alert").hide();
                clearInterval(myVar1);
            }, 3000);
        }
    });
    return false;
}
//get email
function getemail() {

    $.ajax({
        url: path + "HospitalManagementSystem/HMS/Profile/getemail.jsp",
        type: "POST",
        success: function (data) {
            var msg = jQuery.trim(data);
            $("#contact_email").val(msg);
        }
    });
    
    return false;
}
