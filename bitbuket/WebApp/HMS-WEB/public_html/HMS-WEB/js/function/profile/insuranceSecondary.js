var today = new Date();
var dd = today.getDate();
var mm = today.getMonth() + 1; //January is 0!

var yyyy = today.getFullYear();
if (dd < 10) {
    dd = '0' + dd;
}
if (mm < 10) {
    mm = '0' + mm;
}
today = yyyy + '-' + mm + '-' + dd;

other_insurance = ["subscriber", "subscriber_street", "subscriber_city", "subscriber_state", "se_employer", "se_street", "se_city", "se_state"];

//checkisnan
function checkisnan(passed) {
    var check = 0;
    for (var x = 0; x < passed.length; x++) {
        if ($.trim($("#" + passed[x]).val())) {
            if (!isNaN($.trim($("#" + passed[x]).val()))) {
                $("#" + passed[x]).addClass("input-alert");
                check++;
            }
        }
    }
    return check;
}

$("#effective_date").datepicker({
    dateFormat: "yy-mm-dd",
    changeMonth: true,
    changeYear: true,
    yearRange: '1900:2025'
});

$("#subscriber_DOB").datepicker({
    dateFormat: "yy-mm-dd",
    changeMonth: true,
    changeYear: true,
    yearRange: '1900:2025'
});

$("input,select").focus(function () {
    var input_id = $(this).attr("id");
    if ($("#" + input_id).hasClass("input-alert"))
        $("#" + input_id).removeClass("input-alert");
});


//secondary insurance
function secondary() {
    if (datecheck() && checkisnan(other_insurance) === 0) {
        setsecoundaryinsurance();
    }
}

//date validate
var datevalidate = 0;
var dateres = true;

//function date check
function datecheck() {

    datevalidate = 0;
    dateres = true;
    $count = 0;

    if ($("#provider").val() === "" || $("#provider").val() === " " || $("#provider").val() === null || !isNaN($("#provider").val())) {
        $("#provider").addClass("input-alert");
        $count++;
    }

    if ($("#plan_name").val() === "" || $("#plan_name").val() === " " || $("#plan_name").val() === null || !isNaN($("#plan_name").val())) {
        $("#plan_name").addClass("input-alert");
        $count++;
    }
    if ($count !== 0) {
        return false;
    }

    if ($("#effective_date").val()) {

        var today = new Date();
        var dd = today.getDate();
        var mm = today.getMonth() + 1; //January is 0!
        var yyyy = today.getFullYear();
        if (dd < 10) {
            dd = '0' + dd;
        }
        if (mm < 10) {
            mm = '0' + mm;
        }
        var today = yyyy + '-' + mm + '-' + dd;

        if (new Date($("#effective_date").val()) < new Date(today)) {
            $("#effective_date").addClass("input-alert");
            datevalidate++;
        }

        if (myFunction($("#effective_date").val()) === false) {
            $("#effective_date").addClass("input-alert");
            $("#errormsg-alert").html(dateerror);
            $("#errormsg-alert").show();
            var myVar1 = setInterval(function () {
                $("#errormsg-alert").hide();
                clearInterval(myVar1);
            }, 3000);
            datevalidate++;
        }
    }

    if ($("#subscriber_DOB").val()) {
        if ($("#subscriber_DOB").val() >= today) {
            $("#subscriber_DOB").addClass("input-alert");
            datevalidate++;
        }
        if (myFunction($("#subscriber_DOB").val()) === false) {
            $("#subscriber_DOB").addClass("input-alert");
            $("#errormsg-alert").html(dateerror);
            $("#errormsg-alert").show();
            var myVar1 = setInterval(function () {
                $("#errormsg-alert").hide();
                clearInterval(myVar1);
            }, 3000);
            datevalidate++;
        }
    }

    if (datevalidate !== 0) {
        dateres = false;
    }
    return dateres;
}


//setsecoundary insurance
function setsecoundaryinsurance() {

    /***** Internet check ***/
    if (getConnection() === "false") {
        return false;
    }

    $("#status_out").show();
    $("#submit,#cancel").prop("disabled", true);

    $.ajax({
        url: path + "HospitalManagementSystem/HMS/Profile/addsecoundaryinsurance.jsp",
        type: "POST",
        data: $("#secondary_insurance").serialize(),
        success: function (data) {
            var msg = jQuery.trim(data);
            if (msg === "success") {
                $("#status_out").hide();
                $("#successfull-send").html(successfulladddetails);
                $("#successfull-send").show();
                var myVar1 = setInterval(function () {
                    $("#submit,#cancel").removeProp("disabled");
                    $("#successfull-send").hide();
                    clearInterval(myVar1);
                }, 3000);
            } else {
                $("#status_out").hide();
                $("#errormsg-alert").html(unsuccessfulladddetails);
                $("#errormsg-alert").show();
                var myVar1 = setInterval(function () {
                    $("#errormsg-alert").hide();
                    $("#submit,#cancel").removeProp("disabled");
                    clearInterval(myVar1);
                }, 3000);
            }
        },
        error: function () {
            $("#status_out").hide();
            $("#errormsg-alert").html(servererror);
            $("#errormsg-alert").show();
            var myVar1 = setInterval(function () {
                $("#errormsg-alert").hide();
                $("#submit,#cancel").removeProp("disabled");
                clearInterval(myVar1);
            }, 3000);
        }
    });
    return false;
}

// get secoundary insurance
function getsecoundaryinsurance() {

    $("#preload_overlay").show();
    $.ajax({
        url: path + "HospitalManagementSystem/HMS/Profile/getsecoundaryinsurance.jsp",
        type: "POST",
        success: function (data) {
            var msg = jQuery.trim(data);
            if (msg) {

                obj = JSON.parse(msg);

                if (obj.userInsurance.length > 0) {

                    if (obj.userInsurance[0][0] !== "nil") {
                        $("#provider").val(obj.userInsurance[0][0]);
                    } else {
                        $("#provider").val("");
                    }

                    if (obj.userInsurance[0][1] !== "nil") {
                        $("#plan_name").val(obj.userInsurance[0][1]);
                    } else {
                        $("#plan_name").val("");
                    }

                    if (obj.userInsurance[0][2] !== "nil") {
                        $("#subscriber").val(obj.userInsurance[0][2]);
                    } else {
                        $("#subscriber").val("");
                    }

                    if (obj.userInsurance[0][3] !== "1000-10-10") {
                        $("#effective_date").val(obj.userInsurance[0][3]);
                    } else {
                        $("#effective_date").val("");
                    }

                    if (obj.userInsurance[0][4] !== "nil") {
                        $("#relationship").val(obj.userInsurance[0][4]);
                    } else {
                        $("#relationship").val("");
                    }

                    if (obj.userInsurance[0][5] !== "nil") {
                        $("#policy_number").val(obj.userInsurance[0][5]);
                    } else {
                        $("#policy_number").val("");
                    }

                    if (obj.userInsurance[0][6] !== "1000-10-10") {
                        $("#subscriber_DOB").val(obj.userInsurance[0][6]);
                    } else {
                        $("#subscriber_DOB").val("");
                    }

                    if (obj.userInsurance[0][7] !== "nil") {

                        $("#subscriber_sex").val(obj.userInsurance[0][7]);
                    } else {
                        $("#subscriber_sex").val("");
                    }

                    if (obj.userInsurance[0][8] !== "nil") {
                        $("#group_number").val(obj.userInsurance[0][8]);
                    } else {
                        $("#group_number").val("");
                    }

                    if (obj.userInsurance[0][9] !== "nil") {
                        $("#subscriber_street").val(obj.userInsurance[0][9]);
                    } else {
                        $("#subscriber_street").val("");
                    }

                    if (obj.userInsurance[0][10] !== "nil") {
                        $("#subscriber_city").val(obj.userInsurance[0][10]);
                    } else {
                        $("#subscriber_city").val("");
                    }

                    if (obj.userInsurance[0][11] !== "nil") {
                        $("#subscriber_state").val(obj.userInsurance[0][11]);
                    } else {
                        $("#subscriber_state").val("");
                    }

                    if (obj.userInsurance[0][12] !== "nil") {
                        $("#subscriber_postal").val(obj.userInsurance[0][12]);
                    } else {
                        $("#subscriber_postal").val("");
                    }

                    if (obj.userInsurance[0][13] !== "nil") {
                        document.getElementById("subscriber_country").value = obj.userInsurance[0][13];
                    } else {
                        document.getElementById("subscriber_country").value = "";
                    }

                    if (obj.userInsurance[0][14] !== "nil") {
                        $("#subscriber_phone").val(obj.userInsurance[0][14]);
                    } else {
                        $("#subscriber_phone").val("");
                    }
                    if (obj.userInsurance[0][15] !== "nil") {

                        $("#se_employer").val(obj.userInsurance[0][15]);
                    } else {
                        $("#se_employer").val("");
                    }

                    if (obj.userInsurance[0][16] !== "nil") {
                        $("#se_street").val(obj.userInsurance[0][16]);
                    } else {
                        $("#se_street").val("");
                    }

                    if (obj.userInsurance[0][17] !== "nil") {
                        $("#se_city").val(obj.userInsurance[0][17]);
                    } else {
                        $("#se_city").val("");
                    }

                    if (obj.userInsurance[0][18] !== "nil") {
                        $("#se_state").val(obj.userInsurance[0][18]);
                    } else {
                        $("#se_state").val("");
                    }

                    if (obj.userInsurance[0][19] !== "nil") {
                        $("#se_postal_code").val(obj.userInsurance[0][19]);
                    } else {
                        $("#se_postal_code").val("");
                    }

                    if (obj.userInsurance[0][20] !== "nil") {
                        $("#se_country").val(obj.userInsurance[0][20]);
                    } else {
                        $("#se_country").val("");
                    }
                }
            }
            $("#preload_overlay").hide();
        },
        error: function () {
            $("#preload_overlay").hide();
            $("#errormsg-alert").html(unsuccessfulladddetails);
            $("#errormsg-alert").show();
            var myVar1 = setInterval(function () {
                $("#errormsg-alert").hide();
                clearInterval(myVar1);
            }, 3000);
        }
    });
    return false;
}