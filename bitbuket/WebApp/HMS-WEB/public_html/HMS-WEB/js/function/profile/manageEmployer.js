//check required
$("input,select").focus(function () {
    var input_id = $(this).attr("id");
    if ($("#" + input_id).hasClass("input-alert"))
        $("#" + input_id).removeClass("input-alert");
});


// validate employer
function employer() {

    error = 0;
    var required = [];
    fields = $("form#employer").serialize();
    form_array = fields.split('&');

    for (var i = 0; i < form_array.length; i++) {
        name = form_array[i].split('=')[0];
        value = form_array[i].split('=')[1];
        required[i] = value;
        if (name === "employer_mname" || name === "employer_postal_code") {

        } else {

            if ($.trim(value) === "" || !isNaN($.trim(value))) {
                var name_of_input = "#" + name;
                if (name === "employer_country") {
                    $(name_of_input).addClass("input-alert");
                    error++;
                } else {
                    $(name_of_input).addClass("input-alert");
                    error++;
                }
            }
        }
    }

    postal = $("#employer_postal_code").val();

    if (postal.length === 0) {
        $("#employer_postal_code").addClass("input-alert");
        error++;
    }
    count = 0;
    for (i = 0; i <= required.length; i++) {
        if ($.trim(required[i]) === "") {
            count++;
        }
    }
    if ((count - 1) === 0 && (error === 0)) {
        setemp();
    }
}

// set employer
function setemp() {

    /***** Internet check ***/
    if (getConnection() === "false") {
        return false;
    }

    $("#status_out").show();
    $("#submit,#cancel").prop("disabled", true);
    $.ajax({
        url: path + "HospitalManagementSystem/HMS/Profile/addemp.jsp",
        type: "POST",
        data: $("#employer").serialize(),
        success: function (data) {
            var msg = jQuery.trim(data);
            if (msg === "success") {
                $("#status_out").hide();
                $("#successfull-send").html(successfulladddetails);
                $("#successfull-send").show();
                var myVar1 = setInterval(function () {
                    $("#successfull-send").hide();
                    $("#submit,#cancel").removeProp("disabled");
                    clearInterval(myVar1);
                }, 3000);
            }
            else {
                $("#status_out").hide();
                $("#errormsg-alert").html(unsuccessfulladddetails);
                $("#errormsg-alert").show();
                var myVar1 = setInterval(function () {
                    $("#errormsg-alert").hide();
                    $("#submit,#cancel").removeProp("disabled");
                    clearInterval(myVar1);
                }, 3000);
            }
        }, error: function () {
            $("#status_out").hide();
            $("#errormsg-alert").html(servererror);
            $("#errormsg-alert").show();
            var myVar1 = setInterval(function () {
                $("#errormsg-alert").hide();
                $("#submit,#cancel").removeProp("disabled");
                clearInterval(myVar1);
            }, 3000);
        }
    });
    return false;
}

//get emp
function getemp() {
    $("#preload_overlay").show();
    $.ajax({
        url: path + "HospitalManagementSystem/HMS/Profile/getemp.jsp",
        type: "POST",
        success: function (data) {
            var msg = jQuery.trim(data);
            if (msg) {
                obj = JSON.parse(msg);
                if (obj.userEmployer.length > 0) {
                    $("#employer_name").val(obj.userEmployer[0][0]);
                    $("#employer_occupation").val(obj.userEmployer[0][1]);
                    $("#employer_street").val(obj.userEmployer[0][2]);
                    $("#employer_city").val(obj.userEmployer[0][3]);
                    $("#employer_state").val(obj.userEmployer[0][4]);
                    $("#employer_postal_code").val(obj.userEmployer[0][5]);
                    $("#employer_country").val(obj.userEmployer[0][6]);
                }
            }
            $("#preload_overlay").hide();
        },
        error: function () {
            $("#preload_overlay").hide();
            $("#errormsg-alert").html(servererror);
            $("#errormsg-alert").show();
            var myVar1 = setInterval(function () {
                $("#errormsg-alert").hide();
                clearInterval(myVar1);
            }, 3000);
        }
    });
    return false;
}