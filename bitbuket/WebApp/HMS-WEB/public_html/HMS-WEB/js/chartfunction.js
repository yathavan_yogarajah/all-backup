var process_status = 0;
var profilename = localStorage.getItem("userprofilename");

function process() {
    process_status++;
    if (process_status === 9) {

        $("#preload_overlaychart").hide("fast", function () {
            $(".pageview").hide();
            $(".chartsload").css({
                "opacity": 1
            });
            $('#add_custom').modal('show');
            $(".boxwrap").show();
            $("#page-1").fadeIn();
        });
    }
}

var date1 = [];
var date2 = [];
var heartrate1 = [];
var heartrate2 = [];

//chartassign
function chartassign() {
    getheartrate();
}

//date format
function dateconvert(dates) {

    var d = dates;
    splitData = d.split('-');
    var months = ["Jan", "Feb", "Mar", "April", "May", "June", "July", "Aug", "Sept", "Oct", "Nov", "Dec"];
    var date = splitData[2];
    var month = months[(splitData[1].replace("", "")) - 1];
    return date + " " + month;
}

//heartrateassign
function heartrateAssign() {

    $('#heartrate').highcharts({
        chart: {
            zoomType: 'xy'
        },
        credits: {
            enabled: false
        },
        title: {
            text: 'Heart Rate'
        },
        xAxis: {
            categories: date1
        },
        yAxis: {
            title: {
                text: ''
            }
        },
        tooltip: {
            crosshairs: true,
            headerFormat: '<b>heart rate</b><br>',
            pointFormat: '{point.y}bpm'

        },
        legend: {},
        noData: {
            style: {
                fontWeight: 'bold',
                fontSize: '15px',
                color: '#303030 '
            }
        },
        series: [{
                name: 'Bpm',
                showInLegend: false,
                color: '#d9534f',
                data: heartrate1,
                marker: {
                    symbol: 'circle'
                }
            }],
    });

    var chart = $('#heartrate').highcharts();
    $('#next').on("click", function () {
        chart.series[0].update({
            data: heartrate2
        });
        chart.xAxis[0].setCategories(date2);
    });
    $('#prev').on("click", function () {
        chart.series[0].update({
            data: heartrate1
        });
        chart.xAxis[0].setCategories(date1);
    });

    getoximarity();
}

//get heartrate
function getheartrate() {

    $.ajax({
        url: path + "HospitalManagementSystem/HMS/AppDashBoard/getheartrate.jsp",
        type: "POST",
        data: {},
        success: function (data) {
            var msg = jQuery.trim(data);
            if (msg) {
                obj = JSON.parse(msg);
                if (obj.heartrate.length > 0) {
                    $("#vital-heartrate").html(obj.heartrate[obj.heartrate.length - 1][1]);
                    for (i = 0; i < obj.heartrate.length; i++) {
                        if (i < 5) {
                            var covertdate = dateconvert(obj.heartrate[i][0]);
                            date1.push(covertdate);
                            heartrate1.push(obj.heartrate[i][1]);
                        } else {
                            var covertdate = dateconvert(obj.heartrate[i][0]);
                            date2.push(covertdate);
                            heartrate2.push(obj.heartrate[i][1]);
                        }
                    }
                    heartrateAssign();
                } else {
                    heartrateAssign();
                }
            }
            process();
        },
        error: function (data) {
            process();
        }
    });
    return false;
}

var oxdate1 = [];
var oxdate2 = [];
var oxrate1 = [];
var oxrate2 = [];

//assign oximarity
function assignOximarity() {

    $('#oximetry').highcharts({
        chart: {
            zoomType: 'xy'
        },
        title: {
            text: 'Oximetry'
        },
        xAxis: {
            categories: oxdate1

        },
        yAxis: {
            title: {
                text: ''
            }
        },
        credits: {
            enabled: false
        },
        tooltip: {
            crosshairs: true,
            headerFormat: '<b>Oximetry</b><br>',
            pointFormat: '{point.y}%'
        },
        legend: {},
        noData: {
            style: {
                fontWeight: 'bold',
                fontSize: '15px',
                color: '#303030 '
            }
        },
        series: [{
                name: 'Oximetry',
                showInLegend: false,
                color: '#399225',
                data: oxrate1,
                marker: {
                    symbol: 'circle'
                }
            }]
    });

    var chart2 = $('#oximetry').highcharts();
    $('#oximetry_next').on("click", function () {
        chart2.series[0].update({
            data: oxrate2
        });
        chart2.xAxis[0].setCategories(oxdate2);
    });

    $('#oximetry_prev').on("click", function () {
        chart2.series[0].update({
            data: oxrate1
        });
        chart2.xAxis[0].setCategories(oxdate1);
    });

    getbloodpreasure();
}

// get oximarity
function getoximarity() {

    $.ajax({
        url: path + "HospitalManagementSystem/HMS/AppDashBoard/getoximetry.jsp",
        type: "POST",
        data: {},
        success: function (data) {
            var msg = jQuery.trim(data);
            if (msg) {
                obj = JSON.parse(msg);
                if (obj.oximetry.length > 0) {
                    for (i = 0; i < obj.oximetry.length; i++) {
                        if (i < 5) {
                            var covertdate = dateconvert(obj.oximetry[i][0]);
                            oxdate1.push(covertdate);
                            oxrate1.push(obj.oximetry[i][1]);
                        } else {
                            var covertdate = dateconvert(obj.oximetry[i][0]);
                            oxdate2.push(covertdate);
                            oxrate2.push(obj.oximetry[i][1]);
                        }
                    }
                    assignOximarity();
                } else {
                    assignOximarity();
                }
            }
            process();
        },
        error: function () {
            process();
        }
    });
    return false;
}

var blooddate1 = [];
var blooddate2 = [];
var bloodrate1 = [];
var bloodrate2 = [];
var bloodrate3 = [];
var bloodrate4 = [];

// assign bloodpreasure
function assignBloodpreasure() {

    $('#blood').highcharts({
        chart: {
            zoomType: 'xy'
        },
        title: {
            text: 'Blood Pressure'
        },
        xAxis: {
            categories: blooddate1
        },
        yAxis: {
            title: {
                text: ''
            }
        },
        credits: {
            enabled: false
        },
        tooltip: {
            crosshairs: true,
            headerFormat: '<b>Blood Pressure</b><br>',
            pointFormat: '{point.y}mmHg'

        },
        legend: {},
        noData: {
            style: {
                fontWeight: 'bold',
                fontSize: '15px',
                color: '#303030 '
            }
        },
        series: [{
                name: 'SYS(mmHg)',
                color: '#0db5ff',
                marker: {
                    symbol: 'circle'
                },
                data: bloodrate1
            }, {
                name: 'DIA(mmHg)',
                color: '#3a75c4',
                marker: {
                    symbol: 'circle'
                },
                data: bloodrate3
            }]

    });

    var chart3 = $('#blood').highcharts();
    $('#blood_next').on("click", function () {
        chart3.series[0].update({
            data: bloodrate2
        });
        chart3.series[1].update({
            data: bloodrate4
        });
        chart3.xAxis[0].setCategories(blooddate2);
    });

    $('#blood_prev').on("click", function () {
        ;
        chart3.series[0].update({
            data: bloodrate1
        });
        chart3.series[1].update({
            data: bloodrate3
        });
        chart3.xAxis[0].setCategories(blooddate1);
    });

    getsleep();
}

// get bloodpreasure
function getbloodpreasure() {

    $.ajax({
        url: path + "HospitalManagementSystem/HMS/AppDashBoard/getbloodpreasure.jsp",
        type: "POST",
        data: {},
        success: function (data) {
            var msg = jQuery.trim(data);
            if (msg) {
                var obj = JSON.parse(msg);
                if (obj.bloodrate.length > 0) {
                    var preasuretype1 = 0;
                    var preasuretype2 = 0;
                    for (i = 0; i < obj.bloodrate.length; i++) {

                        var covertdate = dateconvert(obj.bloodrate[i][0]);

                        if (obj.bloodrate[i][2] === "dbp") {
                            if (preasuretype1 < 5) {
                                bloodrate1.push(obj.bloodrate[i][1]);
                            } else {
                                bloodrate2.push(obj.bloodrate[i][1]);
                            }
                            $("#vital-diastolic").html("");
                            $("#vital-diastolic").html(obj.bloodrate[i][1]);

                            preasuretype1++;
                        } else {
                            if (preasuretype2 < 5) {
                                bloodrate3.push(obj.bloodrate[i][1]);
                            } else {
                                bloodrate4.push(obj.bloodrate[i][1]);
                            }

                            $("#vital-systolic").html("");
                            $("#vital-systolic").html(obj.bloodrate[i][1]);

                            preasuretype2++;
                        }

                        if (blooddate1.length < 5) {
                            if (blooddate1[blooddate1.length - 1] !== covertdate) {
                                blooddate1.push(covertdate);
                            }
                        } else {
                            if (blooddate2[blooddate2.length - 1] !== covertdate && blooddate1[blooddate1.length - 1] !== covertdate) {
                                blooddate2.push(covertdate);
                            }
                        }
                    }
                    assignBloodpreasure();
                } else {
                    assignBloodpreasure();
                }
            }
            process();
        },
        error: function () {
            process();
        }
    });
    return false;
}

var sleepdate1 = [];
var sleepdate2 = [];
var darksleep1 = [];
var darksleep2 = [];
var lightsleep1 = [];
var lightsleep2 = [];
var awake1 = [];
var awake2 = [];

// assign sleep
function assignsleep() {

    $('#sleep').highcharts({
        chart: {
            type: 'column'
        },
        title: {
            text: 'Sleep'
        },
        credits: {
            enabled: false
        },
        xAxis: {
            categories: sleepdate1
        },
        yAxis: {
            min: 0,
            title: {
                text: ''
            },
            plotLines: [{
                    value: 8,
                    color: 'green',
                    dashStyle: 'shortdash',
                    width: 3,
                    label: {
                        text: '8.00'
                    }
                }]
        },
        tooltip: {
            formatter: function () {
                return '<b>' + this.x + ' </b><br/>' + this.series.name + ': ' + this.y + ' Hour<br/>' + 'Total Sleep : ' + this.point.stackTotal + 'Hour';
            }
        },
        plotOptions: {
            column: {
                stacking: 'normal'
            }
        },
        noData: {
            style: {
                fontWeight: 'bold',
                fontSize: '15px',
                color: '#303030 '
            }
        },
        series: [{
                name: 'awake',
                color: '#ffbc52',
                data: awake1
            }, {
                name: 'deep sleep',
                color: '#3a75c4',
                data: darksleep1
            }, {
                name: 'light sleep',
                color: '#78a1d7',
                data: lightsleep1
            }]
    });

    var chart4 = $('#sleep').highcharts();
    $('#sleep_next').on("click", function () {

        chart4.series[0].update({
            data: awake2
        });
        chart4.series[1].update({
            data: darksleep2
        });
        chart4.series[2].update({
            data: lightsleep2
        });
        chart4.xAxis[0].setCategories(sleepdate2);
    });

    $('#sleep_prev').on("click", function () {

        chart4.series[0].update({
            data: awake1
        });
        chart4.series[1].update({
            data: darksleep1
        });
        chart4.series[2].update({
            data: lightsleep1
        });
        chart4.xAxis[0].setCategories(sleepdate1);
    });
    getweight();
}

//get sleep
function getsleep() {

    $.ajax({
        url: path + "HospitalManagementSystem/HMS/AppDashBoard/getsleep.jsp",
        type: "POST",
        data: {},
        success: function (data) {
            var msg = jQuery.trim(data);
            if (msg) {
                obj = JSON.parse(msg);
                if (obj.sleepdata.length > 0) {
                    for (i = 0; i < obj.sleepdata.length; i++) {
                        if (i < 5) {
                            var covertdate = dateconvert(obj.sleepdata[i][0]);
                            sleepdate1.push(covertdate);
                            awake1.push(obj.sleepdata[i][1]);
                            darksleep1.push(obj.sleepdata[i][2]);
                            lightsleep1.push(obj.sleepdata[i][3]);
                        } else {
                            var covertdate = dateconvert(obj.sleepdata[i][0]);
                            sleepdate2.push(covertdate);
                            awake2.push(obj.sleepdata[i][1]);
                            darksleep2.push(obj.sleepdata[i][2]);
                            lightsleep2.push(obj.sleepdata[i][3]);
                        }
                    }
                    assignsleep();
                } else {
                    assignsleep();
                }
            }
            process();
        },
        error: function () {
            process();
        }
    });
    return false;
}

var weightdate1 = [];
var weightdate2 = [];
var weightrate1 = [];
var weightrate2 = [];

//assign weight
function assignweight() {

    $('#weightChart').highcharts({
        title: {
            text: 'Weight'
        },
        xAxis: {
            categories: weightdate1
        },
        yAxis: {
            title: {
                text: ''
            }
        },
        credits: {
            enabled: false
        },
        tooltip: {
            crosshairs: true,
            shared: true,
            valueSuffix: 'Kg'
        },
        legend: {},
        noData: {
            style: {
                fontWeight: 'bold',
                fontSize: '15px',
                color: '#303030 '
            }
        },
        series: [{
                name: 'Weight',
                showInLegend: false,
                data: weightrate1,
                zIndex: 1,
                marker: {
                    fillColor: 'white',
                    lineWidth: 2,
                    lineColor: Highcharts.getOptions().colors[0]
                }
            }]
    });

    var chart5 = $('#weightChart').highcharts();
    $('#weight_next').on("click", function () {

        chart5.series[0].update({
            data: weightrate2
        });
        chart5.xAxis[0].setCategories(weightdate2);
    });
    $('#weight_prev').on("click", function () {
        chart5.series[0].update({
            data: weightrate1
        });
        chart5.xAxis[0].setCategories(weightdate1);
    });

    moodmanagement();
}

// get weight
function getweight() {

    $.ajax({
        url: path + "HospitalManagementSystem/HMS/AppDashBoard/getweight.jsp",
        type: "POST",
        data: {},
        success: function (data) {
            var msg = jQuery.trim(data);
            if (msg) {
                obj = JSON.parse(msg);
                if (obj.weight.length > 0) {

                    $("#vital-weight").html(obj.weight[obj.weight.length - 1][1] + obj.weight[obj.weight.length - 1][2]);
                    for (i = 0; i < obj.weight.length; i++) {
                        if (i < 5) {
                            var covertdate = dateconvert(obj.weight[i][0]);
                            weightdate1.push(covertdate);
                            weightrate1.push(obj.weight[i][1]);
                        } else {
                            var covertdate = dateconvert(obj.weight[i][0]);
                            weightdate2.push(covertdate);
                            weightrate2.push(obj.weight[i][1]);
                        }
                    }

                    assignweight();
                } else {
                    assignweight();
                }
            }
            process();
        },
        error: function () {
            process();
        }

    });
    return false;
}

//mood management 
var moodData1 = [];
var moodData2 = [];
var moodfirstdate;
var moodlastdate;
var moodsecoundate;
var moodsecoundlastdate;

//mooddataconvert
function mooddataconvert(dates, moodtypes) {
    var localmoodpush = [];
    localmoodpush.length = 0;
    var d = dates;
    var splitData = d.split(' ');
    var splitData1 = splitData[0];
    var splitData2 = splitData1.split('-');
    var year = splitData2[0];
    var month = splitData2[1];
    var convertmonth = month - 1;
    var date = splitData2[2];
    var time = splitData[1];
    var time1 = time.split(":");
    var hour = time1[0];
    var minutes = time1[1];
    var data1;

    if (moodtypes === "Bored") {
        data1 = 1;
    } else if (moodtypes === "Sad") {
        data1 = 2;
    } else if (moodtypes === "Irritated") {
        data1 = 3;
    } else if (moodtypes === "Tense") {
        data1 = 4;
    } else if (moodtypes === "Neutarl") {
        data1 = 5;
    } else if (moodtypes === "Good") {
        data1 = 6;
    } else if (moodtypes === "Calm") {
        data1 = 7;
    } else if (moodtypes === "Relaxed") {
        data1 = 8;
    } else if (moodtypes === "Cheerful") {
        data1 = 9;
    } else if (moodtypes === "Excited") {
        data1 = 10;
    }

    var data = Date.UTC(year, convertmonth, date, hour, minutes);
    localmoodpush.push(data);
    localmoodpush.push(data1);
    return localmoodpush;
}

//assign mood
function assignmood(moodData1, moodData2) {

    $('#modeChart').highcharts({
        chart: {
            type: 'area'
        },
        tooltip: {
            formatter: function () {
                var value = this.y;
                if (value === 1) {
                    return '<b>' + 'Mood' + '</b><br/>' +
                            Highcharts.dateFormat('%e - %b - %Y' + '<br/>' + '%I:%M - %p',
                                    new Date(this.x)) + '<br/><b>' + 'Bored' + '<b/>';
                } else if (value === 2) {
                    return '<b>' + 'Mood' + '</b><br/>' +
                            Highcharts.dateFormat('%e - %b - %Y' + '<br/>' + '%I:%M - %p',
                                    new Date(this.x)) + '<br/><b>' + 'Sad' + '<b/>';
                } else if (value === 3) {
                    return '<b>' + 'Mood' + '</b><br/>' +
                            Highcharts.dateFormat('%e - %b - %Y' + '<br/>' + '%I:%M - %p',
                                    new Date(this.x)) + '<br/><b>' + 'Irritated' + '<b/>';
                } else if (value === 4) {
                    return '<b>' + 'Mood' + '</b><br/>' +
                            Highcharts.dateFormat('%e - %b - %Y' + '<br/>' + '%I:%M - %p',
                                    new Date(this.x)) + '<br/><b>' + 'Tense' + '<b/>';
                } else if (value === 5) {
                    return '<b>' + 'Mood' + '</b><br/>' +
                            Highcharts.dateFormat('%e - %b - %Y' + '<br/>' + '%I:%M - %p',
                                    new Date(this.x)) + '<br/><b>' + 'Neutarl' + '<b/>';
                } else if (value === 6) {
                    return '<b>' + 'Mood' + '</b><br/>' +
                            Highcharts.dateFormat('%e - %b - %Y' + '<br/>' + '%I:%M - %p',
                                    new Date(this.x)) + '<br/><b>' + 'Good' + '<b/>';
                } else if (value === 7) {
                    return '<b>' + 'Mood' + '</b><br/>' +
                            Highcharts.dateFormat('%e - %b - %Y' + '<br/>' + '%I:%M - %p',
                                    new Date(this.x)) + '<br/><b>' + 'Calm' + '<b/>';
                } else if (value === 8) {
                    return '<b>' + 'Mood' + '</b><br/>' +
                            Highcharts.dateFormat('%e - %b - %Y' + '<br/>' + '%I:%M - %p',
                                    new Date(this.x)) + '<br/><b>' + 'Relaxed' + '<b/>';
                } else if (value === 9) {
                    return '<b>' + 'Mood' + '</b><br/>' +
                            Highcharts.dateFormat('%e - %b - %Y' + '<br/>' + '%I:%M - %p',
                                    new Date(this.x)) + '<br/><b>' + 'Cheerful' + '<b/>';
                } else if (value === 10) {
                    return '<b>' + 'Mood' + '</b><br/>' +
                            Highcharts.dateFormat('%e - %b - %Y' + '<br/>' + '%I:%M - %p',
                                    new Date(this.x)) + '<br/><b>' + 'Excited' + '<b/>';
                }
            }
        },
        yAxis: {
            min: 1,
            max: 10,
            tickInterval: 1,
            labels: {},
            title: {
                text: 'Mood'
            }
        },
        xAxis: {
            type: 'datetime',
            min: moodfirstdate,
            max: moodlastdate,
            dateTimeLabelFormats: {// don't display the dummy year
                month: '%e. %b',
                year: '%b'
            },
            title: {
                text: 'Date'
            },
            minTickInterval: 24 * 3600 * 1000
        },
        credits: {
            enabled: false
        },
        title: {
            text: 'Mood Chart'
        },
        noData: {
            style: {
                fontWeight: 'bold',
                fontSize: '15px',
                color: '#303030 '
            }
        },
        series: [{
                name: profilename,
                data: moodData1,
                showInLegend: false
            }]
    });

    $("#mode_next").click(function () {
        var modeChart = $('#modeChart').highcharts();
        modeChart.series[0].update({
            data: moodData2
        });
        modeChart.xAxis[0].setExtremes(moodsecoundate, moodsecoundlastdate);
    });

    $("#mode_prev").click(function () {
        var modeChart = $('#modeChart').highcharts();
        modeChart.series[0].update({
            data: moodData1
        });
        modeChart.xAxis[0].setExtremes(moodfirstdate, moodlastdate);
    });

    painmanagement();
}

//get mood
function moodmanagement() {

    $.ajax({
        url: path + "HospitalManagementSystem/HMS/AppDashBoard/getmoodchart.jsp",
        type: "POST",
        data: {},
        success: function (data) {
            var msg = jQuery.trim(data);
            if (msg) {
                obj = JSON.parse(msg);
                if (obj.mooddetails.length > 0) {
                    moodData1.length = 0;
                    moodData2.length = 0;
                    for (i = 0; i < obj.mooddetails.length; i++) {
                        if (i < 7) {
                            var covertdate = mooddataconvert(obj.mooddetails[i][0], obj.mooddetails[i][1]);
                            moodData1.push(covertdate);

                            if (i === 0) {
                                moodfirstdate = covertdate[0];
                            }
                            if (obj.mooddetails.length > 7) {
                                if (i === 6) {
                                    moodlastdate = covertdate[0];
                                }
                            } else {
                                if (i === obj.mooddetails.length - 1) {
                                    moodlastdate = covertdate[0];
                                }
                            }
                        } else {
                            var covertdate = mooddataconvert(obj.mooddetails[i][0], obj.mooddetails[i][1]);
                            moodData2.push(covertdate);
                            if (i === 7) {
                                moodsecoundate = covertdate[0];
                            }
                            if (i === obj.mooddetails.length - 1) {
                                moodsecoundlastdate = covertdate[0];
                            }
                        }
                    }
                    assignmood(moodData1, moodData2);
                } else {
                    assignmood(moodData1, moodData2);
                }
            }
            process();
        },
        error: function () {
            process();
        }
    });
    return false;
}

//pain management 
var paindata1 = [];
var paindata2 = [];
var firstdate;
var lastdate;
var secoundate;
var secoundlastdate;

//dataconvert
function dataconvert(dates, painttypes) {

    var localpainpush = [];
    localpainpush.length = 0;
    var d = dates;
    var splitData = d.split(' ');
    var splitData1 = splitData[0];
    var splitData2 = splitData1.split('-');
    var year = splitData2[0];
    var month = splitData2[1];
    var convertmonth = month - 1;
    var date = splitData2[2];
    var time = splitData[1];
    var time1 = time.split(":");
    var hour = time1[0];
    var minutes = time1[1];
    var data1 = painttypes;
    var data = Date.UTC(year, convertmonth, date, hour, minutes);

    localpainpush.push(data);
    localpainpush.push(data1);
    return localpainpush;
}

//assign pain
function assignpain(paindata1, paindata2) {
    var painLabels = ["0", "1", "2", "3", "4", "5", "6", "7", "8", "9", "10"];

    $('#painChart').highcharts({
        chart: {
            type: 'area'
        },
        tooltip: {
            formatter: function () {
                var value = this.y;
                if (value === 0) {
                    return '<b>' + "Pain Type" + '</b><br/>' +
                            Highcharts.dateFormat('%e - %b - %Y' + '<br/>' + '%I:%M - %p',
                                    new Date(this.x)) + '<br/><b>' + 'No Pain' + '<b/>';
                } else if (value === 1) {
                    return '<b>' + "Pain Type" + '</b><br/>' +
                            Highcharts.dateFormat('%e - %b - %Y' + '<br/>' + '%I:%M - %p',
                                    new Date(this.x)) + '<br/><b>' + 'Very Mild' + '<b/>';
                } else if (value === 2) {
                    return '<b>' + "Pain Type" + '</b><br/>' +
                            Highcharts.dateFormat('%e - %b - %Y' + '<br/>' + '%I:%M - %p',
                                    new Date(this.x)) + '<br/><b>' + 'Discomforting' + '<b/>';
                } else if (value === 3) {
                    return '<b>' + "Pain Type" + '</b><br/>' +
                            Highcharts.dateFormat('%e - %b - %Y' + '<br/>' + '%I:%M - %p',
                                    new Date(this.x)) + '<br/><b>' + 'Tolerable' + '<b/>';
                } else if (value === 4) {
                    return '<b>' + "Pain Type" + '</b><br/>' +
                            Highcharts.dateFormat('%e - %b - %Y' + '<br/>' + '%I:%M - %p',
                                    new Date(this.x)) + '<br/><b>' + 'Distressing' + '<b/>';
                } else if (value === 5) {
                    return '<b>' + "Pain Type" + '</b><br/>' +
                            Highcharts.dateFormat('%e - %b - %Y' + '<br/>' + '%I:%M - %p',
                                    new Date(this.x)) + '<br/><b>' + 'Very Distressing' + '<b/>';
                } else if (value === 6) {
                    return '<b>' + "Pain Type" + '</b><br/>' +
                            Highcharts.dateFormat('%e - %b - %Y' + '<br/>' + '%I:%M - %p',
                                    new Date(this.x)) + '<br/><b>' + 'Intense' + '<b/>';
                } else if (value === 7) {
                    return '<b>' + this.series.name + '</b><br/>' +
                            Highcharts.dateFormat('%e - %b - %Y' + '<br/>' + '%I:%M - %p',
                                    new Date(this.x)) + '<br/><b>' + 'Very Intense' + '<b/>';
                } else if (value === 8) {
                    return '<b>' + "Pain Type" + '</b><br/>' +
                            Highcharts.dateFormat('%e - %b - %Y' + '<br/>' + '%I:%M - %p',
                                    new Date(this.x)) + '<br/><b>' + 'Utterly Horrible' + '<b/>';
                } else if (value === 9) {
                    return '<b>' + "Pain Type" + '</b><br/>' +
                            Highcharts.dateFormat('%e - %b - %Y' + '<br/>' + '%I:%M - %p',
                                    new Date(this.x)) + '<br/><b>' + 'Excruciating Unbearable' + '<b/>';
                } else if (value === 10) {
                    return '<b>' + "Pain Type" + '</b><br/>' +
                            Highcharts.dateFormat('%e - %b - %Y' + '<br/>' + '%I:%M - %p',
                                    new Date(this.x)) + '<br/><b>' + 'Unimaginable Unspeakable' + '<b/>';
                }
            }
        },
        title: {
            text: 'Pain Chart'
        },
        yAxis: {
            min: 1,
            max: 11,
            tickInterval: 1,
            labels: {},
            credits: {
                enabled: false
            },
            title: {
                text: 'Pain Types'
            }
        },
        xAxis: {
            type: 'datetime',
            min: firstdate,
            max: lastdate,
            dateTimeLabelFormats: {// don't display the dummy year
                month: '%e. %b',
                year: '%b'
            },
            visible: false,
            title: {
                text: 'Date'
            },
            minTickInterval: 24 * 3600 * 1000
        },
        credits: {
            enabled: false
        },
        noData: {
            style: {
                fontWeight: 'bold',
                fontSize: '15px',
                color: '#303030 '
            }
        },
        series: [{
                name: profilename,
                data: paindata1,
                showInLegend: false
            }]
    });

    $("#pain_next").click(function () {
        var painChart = $('#painChart').highcharts();
        painChart.series[0].update({
            data: paindata2
        });
        painChart.xAxis[0].setExtremes(secoundate, secoundlastdate);
    });

    $("#pain_prev").click(function () {
        var painChart = $('#painChart').highcharts();
        painChart.series[0].update({
            data: paindata1
        });
        painChart.xAxis[0].setExtremes(firstdate, lastdate);
    });

    getfatmass();
}

function painmanagement() {

    $.ajax({
        url: path + "HospitalManagementSystem/HMS/AppDashBoard/getpainchart.jsp",
        type: "POST",
        data: {},
        success: function (data) {
            var msg = jQuery.trim(data);
            if (msg) {
                obj = JSON.parse(msg);
                if (obj.paindetails.length > 0) {
                    paindata1.length = 0;
                    paindata2.length = 0;
                    for (i = 0; i < obj.paindetails.length; i++) {
                        if (i < 7) {
                            var covertdate = dataconvert(obj.paindetails[i][0], obj.paindetails[i][1]);
                            paindata1.push(covertdate);

                            if (i === 0) {
                                firstdate = covertdate[0];
                            }
                            if (obj.paindetails.length > 7) {
                                if (i === 6) {
                                    lastdate = covertdate[0];
                                }
                            } else {
                                if (i === obj.paindetails.length - 1) {
                                    lastdate = covertdate[0];
                                }
                            }
                        } else {
                            var covertdate = dataconvert(obj.paindetails[i][0], obj.paindetails[i][1]);
                            paindata2.push(covertdate);
                            if (i === 7) {
                                secoundate = covertdate[0];
                            }
                            if (i === obj.paindetails.length - 1) {
                                secoundlastdate = covertdate[0];
                            }
                        }
                    }
                    assignpain(paindata1, paindata2);
                } else {
                    assignpain(paindata1, paindata2);
                }
            }
            process();
        },
        error: function () {
            process();
        }
    });
    return false;
}


var fatmassdate1 = [];
var fatmassdate2 = [];
var fatmassrate1 = [];
var fatmassrate2 = [];

// assign fatmass
function assignfatmass() {

    $('#fatmass').highcharts({
        title: {
            text: 'Fat Mass'
        },
        xAxis: {
            categories: fatmassdate1
        },
        yAxis: {
            title: {
                text: ''
            }
        },
        credits: {
            enabled: false
        },
        tooltip: {
            crosshairs: true,
            shared: true
        },
        legend: {},
        noData: {
            style: {
                fontWeight: 'bold',
                fontSize: '15px',
                color: '#303030 '
            }
        },
        series: [{
                name: 'Fatmass',
                showInLegend: false,
                color: '#c7c11c',
                data: fatmassrate1,
                zIndex: 1,
                marker: {
                    fillColor: 'white',
                    lineWidth: 2,
                    lineColor: '#c7c11c',
                    color: '#c7c11c'
                }
            }]
    });

    var chart6 = $('#fatmass').highcharts();
    $('#fatmass_next').on("click", function () {

        chart6.series[0].update({
            data: fatmassrate2
        });
        chart6.xAxis[0].setCategories(fatmassdate2);
    });

    $('#fatmass_prev').on("click", function () {
        chart6.series[0].update({
            data: fatmassrate1
        });
        chart6.xAxis[0].setCategories(fatmassdate1);
    });

    getglucose();
}

// get fatmass
function getfatmass() {

    $.ajax({
        url: path + "HospitalManagementSystem/HMS/AppDashBoard/getfatmass.jsp",
        type: "POST",
        data: {},
        success: function (data) {
            var msg = jQuery.trim(data);
            if (msg) {
                obj = JSON.parse(msg);
                if (obj.fatmass.length > 0) {
                    for (i = 0; i < obj.fatmass.length; i++) {
                        if (i < 5) {
                            var covertdate = dateconvert(obj.fatmass[i][0]);
                            fatmassdate1.push(covertdate);
                            fatmassrate1.push(obj.fatmass[i][1]);
                        } else {
                            var covertdate = dateconvert(obj.fatmass[i][0]);
                            fatmassdate2.push(covertdate);
                            fatmassrate2.push(obj.fatmass[i][1]);
                        }
                    }
                    assignfatmass();
                } else {
                    assignfatmass();
                }
            }
            process();
        },
        error: function () {
            process();
        }
    });
    return false;
}

var glucosedate1 = [];
var glucosedate2 = [];
var glucoserate1 = [];
var glucoserate2 = [];


//glucoserateassign
function glucoseAssign() {

    $('#glucoseChart').highcharts({
        chart: {
            zoomType: 'xy'
        },
        title: {
            text: 'Glucose Rate',
            x: -20
        },
        credits: {
            enabled: false
        },
        xAxis: {
            categories: glucosedate1
        },
        yAxis: {
            min: 0,
            max: 500,
            tickInterval: 50,
            labels: {
                format: '{value}'
            },
            title: {
                text: 'Glucose (mg/dL)'
            },
            credits: {
                enabled: false
            },
            plotLines: [{
                    value: 0,
                    width: 1,
                    color: '#808080'
                }]
        },
        tooltip: {
            crosshairs: true,
            headerFormat: '<b>Glucose Rate</b><br>',
            pointFormat: '{point.y}(mg/dL)'

        },
        legend: {
            align: 'center',
            borderWidth: 0
        },
        noData: {
            style: {
                fontWeight: 'bold',
                fontSize: '15px',
                color: '#303030 '
            }
        },
        series: [{
                name: 'Glucose Rate',
                showInLegend: false,
                color: '#d9534f',
                data: glucoserate1,
                marker: {
                    symbol: 'circle'
                }
            }]
    });

    var chart = $('#glucoseChart').highcharts();
    $('#glucose_next').on("click", function () {
        chart.series[0].update({
            data: glucoserate2
        });
        chart.xAxis[0].setCategories(glucosedate2);
    });
    $('#glucose_prev').on("click", function () {
        chart.series[0].update({
            data: glucoserate1
        });
        chart.xAxis[0].setCategories(glucosedate1);
    });

}


// get glucose
function getglucose() {

    $.ajax({
        url: path + "HospitalManagementSystem/HMS/AppDashBoard/getglucose.jsp",
        type: "POST",
        data: {},
        success: function (data) {
            var msg = jQuery.trim(data);
            if (msg) {
                obj = JSON.parse(msg);
                if (obj.glucoserate.length > 0) {

                    for (i = 0; i < obj.glucoserate.length; i++) {

                        if (i < 15) {
                            var covertdate = dateconvert(obj.glucoserate[i][0]);
                            glucosedate1.push(covertdate);
                            glucoserate1.push(obj.glucoserate[i][1]);
                        } else {
                            var covertdate = dateconvert(obj.glucoserate[i][0]);
                            glucosedate2.push(covertdate);
                            glucoserate2.push(obj.glucoserate[i][1]);
                        }
                    }
                    glucoseAssign();
                } else {
                    glucoseAssign();
                }
            }
            process();
        },
        error: function () {
            process();
        }

    });
    return false;
}