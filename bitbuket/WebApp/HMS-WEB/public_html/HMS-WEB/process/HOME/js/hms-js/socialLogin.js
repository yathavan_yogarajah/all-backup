var socialemail = "";
var socialtype = "";

// facebook login
window.fbAsyncInit = function () {
    FB.init({
        appId: '466847043440271', // App ID
        channelUrl: 'http://ec2-52-24-34-18.us-west-2.compute.amazonaws.com:8080/HMS-Web/channel.html', // Channel File
        status: true, // check login status
        cookie: true, // enable cookies to allow the server to access the session
        xfbml: true // parse XFBML
    });

};

function fb_login() {
    FB.login(function (response) {
        if (response.authResponse) {
            getUserInfo();
        } else {
            console.log('User cancelled login or did not fully authorize.');
        }
    }, {
        scope: 'email,user_photos,user_videos'
    });
}

function getUserInfo() {
    FB.api('/me', function (response) {
        var name = response.name; //get fb name
        var email = response.email; // get fb email
    });
}

function Logout() {
    FB.logout(function () {
        document.location.reload();
    });
}

// Load the SDK asynchronously
(function (d) {
    var js, id = 'facebook-jssdk',
            ref = d.getElementsByTagName('script')[0];
    if (d.getElementById(id)) {
        return;
    }
    js = d.createElement('script');
    js.id = id;
    js.async = true;
    js.src = "//connect.facebook.net/en_US/all.js";
    ref.parentNode.insertBefore(js, ref);
}(document));

// google 
function googlelogout() {
    gapi.auth.signOut();
    location.reload();
}

function google_login() {
    var myParams = {
        'clientid': '228321503774-3ia1vrq5ntpgqe9f0eqp24cs9fchng3v.apps.googleusercontent.com', //chnge
        'cookiepolicy': 'single_host_origin',
        'callback': 'loginCallback',
        'approvalprompt': 'force',
        'scope': 'https://www.googleapis.com/auth/plus.login https://www.googleapis.com/auth/plus.profile.emails.read'
    };
    gapi.auth.signIn(myParams);
}

function loginCallback(result) {
    if (result['status']['signed_in']) {
        var request = gapi.client.plus.people.get({
            'userId': 'me'
        });
        request.execute(function (resp) {
            var email = '';
            if (resp['emails']) {
                for (i = 0; i < resp['emails'].length; i++) {
                    if (resp['emails'][i]['type'] == 'account') {
                        email = resp['emails'][i]['value'];
                    }
                }
            }
            socialemail = email;
            socialtype = "gplus";
            sociallogin();/** function call */
        });

    }

}

function onLoadCallback() {
    gapi.client.load('plus', 'v1', function () {
    });
}

(function () {
    var po = document.createElement('script');
    po.type = 'text/javascript';
    po.async = true;
    po.src = 'https://apis.google.com/js/client.js?onload=onLoadCallback';
    var s = document.getElementsByTagName('script')[0];
    s.parentNode.insertBefore(po, s);
})();

//linked in 
function linkedin_login() {
    IN.UI.Authorize().place();
    IN.Event.on(IN, "auth", function () {
        onLinkedInAuth()
    });
}

function onLogin() {
    IN.API.Profile("me").result(displayResult);
}

function onLinkedInAuth() {
    IN.API.Profile("me").fields("first-name", "last-name", "email-address").result(function (data) {
        member = data.values[0];
        email = member.emailAddress; // get linked in email
        socialemail = email;
        socialemail = email;
        socialtype = "linkedin";
        sociallogin();/** function call */
    }).error(function (data) {
    });
}

function sociallogin() {
    $.ajax({
        url: path + "HospitalManagementSystem/HMS/User/socialnetworkRegister.jsp",
        type: "post",
        data: "Usertype=" + socialtype + "&InputEmailReg=" + socialemail,
        success: function (data) {
            getuserid();
            localStorage.setItem("usertype", "socialuser");
        },
        error: function () {
            
        }

    });
}