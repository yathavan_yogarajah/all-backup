$("input,select").focus(function () {
    $input_id = $(this).attr("id");
    if ($("#" + $input_id).hasClass("input-alert"))
        $("#" + $input_id).removeClass("input-alert");
});

// other checkbox after text control
function callenable(elem) {
    if ($("#" + elem).prop("checked") === true) {
        $("#" + elem + "text").attr('disabled', false);
    } else if ($("#" + elem).prop("checked") === false) {
        $("#" + elem + "text").val("");
        $("#" + elem + "text").attr('disabled', true);
    }

}

function validateEmail(Email) {
    var filter = /^[\w\-\.\+]+\@[a-zA-Z0-9\.\-]+\.[a-zA-z0-9]{2,4}$/;
    if (filter.test(Email)) {
        return true;
    } else {
        return false;
    }
}

function form1validate() {

    var required = [];
    error = 0;
    $fields = $("form#partrner_Form").serialize();
    form_array = $fields.split('&');
    for (var i = 0; i < 8; i++) {
        name = form_array[i].split('=')[0];
        value = form_array[i].split('=')[1];
        required[i] = $.trim(value);

        if ($.trim(value) === "") {
            var name_of_input = "#" + name;
            error++;
            $(name_of_input).addClass("input-alert");
        }
    }

    if (validateEmail($("#email").val()) === false) {
        error++;
        $("#email").addClass("input-alert");
    }

    if (error === 0) {
        becomeaPartrner();
    }
}

function becomeaPartrner() {
    $("#status_out").show();
    $.ajax({
        type: "POST",
        url: path + "HospitalManagementSystem/HMS/User/becomeapartner.jsp",
        data: $("form#partrner_Form").serialize(),
        success: function (html) {
            var msg = jQuery.trim(html);
            if (msg === "success") {
                $("#status_out").hide();
                $("#partner-successfull").html(successfulladddetails);
                $("#partner-successfull").show();
                var myVar1 = setInterval(function () {
                    $("#partner-successfull").hide();
                    clearInterval(myVar1);
                }, 3000);

            } else {
                $("#partner-warning").html(unsuccessfulladddetails);
                $("#partner-warning").show();
                var myVar1 = setInterval(function () {
                    $("#partner-warning").hide();
                    clearInterval(myVar1);
                }, 3000);
                $("#status_out").hide();
            }
        }, error: function () {
            $("#partner-warning").html(servererror);
            $("#partner-warning").show();
            var myVar1 = setInterval(function () {
                $("#partner-warning").hide();
                clearInterval(myVar1);
            }, 3000);
            $("#status_out").hide();
        }
    });
}