/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


$(document).ready(function () {

    setInterval(function () {
        cycleItems();
    }, 1000);

    var currentIndex = 0,
            items = $('.containers .row .slide'),
            itemAmt = items.length;


    function cycleItems() {
        var item = $('.containers .row .slide').eq(currentIndex);
        items.hide();
        item.css('display', 'inline-block');
    }

//    var autoSlide = setInterval(function () {
//        currentIndex += 1;
//        if (currentIndex > itemAmt - 1) {
//            currentIndex = 0;
//        }
//        cycleItems();
//    }, 8000);

    $('.next').click(function () {
//        clearInterval(autoSlide);
        currentIndex += 1;
        if (currentIndex > itemAmt - 1) {
            currentIndex = 0;
        }
        cycleItems();
    });

    $('.prev').click(function () {
//        clearInterval(autoSlide);
        currentIndex -= 1;
        if (currentIndex < 0) {
            currentIndex = itemAmt - 1;
        }
        cycleItems();
    });
});

