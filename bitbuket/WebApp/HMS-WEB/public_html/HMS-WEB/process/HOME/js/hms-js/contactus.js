$(function () {

    $("input,textarea").focus(function () {
        var input_id = $(this).attr("id");
        $("#" + input_id).removeClass("input-alert");
    });

});


//validateEmail
function validateEmail(Email) {

    var filter = /^[\w\-\.\+]+\@[a-zA-Z0-9\.\-]+\.[a-zA-z0-9]{2,4}$/;
    if (filter.test(Email)) {
        return true;
    } else {
        return false;
    }
}


// send email
function contactus() {
    var email = $("#sender_email").val();
    var reason = $("#reason").val();

    var emailvalidation = validateEmail(email);
    if (emailvalidation == true && reason != "" && reason != " ") {
        $("#status_out").show();

        $.ajax({
            url: path + "HospitalManagementSystem/HMS/User/contactus.jsp",
            type: "POST",
            data: {
                "email": email,
                "message": reason
            },
            success: function (data) {
                var msg = jQuery.trim(data);
                if (msg == "success") {
                    $("#status_out").hide();
                    $("#successfull-send").html();
                    $("#successfull-send").show();
                    var hidestatus = setInterval(function () {
                        $("#successfull-send").hide();
                        clearInterval(hidestatus);
                    }, 3000);
                } else {
                    $("#status_out").hide();
                    $("#errormsg-alert").html();
                    $("#errormsg-alert").show();
                    var hidestatus = setInterval(function () {
                        $("#errormsg-alert").hide();
                        clearInterval(hidestatus);
                    }, 3000);
                }
            }
        });
        return false;
    } else {

        if ($("#reason").val() == "" || $("#reason").val() == null) {
            $("#reason").addClass("input-alert");
        }
        if ($("#sender_email").val() == "" || $("#sender_email").val() == null || emailvalidation == false) {
            $("#sender_email").addClass("input-alert");
        }



    }
}
