/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

$("#requestDemo").click(function () {
    requestDemoFunction();
});

function validateEmail(Email) {

    var filter = /^[\w\-\.\+]+\@[a-zA-Z0-9\.\-]+\.[a-zA-z0-9]{2,4}$/;
    if (filter.test(Email)) {
        return true;
    } else {
        return false;
    }
}

function requestDemoFunction() {
    var currentdate = new Date();
    date = currentdate.getFullYear() + "-" + (currentdate.getMonth() + 1) + "-" + currentdate.getDate();

    var email = $("#req_email").val();

    var emailvalidation = validateEmail(email);
    if (emailvalidation === true) {
        $("#status_out").show();

        $.ajax({
            url: path + "HospitalManagementSystem/HMS/User/demorequest.jsp",
            type: "POST",
            data: {
                "date":date,
                "demoemail":email
            },
            success: function (data) {
                var msg = jQuery.trim(data);
                if (msg == "success") {
                    $("#status_out").hide();
                    $("#successfull-send").html(successfullsend);
                    $("#successfull-send").show();
					$("#req_email").val("");
                    var hidestatus = setInterval(function () {
                        $("#successfull-send").hide();
                        $('#SeeDemoBox').fadeOut();
                        clearInterval(hidestatus);
                    }, 3000);
                } else if (msg == "alreadyhave") {
                    $("#status_out").hide();
                    $("#errormsg-alert").html(alreadyhave);
                    $("#errormsg-alert").show();
					$("#req_email").val("");
                    var hidestatus = setInterval(function () {
                        $("#errormsg-alert").hide();
                        $('#SeeDemoBox').fadeOut();
                        clearInterval(hidestatus);
                    }, 3000);
                } else {
                    $("#status_out").hide();
                    $("#errormsg-alert").html(unsuccessfullsend);
                    $("#errormsg-alert").show();
                    var hidestatus = setInterval(function () {
                        $("#errormsg-alert").hide();
                        clearInterval(hidestatus);
                    }, 3000);
                }
            }
        });
        return false;
    } else {
        if ($("#req_email").val() == "" || $("#req_email").val() == null || emailvalidation == false) {
            $("#req_email").addClass("input-alert");
        }
    }
}