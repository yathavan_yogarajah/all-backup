package com.hms.controller;

import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import org.hibernate.annotations.ColumnTransformer;

@Entity
@Table(name = "tblheartrate")
public class UserHeartRate {

	@Id
	@GeneratedValue
	@Column(name = "heartrateId")
	private int heartrateId;

	@Column(name = "userId")
	private int userId;

	@Column(name = "date")
	private Date date;

	@Column(name = "time")
	private Date time;

	@Column(name = "rate")
	@ColumnTransformer(read = "AES_DECRYPT(rate,'hmsincadmin')", write = "AES_ENCRYPT(?, 'hmsincadmin')")
	private String rate;

	@Column(name = "dataFrom")
	@ColumnTransformer(read = "AES_DECRYPT(dataFrom,'hmsincadmin')", write = "AES_ENCRYPT(?, 'hmsincadmin')")
	private String dataFrom;

	public void setheartrateId(int id) {
		this.heartrateId = id;
	}

	public int getheartrateId() {
		return heartrateId;
	}

	public void setuserId(int id) {
		this.userId = id;
	}

	public int getuserId() {
		return userId;
	}

	public void setdate(Date dob) {
		this.date = dob;
	}

	public Date getdate() {
		return this.date;
	}

	public void settime(Date time) {
		this.time = time;
	}

	public Date gettime() {
		return this.time;
	}

	public void setrate(String val) {
		this.rate = val;
	}

	public String getrate() {
		return rate;
	}

	public void setdataFrom(String from) {
		this.dataFrom = from;
	}

	public String getdataFrom() {
		return dataFrom;
	}
}
