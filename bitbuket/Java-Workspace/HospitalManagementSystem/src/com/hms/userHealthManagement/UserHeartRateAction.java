package com.hms.userHealthManagement;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.List;
import org.hibernate.*;
import com.hibernate.util.*;
import com.hms.config.Configreader;
import com.hms.config.Constants;
import com.hms.controller.UserHeartRate;

public class UserHeartRateAction {

	// delete heart rate
	public String deleteheartratedatas(int userid) {

		String status = null;
		Session session = HibernateUtil.getSessionFactory().openSession();
		Transaction trans = session.beginTransaction();

		try {

			Query qry = session.createQuery("delete UserHeartRate where userId=:userid");
			qry.setParameter("userid", userid);
			qry.executeUpdate();
			trans.commit();
			session.clear();
			session.flush();
			status = "success";

		} catch (Exception e) {

			status = "unsuccess";
			System.out.println("user heart rate:" + e.toString());
			e.printStackTrace();
			trans.rollback();

		} finally {

			session.close();
		}

		return status;
	}

	public static String deletedeviceheartratedatas(int userid) {

		String status = null;
		Session session = HibernateUtil.getSessionFactory().openSession();
		Transaction trans = session.beginTransaction();

		try {

			Query qry = session.createQuery("delete UserHeartRate where userId=:userid AND dataFrom=:dataFrom");
			qry.setParameter("userid", userid);
			qry.setParameter("dataFrom", "device");
			qry.executeUpdate();
			trans.commit();
			session.clear();
			session.flush();
			status = "success";

		} catch (Exception e) {

			status = "unsuccess";
			System.out.println("user heart rate:" + e.toString());
			e.printStackTrace();
			trans.rollback();

		} finally {

			session.close();
		}

		return status;
	}

	// set device heartrate
	public String setHeartrate(int userid, String date, String time, String rate) throws ParseException {

		@SuppressWarnings("unused")
		String status = null;
		Session session = HibernateUtil.getSessionFactory().openSession();
		Transaction trans = session.beginTransaction();
		DateFormat timeformatter = new SimpleDateFormat("HH:mm");
		DateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");

		try {

			UserHeartRate heartrate = new UserHeartRate();
			heartrate.setuserId(userid);
			heartrate.setdate(formatter.parse(date));
			heartrate.settime(timeformatter.parse(time));
			heartrate.setrate(rate);
			heartrate.setdataFrom("device");
			session.persist(heartrate);
			trans.commit();
			session.clear();
			session.flush();
			status = "success";

		} catch (Exception e) {

			status = "unsuccess";
			System.out.println("user heart rate :" + e.toString());
			e.printStackTrace();
			trans.rollback();

		} finally {

			session.close();
		}

		return status;
	}

	// set heartrate
	public static String setmanualHeartrate(int userid, String heartratedate, String time, String rate) throws ParseException {

		String status = null;
		Session session = HibernateUtil.getSessionFactory().openSession();
		Transaction trans = session.beginTransaction();
		String res = deletedeviceheartratedatas(userid);
		DateFormat timeformatter = new SimpleDateFormat("HH:mm");
		DateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
		int val = 0;
		// get maxval
		int maxval = Integer.parseInt(new Configreader().getProperty(Constants.heartratedata));

		if (res.equals("success")) {

			try {

				Query qry0 = session.createQuery("select count(UserId) from UserHeartRate where userId=:userid");
				qry0.setParameter("userid", userid);
				@SuppressWarnings("rawtypes")
				List list = qry0.list();

				if (list.size() == 1) {
					val = Integer.parseInt(list.get(0).toString());
				}

				session.flush();
				session.clear();

			} catch (Exception ex) {

				status = "unsuccess";
				System.out.println("setvisiting history :" + ex.toString());
				ex.printStackTrace();
				trans.rollback();

			}

			if (val < maxval) {

				try {

					UserHeartRate heartrate = new UserHeartRate();
					heartrate.setuserId(userid);
					heartrate.setdate(formatter.parse(heartratedate));
					heartrate.settime(timeformatter.parse(time));
					heartrate.setrate(rate);
					heartrate.setdataFrom("manual");
					session.persist(heartrate);
					trans.commit();
					session.clear();
					session.flush();
					status = "success";

				} catch (Exception e) {

					status = "unsuccess";
					System.out.println("user masnual heart rate :" + e.toString());
					e.printStackTrace();
					trans.rollback();

				} finally {

					session.close();
				}

			} else {

				int heartid = 0;

				try {

					Query qry1 = session.createQuery("from UserHeartRate where userId=:userid ORDER BY date, time");
					qry1.setParameter("userid", userid);
					qry1.setMaxResults(1);
					@SuppressWarnings("unchecked")
					List<UserHeartRate> user = (List<UserHeartRate>) qry1.list();
					if (user.size() == 1) {
						heartid = user.get(0).getheartrateId();
					}

					session.flush();
					session.clear();

				} catch (Exception ex) {

					status = "unsuccess";
					System.out.println("set manual heart rate :" + ex.toString());
					ex.printStackTrace();
					trans.rollback();

				}

				try {

					UserHeartRate heartrate = (UserHeartRate) session.get(UserHeartRate.class, heartid);
					heartrate.setdate(formatter.parse(heartratedate));
					heartrate.settime(timeformatter.parse(time));
					heartrate.setrate(rate);
					session.update(heartrate);
					trans.commit();
					session.clear();
					session.flush();
					status = "success";

				} catch (Exception e) {

					status = "unsuccess";
					System.out.println("set manual heart rate :" + e.toString());
					e.printStackTrace();
					trans.rollback();

				} finally {

					session.close();
				}
			}

		} else {

			status = "unsuccess";
		}

		return status;
	}
	
	
	public static void main(String args[]) throws ParseException{  
		System.out.println("programme run");
		System.out.println(setmanualHeartrate(4,"2016-03-22","00:00","80"));
	}
}
