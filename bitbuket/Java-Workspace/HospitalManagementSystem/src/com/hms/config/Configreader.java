package com.hms.config;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

public class Configreader {

	public String getProperty(String key) {

		Properties prop = new Properties();
		InputStream input = null;
		String res = "";

		try {

			prop.load(getClass().getClassLoader().getResourceAsStream("config.properties"));
			res = prop.getProperty(key);

		} catch (IOException ex) {

			ex.printStackTrace();

		} finally {

			if (input != null) {
				
				try {
					
					input.close();
					
				} catch (IOException e) {
					
					e.printStackTrace();
				}
			}
		}

		return res;
	}
}
