package com.hms.config;

public class Constants {

	public static String basepath = "basepath";
	public static String filestorebasepath = "filestorebasepath";
	public static String fitbitconsumerKey = "fitbitconsumerKey";
	public static String fitbitconsumerSecret = "fitbitconsumerSecret";
	public static String fitbitapibaseurl = "fitbitapibaseurl";
	public static String fitbitwebbaseurl = "fitbitwebbaseurl";
	public static String jawboneclientId = "jawboneclientId";
	public static String jawboneclientSecrect = "jawboneclientSecrect";
	public static String withingscustomerkey = "withingscustomerkey";
	public static String withingscustomersecrect = "withingscustomersecrect";
	public static String from = "from";
	public static String smpt_username = "smpt_username";
	public static String smpt_password = "smpt_password";
	public static String smpt_host = "smpt_host";
	public static String databasekey = "databasekey";
	public static String databaseurl = "databaseurl";
	public static String databasedriver = "databasedriver";
	public static String databaseusername = "databaseusername";
	public static String databasepassword = "databasepassword";
	public static String xrayimgpath = "xrayimgpath";
	public static String labreportpath = "labreportpath";
	public static String doctornotespath = "doctornotespath";
	public static String imageattachmentpath = "imageattachmentpath";
	public static String profileimagepath = "profileimagepath";
	public static String stripapi = "stripapi";
	public static String emaillist = "emaillist";
	public static String ihealthcustomerkey = "ihealthcustomerkey";
	public static String ihealthcustomersecrect = "ihealthcustomersecrect";
	public static String bloodpreasuredatacount = "bloodpreasuredatacount";
	public static String withingsdata = "withingsdata";
	public static String fitbitdata = "fitbitdata";
	public static String ihealthdata = "ihealthdata";
	public static String diabitiesprogramdata = "diabitiesprogramdata";
	public static String diabitiestestdata = "diabitiestestdata";
	public static String diabitiestragetdata = "diabitiestragetdata";
	public static String fatmassdata = "fatmassdata";
	public static String glucosedata = "glucosedata";
	public static String goaldata = "goaldata";
	public static String heartratedata = "heartratedata";
	public static String getpaindata = "getpaindata";
	public static String getmooddata = "getmooddata";
	public static String overcounterdata = "overcounterdata";
	public static String oximaritydata = "oximaritydata";
	public static String premedicationdata = "premedicationdata";
	public static String weightdata = "weightdata";

}
